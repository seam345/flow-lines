<img src="readme-images/logo-upline.png" alt="Logo" width="150"/>

# UpLine

UpLine is an Android app to upload manual data to an Influx DB server fast

Available in open beta on Google play <https://play.google.com/store/apps/details?id=tech.seanborg.flowlines>

## Why use UpLine

If you have time series data you would like to put in Influx that cannot be automated this app is for you!
Things like Odometer readings, game scores/winners, your weight, bank balance, or any other data that's can't really be
automated.

This app is mainly made to reduce typos and speedup data entry when manually putting data into InfluxDB. To achieve this
I autoload any tags/fields/measurements that already exist on the influx server providing simple radio button selects,
saving as much typing as possible.

## Why *not* use UpLine

If you don't already use [InfluxDB](https://www.influxdata.com/lp/influxdb-database/) this app possibly isn't for you,
I was already using it to track solar data and realised there were loads of other things I could track but no easy way
to
input the data

## Use cases

### Tracking waist size

Now I can easily upload data I can log my waist daily but when graphing it in grafana smooth out the data to get rid of
daily fluctuations
(Yes I spelt waist wrong)
![img_6.png](readme-images/img_6.png)

## Issues

Found a bug please open an issue or email me at <support@trackatrain.co.uk>

## Contributing

Currently not accepting any contributions until I work out how licensing would work with me distributing the app
commercially. The app is open source mainly to act as an example [Crux](https://github.com/redbadger/crux) app but also
if people just feel like building the app for themselves.

## Git convention

loosely following a similar style to conventional commits

for git commit hints during commits run `git config commit.template .gitmessage.txt`