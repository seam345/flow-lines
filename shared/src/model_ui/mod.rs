pub mod screens {
    pub mod initial;
    pub mod input_fields_and_tags;
    pub mod post_write_review;
    pub mod select_org_bucket_measurement;
    pub mod settings;
    pub mod settings_preview_connections_actions;
    pub mod settings_quick_actions;
}

pub mod screens_common {
    pub mod loading;
    pub mod remote_multi_choice;
    pub mod validated_string;
}

pub mod user_messages;

use crate::model::{ExampleData, Model, Screens};
use crate::model_ui::screens::initial::UiConnectQuickActionScreenData;
use crate::model_ui::screens::input_fields_and_tags::UiInputFieldsAndTagsScreenData;
use crate::model_ui::screens::post_write_review::UiPostWriteReviewScreenData;
use crate::model_ui::screens::select_org_bucket_measurement::UiSelectOrgBucketMeasurementScreenData;
use crate::model_ui::screens::settings_preview_connections_actions::{
    previous_connections_to_ui_previous_connections, UiSettingsPreviousConnectionsScreenData,
};
use crate::model_ui::user_messages::UiUserMessages;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone)]
pub struct UiModel {
    pub screen: UiScreens,
    pub user_messages: UiUserMessages,
    pub tutorial: Option<u8>,
}

#[derive(Serialize, Deserialize, Clone)]
pub enum UiScreens {
    InitialScreen(UiConnectQuickActionScreenData),
    InputFieldsTagsScreen(UiInputFieldsAndTagsScreenData),
    PostWriteReview(UiPostWriteReviewScreenData),
    SelectMeasurementScreen(UiSelectOrgBucketMeasurementScreenData),
    Settings,
    SettingPreviousConnections(UiSettingsPreviousConnectionsScreenData),
}

impl From<&Model> for UiModel {
    fn from(model: &Model) -> Self {
        UiModel {
            screen: (&model.screen).into(),
            user_messages: model.user_messages.clone().into(),
            tutorial: match model.example_data {
                ExampleData::Tutorial(inner) => Some(inner),
                ExampleData::FakeData => None,
                ExampleData::Real => None,
            },
        }
    }
}

impl From<&Screens> for UiScreens {
    fn from(value: &Screens) -> Self {
        match value {
            Screens::InitialScreen(initial_screen) => {
                UiScreens::InitialScreen(UiConnectQuickActionScreenData {
                    api_token: initial_screen.api_token.clone(),
                    base_url: initial_screen.base_url.clone(),
                    quick_actions: (&initial_screen.quick_actions).into(),
                    previous_connections: (&initial_screen.previous_connections).into(),
                    failed_to_contact_influx: initial_screen.failed_to_contact_influx,
                })
            }
            Screens::SelectOrgBucketMeasurementScreen(select_org_bucket_measurement) => {
                UiScreens::SelectMeasurementScreen(UiSelectOrgBucketMeasurementScreenData {
                    base_url: select_org_bucket_measurement.base_url.to_string(),
                    orgs: (&select_org_bucket_measurement.orgs).into(), //vec_to_drop_down_list(&select_org_bucket_measurement.orgs),
                    buckets: (&select_org_bucket_measurement.buckets).into(),
                    measurements: (&select_org_bucket_measurement.measurements).into(),
                })
            }
            Screens::InputFieldsTagsScreen(input_fields_tag_screen) => {
                UiScreens::InputFieldsTagsScreen(input_fields_tag_screen.into())
            }
            Screens::SettingsScreen => UiScreens::Settings,
            Screens::SettingPreviousConnectionsScreen(
                settings_previous_connections_actions_screen,
            ) => UiScreens::SettingPreviousConnections(UiSettingsPreviousConnectionsScreenData {
                ui_previous_connections: previous_connections_to_ui_previous_connections(
                    &settings_previous_connections_actions_screen.previous_connections,
                ),
                ui_unsaved_data: settings_previous_connections_actions_screen.unsaved_data,
                ui_display_save_dialog: settings_previous_connections_actions_screen
                    .display_save_dialog,
            }),
            Screens::PostWriteReviewScreen(inner) => UiScreens::PostWriteReview(inner.into()),
        }
    }
}
