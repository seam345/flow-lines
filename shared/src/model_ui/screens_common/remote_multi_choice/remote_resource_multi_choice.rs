use crate::model_ui::screens_common::loading::UiLoadingResource;
use crate::model_ui::screens_common::remote_multi_choice::multi_choice::UiMultiChoice;
use crate::screens::input_fields_and_tags::RemoteResource;
use crate::screens_common::model::multi_choice::MultiChoice;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum UiRemoteResourceMultiChoice {
    Loading(UiLoadingResource),
    Loaded(UiMultiChoice),
    NonExistent,
}

impl<T> From<&RemoteResource<MultiChoice<T>>> for UiRemoteResourceMultiChoice
where
    T: Clone + std::fmt::Display,
{
    fn from(value: &RemoteResource<MultiChoice<T>>) -> Self {
        match value {
            RemoteResource::NotLoaded(inner) => UiRemoteResourceMultiChoice::Loading(inner.into()),
            RemoteResource::Loaded(inner) => UiRemoteResourceMultiChoice::Loaded(inner.into()),
            RemoteResource::NonExistent => UiRemoteResourceMultiChoice::NonExistent,
        }
    }
}
