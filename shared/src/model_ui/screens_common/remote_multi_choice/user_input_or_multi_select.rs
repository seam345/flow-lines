use crate::model_ui::screens_common::remote_multi_choice::multi_choice::{
    multi_choice_options_to_ui_multi_select_choice, UiMultiChoiceItem, UiStringIndex,
};
use crate::model_ui::screens_common::validated_string::UiValidatedString;
use crate::screens::input_fields_and_tags::RemoteResource;
use crate::screens_common::model::multi_choice::MultiChoice;
use crate::screens_common::model::user_input_or_multi_choice::{
    SelectedUserInputOrMultiChoice, UserInputOrMultiChoice,
};
use serde::{Deserialize, Serialize};

/// A drop-down list that the user can create a new item by just typing
///
/// As some UI's the mode switch is not obvious when making a new value there is the notify_user_of_mode_switch
/// which if Some() should display the inner string to the user to dismiss. Ideally add a user preference to
/// not show these notifications if the user understands how it works
///
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct UiUserInputOrMultiChoice {
    pub options: UiRemoteResourceUserInputOrMultiChoiceOptions,
    pub selected: Option<UiSelectedUserInputOrMultiChoice>,
    pub notify_user_of_mode_switch: Option<String>,
}

impl From<&UserInputOrMultiChoice> for UiUserInputOrMultiChoice {
    fn from(value: &UserInputOrMultiChoice) -> Self {
        UiUserInputOrMultiChoice {
            options: (&value.options).into(),
            selected: value.selected.as_ref().map(|value| value.into()),
            notify_user_of_mode_switch: value
                .notify_user_of_mode_switch
                .as_ref()
                .map(|value| value.0.clone()),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum UiRemoteResourceUserInputOrMultiChoiceOptions {
    Loading,
    Loaded(Vec<UiMultiChoiceItem>),
    NonExistent,
}

impl<T> From<&RemoteResource<MultiChoice<T>>> for UiRemoteResourceUserInputOrMultiChoiceOptions
where
    T: ToString + Clone,
{
    fn from(value: &RemoteResource<MultiChoice<T>>) -> Self {
        match value {
            // todo maybe add in a failed load
            RemoteResource::NotLoaded(_) => UiRemoteResourceUserInputOrMultiChoiceOptions::Loading,
            RemoteResource::NonExistent => {
                UiRemoteResourceUserInputOrMultiChoiceOptions::NonExistent
            }
            RemoteResource::Loaded(inner_value) => {
                UiRemoteResourceUserInputOrMultiChoiceOptions::Loaded(
                    multi_choice_options_to_ui_multi_select_choice(&inner_value.options),
                )
            }
        }
    }
}

impl<T> From<&RemoteResource<Vec<T>>> for UiRemoteResourceUserInputOrMultiChoiceOptions
where
    T: ToString + Clone,
{
    fn from(value: &RemoteResource<Vec<T>>) -> Self {
        match value {
            // todo maybe add in a failed load
            RemoteResource::NotLoaded(_) => UiRemoteResourceUserInputOrMultiChoiceOptions::Loading,
            RemoteResource::NonExistent => {
                UiRemoteResourceUserInputOrMultiChoiceOptions::NonExistent
            }
            RemoteResource::Loaded(inner_value) => {
                UiRemoteResourceUserInputOrMultiChoiceOptions::Loaded(
                    inner_value
                        .clone()
                        .iter()
                        .enumerate()
                        .map(|(index, inner_string)| UiMultiChoiceItem {
                            hidden_uid: index.into(),
                            user_facing_value: inner_string.to_string(),
                        })
                        .collect(),
                )
            }
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum UiSelectedUserInputOrMultiChoice {
    IndexMode(UiStringIndex),
    UserMode(UiValidatedString),
}

impl From<&SelectedUserInputOrMultiChoice> for UiSelectedUserInputOrMultiChoice {
    fn from(value: &SelectedUserInputOrMultiChoice) -> Self {
        match value {
            SelectedUserInputOrMultiChoice::Index(inner_value) => {
                UiSelectedUserInputOrMultiChoice::IndexMode(UiStringIndex(inner_value.to_string()))
            }
            SelectedUserInputOrMultiChoice::User(inner_value) => {
                UiSelectedUserInputOrMultiChoice::UserMode(inner_value.as_ui_validated_string())
            }
        }
    }
}

impl From<SelectedUserInputOrMultiChoice> for UiSelectedUserInputOrMultiChoice {
    fn from(value: SelectedUserInputOrMultiChoice) -> Self {
        (&value).into()
    }
}
