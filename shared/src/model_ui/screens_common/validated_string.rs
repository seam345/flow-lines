use crate::screens_common::model::field_type::UserFieldValue;
use crate::screens_common::model::user_input_or_multi_choice::ValidatingStringPreTrimmed;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct UiValidatedString {
    pub value: String,
    pub valid: bool,
}

impl From<UserFieldValue> for UiValidatedString {
    fn from(value: UserFieldValue) -> Self {
        value.as_ui_validated_string()
    }
}

impl From<&ValidatingStringPreTrimmed> for UiValidatedString {
    fn from(value: &ValidatingStringPreTrimmed) -> Self {
        value.as_ui_validated_string()
    }
}
