use crate::screens::input_fields_and_tags::{LoadingResource, LoadingState};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct UiLoadingResource {
    pub retry_count: u8,
    pub current_state: UiLoadingState,
    // todo consider a timestamp?? for timeout or something like that? in theory i dont need it but maybe?
}

impl From<&LoadingResource> for UiLoadingResource {
    fn from(value: &LoadingResource) -> Self {
        UiLoadingResource {
            retry_count: value.retry_count,
            current_state: (&value.current_state).into(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum UiLoadingState {
    AwaitingUserInteraction,
    RequestSent,
    Initialised,
}

impl From<&LoadingState> for UiLoadingState {
    fn from(value: &LoadingState) -> Self {
        match value {
            LoadingState::AwaitingUserInteraction => UiLoadingState::AwaitingUserInteraction,
            LoadingState::RequestSent => UiLoadingState::RequestSent,
            LoadingState::Initialised => UiLoadingState::Initialised,
        }
    }
}
