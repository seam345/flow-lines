use crate::model;
use serde::{Deserialize, Serialize};
use std::collections::VecDeque;

#[derive(Serialize, Deserialize, Clone)]
pub struct UiUserMessages {
    pub messages: VecDeque<UiUserMessage>,
    // current_id: u64,
}

impl From<model::UserMessages> for UiUserMessages {
    fn from(value: model::UserMessages) -> Self {
        let temp: VecDeque<UiUserMessage> = value
            .messages
            .into_iter()
            .map(|value| value.into())
            .collect();

        UiUserMessages { messages: temp }
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct UiUserMessage {
    pub message: String,
    pub level: UiLevel,
    pub id: u64,
}

impl From<model::UserMessage> for UiUserMessage {
    fn from(value: model::UserMessage) -> Self {
        UiUserMessage {
            message: value.message.clone(),
            level: value.level.into(),
            id: value.id,
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub enum UiLevel {
    /// The "error" level.
    ///
    /// Designates very serious errors.
    // This way these line up with the discriminants for LevelFilter below
    // This works because Rust treats field-less enums the same way as C does:
    // https://doc.rust-lang.org/reference/items/enumerations.html#custom-discriminant-values-for-field-less-enumerations
    Error = 1,
    /// The "warn" level.
    ///
    /// Designates hazardous situations.
    Warn,
    /// The "info" level.
    ///
    /// Designates useful information.
    Info,
    /// The "debug" level.
    ///
    /// Designates lower priority information.
    Debug,
    /// The "trace" level.
    ///
    /// Designates very low priority, often extremely verbose, information.
    Trace,
}

impl From<log::Level> for UiLevel {
    fn from(value: log::Level) -> Self {
        match value {
            log::Level::Error => UiLevel::Error,
            log::Level::Warn => UiLevel::Warn,
            log::Level::Info => UiLevel::Info,
            log::Level::Debug => UiLevel::Debug,
            log::Level::Trace => UiLevel::Trace,
        }
    }
}
