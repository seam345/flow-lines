use crate::screens::initial::QuickAction;
use crate::screens::post_write_review::PostWriteReviewScreenData;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone)]
pub struct UiPostWriteReviewScreenData {
    // hold stuff just sent to allow a fast repeat and to save for later
    pub previous_write: UiQuickActionSummery,
    /// data point just sent plus 10 more
    pub eleven_data_points: Vec<String>,
}

impl From<&PostWriteReviewScreenData> for UiPostWriteReviewScreenData {
    fn from(value: &PostWriteReviewScreenData) -> Self {
        UiPostWriteReviewScreenData {
            previous_write: (&value.previous_write).into(),
            eleven_data_points: value.eleven_data_points.clone(),
        }
    }
}
#[derive(Clone, Serialize, Deserialize)]
pub struct UiQuickActionSummery(String);

impl From<&QuickAction> for UiQuickActionSummery {
    fn from(value: &QuickAction) -> Self {
        UiQuickActionSummery(format!(
            "Preloads into input value screen with selected:\n\
            Org: {}\n\
            Bucket: {}\n\
            Measurement: {}\n\
            With preselected tags: {}\n\
            And empty preselected fields: {}",
            value.org.name,
            value.bucket,
            value.measurement,
            value
                .tags
                .iter()
                .fold("".to_owned(), |acc, (key, value)| format!(
                    "{acc}, {key}={}",
                    value.clone().unwrap_or("<unset>".to_owned())
                )),
            value
                .fields
                .iter()
                .fold("".to_owned(), |acc, (key, _)| format!("{acc}, {key}"))
        ))
    }
}
