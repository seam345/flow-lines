use crate::model_ui::screens_common::remote_multi_choice::remote_resource_multi_choice::UiRemoteResourceMultiChoice;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone)]
pub struct UiConnectQuickActionScreenData {
    pub api_token: Option<String>,
    pub base_url: Option<String>,
    pub quick_actions: UiRemoteResourceMultiChoice,
    pub previous_connections: UiRemoteResourceMultiChoice,
    pub failed_to_contact_influx: bool,
}
