use crate::model_ui::screens::settings_preview_connections_actions::UiPrecision;
use crate::model_ui::screens_common::remote_multi_choice::multi_choice::UiStringIndex;
use crate::model_ui::screens_common::remote_multi_choice::user_input_or_multi_select::UiUserInputOrMultiChoice;
use crate::model_ui::screens_common::{
    loading::UiLoadingResource, validated_string::UiValidatedString,
};
use crate::screens::input_fields_and_tags::{
    AvailableFields, AvailableTagValues, AvailableTags, InputFieldsAndTagsScreenData, Precision,
    RemoteResource, UserTag,
};
use crate::screens_common::influx::generate_flux_line;
use crate::screens_common::model::field_type::{FieldType, UserFieldValue};
use chrono::{DateTime, Utc};
use log::trace;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

#[derive(Serialize, Deserialize, Clone)]
pub struct UiInputFieldsAndTagsScreenData {
    // pub api_token: String, // I don't think there is much point to sending this to UI at this point
    pub base_url: String,
    pub org: String,
    pub bucket: String,
    pub measurement: String,
    // todo i need the above?
    pub available_tags: UiRemoteResourceAvailableTags,
    pub tags: BTreeMap<String, UiUserInputOrMultiChoice>,
    pub new_tags: Vec<UiUserTags>,
    pub available_fields: UiRemoteResourceAvailableFields,
    /// Key matches the key in the available fields and the UiValidated string is the raw user input
    /// with some pre-computed valid identifier
    pub fields: BTreeMap<String, UiFieldUserInput>,
    pub new_fields: Vec<UiUserFields>,
    pub line: String,
    // convert i64 to localised 8601 (i cba with time zones atm)
    // e.g. 0 = 1970-01-01T00:00:00
    // precision changes how much of an effect this number has
    pub ui_time: Option<String>,
    pub precision: UiPrecision,
    pub expected_line: String,
}

impl From<&InputFieldsAndTagsScreenData> for UiInputFieldsAndTagsScreenData {
    fn from(input_fields_tag_screen: &InputFieldsAndTagsScreenData) -> Self {
        UiInputFieldsAndTagsScreenData {
            base_url: input_fields_tag_screen.base_url.to_string(),
            org: input_fields_tag_screen.org.name.clone(),
            bucket: input_fields_tag_screen.bucket.clone(),
            measurement: input_fields_tag_screen.measurement.clone(),
            available_tags: (&input_fields_tag_screen.available_tags).into(),
            tags: input_fields_tag_screen.tags.iter().fold(
                BTreeMap::new(),
                |mut acc, (key, value)| {
                    acc.insert(key.clone(), value.into());
                    acc
                },
            ),
            new_tags: new_tags_to_ui_new_tags(&input_fields_tag_screen.new_tags),
            available_fields: (&input_fields_tag_screen.available_fields).into(),
            fields: input_fields_tag_screen.fields.clone().into_iter().fold(
                BTreeMap::new(),
                |mut acc, (key, value)| {
                    acc.insert(key, (&value).into());
                    acc
                },
            ),
            new_fields: input_fields_tag_screen
                .new_fields
                .clone()
                .iter()
                .enumerate()
                .fold(vec![], |mut acc, (index, (key, value))| {
                    acc.push(UiUserFields {
                        key: key.into(),
                        value: value.into(),
                        hidden_uid: index.into(),
                    });
                    acc
                }),
            line: input_fields_tag_screen.line.clone(),
            ui_time: time_precision_into_ui_time(
                input_fields_tag_screen.time,
                &input_fields_tag_screen.precision,
            ),
            precision: input_fields_tag_screen.precision.clone().into(),
            expected_line: generate_flux_line(
                &input_fields_tag_screen.tags,
                &input_fields_tag_screen.new_tags,
                &input_fields_tag_screen.fields,
                &input_fields_tag_screen.new_fields,
                &input_fields_tag_screen.time,
                &input_fields_tag_screen.measurement,
            )
            .unwrap_or_else(|error| error.partial_line),
        }
    }
}

// ui code bad with generics
#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum UiRemoteResourceAvailableTags {
    Loading(UiLoadingResource),
    Loaded(UiAvailableTags),
    NonExistent,
}

impl From<&RemoteResource<AvailableTags>> for UiRemoteResourceAvailableTags {
    fn from(value: &RemoteResource<AvailableTags>) -> Self {
        match value {
            RemoteResource::NotLoaded(inner) => {
                UiRemoteResourceAvailableTags::Loading(inner.into())
            }
            RemoteResource::Loaded(inner) => UiRemoteResourceAvailableTags::Loaded(inner.into()),
            RemoteResource::NonExistent => UiRemoteResourceAvailableTags::NonExistent,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct UiAvailableTags(BTreeMap<String, UiRemoteResourceAvailableTagValues>);

impl UiAvailableTags {
    pub fn insert(
        &mut self,
        key: String,
        value: UiRemoteResourceAvailableTagValues,
    ) -> Option<UiRemoteResourceAvailableTagValues> {
        self.0.insert(key, value)
    }
}

impl From<&AvailableTags> for UiAvailableTags {
    fn from(value: &AvailableTags) -> Self {
        value
            .iter()
            .fold(UiAvailableTags(BTreeMap::new()), |mut acc, (key, value)| {
                acc.insert(key.clone(), value.into());
                acc
            })
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum UiRemoteResourceAvailableTagValues {
    Loading(UiLoadingResource),
    Loaded(UiAvailableTagValues),
    NonExistent,
}

impl From<&RemoteResource<AvailableTagValues>> for UiRemoteResourceAvailableTagValues {
    fn from(value: &RemoteResource<AvailableTagValues>) -> Self {
        match value {
            RemoteResource::NotLoaded(inner) => {
                UiRemoteResourceAvailableTagValues::Loading(inner.into())
            }
            RemoteResource::Loaded(inner) => {
                UiRemoteResourceAvailableTagValues::Loaded(inner.into())
            }
            RemoteResource::NonExistent => UiRemoteResourceAvailableTagValues::NonExistent,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct UiAvailableTagValues(Vec<String>);

impl From<&AvailableTagValues> for UiAvailableTagValues {
    fn from(value: &AvailableTagValues) -> Self {
        UiAvailableTagValues(value.0.clone())
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum UiRemoteResourceAvailableFields {
    Loading(UiLoadingResource),
    Loaded(UiAvailableFields),
    NonExistent,
}

impl From<&RemoteResource<AvailableFields>> for UiRemoteResourceAvailableFields {
    fn from(value: &RemoteResource<AvailableFields>) -> Self {
        match value {
            RemoteResource::NotLoaded(inner) => {
                UiRemoteResourceAvailableFields::Loading(inner.into())
            }
            RemoteResource::Loaded(inner) => UiRemoteResourceAvailableFields::Loaded(inner.into()),
            RemoteResource::NonExistent => UiRemoteResourceAvailableFields::NonExistent,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct UiAvailableFields(Vec<String>);

impl UiAvailableFields {
    pub fn push(&mut self, value: String) {
        self.0.push(value)
    }
}

impl From<&AvailableFields> for UiAvailableFields {
    fn from(value: &AvailableFields) -> Self {
        value
            .keys()
            .fold(UiAvailableFields(vec![]), |mut acc, val| {
                acc.push(val.to_owned());
                acc
            })
        // .map(|field| field.to_owned())
        // .collect()
    }
}
/*
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum UiRemoteResource<T> {
    Loading(UiLoadingResource),
    Loaded(T),
}

impl<'a, T: 'a, U> From<&'a RemoteResource<T>> for UiRemoteResource<U>
where
    &'a T: Into<U>,
{
    fn from(value: &'a RemoteResource<T>) -> Self {
        match value {
            RemoteResource::NotLoaded(inner) => UiRemoteResource::Loading(inner.into()),
            RemoteResource::Loaded(inner) => UiRemoteResource::Loaded(inner.into()),
        }
    }
}*/

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct UiUserFields {
    pub hidden_uid: UiStringIndex,
    pub key: UiValidatedString,
    pub value: UiFieldUserInput,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct UiUserTags {
    pub hidden_uid: String,
    pub key: Option<String>,
    pub value: Option<String>,
}

fn new_tags_to_ui_new_tags(internal: &[UserTag]) -> Vec<UiUserTags> {
    let mut drop_down_list: Vec<UiUserTags> = vec![];

    internal.iter().enumerate().for_each(|(index, value)| {
        drop_down_list.push(UiUserTags {
            hidden_uid: index.to_string(),
            key: value.key.clone(),
            value: value.value.clone(),
        })
    });
    trace!("drop down list items: {:?}", drop_down_list);
    drop_down_list
}

fn time_precision_into_ui_time(time: Option<i64>, precision: &Precision) -> Option<String> {
    let dt: DateTime<Utc> = match precision {
        Precision::Seconds => DateTime::from_timestamp(time?, 0).unwrap(),
        Precision::Milliseconds => DateTime::from_timestamp_millis(time?).unwrap(),
        Precision::Microseconds => DateTime::from_timestamp_micros(time?).unwrap(),
        Precision::Nanoseconds => DateTime::from_timestamp_nanos(time?),
    };
    Some(format!("{}", dt.format("%+")))
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct UiFieldUserInput {
    pub field_type: UiFieldType,
    pub input: UiValidatedString,
}

impl From<&UserFieldValue> for UiFieldUserInput {
    fn from(value: &UserFieldValue) -> Self {
        UiFieldUserInput {
            field_type: value.into(),
            input: value.as_ui_validated_string(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub enum UiFieldType {
    String,
    Int,
    UInt,
    #[default]
    Float,
    Bool,
}

impl From<FieldType> for UiFieldType {
    fn from(value: FieldType) -> Self {
        match value {
            FieldType::String => UiFieldType::String,
            FieldType::Int => UiFieldType::Int,
            FieldType::UInt => UiFieldType::UInt,
            FieldType::Float => UiFieldType::Float,
            FieldType::Bool => UiFieldType::Bool,
        }
    }
}

impl From<&UserFieldValue> for UiFieldType {
    fn from(value: &UserFieldValue) -> Self {
        match value {
            UserFieldValue::String(_) => UiFieldType::String,
            UserFieldValue::Int(_) => UiFieldType::Int,
            UserFieldValue::UInt(_) => UiFieldType::UInt,
            UserFieldValue::Float(_) => UiFieldType::Float,
            UserFieldValue::Bool(_) => UiFieldType::Bool,
        }
    }
}
