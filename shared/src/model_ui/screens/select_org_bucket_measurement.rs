use crate::model_ui::screens_common::remote_multi_choice::remote_resource_multi_choice::UiRemoteResourceMultiChoice;
use crate::model_ui::screens_common::remote_multi_choice::user_input_or_multi_select::UiUserInputOrMultiChoice;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone)]
pub struct UiSelectOrgBucketMeasurementScreenData {
    // pub api_token: String, // I don't think there is much point to sending this to UI at this point
    pub base_url: String,
    pub orgs: UiRemoteResourceMultiChoice,
    pub buckets: UiRemoteResourceMultiChoice,
    pub measurements: UiUserInputOrMultiChoice,
}
