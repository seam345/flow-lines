use crate::screens::input_fields_and_tags::Precision;
use crate::screens_common::model::previous_connection::PreviousConnection;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone)]
pub struct UiSettingsPreviousConnectionsScreenData {
    pub ui_previous_connections: Vec<UiPreviousConnection>,
    /// this will be set to true once the user has made any edits
    /// plan is to show a save button when true
    pub ui_unsaved_data: bool,
    /// if the users selects back when we have unsaved data, we should pop up a dialog to ask if
    /// they would like to save before exiting
    pub ui_display_save_dialog: bool,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct UiPreviousConnection {
    pub internal_index: String,
    pub name: Option<String>,
    pub base_url: String,
}

pub fn previous_connections_to_ui_previous_connections(
    previous_connections: &[PreviousConnection],
) -> Vec<UiPreviousConnection> {
    previous_connections
        .iter()
        .enumerate()
        .map(|(index, previous_connection)| UiPreviousConnection {
            internal_index: index.to_string(),
            name: previous_connection.name.clone(), //.unwrap_or("".to_string()),
            base_url: previous_connection.base_url.clone(),
        })
        .collect()
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub enum UiPrecision {
    Seconds,
    //s
    Milliseconds,
    // ms
    Microseconds,
    // us
    #[default]
    Nanoseconds, //ns
}

impl From<Precision> for UiPrecision {
    fn from(value: Precision) -> Self {
        match value {
            Precision::Seconds => UiPrecision::Seconds,
            Precision::Milliseconds => UiPrecision::Milliseconds,
            Precision::Microseconds => UiPrecision::Microseconds,
            Precision::Nanoseconds => UiPrecision::Nanoseconds,
        }
    }
}
