use serde::{Deserialize, Serialize};

use crux_core::capability::CapabilityContext;
use crux_core::capability::Operation;
use crux_core::Command;
use crux_macros::Capability;

use std::future::Future;

#[derive(Capability)]
pub struct PersistentStorage<Ev> {
    context: CapabilityContext<PersistentStorageOperation, Ev>,
}

/// Public API of the capability, called by App::update.
impl<Ev> PersistentStorage<Ev>
where
    Ev: 'static,
{
    pub fn new(context: CapabilityContext<PersistentStorageOperation, Ev>) -> Self {
        Self { context }
    }
}

pub type Data = String;
type Filename = String;
#[derive(Clone, Serialize, Deserialize, Debug, PartialEq, Eq)]
pub enum PersistentStorageOperation {
    Save(Filename, Data),
    AppendOrCreate(Filename, Data),
    Load(Filename),
}

impl Operation for PersistentStorageOperation {
    type Output = PersistentStorageOutput;
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum PersistentStorageOutput {
    FileData(String),
    SaveOk,
    AppendOk,
    Error,
    LoadErrorFileDoesntExist, // Failed to load as file doesn't exist
}

/// Instructs shell code to append to an existing file or create a new one if it doesn't exist
pub fn append_or_create_command<Effect, Event>(
    file_name: Filename,
    data: Data,
) -> crux_core::command::RequestBuilder<
    Effect,
    Event,
    impl Future<Output = <PersistentStorageOperation as Operation>::Output>,
>
where
    Effect: From<crate::Request<PersistentStorageOperation>> + Send + 'static,
    Event: Send + 'static,
{
    let request = PersistentStorageOperation::AppendOrCreate(file_name, data);
    Command::request_from_shell(request)
}

pub fn load<Effect, Event>(
    file_name: Filename,
) -> crux_core::command::RequestBuilder<
    Effect,
    Event,
    impl Future<Output = <PersistentStorageOperation as Operation>::Output>,
>
where
    Effect: From<crate::Request<PersistentStorageOperation>> + Send + 'static,
    Event: Send + 'static,
{
    let request = PersistentStorageOperation::Load(file_name);
    Command::request_from_shell(request)
}

pub fn save<Effect, Event>(
    file_name: Filename,
    data: Data,
) -> crux_core::command::RequestBuilder<
    Effect,
    Event,
    impl Future<Output = <PersistentStorageOperation as Operation>::Output>,
>
where
    Effect: From<crate::Request<PersistentStorageOperation>> + Send + 'static,
    Event: Send + 'static,
{
    let request = PersistentStorageOperation::Save(file_name, data);
    Command::request_from_shell(request)
}
