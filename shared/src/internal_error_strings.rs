#![allow(dead_code)]
/// List of internal errors
/// All these errors are unrecoverable and really a programming error, the user cant do anything
/// about them but report to developer, so I leave out any human understanding from the error message
///
/// All variables here should only be used in 1 place
/// Put them all in 1 file, so I can keep track of the numbers and not use the same number twice,
/// also means on report of these errors I have 1 place of truth to go to then ctrl+click to
/// find where it's used
///
/// To keep track of all usages the moment I use a number I prepend _USED to the variable to help
/// prevent another accidental usage
pub const N001_USED: &str = "Internal error 001, Please report to developer";
pub const N002_USED: &str = "Internal error 002, Please report to developer";
pub const N003_USED: &str = "Internal error 003, Please report to developer";
pub const N004_USED: &str = "Internal error 004, Please report to developer";
pub const N005_USED: &str = "Internal error 005, Please report to developer";
pub const N006_USED: &str = "Internal error 006, Please report to developer";
pub const N007_USED: &str = "Internal error 007, Please report to developer";
pub const N008_USED: &str = "Internal error 008, Please report to developer";
pub const N009_USED: &str = "Internal error 009, Please report to developer";
pub const N010_USED: &str = "Internal error 010, Please report to developer";
pub const N011_USED: &str = "Internal error 011, Please report to developer";
pub const N012_USED: &str = "Internal error 012, Please report to developer";
pub const N013_USED: &str = "Internal error 013, Please report to developer";
pub const N014_USED: &str = "Internal error 014, Please report to developer";
pub const N015_USED: &str = "Internal error 015, Please report to developer";
pub const N016_USED: &str = "Internal error 016, Please report to developer";
pub const N017_USED: &str = "Internal error 017, Please report to developer";
pub const N018_USED: &str = "Internal error 018, Please report to developer";
pub const N019_USED: &str = "Internal error 019, Please report to developer";
pub const N020_USED: &str = "Internal error 020, Please report to developer";
pub const N021_USED: &str = "Internal error 021, Please report to developer";
pub const N022_USED: &str = "Internal error 022, Please report to developer";
pub const N023_USED: &str = "Internal error 023, Please report to developer";
pub const N024_USED: &str = "Internal error 024, Please report to developer";
pub const N025_USED: &str = "Internal error 025, Please report to developer";
pub const N026_USED: &str = "Internal error 026, Please report to developer";
pub const N027_USED: &str = "Internal error 027, Please report to developer";
pub const N028_USED: &str = "Internal error 028, Please report to developer";
pub const N029_USED: &str = "Internal error 029, Please report to developer";
pub const N030_USED: &str = "Internal error 030, Please report to developer";
pub const N031_USED: &str = "Internal error 031, Please report to developer";
pub const N032_USED: &str = "Internal error 032, Please report to developer";
pub const N033_USED: &str = "Internal error 033, Please report to developer";
pub const N034_USED: &str = "Internal error 034, Please report to developer";
pub const N035_USED: &str = "Internal error 035, Please report to developer";
pub const N036_USED: &str = "Internal error 036, Please report to developer";
pub const N037_USED: &str = "Internal error 037, Please report to developer";
pub const N038_USED: &str = "Internal error 038, Please report to developer";
pub const N039_USED: &str = "Internal error 039, Please report to developer";
pub const N040_USED: &str = "Internal error 040, Please report to developer";
pub const N041_USED: &str = "Internal error 041, Please report to developer";
pub const N042_USED: &str = "Internal error 042, Please report to developer";
pub const N043_USED: &str = "Internal error 043, Please report to developer";
pub const N044_USED: &str = "Internal error 044, Please report to developer";
pub const N045_USED: &str = "Internal error 045, Please report to developer";
pub const N046_USED: &str = "Internal error 046, Please report to developer";
pub const N047_USED: &str = "Internal error 047, Please report to developer";
pub const N048_USED: &str = "Internal error 048, Please report to developer";
pub const N049_USED: &str = "Internal error 049, Please report to developer";
pub const N050_USED: &str = "Internal error 050, Please report to developer";
pub const N051_USED: &str = "Internal error 051, Please report to developer";
pub const N052_USED: &str = "Internal error 052, Please report to developer";
pub const N053_USED: &str = "Internal error 053, Please report to developer";
pub const N054_USED: &str = "Internal error 054, Please report to developer";
pub const N055_USED: &str = "Internal error 055, Please report to developer";
pub const N056_USED: &str = "Internal error 056, Please report to developer";
pub const N057_USED: &str = "Internal error 057, Please report to developer";
pub const N058_USED: &str = "Internal error 058, Please report to developer";
pub const N059_USED: &str = "Internal error 059, Please report to developer";
pub const N060_USED: &str = "Internal error 060, Please report to developer";
pub const N061_USED: &str = "Internal error 061, Please report to developer";
pub const N062_USED: &str = "Internal error 062, Please report to developer";
pub const N063_USED: &str = "Internal error 063, Please report to developer";
pub const N064_USED: &str = "Internal error 064, Please report to developer";
pub const N065_USED: &str = "Internal error 065, Please report to developer";
pub const N066: &str = "Internal error 066, Please report to developer";
pub const N067: &str = "Internal error 067, Please report to developer";
pub const N068: &str = "Internal error 068, Please report to developer";
pub const N069: &str = "Internal error 069, Please report to developer";
