pub mod app;
pub mod global;
mod internal_error_strings;
pub mod model;
pub mod model_ui;
mod screens_common;

pub mod capabilities {
    pub mod persistent_storage;
}

pub mod screens {
    pub mod initial;
    pub mod input_fields_and_tags;
    pub mod post_write_review;
    pub mod select_org_bucket_measurements;
    pub mod settings;
    pub mod settings_previous_connections_actions;
}

use lazy_static::lazy_static;
use wasm_bindgen::prelude::wasm_bindgen;

pub use app::*;
// use crux_core::Effect;
pub use crux_core::{bridge::Bridge, Core, Request};
pub use crux_http as http;

// TODO hide this plumbing

uniffi::include_scaffolding!("shared");

lazy_static! {
    static ref CORE: Bridge<Counter> = Bridge::new(Core::new());
}

#[wasm_bindgen]
pub fn process_event(data: &[u8]) -> Vec<u8> {
    CORE.process_event(data)
}

#[wasm_bindgen]
pub fn handle_response(id: u32, data: &[u8]) -> Vec<u8> {
    CORE.handle_response(id, data)
}

#[wasm_bindgen]
pub fn view() -> Vec<u8> {
    CORE.view()
}
