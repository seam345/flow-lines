use crate::internal_error_strings::{N030_USED, N035_USED};
use crate::model::{ExampleData, UserMessages};
use crate::{Effect, Event};
use crux_core::Command;
use log::Level::Error;

pub fn update_tutorial(
    new_value: u8,
    example_data: &mut ExampleData,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    match example_data {
        ExampleData::Tutorial(ref mut inner) => *inner = new_value,
        ExampleData::FakeData => user_messages.add_message(N030_USED, Error),
        ExampleData::Real => user_messages.add_message(N035_USED, Error),
    }
    crux_core::render::render()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::Model;
    use crate::Counter;
    use crux_core::testing::AppTester;

    #[test]
    fn update_tutorial_test() {
        let mut model = Model {
            example_data: ExampleData::Tutorial(0),
            ..Default::default()
        };

        update_tutorial(5, &mut model.example_data, &mut model.user_messages);

        let app = AppTester::<Counter>::default();

        insta::assert_yaml_snapshot!(app.view(&mut model), @r#"
        ---
        screen:
          InitialScreen:
            api_token: ~
            base_url: ~
            quick_actions:
              Loading:
                retry_count: 0
                current_state: Initialised
            previous_connections:
              Loading:
                retry_count: 0
                current_state: Initialised
            failed_to_contact_influx: false
        user_messages:
          messages: []
        tutorial: 5
        "#);
    }
}
