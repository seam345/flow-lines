use crate::model::{Model, Screens};
use crate::screens::initial::InitialScreenData;
use crate::screens::input_fields_and_tags::{input_fields_and_tags_events, ScEvInputFieldsAndTags};
use crate::screens::select_org_bucket_measurements::{
    select_org_bucket_measurement_screen_events, ScEvSelectOrgBucketMeasurement,
};
use crate::screens::settings_previous_connections_actions::{
    setting_previous_connections_events, ScEvSettingPreviousConnections,
};
use crate::{Effect, Event};
use crux_core::render::render;
use crux_core::Command;

pub fn back_button_clicked(model: &mut Model) -> Command<Effect, Event> {
    match model.screen {
        Screens::InitialScreen(_) => Command::done(),
        Screens::InputFieldsTagsScreen(_) => {
            input_fields_and_tags_events(ScEvInputFieldsAndTags::BackButtonPressed, model)
        }
        Screens::SelectOrgBucketMeasurementScreen(_) => {
            select_org_bucket_measurement_screen_events(
                ScEvSelectOrgBucketMeasurement::BackButtonClicked,
                model,
            )
        }
        Screens::SettingPreviousConnectionsScreen(_) => setting_previous_connections_events(
            ScEvSettingPreviousConnections::ExitToSettingsScreen,
            model,
        ),
        Screens::SettingsScreen => {
            model.screen = Screens::InitialScreen(InitialScreenData::default());
            render()
        }
        Screens::PostWriteReviewScreen(_) => Command::done(),
    }
}
