mod back_button_clicked;
mod update_tutorial;

use crate::global::back_button_clicked::back_button_clicked;
use crate::global::update_tutorial::update_tutorial;
use crate::model::Model;
use crate::{Effect, Event};
use crux_core::Command;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum GlEvGlobal {
    BackButtonClicked,
    UpdateTutorial(u8),
}

// ============================================================================
// Event processing.
// ============================================================================

pub fn global_events(event: GlEvGlobal, model: &mut Model) -> Command<Effect, Event> {
    match event {
        GlEvGlobal::BackButtonClicked => back_button_clicked(model),
        GlEvGlobal::UpdateTutorial(new_number) => update_tutorial(
            new_number,
            &mut model.example_data,
            &mut model.user_messages,
        ),
    }
}
