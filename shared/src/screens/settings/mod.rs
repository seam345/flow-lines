mod back_screen;
mod enter_tutorial_mode;
mod open_previous_connections_screen;
mod open_previous_connections_screen_result;
mod open_quick_actions_screen;
mod open_quick_actions_screen_result;

use crate::capabilities::persistent_storage::PersistentStorageOutput;
use crate::model::Model;
use crate::screens::settings::back_screen::back_screen;
use crate::screens::settings::enter_tutorial_mode::enter_tutorial_mode;
use crate::screens::settings::open_previous_connections_screen::open_previous_connections_screen;
use crate::screens::settings::open_previous_connections_screen_result::open_previous_connections_screen_result;
use crate::{Effect, Event};
use crux_core::Command;
use log::trace;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum ScEvSettings {
    Back, // atm this is just going back to initial screen… if I want to always access settings I need to store previous screen
    OpenPreviousConnectionsScreen,
    #[serde(skip)]
    OpenPreviousConnectionsScreenResult(PersistentStorageOutput),
    OpenQuickActionsScreen,
    #[serde(skip)]
    OpenQuickActionsScreenResult(PersistentStorageOutput),
    EnterTutorialMode,
}

pub fn settings_events(event: ScEvSettings, model: &mut Model) -> Command<Effect, Event> {
    // trace!("select org,bucket,measurement screen event: Validating model");

    // let Screens::Settings = model.screen else {
    //     error!("Screen was in an unexpected state {:?}", model.screen);
    //     model
    //         .user_messages
    //         .add_message("Unrecoverable internal error state", Level::Error);
    //     return;
    // };

    trace!("Running select settings event");

    match event {
        ScEvSettings::OpenPreviousConnectionsScreen => open_previous_connections_screen(),
        ScEvSettings::OpenPreviousConnectionsScreenResult(result) => {
            open_previous_connections_screen_result(
                &mut model.screen,
                &mut model.user_messages,
                result,
            );
            Command::done()
        }
        ScEvSettings::Back => back_screen(&mut model.screen),
        ScEvSettings::OpenQuickActionsScreen => Command::done(),
        ScEvSettings::OpenQuickActionsScreenResult(_) => Command::done(),
        ScEvSettings::EnterTutorialMode => {
            enter_tutorial_mode(&mut model.screen, &mut model.example_data)
        }
    }
}
