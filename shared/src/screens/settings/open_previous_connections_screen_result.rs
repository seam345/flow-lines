use crate::capabilities::persistent_storage::PersistentStorageOutput;
use crate::model::{Screens, UserMessages};
use crate::screens::settings_previous_connections_actions::SettingsPreviousConnectionsActionsScreenData;
use crate::screens_common::model::previous_connection::PreviousConnection;
use log::{debug, error, warn, Level};

pub fn open_previous_connections_screen_result(
    screen: &mut Screens,
    user_messages: &mut UserMessages,
    response: PersistentStorageOutput,
) {
    debug!("load_previous_connections_results called");
    match response {
        PersistentStorageOutput::FileData(data) => {
            debug!("loaded data: {data}");
            match serde_json::from_str::<Vec<PreviousConnection>>(&data) {
                Ok(value) => {
                    *screen = Screens::SettingPreviousConnectionsScreen(
                        SettingsPreviousConnectionsActionsScreenData {
                            previous_connections: value,
                            unsaved_data: false,
                            display_save_dialog: false,
                        },
                    );
                }
                Err(error) => {
                    error!("Failed to get previous connections, error: {:?}", error);
                    error!("data was {}", data);
                    user_messages.add_message(
                        format!("Failed to get previous connections, error: {:?}", error),
                        Level::Error,
                    );
                }
            }
        }
        PersistentStorageOutput::SaveOk
        | PersistentStorageOutput::Error
        | PersistentStorageOutput::AppendOk => todo!(),
        PersistentStorageOutput::LoadErrorFileDoesntExist => {
            warn!("Previous connections file doesnt exist!");
        }
    }
}
