use crate::model::Screens;
use crate::{Effect, Event};
use crux_core::Command;

pub fn back_screen(screen: &mut Screens) -> Command<Effect, Event> {
    *screen = Screens::InitialScreen(Default::default());
    crux_core::render::render()
}
