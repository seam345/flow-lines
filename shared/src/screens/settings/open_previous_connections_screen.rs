use crate::capabilities::persistent_storage;
use crate::capabilities::persistent_storage::PersistentStorageOutput;
use crate::model::PREVIOUS_CONNECTIONS_FILENAME;
use crate::screens::settings::ScEvSettings;
use crate::Event::EvSettingsScreen;
use crate::{Effect, Event};
use crux_core::Command;
use log::debug;

pub fn open_previous_connections_screen() -> Command<Effect, Event> {
    debug!("load_previous_connections called");
    persistent_storage::load(PREVIOUS_CONNECTIONS_FILENAME.to_owned())
        .then_send(construct_response_event)
}

fn construct_response_event(result: PersistentStorageOutput) -> Event {
    EvSettingsScreen(ScEvSettings::OpenPreviousConnectionsScreenResult(result))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::screens_common::test_common::test_shell;

    #[test]
    fn test_load_quick_actions() {
        let mut commands = open_previous_connections_screen();

        // confirm the correct events are called
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            PersistentStorage(
                Request(
                    Load(
                        "previous_connections",
                    ),
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let events: Vec<Event> = commands.events().collect();

        insta::assert_debug_snapshot!(
            events, @r#"
        [
            EvSettingsScreen(
                OpenPreviousConnectionsScreenResult(
                    FileData(
                        "Just a fake test",
                    ),
                ),
            ),
        ]
        "#);
    }
}
