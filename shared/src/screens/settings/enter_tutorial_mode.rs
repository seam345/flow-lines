use crate::model::{ExampleData, Screens};
use crate::screens::initial::InitialScreenData;
use crate::{Effect, Event};
use crux_core::Command;

pub fn enter_tutorial_mode(
    screen: &mut Screens,
    example_date: &mut ExampleData,
) -> Command<Effect, Event> {
    *screen = Screens::InitialScreen(InitialScreenData {
        api_token: Some("tutorial".to_owned()),
        base_url: Some("http://example.com".to_owned()),
        quick_actions: Default::default(),
        previous_connections: Default::default(),
        failed_to_contact_influx: false,
    });
    *example_date = ExampleData::Tutorial(0);

    crux_core::render::render()
}
