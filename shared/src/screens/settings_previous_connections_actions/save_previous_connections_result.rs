use crate::capabilities::persistent_storage::PersistentStorageOutput;
use crate::model::{Screens, UserMessages};
use crate::{Effect, Event};
use crux_core::Command;
use log::{debug, trace, warn, Level};

pub fn save_previous_connections_result(
    screen: &mut Screens,
    user_messages: &mut UserMessages,
    response: PersistentStorageOutput,
    and_exit: bool,
) -> Command<Effect, Event> {
    debug!("load_previous_connections_results called");
    match response {
        PersistentStorageOutput::SaveOk => {
            if and_exit {
                trace!(
                    "We have saved the credentials and told to exit, going back to settings screen"
                );
                *screen = Screens::SettingsScreen;
                crux_core::render::render()
            } else {
                trace!("previous connections have been saved, marking previous connections as not edited");
                match screen {
                    Screens::SettingPreviousConnectionsScreen(data) => {
                        data.unsaved_data = false;
                        crux_core::render::render()
                    }
                    Screens::SettingsScreen
                    | Screens::InitialScreen(_)
                    | Screens::SelectOrgBucketMeasurementScreen(_)
                    | Screens::InputFieldsTagsScreen(_)
                    | Screens::PostWriteReviewScreen(_) => {
                        unimplemented!()
                    }
                }
            }
        }
        PersistentStorageOutput::LoadErrorFileDoesntExist => {
            warn!("Previous connections file doesnt exist!");
            Command::done()
        }
        PersistentStorageOutput::Error => {
            user_messages.add_message("Error saving data", Level::Error);
            crux_core::render::render()
        }
        PersistentStorageOutput::FileData(_) | PersistentStorageOutput::AppendOk => {
            unimplemented!()
        }
    }
}
