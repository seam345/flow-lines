use crate::model::UserMessages;
use crate::screens::settings_previous_connections_actions::{
    Index, SettingsPreviousConnectionsActionsScreenData,
};
use crate::{Effect, Event};
use crux_core::Command;
use log::Level;

pub fn delete_a_previous_connection(
    screen_data: &mut SettingsPreviousConnectionsActionsScreenData,
    user_messages: &mut UserMessages,
    index: Index,
) -> Command<Effect, Event> {
    let Ok(index) = index.parse::<usize>() else {
        user_messages.add_message(
            "Internal error parsing, for previous connections index",
            Level::Error,
        );
        return crux_core::render::render();
    };

    // check index exits before .remove as remove panics. LLVM will optimise out the multiple checks
    let Some(_) = screen_data.previous_connections.get(index) else {
        user_messages.add_message(
            "Internal error getting data, for previous connections",
            Level::Error,
        );
        return crux_core::render::render();
    };
    screen_data.previous_connections.remove(index);

    screen_data.unsaved_data = true;
    crux_core::render::render()
}
