mod confirm_exit_to_settings_screen;
mod delete_a_previous_connection;
mod exit_to_settings_screen;
mod rename_previous_connection;
mod save_previous_connections;
mod save_previous_connections_result;

use crate::capabilities::persistent_storage::PersistentStorageOutput;
use crate::model::{Model, Screens};
use crate::screens::settings_previous_connections_actions::confirm_exit_to_settings_screen::confirm_exit_to_settings_screen;
use crate::screens::settings_previous_connections_actions::delete_a_previous_connection::delete_a_previous_connection;
use crate::screens::settings_previous_connections_actions::exit_to_settings_screen::exit_to_settings_screen;
use crate::screens::settings_previous_connections_actions::rename_previous_connection::rename_previous_connection;
use crate::screens::settings_previous_connections_actions::save_previous_connections::save_previous_connections;
use crate::screens::settings_previous_connections_actions::save_previous_connections_result::save_previous_connections_result;
use crate::screens_common::model::previous_connection::PreviousConnection;
use crate::{Effect, Event};
use crux_core::Command;
use log::{error, trace, Level};
use serde::{Deserialize, Serialize};

// ============================================================================
// Screen data
// ============================================================================

#[derive(Default, Clone, Debug)]
pub struct SettingsPreviousConnectionsActionsScreenData {
    pub previous_connections: Vec<PreviousConnection>,
    /// this will be set to true once the user has made any edits
    /// plan is to show a save button when true
    pub unsaved_data: bool,
    /// if the users selects back when we have unsaved data, we should pop up a dialog to ask if
    /// they would like to save before exiting
    pub display_save_dialog: bool,
}

// ============================================================================
// Screen events data
// ============================================================================

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum ScEvSettingPreviousConnections {
    /// User renames a connection, edits are not saved to disk
    RenamePreviousConnection(Index, Name),
    /// User deletes a connection, edits are not saved to disk
    DeletePreviousConnection(Index),
    /// Called when user would like to commit changes to disk,
    /// AndExit should be true if the user has chosen to save on the dialog when hitting exit
    SavePreviousConnections(AndExit),
    #[serde(skip)]
    SavePreviousConnectionsResult(PersistentStorageOutput, AndExit),
    ExitToSettingsScreen,
    ConfirmExitToSettingsScreen,
}
type Index = String;
type Name = String;
type AndExit = bool;

// ============================================================================
// Screen event processing.
// ============================================================================

pub fn setting_previous_connections_events(
    event: ScEvSettingPreviousConnections,
    model: &mut Model,
) -> Command<Effect, Event> {
    trace!("select setting_previous_connections_events screen event: Validating model");

    let Screens::SettingPreviousConnectionsScreen(ref mut screen_data) = model.screen else {
        error!("Screen was in an unexpected state {:?}", model.screen);
        model
            .user_messages
            .add_message("Unrecoverable internal error state", Level::Error);
        return crux_core::render::render();
    };

    trace!("Running select SettingPreviousConnectionsEvents");

    match event {
        ScEvSettingPreviousConnections::RenamePreviousConnection(index, name) => {
            rename_previous_connection(screen_data, &mut model.user_messages, index, name)
        }
        ScEvSettingPreviousConnections::DeletePreviousConnection(index) => {
            delete_a_previous_connection(screen_data, &mut model.user_messages, index)
        }
        ScEvSettingPreviousConnections::SavePreviousConnections(and_exit) => {
            save_previous_connections(screen_data, and_exit)
        }
        ScEvSettingPreviousConnections::SavePreviousConnectionsResult(result, and_edit) => {
            save_previous_connections_result(
                &mut model.screen,
                &mut model.user_messages,
                result,
                and_edit,
            )
        }
        ScEvSettingPreviousConnections::ExitToSettingsScreen => {
            exit_to_settings_screen(&mut model.screen, &mut model.user_messages)
        }
        ScEvSettingPreviousConnections::ConfirmExitToSettingsScreen => {
            confirm_exit_to_settings_screen(&mut model.screen, &mut model.user_messages)
        }
    }
}
