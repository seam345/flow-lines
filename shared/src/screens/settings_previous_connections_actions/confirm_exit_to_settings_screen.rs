use crate::model::{Screens, UserMessages};
use crate::{Effect, Event};
use crux_core::Command;
use log::{trace, Level};

pub fn confirm_exit_to_settings_screen(
    screen: &mut Screens,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    trace!("exit_to_settings_screen called, confirming on the correct screen");
    let Screens::SettingPreviousConnectionsScreen(_) = screen else {
        // todo the user can do nothing if this happens... it's really here for me at some point I need todo something about this
        user_messages.add_message("Internal error, unexpected screen", Level::Error);
        return crux_core::render::render();
    };

    *screen = Screens::SettingsScreen;
    crux_core::render::render()
}
