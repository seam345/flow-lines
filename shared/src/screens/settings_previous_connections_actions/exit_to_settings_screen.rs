use crate::internal_error_strings::N063_USED;
use crate::model::{Screens, UserMessages};
use crate::{Effect, Event};
use crux_core::Command;
use log::trace;

pub fn exit_to_settings_screen(
    screen: &mut Screens,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    trace!("exit_to_settings_screen called, confirming on the correct screen");
    let Screens::SettingPreviousConnectionsScreen(data) = screen else {
        user_messages.error_report_to_developer(N063_USED);
        return crux_core::render::render();
    };

    if data.unsaved_data {
        data.display_save_dialog = true;
    } else {
        *screen = Screens::SettingsScreen;
    }
    crux_core::render::render()
}
