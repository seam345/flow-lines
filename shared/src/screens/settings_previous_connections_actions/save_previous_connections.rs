use crate::capabilities::persistent_storage::PersistentStorageOutput;
use crate::model::PREVIOUS_CONNECTIONS_FILENAME;
use crate::screens::settings_previous_connections_actions::{
    ScEvSettingPreviousConnections, SettingsPreviousConnectionsActionsScreenData,
};
use crate::Event::EvSettingsPreviousConnectionsScreen;
use crate::{capabilities, Effect, Event};
use crux_core::Command;

/// Uses connection details just entered into the model to append to all previous connections
/// if connection is the same as a previous it will skip saving
/// For now i shall write the whole file out from here todo later send only the append data
pub fn save_previous_connections(
    model: &SettingsPreviousConnectionsActionsScreenData,
    and_exit: bool,
) -> Command<Effect, Event> {
    let values = model.previous_connections.clone();
    let data = serde_json::to_string(&values).unwrap();
    capabilities::persistent_storage::save(PREVIOUS_CONNECTIONS_FILENAME.to_owned(), data)
        .then_send(move |result| construct_response_event(result, and_exit))
}

//  F: FnOnce(crate::Result<Response<ExpectBody>>) -> Event + Send + 'static,
fn construct_response_event(body: PersistentStorageOutput, and_exit: bool) -> Event {
    EvSettingsPreviousConnectionsScreen(
        ScEvSettingPreviousConnections::SavePreviousConnectionsResult(body, and_exit),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::screens_common::model::previous_connection::PreviousConnection;
    use crate::screens_common::test_common::test_shell;

    #[test]
    fn test_load_quick_actions() {
        let screen = SettingsPreviousConnectionsActionsScreenData {
            previous_connections: vec![PreviousConnection {
                name: Some("name".to_owned()),
                api_token: "token".to_owned(),
                base_url: "url".to_owned(),
            }],
            ..Default::default()
        };

        let mut commands = save_previous_connections(&screen, false);

        // confirm the correct events are called
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            PersistentStorage(
                Request(
                    Save(
                        "previous_connections",
                        "[{\"name\":\"name\",\"api_token\":\"token\",\"base_url\":\"url\"}]",
                    ),
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let events: Vec<Event> = commands.events().collect();

        insta::assert_debug_snapshot!(
            events, @r#"
        [
            EvSettingsPreviousConnectionsScreen(
                SavePreviousConnectionsResult(
                    FileData(
                        "Just a fake test",
                    ),
                    false,
                ),
            ),
        ]
        "#);
    }
}
