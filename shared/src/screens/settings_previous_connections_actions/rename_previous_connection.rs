use crate::model::UserMessages;
use crate::screens::settings_previous_connections_actions::{
    Index, Name, SettingsPreviousConnectionsActionsScreenData,
};
use crate::{Effect, Event};
use crux_core::Command;
use log::Level;

pub fn rename_previous_connection(
    screen_data: &mut SettingsPreviousConnectionsActionsScreenData,
    user_messages: &mut UserMessages,
    index: Index,
    name: Name,
) -> Command<Effect, Event> {
    let Ok(index) = index.parse::<usize>() else {
        user_messages.add_message(
            "Internal error parsing, for previous connections index",
            Level::Error,
        );
        return crux_core::render::render();
    };

    let Some(previous_connection) = screen_data.previous_connections.get_mut(index) else {
        user_messages.add_message(
            "Internal error getting data, for previous connections",
            Level::Error,
        );
        return crux_core::render::render();
    };

    previous_connection.name = Some(name);
    screen_data.unsaved_data = true;
    crux_core::render::render()
}
