use crate::capabilities::persistent_storage::append_or_create_command;
use crate::capabilities::persistent_storage::PersistentStorageOutput;
use crate::internal_error_strings::{N056_USED, N057_USED};
use crate::model::{UserMessages, QUICK_ACTIONS_FILENAME};
use crate::screens::post_write_review::{PostWriteReviewScreenData, ScEvPostWriteReview};
use crate::Effect;
use crate::Event;
use crate::Event::EvPostWriteReviewScreen;
use crux_core::Command;
use log::trace;
use log::Level::Debug;
use serde_jsonlines::JsonLinesWriter;

pub fn save_quick_action(
    name: String,
    screen_date: &mut PostWriteReviewScreenData,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    trace!("Running save_quick_action event");

    screen_date.previous_write.name = name;

    let mut bytes: Vec<u8> = vec![];
    let mut writer = JsonLinesWriter::new(&mut bytes);
    let Ok(_) = writer.write(&screen_date.previous_write) else {
        user_messages.error_report_to_developer(N057_USED);
        return Command::done();
    };
    let Ok(_) = writer.flush() else {
        user_messages.error_report_to_developer(N056_USED);
        return Command::done();
    };

    user_messages.add_message(std::str::from_utf8(&bytes).unwrap_or(""), Debug);
    append_or_create_command(
        QUICK_ACTIONS_FILENAME.to_owned(),
        std::str::from_utf8(&bytes).unwrap_or("").to_string(),
    )
    .then_send(construct_response_event)
}

fn construct_response_event(body: PersistentStorageOutput) -> Event {
    EvPostWriteReviewScreen(ScEvPostWriteReview::SaveQuickActionResult(body))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model_ui::screens::post_write_review::UiPostWriteReviewScreenData;
    use crate::model_ui::user_messages::UiUserMessages;
    use crate::screens_common::test_common::test_shell;

    #[test]
    fn test_with_command() {
        let mut screen_data = PostWriteReviewScreenData {
            ..Default::default()
        };
        let mut user_messages: UserMessages = Default::default();

        let mut commands =
            save_quick_action("save name".to_owned(), &mut screen_data, &mut user_messages);
        let test: UiPostWriteReviewScreenData = (&screen_data).into();

        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(test, @r#"
        previous_write: "Preloads into input value screen with selected:\nOrg: fake_name\nBucket: \nMeasurement: \nWith preselected tags: \nAnd empty preselected fields: "
        eleven_data_points: []
        "#);

        let ui_user_message: UiUserMessages = user_messages.into();
        insta::assert_yaml_snapshot!(ui_user_message, @r#"
        messages:
          - message: "{\"name\":\"save name\",\"api_token\":\"test\",\"base_url\":\"http://notaurl.com/\",\"org\":{\"name\":\"fake_name\",\"id\":\"fake_id\"},\"bucket\":\"\",\"measurement\":\"\",\"tags\":{},\"fields\":{}}\n"
            level: Debug
            id: 1
        "#);

        // confirm the correct events are called
        let effects = commands.effects().fold(vec![], |mut acc, new| {
            acc.push(new);
            acc
        });

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            PersistentStorage(
                Request(
                    AppendOrCreate(
                        "quick_actions",
                        "{\"name\":\"save name\",\"api_token\":\"test\",\"base_url\":\"http://notaurl.com/\",\"org\":{\"name\":\"fake_name\",\"id\":\"fake_id\"},\"bucket\":\"\",\"measurement\":\"\",\"tags\":{},\"fields\":{}}\n",
                    ),
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let effects = commands.events().fold(vec![], |mut acc, new| {
            acc.push(new);
            acc
        });

        // the bellow we only care about "PostWriteReviewScreenEvents" confirming we got the .then_send correct
        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            EvPostWriteReviewScreen(
                SaveQuickActionResult(
                    FileData(
                        "Just a fake test",
                    ),
                ),
            ),
        ]
        "#);
    }
}
