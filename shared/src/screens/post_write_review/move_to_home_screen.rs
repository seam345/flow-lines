use crate::model::{Model, Screens};
use crate::{Effect, Event};
use crux_core::render::render;
use crux_core::Command;

pub(crate) fn move_to_home_screen(screen: &mut Model) -> Command<Effect, Event> {
    screen.screen = Screens::default();
    render()
}
