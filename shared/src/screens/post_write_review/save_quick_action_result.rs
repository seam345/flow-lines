use crate::model::Model;
use crate::screens::post_write_review::move_to_home_screen::move_to_home_screen;
use crate::{Effect, Event};
use crux_core::render::render;
use crux_core::Command;

#[allow(dead_code)]
pub fn save_quick_action_result(model: &mut Model) -> Command<Effect, Event> {
    move_to_home_screen(model);
    render()
}
