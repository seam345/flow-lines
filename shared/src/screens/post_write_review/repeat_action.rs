use crate::model::{Model, Screens};
use crate::screens::input_fields_and_tags::{
    validate_tag_value, InputFieldsAndTagsScreenData, LoadingResource, RemoteResource,
};
use crate::screens_common::model::user_input_or_multi_choice::{
    SelectedUserInputOrMultiChoice, UserInputOrMultiChoice, ValidatingStringPreTrimmed,
};
use crate::{Effect, Event};
use crux_core::render::render;
use crux_core::Command;
use log::{error, trace, Level};
use std::collections::BTreeMap;

pub fn repeat_action(model: &mut Model) -> Command<Effect, Event> {
    trace!("select org,bucket,measurement screen event: Validating model");

    let Screens::PostWriteReviewScreen(ref mut screen_model) = model.screen else {
        error!("Screen was in an unexpected state {:?}", model.screen);
        model
            .user_messages
            .add_message("Unrecoverable internal error state", Level::Error);
        return render();
    };

    trace!("Running select org,bucket,measurement event");
    let quick_action = screen_model.previous_write.clone();

    //tags and fields we are just adding without internet checking...
    // this means I either zip them together after a fetch or I don't know
    model.screen = Screens::InputFieldsTagsScreen(InputFieldsAndTagsScreenData {
        api_token: quick_action.api_token,
        base_url: quick_action.base_url,
        org: quick_action.org,
        bucket: quick_action.bucket,
        measurement: quick_action.measurement,
        available_tags: Default::default(), // todo load all tags
        tags: quick_action
            .tags
            .iter()
            .map(|(key, value)| {
                (
                    key,
                    match value {
                        None => UserInputOrMultiChoice {
                            options: RemoteResource::NotLoaded(LoadingResource {
                                retry_count: 0,
                                failure_reasons: vec![],
                                current_state: Default::default(),
                            }),
                            selected: None,
                            notify_user_of_mode_switch: None,
                            user_validating_function: validate_tag_value,
                        },
                        Some(inner) => UserInputOrMultiChoice {
                            options: RemoteResource::NotLoaded(LoadingResource {
                                retry_count: 0,
                                failure_reasons: vec![],
                                current_state: Default::default(),
                            }),
                            selected: Some(SelectedUserInputOrMultiChoice::User(
                                ValidatingStringPreTrimmed {
                                    inner: inner.clone(),
                                    validating_function: validate_tag_value,
                                },
                            )),
                            notify_user_of_mode_switch: None,
                            user_validating_function: validate_tag_value,
                        },
                    },
                )
            })
            .fold(BTreeMap::new(), |mut map, (key, value)| {
                map.insert(key.clone(), value);
                map
            }),
        new_tags: vec![],
        available_fields: Default::default(),
        fields: quick_action.fields,
        new_fields: vec![],
        line: "".to_string(),
        time: None,
        precision: Default::default(),
    });
    render()
}
