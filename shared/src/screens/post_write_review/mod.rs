mod move_to_home_screen;
mod repeat_action;
mod save_quick_action;
mod save_quick_action_result;

use crate::capabilities::persistent_storage::PersistentStorageOutput;
use crate::internal_error_strings::N059_USED;
use crate::model::{Model, Screens};
use crate::screens::initial::QuickAction;
use crate::screens::post_write_review::move_to_home_screen::move_to_home_screen;
use crate::screens::post_write_review::repeat_action::repeat_action;
use crate::screens::post_write_review::save_quick_action::save_quick_action;
use crate::screens::post_write_review::save_quick_action_result::save_quick_action_result;
use crate::{Effect, Event};
use crux_core::Command;
use log::{error, trace, Level};
use serde::{Deserialize, Serialize};
// my plan with this screen is display 10 previous inputs... maybe 5 either way if entered in the past
// have some way to get back to previous screen for repeat fast upload
// have a way to save a quick actions

#[derive(Clone, Debug)]
pub struct PostWriteReviewScreenData {
    /// hold stuff just sent to allow a fast repeat and to save for later
    pub previous_write: QuickAction,
    /// data point just sent plus 10 more
    pub eleven_data_points: Vec<String>,
}

// ============================================================================
// Screen event data.
// ============================================================================

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum ScEvPostWriteReview {
    MoveToHomeScreen,
    RepeatAction,
    SaveQuickAction(String),
    #[serde(skip)]
    SaveQuickActionResult(PersistentStorageOutput),
    EditQuickAction,
    GetElevenDataPoints,
    #[serde(skip)]
    GetElevenDataPointsResponse,
}

pub fn post_write_review_screen_events(
    screen_event: ScEvPostWriteReview,
    model: &mut Model,
) -> Command<Effect, Event> {
    trace!("post write review screen event: Validating model");

    let Screens::PostWriteReviewScreen(ref mut screen_data) = model.screen else {
        error!("Screen was in an unexpected state {:?}", model.screen);
        model.user_messages.add_message(N059_USED, Level::Error);
        return Command::done();
    };

    trace!("Running select org,bucket,measurement event");

    match screen_event {
        ScEvPostWriteReview::MoveToHomeScreen => move_to_home_screen(model),
        ScEvPostWriteReview::RepeatAction => repeat_action(model),
        ScEvPostWriteReview::SaveQuickAction(name) => {
            save_quick_action(name, screen_data, &mut model.user_messages)
        }
        ScEvPostWriteReview::GetElevenDataPoints => Command::done(),
        ScEvPostWriteReview::GetElevenDataPointsResponse => Command::done(),
        ScEvPostWriteReview::EditQuickAction => Command::done(),
        ScEvPostWriteReview::SaveQuickActionResult(_) => save_quick_action_result(model),
    }
}

#[cfg(test)]
use http_types::Url;

#[cfg(test)]
use crate::screens::select_org_bucket_measurements::influx_get_orgs_response::Org;

#[cfg(test)]
impl Default for PostWriteReviewScreenData {
    fn default() -> Self {
        PostWriteReviewScreenData {
            previous_write: QuickAction {
                name: "test".to_string(),
                api_token: "test".to_string(),
                base_url: Url::parse("http://notaurl.com").unwrap(),
                org: Org {
                    name: "fake_name".to_string(),
                    id: "fake_id".to_string(),
                },
                bucket: "".to_string(),
                measurement: "".to_string(),
                tags: Default::default(),
                fields: Default::default(),
            },
            eleven_data_points: vec![],
        }
    }
}
