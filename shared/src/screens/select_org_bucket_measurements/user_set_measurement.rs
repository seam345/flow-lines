use crate::screens_common::model::user_input_or_multi_choice::UserInputOrMultiChoice;
use crate::{Effect, Event};
use crux_core::Command;

pub fn user_set_measurement(
    measurements: &mut UserInputOrMultiChoice,
    user_measurement: String,
) -> Command<Effect, Event> {
    //todo change true to a user preference
    measurements.set_user_input(user_measurement, true);
    crux_core::render::render()
}
