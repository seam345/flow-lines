/// gets all Measurements in a bucket
/// used to make it easier for a user to correctly fill in the Measurement field
//https://docs.influxdata.com/flux/v0/stdlib/influxdata/influxdb/schema/measurements/
//
//import "influxdata/influxdb/schema"
//
// schema.measurements(bucket: "example-bucket")
use crate::model::{ExampleData, UserMessages};
use crate::screens::input_fields_and_tags::RemoteResource::Loaded;
use crate::screens::select_org_bucket_measurements::{
    validate_measurement_string, ScEvSelectOrgBucketMeasurement,
    SelectOrgBucketMeasurementScreenData,
};
use crate::screens_common::model::user_input_or_multi_choice::UserInputOrMultiChoice;
use crate::Event::EvSelectOrgBucketMeasurementScreen;
use crate::{Effect, Event};
use crux_core::Command;
use log::Level::Trace;
use log::{error, Level};

/// This requires a bucket to have already been selected
/// ATM all flux calls require org... I may be able to fix that based on different tokens
pub fn flux_get_buckets_existing_measurements(
    screen_data: &mut SelectOrgBucketMeasurementScreenData,
    user_messages: &mut UserMessages,
    example_data: &ExampleData,
) -> Command<Effect, Event> {
    user_messages.add_message("Called flux_get_buckets_existing_measurements", Trace);
    let Loaded(multi_buckets) = &screen_data.buckets else {
        error!("Bucket value was not loaded, unable to convert SelectOrgBucketMeasurement to InputFieldsAndTags");
        return crux_core::render::render();
    };
    match multi_buckets.selected_as_option() {
        None => {
            user_messages.add_message(
                "Unable to get measurements, no bucket selected! Please select a bucket",
                Level::Error,
            );
            crux_core::render::render()
        }
        Some(selected_bucket) => match example_data {
            ExampleData::Tutorial(_) | ExampleData::FakeData => {
                user_messages.add_message("in fake data/tutorial mode", Trace);
                screen_data.measurements = UserInputOrMultiChoice {
                    options: Loaded(vec![
                        "measurement1".to_owned(),
                        "measurement2".to_owned(),
                        "measurement3".to_owned(),
                    ]),
                    selected: None,
                    notify_user_of_mode_switch: None,
                    user_validating_function: validate_measurement_string,
                };
                crux_core::render::render()
            }
            ExampleData::Real => screen_data.flux_call(
                format!(
                    r#"
                                import "influxdata/influxdb/schema"
                                schema.measurements(bucket: "{}")
                            "#,
                    selected_bucket
                ),
                user_messages,
                construct_response_event,
            ),
        },
    }
}

//  F: FnOnce(crate::Result<Response<ExpectBody>>) -> Event + Send + 'static,
fn construct_response_event(body: crux_http::Result<crux_http::Response<String>>) -> Event {
    EvSelectOrgBucketMeasurementScreen(
        ScEvSelectOrgBucketMeasurement::FluxGetBucketsExistingMeasurementsResponse(body),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::{Model, Screens};
    use crate::screens::select_org_bucket_measurements::influx_get_orgs_response::Org;
    use crate::screens_common::model::multi_choice::MultiChoice;
    use crate::Counter;
    use crate::Effect;
    use crux_core::assert_effect;
    use crux_core::testing::AppTester;
    use crux_http::http::Url;

    #[test]
    fn unhappy_path_no_bucket() {
        let app = AppTester::<Counter>::default();

        let mut model = Model {
            screen: Screens::SelectOrgBucketMeasurementScreen(Box::from(
                SelectOrgBucketMeasurementScreenData {
                    base_url: Url::parse("http://notaurl.com").unwrap(),
                    orgs: Loaded(MultiChoice {
                        options: (vec![Org {
                            name: "org".to_owned(),
                            id: "2bao2".to_owned(),
                        }]),
                        selected: Some(0),
                    }),
                    ..Default::default()
                },
            )),
            ..Default::default()
        };

        let update = app.update(
            EvSelectOrgBucketMeasurementScreen(
                ScEvSelectOrgBucketMeasurement::FluxGetBucketsExistingMeasurements,
            ),
            &mut model,
        );

        // check that the app asked the shell to render
        assert_effect!(update, Effect::Render(_));
        for effect in update.effects {
            if matches!(effect, Effect::Http(_)) {
                panic!("update contains http call, when it shouldn't do");
            }
        }

        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(&mut model), @r#"
        ---
        screen:
          SelectMeasurementScreen:
            base_url: "http://notaurl.com/"
            orgs:
              Loaded:
                options:
                  - hidden_uid: "0"
                    user_facing_value: org
                selected_index: "0"
            buckets:
              Loading:
                retry_count: 0
                current_state: Initialised
            measurements:
              options: Loading
              selected: ~
              notify_user_of_mode_switch: ~
        user_messages:
          messages:
            - message: Called flux_get_buckets_existing_measurements
              level: Trace
              id: 1
        tutorial: ~
        "#);
    }

    /// atm flux call requires org, hopefully later we can fix that
    #[test]
    fn unhappy_path_no_org_with_bucket() {
        let app = AppTester::<Counter>::default();

        let mut model = Model {
            screen: Screens::SelectOrgBucketMeasurementScreen(Box::from(
                SelectOrgBucketMeasurementScreenData {
                    buckets: Loaded(MultiChoice {
                        options: vec!["bucket".to_owned()],
                        selected: Some(0),
                    }),
                    ..Default::default()
                },
            )),
            ..Default::default()
        };

        let update = app.update(
            EvSelectOrgBucketMeasurementScreen(
                ScEvSelectOrgBucketMeasurement::FluxGetBucketsExistingMeasurements,
            ),
            &mut model,
        );

        // check that the app asked the shell to render
        assert_effect!(update, Effect::Render(_));
        for effect in update.effects {
            if matches!(effect, Effect::Http(_)) {
                panic!("update contains http call, when it shouldn't do");
            }
        }
        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(&mut model), @r#"
        ---
        screen:
          SelectMeasurementScreen:
            base_url: "http://notaurl.com/"
            orgs:
              Loading:
                retry_count: 0
                current_state: Initialised
            buckets:
              Loaded:
                options:
                  - hidden_uid: "0"
                    user_facing_value: bucket
                selected_index: "0"
            measurements:
              options: Loading
              selected: ~
              notify_user_of_mode_switch: ~
        user_messages:
          messages:
            - message: Called flux_get_buckets_existing_measurements
              level: Trace
              id: 1
            - message: "Internal error 050, Please report to developer"
              level: Error
              id: 2
        tutorial: ~
        "#);
    }

    #[test]
    fn happy_path() {
        let app = AppTester::<Counter>::default();

        let mut model = Model {
            screen: Screens::SelectOrgBucketMeasurementScreen(Box::from(
                SelectOrgBucketMeasurementScreenData {
                    base_url: Url::parse("http://notaurl.com").unwrap(),
                    orgs: Loaded(MultiChoice {
                        options: vec![Org {
                            name: "org".to_owned(),
                            id: "2bao2".to_owned(),
                        }],
                        selected: Some(0),
                    }),
                    buckets: Loaded(MultiChoice {
                        options: vec!["bucket".to_owned()],
                        selected: Some(0),
                    }),
                    ..Default::default()
                },
            )),
            ..Default::default()
        };

        let _update = app.update(
            EvSelectOrgBucketMeasurementScreen(
                ScEvSelectOrgBucketMeasurement::FluxGetBucketsExistingMeasurements,
            ),
            &mut model,
        );

        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(&mut model), @r#"
        ---
        screen:
          SelectMeasurementScreen:
            base_url: "http://notaurl.com/"
            orgs:
              Loaded:
                options:
                  - hidden_uid: "0"
                    user_facing_value: org
                selected_index: "0"
            buckets:
              Loaded:
                options:
                  - hidden_uid: "0"
                    user_facing_value: bucket
                selected_index: "0"
            measurements:
              options: Loading
              selected: ~
              notify_user_of_mode_switch: ~
        user_messages:
          messages:
            - message: Called flux_get_buckets_existing_measurements
              level: Trace
              id: 1
        tutorial: ~
        "#);
    }
}
