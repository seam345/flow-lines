use crate::internal_error_strings::N001_USED;
use crate::model::{ExampleData, UserMessages};
use crate::screens::input_fields_and_tags::RemoteResource;
use crate::screens::select_org_bucket_measurements::{
    ScEvSelectOrgBucketMeasurement, SelectOrgBucketMeasurementScreenData,
};
use crate::screens_common::model::multi_choice::MultiChoice;
use crate::screens_common::requests::{influx_bucket_connection_request, BucketsResult};
use crate::Event::EvSelectOrgBucketMeasurementScreen;
use crate::{Effect, Event};
use crux_core::Command;
use crux_http::http::Url;
use log::error;

pub fn influx_get_buckets(
    screen_data: &mut SelectOrgBucketMeasurementScreenData,
    example_data: &ExampleData,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    match example_data {
        ExampleData::Tutorial(_) => {
            screen_data.buckets = RemoteResource::Loaded(MultiChoice {
                options: vec![
                    "First bucket".to_string(),
                    "second bucket".to_string(),
                    "Third bucket".to_string(),
                ],
                selected: Some(0),
            });
            crux_core::render::render()
        }
        ExampleData::FakeData => {
            screen_data.buckets = RemoteResource::Loaded(MultiChoice {
                options: vec![
                    "First bucket".to_string(),
                    "second bucket".to_string(),
                    "Third bucket".to_string(),
                ],
                selected: None,
            });
            crux_core::render::render()
        }
        ExampleData::Real => {
            let Ok(url) = Url::parse(&format!("{}api/v2/buckets", screen_data.base_url)) else {
                error!("Unable to parse base url");
                user_messages.error_report_to_developer(N001_USED);
                return crux_core::render::render();
            };
            influx_bucket_connection_request(url, &screen_data.api_token, construct_response_event)
        }
    }
}

//  F: FnOnce(crate::Result<Response<ExpectBody>>) -> Event + Send + 'static,
fn construct_response_event(body: crux_http::Result<crux_http::Response<BucketsResult>>) -> Event {
    EvSelectOrgBucketMeasurementScreen(ScEvSelectOrgBucketMeasurement::InfluxGetBucketsResponse(
        body,
    ))
}

// todo come up with unhappy tests malformed base url and such, ideally i should catch that earlier but good to check again
#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::{Model, Screens};
    use crate::screens::select_org_bucket_measurements::influx_get_orgs_response::{
        Org, OrgsResult,
    };
    use crate::{Counter, Effect};
    use crux_core::{assert_effect, testing::AppTester};
    use crux_http::http::Url;
    use crux_http::{
        protocol::{HttpRequest, HttpResponse},
        testing::ResponseBuilder,
    };

    #[test]
    fn confirm_happy_path() {
        // instantiate our app via the test harness, which gives us access to the model
        let app = AppTester::<Counter>::default();

        // set up our initial model
        let mut model = Model {
            screen: Screens::SelectOrgBucketMeasurementScreen(Box::from(
                SelectOrgBucketMeasurementScreenData {
                    base_url: Url::parse("http://notaurl.com").unwrap(),
                    api_token: "some random token".to_owned(),
                    ..Default::default()
                },
            )),
            ..Default::default()
        };

        // send a `Get` event to the app
        let mut update = app.update(
            EvSelectOrgBucketMeasurementScreen(ScEvSelectOrgBucketMeasurement::InfluxGetOrgs),
            &mut model,
        );

        assert_effect!(update, Effect::Http(_));
        let Effect::Http(ref mut request) = update.effects[0] else {
            panic!("Not HTTP Effect")
        };

        // check that the request is a GET to the correct URL
        let actual = &request.operation;
        let expected = &HttpRequest::get("http://notaurl.com/api/v2/orgs")
            .header("authorization", "Token some random token")
            .build();
        assert_eq!(actual, expected);

        // resolve the request with a simulated response from the web API
        let response = HttpResponse::ok()
            .body(
                r#"
                {
                  "links": {
                    "self": "/api/v2/orgs"
                  },
                  "orgs": [
                    {
                      "createdAt": "2022-07-17T23:00:30.778487Z",
                      "description": "Example InfluxDB organization",
                      "id": "INFLUX_ORG_ID",
                      "links": {
                        "buckets": "/api/v2/buckets?org=INFLUX_ORG",
                        "dashboards": "/api/v2/dashboards?org=INFLUX_ORG",
                        "labels": "/api/v2/orgs/INFLUX_ORG_ID/labels",
                        "logs": "/api/v2/orgs/INFLUX_ORG_ID/logs",
                        "members": "/api/v2/orgs/INFLUX_ORG_ID/members",
                        "owners": "/api/v2/orgs/INFLUX_ORG_ID/owners",
                        "secrets": "/api/v2/orgs/INFLUX_ORG_ID/secrets",
                        "self": "/api/v2/orgs/INFLUX_ORG_ID",
                        "tasks": "/api/v2/tasks?org=InfluxData"
                      },
                      "name": "INFLUX_ORG",
                      "updatedAt": "2022-07-17T23:00:30.778487Z"
                    }
                  ]
                }
            "#,
            )
            .build();
        let update = app
            .resolve(request, crux_http::protocol::HttpResult::Ok(response))
            .expect("an update");

        // check that the app emitted an (internal) event to update the model
        // let actual = update.events;
        let expected = ResponseBuilder::ok()
            .header("content-type", "application/oct et-stream")
            .body(OrgsResult {
                orgs: vec![Org {
                    name: "INFLUX_ORG".to_owned(),
                    id: "INFLUX_ORG_ID".to_owned(),
                }],
            })
            .build();
        let EvSelectOrgBucketMeasurementScreen(
            ScEvSelectOrgBucketMeasurement::InfluxGetOrgsResponse(actual),
        ) = &update.events[0]
        else {
            panic!("Not InfluxBucketsConnectionResponse")
        };
        assert_eq!(actual.clone().unwrap().body(), expected.body());
    }
}
