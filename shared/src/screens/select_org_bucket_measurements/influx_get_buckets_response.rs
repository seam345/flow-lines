use crate::internal_error_strings::N061_USED;
use crate::model::UserMessages;
use crate::screens::input_fields_and_tags::RemoteResource;
use crate::screens::select_org_bucket_measurements::SelectOrgBucketMeasurementScreenData;
use crate::screens_common::model::multi_choice::MultiChoice;
use crate::screens_common::requests::BucketsResult;
use crate::{Effect, Event};
use crux_core::Command;
use crux_http::Response;
use log::{error, trace, Level};

/// Deserialise bucket request from Influxdb2 https://docs.influxdata.com/influxdb/v2/api/#operation/GetBuckets
/// There are a lot more fields to the reply but we only car about the name to be added to the Flux query so that's all we are deserializing
// todo implement retry and example data
pub fn influx_get_buckets_response(
    screen_data: &mut SelectOrgBucketMeasurementScreenData,
    user_messages: &mut UserMessages,
    response: crux_http::Result<Response<BucketsResult>>,
) -> Command<Effect, Event> {
    trace!("influx_get_buckets_response called");
    match response {
        Ok(response) => {
            // ignoring status codes we will only get an Ok if an earlier block managed to parse the body to buckets result
            match response.body() {
                None => {
                    error!(
                        "How did I end up with no body in influx_buckets_connection_test_response"
                    );
                    user_messages.error_report_to_developer(N061_USED);
                    crux_core::render::render()
                }
                Some(response_body) => {
                    trace!("response body was {:?}", response_body);
                    let mut buckets = MultiChoice::default();
                    for bucket in &response_body.buckets {
                        buckets.push(bucket.name.clone());
                    }

                    screen_data.buckets = RemoteResource::Loaded(buckets);
                    crux_core::render::render()
                }
            }
        }
        Err(err) => {
            error!("Got error response: {:?}", err);
            user_messages.add_message(
                "Unable to contact Influx server! Please check Url and Token, Url should not end in a /",
                Level::Error,
            );
            crux_core::render::render()
        }
    }
}
