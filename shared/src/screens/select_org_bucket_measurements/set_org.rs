use crate::internal_error_strings::N046_USED;
use crate::model::UserMessages;
use crate::model_ui::screens_common::remote_multi_choice::multi_choice::UiStringIndex;
use crate::screens::input_fields_and_tags::RemoteResource;
use crate::screens::input_fields_and_tags::RemoteResource::Loaded;
use crate::screens::select_org_bucket_measurements::influx_get_orgs_response::Org;
use crate::screens_common::model::multi_choice::MultiChoice;
use crate::{internal_error_strings, Effect, Event};
use crux_core::Command;
use log::Level::Error;

pub fn set_org(
    // model: &mut SelectOrgBucketMeasurementScreenData,
    orgs: &mut RemoteResource<MultiChoice<Org>>,
    user_messages: &mut UserMessages,
    user_org_index: UiStringIndex,
) -> Command<Effect, Event> {
    let Loaded(multi_orgs) = orgs else {
        user_messages.error_report_to_developer(N046_USED);
        return crux_core::render::render();
    };

    let Ok(index) = user_org_index.to_bound_usize(multi_orgs.len()) else {
        user_messages.add_message(internal_error_strings::N014_USED, Error);
        return crux_core::render::render();
    };

    match multi_orgs.select_index(index) {
        Ok(_) => {
            // get buckets seem to not require orgs... idk why this may bite me, todo investigate
        }
        Err(_) => {
            user_messages.add_message(internal_error_strings::N015_USED, Error);
        }
    }
    crux_core::render::render()
}
