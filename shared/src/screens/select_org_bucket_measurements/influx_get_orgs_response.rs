use crate::model::UserMessages;
use crate::screens::input_fields_and_tags::RemoteResource::Loaded;
use crate::screens::input_fields_and_tags::{LoadingState, RemoteResource};
use crate::screens::select_org_bucket_measurements::SelectOrgBucketMeasurementScreenData;
use crate::screens_common::model::multi_choice::MultiChoice;
use crate::{internal_error_strings, Effect, Event};
use crux_core::Command;
use crux_http::Response;
use log::Level::Error;
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};

/// Deserialise bucket request from Influxdb2 https://docs.influxdata.com/influxdb/v2/api/#operation/GetBuckets
/// There are a lot more fields to the reply but we only car about the name to be added to the Flux query so that's all we are deserializing
#[derive(Deserialize, Clone, Debug, PartialEq)]
pub struct OrgsResult {
    pub(crate) orgs: Vec<Org>,
}

#[derive(Deserialize, Serialize, Clone, Debug, PartialEq, Default)]
pub struct Org {
    pub(crate) name: String,
    pub(crate) id: String,
    // pub(crate) description: String,
}

impl Display for Org {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)
    }
}

pub fn influx_get_orgs_response(
    screen_data: &mut SelectOrgBucketMeasurementScreenData,
    response: crux_http::Result<Response<OrgsResult>>,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    match response {
        Ok(response) => match response.body() {
            None => {
                // todo update
                Command::done()
            }
            Some(response_body) => {
                let mut multi_orgs = MultiChoice {
                    options: vec![],
                    selected: None,
                };

                for bucket in &response_body.orgs {
                    multi_orgs.push(bucket.clone());
                }

                if multi_orgs.len() == 1 {
                    match multi_orgs.select_index(0) {
                        Ok(_) => {}
                        Err(_) => {
                            user_messages.add_message(internal_error_strings::N011_USED, Error)
                        }
                    }
                }

                screen_data.orgs = Loaded(multi_orgs);
                crux_core::render::render()
            }
        },
        Err(_) => {
            user_messages.add_message(
                "Bad response from influx server getting Orgs, Please try again",
                Error,
            );
            match &mut screen_data.orgs {
                RemoteResource::NotLoaded(not_loaded) => {
                    not_loaded.current_state = LoadingState::AwaitingUserInteraction;
                }
                RemoteResource::NonExistent => {}
                Loaded(_) => {}
            }
            crux_core::render::render()
        }
    }
}

/*#[cfg(test)]
mod tests {
    use super::*;
    use crate::Counter;
    use crate::Effect;
    use crate::Event;
    use crux_core::assert_effect;
    use crux_core::testing::AppTester;
    use crux_http::testing::ResponseBuilder;

    #[test]
    fn return_with_buckets() {
        let app = AppTester::<Counter>::default();

        let mut model = Model {
            ..Default::default()
        };

        let update = app.update(
            Event::InfluxBucketsConnectionTestResponse(Ok(ResponseBuilder::ok()
                .body(OrgsResult {
                    buckets: vec![
                        Org {
                            name: "First bucket".to_string(),
                        },
                        Org {
                            name: "second bucket".to_string(),
                        },
                        Org {
                            name: "Third bucket".to_string(),
                        },
                    ],
                })
                .build())),
            &mut model,
        );

        // check that the app asked the shell to render
        assert_effect!(update, Effect::Render(_));

        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(&mut model), @r###"
        ---
        api_token: ""
        base_url: ""
        failed_to_contact_influx: false
        org: ""
        available_measurements: []
        available_tags: {}
        tags: {}
        available_fields: []
        fields: {}
        line: ""
        precision: Nanoseconds
        available_buckets:
          - First bucket
          - second bucket
          - Third bucket
        bucket: ""
        expected_line: ", "
        "###);
    }
}
*/
