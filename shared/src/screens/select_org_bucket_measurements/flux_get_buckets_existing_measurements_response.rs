use crate::screens::input_fields_and_tags::RemoteResource;
use crate::screens::select_org_bucket_measurements::{
    validate_measurement_string, SelectOrgBucketMeasurementScreenData,
};
use crate::screens_common::model::user_input_or_multi_choice::UserInputOrMultiChoice;
use crate::{Effect, Event};
use crux_core::Command;
use crux_http::Response;
use csv::ReaderBuilder;

#[derive(Debug, serde::Deserialize)]
struct Record {
    _value: String,
}

/// Result of the call to getting all Measurements in a bucket
/// used to make it easier for a user to correctly fill in the Measurement field
///
/// https://docs.influxdata.com/flux/v0/stdlib/influxdata/influxdb/schema/measurements/
///
/// import "influxdata/influxdb/schema"
///
/// schema.measurements(bucket: "example-bucket")
pub fn flux_get_buckets_existing_measurements_response(
    model: &mut SelectOrgBucketMeasurementScreenData,
    response: crux_http::Result<Response<String>>,
) -> Command<Effect, Event> {
    // todo remove unwraps
    let binding = response.unwrap();
    let mut rdr = ReaderBuilder::new().from_reader(binding.body().unwrap().as_bytes());
    model.measurements = UserInputOrMultiChoice {
        options: RemoteResource::Loaded(vec![]),
        selected: None,
        notify_user_of_mode_switch: None,
        user_validating_function: validate_measurement_string,
    };
    for result in rdr.deserialize() {
        let record: Record = result.unwrap();
        println!("{:?}", record);
        model.measurements.push(record._value);
    }
    crux_core::render::render()
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use super::*;
    use crate::model::{Model, Screens};
    use crate::screens::input_fields_and_tags::RemoteResource::Loaded;
    use crate::screens::select_org_bucket_measurements::influx_get_orgs_response::Org;
    use crate::screens::select_org_bucket_measurements::ScEvSelectOrgBucketMeasurement;
    use crate::screens_common::model::multi_choice::MultiChoice;
    use crate::Event;
    use crate::{Counter, Effect};
    use crux_core::assert_effect;
    use crux_core::testing::AppTester;
    use crux_http::http::Url;
    use crux_http::testing::ResponseBuilder;

    #[test]
    fn happy_path() {
        let app = AppTester::<Counter>::default();

        let mut model = Model {
            screen: Screens::SelectOrgBucketMeasurementScreen(Box::from(
                SelectOrgBucketMeasurementScreenData {
                    base_url: Url::parse("http://notaurl.com").unwrap(),
                    orgs: Loaded(MultiChoice {
                        options: vec![Org {
                            name: "org".to_owned(),
                            id: "2bao2".to_owned(),
                        }],
                        selected: Some(0),
                    }),
                    buckets: Loaded(MultiChoice {
                        options: vec!["bucket".to_owned()],
                        selected: Some(0),
                    }),
                    ..Default::default()
                },
            )),
            ..Default::default()
        };

        let update = app.update(
            Event::EvSelectOrgBucketMeasurementScreen(ScEvSelectOrgBucketMeasurement::FluxGetBucketsExistingMeasurementsResponse(Ok(ResponseBuilder::ok()
                .body(",result,table,_value\n,_result,0,Push-up\n,_result,0,test\n,_result,0,test_crux".to_owned())
                .build()))),
            &mut model,
        );

        // check that the app asked the shell to render
        assert_effect!(update, Effect::Render(_));

        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(&mut model), @r#"
        screen:
          SelectMeasurementScreen:
            base_url: "http://notaurl.com/"
            orgs:
              Loaded:
                options:
                  - hidden_uid: "0"
                    user_facing_value: org
                selected_index: "0"
            buckets:
              Loaded:
                options:
                  - hidden_uid: "0"
                    user_facing_value: bucket
                selected_index: "0"
            measurements:
              options:
                Loaded:
                  - hidden_uid: "0"
                    user_facing_value: Push-up
                  - hidden_uid: "1"
                    user_facing_value: test
                  - hidden_uid: "2"
                    user_facing_value: test_crux
              selected: ~
              notify_user_of_mode_switch: ~
        user_messages:
          messages: []
        tutorial: ~
        "#);
    }
}
