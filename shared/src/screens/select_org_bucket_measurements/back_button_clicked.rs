use crate::model::Model;
use crate::{Effect, Event};
use crux_core::Command;

pub fn back_button_clicked(model: &mut Model) -> Command<Effect, Event> {
    *model = Model::default(); // going back to initial screen no need to do anything fancy
    crux_core::render::render()
}
