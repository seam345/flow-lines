use crate::internal_error_strings::N010_USED;
use crate::model::Screens::InputFieldsTagsScreen;
use crate::model::{ExampleData, Screens, UserMessages};
use crate::screens::input_fields_and_tags::InputFieldsAndTagsScreenData;
use crate::screens::select_org_bucket_measurements::influx_get_orgs_response::Org;
use crate::{Effect, Event};
use crux_core::Command;
use crux_http::http::Url;
use log::Level::Info;
use log::{error, trace, Level};

pub fn move_to_next_screen(
    screen: &mut Screens,
    user_messages: &mut UserMessages,
    example_data: &ExampleData,
) -> Command<Effect, Event> {
    match example_data {
        ExampleData::Tutorial(_) => {
            // it was just simpler to make a new one
            *screen = Screens::InputFieldsTagsScreen(InputFieldsAndTagsScreenData {
                api_token: "Token".to_string(),
                base_url: Url::parse("http://example.com").unwrap(),
                org: Org {
                    name: "tutorial_org".to_string(),
                    id: "blah".to_string(),
                },
                bucket: "tutorial_bucket".to_string(),
                measurement: "tutorial_measurement".to_string(),
                available_tags: Default::default(),
                tags: Default::default(),
                new_tags: vec![],
                available_fields: Default::default(),
                fields: Default::default(),
                new_fields: vec![],
                line: "".to_string(),
                time: None,
                precision: Default::default(),
            });
            crux_core::render::render()
        }
        ExampleData::FakeData | ExampleData::Real => {
            let Screens::SelectOrgBucketMeasurementScreen(ref mut screen_data) = screen else {
                error!("Screen was in an unexpected state {:?}", screen);
                user_messages.add_message(N010_USED, Level::Error);
                return crux_core::render::render();
            };
            trace!("Convert screen into InputFieldTags");

            match InputFieldsAndTagsScreenData::try_from(screen_data) {
                Ok(next_screen) => {
                    // =================================================================================
                    // Change screen
                    // =================================================================================
                    *screen = InputFieldsTagsScreen(next_screen);
                }
                Err(reason) => {
                    user_messages.add_message(reason, Info);
                }
            }
            crux_core::render::render()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::{Model, Screens};
    use crate::screens::input_fields_and_tags::validate_tag_value;
    use crate::screens::input_fields_and_tags::RemoteResource::Loaded;
    use crate::screens::select_org_bucket_measurements::influx_get_orgs_response::Org;
    use crate::screens::select_org_bucket_measurements::SelectOrgBucketMeasurementScreenData;
    use crate::screens_common::model::multi_choice::MultiChoice;
    use crate::screens_common::model::user_input_or_multi_choice::{
        SelectedUserInputOrMultiChoice, UserInputOrMultiChoice, ValidatingStringPreTrimmed,
    };
    use crate::Counter;
    use crux_core::testing::AppTester;
    use crux_http::http::Url;

    #[test]
    fn user_supplied_measurement() {
        // instantiate our app via the test harness, which gives us access to the model
        let app = AppTester::<Counter>::default();

        // set up our initial model
        let mut screen = Screens::SelectOrgBucketMeasurementScreen(Box::from(
            SelectOrgBucketMeasurementScreenData {
                base_url: Url::parse("http://notaurl.com").unwrap(),
                api_token: "some random token".to_owned(),
                orgs: Loaded(MultiChoice {
                    options: (vec![Org {
                        name: "".to_string(),
                        id: "".to_string(),
                    }]),
                    selected: Some(0),
                }),
                buckets: Loaded(MultiChoice {
                    options: vec!["bucket1".to_owned()],
                    selected: Some(0),
                }),
                measurements: UserInputOrMultiChoice {
                    options: Loaded(vec!["a value".to_owned()]),
                    selected: Some(SelectedUserInputOrMultiChoice::User(
                        ValidatingStringPreTrimmed {
                            inner: "valid".to_string(),
                            validating_function: validate_tag_value,
                        },
                    )),
                    notify_user_of_mode_switch: None,
                    user_validating_function: validate_tag_value,
                },
                ..Default::default()
            },
        ));

        let mut user_messages = UserMessages::default();

        move_to_next_screen(&mut screen, &mut user_messages, &ExampleData::Real);

        let model = Model {
            screen,
            user_messages,
            ..Default::default()
        };
        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(& model), @r#"
        ---
        screen:
          InputFieldsTagsScreen:
            base_url: "http://notaurl.com/"
            org: ""
            bucket: bucket1
            measurement: valid
            available_tags:
              Loading:
                retry_count: 0
                current_state: Initialised
            tags: {}
            new_tags: []
            available_fields:
              Loading:
                retry_count: 0
                current_state: Initialised
            fields: {}
            new_fields: []
            line: ""
            ui_time: ~
            precision: Nanoseconds
            expected_line: "valid  "
        user_messages:
          messages: []
        tutorial: ~
        "#);

        // assert_effect!(update, Effect::Http(_));
        // let Effect::Http(ref mut request) = update.effects[0] else {
        //     panic!("Not HTTP Effect")
        // };

        // check that the request is a GET to the correct URL
        // let actual = &request.operation;
        // let expected = &HttpRequest::get("http://notaurl.com/api/v2/orgs")
        //     .header("authorization", "Token some random token")
        //     .build();
        // assert_eq!(actual, expected);

        // resolve the request with a simulated response from the web API
        /*  let response = HttpResponse::ok()
            .body(
                r#"
                {
                  "links": {
                    "self": "/api/v2/orgs"
                  },
                  "orgs": [
                    {
                      "createdAt": "2022-07-17T23:00:30.778487Z",
                      "description": "Example InfluxDB organization",
                      "id": "INFLUX_ORG_ID",
                      "links": {
                        "buckets": "/api/v2/buckets?org=INFLUX_ORG",
                        "dashboards": "/api/v2/dashboards?org=INFLUX_ORG",
                        "labels": "/api/v2/orgs/INFLUX_ORG_ID/labels",
                        "logs": "/api/v2/orgs/INFLUX_ORG_ID/logs",
                        "members": "/api/v2/orgs/INFLUX_ORG_ID/members",
                        "owners": "/api/v2/orgs/INFLUX_ORG_ID/owners",
                        "secrets": "/api/v2/orgs/INFLUX_ORG_ID/secrets",
                        "self": "/api/v2/orgs/INFLUX_ORG_ID",
                        "tasks": "/api/v2/tasks?org=InfluxData"
                      },
                      "name": "INFLUX_ORG",
                      "updatedAt": "2022-07-17T23:00:30.778487Z"
                    }
                  ]
                }
            "#,
            )
            .build();
        let update = app.resolve(request, response).expect("an update");

        // check that the app emitted an (internal) event to update the model
        // let actual = update.events;
        let expected = ResponseBuilder::ok()
            .header("content-type", "application/oct et-stream")
            .body(OrgsResult {
                orgs: vec![Org {
                    name: "INFLUX_ORG".to_owned(),
                    id: "INFLUX_ORG_ID".to_owned(),
                }],
            })
            .build();
        let Event::SelectOrgBucketMeasurementScreenEvents(
            SelectOrgBucketMeasurementScreenEventsData::InfluxGetOrgsResponse(actual),
        ) = &update.events[0]
        else {
            panic!("Not InfluxBucketsConnectionResponse")
        };
        assert_eq!(actual.clone().unwrap().body(), expected.body());*/
    }
}
