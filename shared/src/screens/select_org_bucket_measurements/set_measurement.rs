use crate::internal_error_strings::{N045_USED, N048_USED};
use crate::model::UserMessages;
use crate::model_ui::screens_common::remote_multi_choice::multi_choice::UiStringIndex;
use crate::screens::select_org_bucket_measurements::SelectOrgBucketMeasurementScreenData;
use crate::screens_common::model::user_input_or_multi_choice::SelectStringIndexError;
use crate::{Effect, Event};
use crux_core::Command;
use log::Level;

pub fn set_measurement(
    screen_model: &mut SelectOrgBucketMeasurementScreenData,
    user_messages: &mut UserMessages,
    user_measurement_index: UiStringIndex,
) -> Command<Effect, Event> {
    //todo change true to a user preference
    match screen_model
        .measurements
        .select_string_index(user_measurement_index, true)
    {
        Ok(_) => {}
        Err(SelectStringIndexError::UiStringIndexErrors(_)) => {
            user_messages.add_message(crate::internal_error_strings::N007_USED, Level::Error);
        }
        Err(SelectStringIndexError::Other(_)) => {
            user_messages.add_message(crate::internal_error_strings::N009_USED, Level::Error);
        }
        Err(SelectStringIndexError::NotLoadedError(_)) => {
            user_messages.error_report_to_developer(N045_USED);
        }
        Err(SelectStringIndexError::SelectIndexError(_)) => {
            user_messages.error_report_to_developer(N048_USED);
        }
    }
    crux_core::render::render()
}
