use crate::internal_error_strings::N043_USED;
use crate::model::{ExampleData, UserMessages};
use crate::model_ui::screens_common::remote_multi_choice::multi_choice::UiStringIndex;
use crate::screens::input_fields_and_tags::RemoteResource::Loaded;
use crate::screens::select_org_bucket_measurements::flux_get_buckets_existing_measurements::flux_get_buckets_existing_measurements;
use crate::screens::select_org_bucket_measurements::SelectOrgBucketMeasurementScreenData;
use crate::{internal_error_strings, Effect, Event};
use crux_core::Command;
use log::Level::Error;

pub fn set_bucket(
    screen_data: &mut SelectOrgBucketMeasurementScreenData,
    user_messages: &mut UserMessages,
    user_bucket_index: UiStringIndex,
    example_data: &ExampleData,
) -> Command<Effect, Event> {
    let Loaded(multi_buckets) = &mut screen_data.buckets else {
        user_messages.error_report_to_developer(N043_USED);
        return crux_core::render::render();
    };

    let Ok(index) = user_bucket_index.to_bound_usize(multi_buckets.len()) else {
        user_messages.add_message(internal_error_strings::N012_USED, Error);
        return crux_core::render::render();
    };

    match multi_buckets.select_index(index) {
        Ok(_) => flux_get_buckets_existing_measurements(screen_data, user_messages, example_data),
        Err(_) => {
            user_messages.add_message(internal_error_strings::N013_USED, Error);
            crux_core::render::render()
        }
    }
}
