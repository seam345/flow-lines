use crate::internal_error_strings::N016_USED;
use crate::model::UserMessages;
use crate::screens::select_org_bucket_measurements::{
    ConfirmUndo, DontShowDialogAgain, SelectOrgBucketMeasurementScreenData,
};
use crate::{Effect, Event};
use crux_core::Command;
use log::Level::Error;

pub fn confirm_input_change_on_measurements(
    model: &mut SelectOrgBucketMeasurementScreenData,
    user_messages: &mut UserMessages,
    confirm_undo: ConfirmUndo, // todo maybe rename to proceed/cancel
    dont_show_dialog_again: DontShowDialogAgain,
) -> Command<Effect, Event> {
    // =============================================================================================
    // update selected value based on answer
    // =============================================================================================
    let Some(notify_value) = &model.measurements.notify_user_of_mode_switch else {
        user_messages.add_message(N016_USED, Error);
        return crux_core::render::render();
    };
    if let ConfirmUndo::Confirm = confirm_undo {
        model.measurements.selected = Some(notify_value.1.clone());
    }

    // =============================================================================================
    // dismiss notification/dialog
    // =============================================================================================
    model.measurements.notify_user_of_mode_switch = None;

    // =============================================================================================
    // update user preferences
    // =============================================================================================
    if let DontShowDialogAgain::SavePreference = dont_show_dialog_again {
        //todo save preference
    }
    crux_core::render::render()
}
