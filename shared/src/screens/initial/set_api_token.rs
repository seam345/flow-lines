use crate::screens::initial::InitialScreenData;
use crate::{Effect, Event};
use crux_core::Command;

pub fn set_api_token(
    screen_data: &mut InitialScreenData,
    api_token: String,
) -> Command<Effect, Event> {
    screen_data.api_token = Some(api_token);
    crux_core::render::render()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::screens::initial::InitialScreenData;
    use crate::screens_common::test_common::test_shell;

    #[test]
    fn test_set_api_token() {
        let mut screen_data = InitialScreenData {
            ..Default::default()
        };
        let mut commands = set_api_token(&mut screen_data, "new api token".to_owned());

        // =========================================================================================
        // check that the view has been updated correctly
        // =========================================================================================
        insta::assert_debug_snapshot!(screen_data, @r#"
        InitialScreenData {
            api_token: Some(
                "new api token",
            ),
            base_url: None,
            quick_actions: NotLoaded(
                LoadingResource {
                    retry_count: 0,
                    failure_reasons: [],
                    current_state: Initialised,
                },
            ),
            previous_connections: NotLoaded(
                LoadingResource {
                    retry_count: 0,
                    failure_reasons: [],
                    current_state: Initialised,
                },
            ),
            failed_to_contact_influx: false,
        }
        "#);

        // =========================================================================================
        // confirm the correct events are called
        // =========================================================================================
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            Render(
                Request(
                    RenderOperation,
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let events: Vec<Event> = commands.events().collect();

        insta::assert_debug_snapshot!(
            events, @"[]");
    }
}
