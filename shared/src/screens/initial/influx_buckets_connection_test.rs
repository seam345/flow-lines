use crate::internal_error_strings::N018_USED;
use crate::model::{ExampleData, Screens, UserMessages};
use crate::screens::initial::{InitialScreenData, ScEvInitial};
use crate::screens::input_fields_and_tags::RemoteResource;
use crate::screens::select_org_bucket_measurements::SelectOrgBucketMeasurementScreenData;
use crate::screens_common::model::multi_choice::MultiChoice;
use crate::screens_common::model::previous_connection::PreviousConnection;
use crate::screens_common::requests::{influx_bucket_connection_request, BucketsResult};
use crate::Event::EvInitialScreen;
use crate::{Effect, Event};
use crux_core::Command;
use crux_http::http::Url;
use log::Level::Error;
use log::{error, trace, warn, Level};

pub fn influx_bucket_connection_test(
    screen: &mut Screens,
    user_messages: &mut UserMessages,
    example_data: &mut ExampleData,
) -> Command<Effect, Event> {
    trace!("Initial screen event: Validating model");

    let Screens::InitialScreen(ref mut screen_data) = screen else {
        error!("Screen was in an unexpected state {:?}", screen);
        user_messages.add_message(N018_USED, Error);
        return crux_core::render::render();
    };

    trace!("Confirming user has entered a base url");
    let Some(base_url) = &screen_data.base_url else {
        warn!("User hasn't entered a base url");
        user_messages.add_message(
            "No base url entered. Please enter a url for the influx instance",
            Level::Warn,
        );
        return crux_core::render::render();
    };
    trace!("Validating user entered base url");
    let Ok(url) = Url::parse(&format!("{}/api/v2/buckets", base_url)) else {
        error!("Unable to parse base url");
        user_messages.add_message(
            "Unable to generate a valid url from base url. Please check and amend base url",
            Error,
        );
        return crux_core::render::render();
    };
    trace!("Confirming user has entered an api token");
    let Some(api_token) = &screen_data.api_token else {
        warn!("User hasn't entered an api token");
        user_messages.add_message(
            "No api token entered. Please enter a url for the influx instance",
            Level::Warn,
        );
        return crux_core::render::render();
    };

    match (&example_data, base_url.as_str(), api_token.as_str()) {
        // =========================================================================================
        // second click in tutorial
        // =========================================================================================
        (ExampleData::Tutorial(_), "http://example.com", "tutorial") => {
            // not a fan of skipping the repose code, todo work out how i could call it

            let Ok(select_org_bucket_measurement): Result<SelectOrgBucketMeasurementScreenData, _> =
                screen_data.try_into()
            else {
                error!("cannot convert to next screen");
                // todo need some user message
                return crux_core::render::render();
            };

            *screen =
                Screens::SelectOrgBucketMeasurementScreen(Box::from(select_org_bucket_measurement));
            crux_core::render::render()
        }
        // =========================================================================================
        // first click enter tutorial mode
        // =========================================================================================
        (ExampleData::Real, "http://example.com", "tutorial") => {
            user_messages.add_message("Entered tutorial mode", Level::Info);

            *example_data = ExampleData::Tutorial(0);
            *screen_data = generate_fake_data();
            crux_core::render::render()
        }
        // =========================================================================================
        // second click with fake data
        // =========================================================================================
        (ExampleData::FakeData, "http://example.com", _) => {
            //     not a fan of skipping the repose code, todo work out how i could call it
            let Ok(select_org_bucket_measurement): Result<SelectOrgBucketMeasurementScreenData, _> =
                screen_data.try_into()
            else {
                error!("cannot convert to next screen");
                // todo need some user message
                return crux_core::render::render();
            };

            *screen =
                Screens::SelectOrgBucketMeasurementScreen(Box::from(select_org_bucket_measurement));
            crux_core::render::render()
        }
        // =========================================================================================
        // first click enter fake data
        // =========================================================================================
        (ExampleData::Real, "http://example.com", _) => {
            user_messages.add_message("Entered fake data mode2", Level::Info);

            *example_data = ExampleData::FakeData;
            *screen_data = generate_fake_data();
            crux_core::render::render()
        }
        // =========================================================================================
        // real data
        // =========================================================================================
        (ExampleData::Real, _, _) => {
            influx_bucket_connection_request(url, api_token, construct_response_event)
        }
        // =========================================================================================
        // unsure what todo with bellow, the user has changed the url after entering into tutorial/ fake data mode.. maybe they intend to leave the mode
        // =========================================================================================
        (ExampleData::Tutorial(_), _, _) => {
            user_messages.add_message("unimplemented what todo if details change on tutorial mode, please restart the app", Error);
            crux_core::render::render()
        }
        (ExampleData::FakeData, _, _) => {
            user_messages.add_message("unimplemented what todo if details change on fake date mode, please restart the app", Error);
            crux_core::render::render()
        }
    }
}

fn generate_fake_data() -> InitialScreenData {
    InitialScreenData {
        api_token: Some("tutorial".to_owned()),
        base_url: Some("http://example.com".to_owned()),
        quick_actions: RemoteResource::Loaded(MultiChoice {
            options: vec![],
            selected: None,
        }),
        previous_connections: RemoteResource::Loaded(MultiChoice {
            options: vec![
                PreviousConnection {
                    name: Some("Fake Production".to_owned()),
                    api_token: "http://example.com".to_string(),
                    base_url: "tutorial".to_string(),
                },
                PreviousConnection {
                    name: Some("Fake staging".to_owned()),
                    api_token: "http://example.com".to_string(),
                    base_url: "tutorial2".to_string(),
                },
            ],
            selected: None,
        }),
        failed_to_contact_influx: false,
    }
}

//  F: FnOnce(crate::Result<Response<ExpectBody>>) -> Event + Send + 'static,
fn construct_response_event(body: crux_http::Result<crux_http::Response<BucketsResult>>) -> Event {
    EvInitialScreen(ScEvInitial::InfluxBucketsConnectionTestResponse(body))
}

// todo come up with unhappy tests malformed base url and such, ideally i should catch that earlier but good to check again
#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::{Model, Screens};
    use crate::screens_common::test_common::test_shell;

    #[test]
    fn confirm_happy_path() {
        // set up our initial model
        let mut model = Model {
            screen: Screens::InitialScreen(InitialScreenData {
                base_url: Some("http://aurl.com".to_owned()),
                api_token: Some("some random token".to_owned()),
                ..Default::default()
            }),
            ..Default::default()
        };

        let mut commands = influx_bucket_connection_test(
            &mut model.screen,
            &mut model.user_messages,
            &mut model.example_data,
        );

        // check that the view has been updated correctly
        insta::assert_debug_snapshot!(model, @r#"
        Model {
            screen: InitialScreen(
                InitialScreenData {
                    api_token: Some(
                        "some random token",
                    ),
                    base_url: Some(
                        "http://aurl.com",
                    ),
                    quick_actions: NotLoaded(
                        LoadingResource {
                            retry_count: 0,
                            failure_reasons: [],
                            current_state: Initialised,
                        },
                    ),
                    previous_connections: NotLoaded(
                        LoadingResource {
                            retry_count: 0,
                            failure_reasons: [],
                            current_state: Initialised,
                        },
                    ),
                    failed_to_contact_influx: false,
                },
            ),
            user_messages: UserMessages {
                messages: [],
                current_id: 0,
            },
            example_data: Real,
        }
        "#);

        // -----------------------------------------------------------------------------------------
        // confirm the correct events are called
        // -----------------------------------------------------------------------------------------
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            Http(
                Request(
                    HttpRequest {
                        method: "GET",
                        url: "http://aurl.com/api/v2/buckets",
                        headers: [
                            HttpHeader {
                                name: "authorization",
                                value: "Token some random token",
                            },
                        ],
                        body: "",
                    },
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let effects: Vec<Event> = commands.events().collect();

        // the bellow we only care about "PostWriteReviewScreenEvents" confirming we got the .then_send correct
        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            EvInitialScreen(
                InfluxBucketsConnectionTestResponse(
                    Err(
                        Url(
                            "Just a fake test",
                        ),
                    ),
                ),
            ),
        ]
        "#);
    }

    #[test]
    fn unhappy_path_bad_url() {
        // set up our initial model
        let mut model = Model {
            screen: Screens::InitialScreen(InitialScreenData {
                base_url: Some("".to_owned()),
                api_token: Some("some random token".to_owned()),
                ..Default::default()
            }),
            ..Default::default()
        };

        let mut commands = influx_bucket_connection_test(
            &mut model.screen,
            &mut model.user_messages,
            &mut model.example_data,
        );

        // check that the view has been updated correctly
        insta::assert_debug_snapshot!(model, @r#"
        Model {
            screen: InitialScreen(
                InitialScreenData {
                    api_token: Some(
                        "some random token",
                    ),
                    base_url: Some(
                        "",
                    ),
                    quick_actions: NotLoaded(
                        LoadingResource {
                            retry_count: 0,
                            failure_reasons: [],
                            current_state: Initialised,
                        },
                    ),
                    previous_connections: NotLoaded(
                        LoadingResource {
                            retry_count: 0,
                            failure_reasons: [],
                            current_state: Initialised,
                        },
                    ),
                    failed_to_contact_influx: false,
                },
            ),
            user_messages: UserMessages {
                messages: [
                    UserMessage {
                        message: "Unable to generate a valid url from base url. Please check and amend base url",
                        level: Error,
                        id: 1,
                    },
                ],
                current_id: 1,
            },
            example_data: Real,
        }
        "#);

        // -----------------------------------------------------------------------------------------
        // confirm the correct events are called
        // -----------------------------------------------------------------------------------------
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            Render(
                Request(
                    RenderOperation,
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let effects: Vec<Event> = commands.events().collect();

        // the bellow we only care about "PostWriteReviewScreenEvents" confirming we got the .then_send correct
        insta::assert_debug_snapshot!(
            effects, @"[]");
    }

    #[test]
    fn enter_example() {
        // set up our initial model
        let mut model = Model {
            screen: Screens::InitialScreen(InitialScreenData {
                base_url: Some("http://example.com".to_owned()),
                api_token: Some("some random token".to_owned()),
                ..Default::default()
            }),
            ..Default::default()
        };

        let mut commands = influx_bucket_connection_test(
            &mut model.screen,
            &mut model.user_messages,
            &mut model.example_data,
        );

        // check that the view has been updated correctly
        insta::assert_debug_snapshot!(model, @r#"
        Model {
            screen: InitialScreen(
                InitialScreenData {
                    api_token: Some(
                        "tutorial",
                    ),
                    base_url: Some(
                        "http://example.com",
                    ),
                    quick_actions: Loaded(
                        MultiChoice {
                            options: [],
                            selected: None,
                        },
                    ),
                    previous_connections: Loaded(
                        MultiChoice {
                            options: [
                                PreviousConnection {
                                    name: Some(
                                        "Fake Production",
                                    ),
                                    api_token: "http://example.com",
                                    base_url: "tutorial",
                                },
                                PreviousConnection {
                                    name: Some(
                                        "Fake staging",
                                    ),
                                    api_token: "http://example.com",
                                    base_url: "tutorial2",
                                },
                            ],
                            selected: None,
                        },
                    ),
                    failed_to_contact_influx: false,
                },
            ),
            user_messages: UserMessages {
                messages: [
                    UserMessage {
                        message: "Entered fake data mode2",
                        level: Info,
                        id: 1,
                    },
                ],
                current_id: 1,
            },
            example_data: FakeData,
        }
        "#);

        // -----------------------------------------------------------------------------------------
        // confirm the correct events are called
        // -----------------------------------------------------------------------------------------
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(effects, @r#"
        [
            Render(
                Request(
                    RenderOperation,
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let effects: Vec<Event> = commands.events().collect();

        // the bellow we only care about "PostWriteReviewScreenEvents" confirming we got the .then_send correct
        insta::assert_debug_snapshot!(effects, @"[]");
    }

    #[test]
    fn example_data() {
        // set up our initial model
        let mut model = Model {
            screen: Screens::InitialScreen(InitialScreenData {
                base_url: Some("http://example.com".to_owned()),
                api_token: Some("some random token".to_owned()),
                ..Default::default()
            }),
            example_data: ExampleData::FakeData,
            ..Default::default()
        };

        let mut commands = influx_bucket_connection_test(
            &mut model.screen,
            &mut model.user_messages,
            &mut model.example_data,
        );

        // check that the view has been updated correctly
        insta::assert_debug_snapshot!(model, @r#"
        Model {
            screen: SelectOrgBucketMeasurementScreen(
                SelectOrgBucketMeasurementScreenData {
                    api_token: "some random token",
                    base_url: Url {
                        scheme: "http",
                        cannot_be_a_base: false,
                        username: "",
                        password: None,
                        host: Some(
                            Domain(
                                "example.com",
                            ),
                        ),
                        port: None,
                        path: "/",
                        query: None,
                        fragment: None,
                    },
                    orgs: NotLoaded(
                        LoadingResource {
                            retry_count: 0,
                            failure_reasons: [],
                            current_state: Initialised,
                        },
                    ),
                    buckets: NotLoaded(
                        LoadingResource {
                            retry_count: 0,
                            failure_reasons: [],
                            current_state: Initialised,
                        },
                    ),
                    measurements: UserInputOrMultiChoice {
                        options: NotLoaded(
                            LoadingResource {
                                retry_count: 0,
                                failure_reasons: [],
                                current_state: Initialised,
                            },
                        ),
                        selected: None,
                        notify_user_of_mode_swtich: None,
                    },
                },
            ),
            user_messages: UserMessages {
                messages: [],
                current_id: 0,
            },
            example_data: FakeData,
        }
        "#);

        // -----------------------------------------------------------------------------------------
        // confirm the correct events are called
        // -----------------------------------------------------------------------------------------
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(effects, @r#"
        [
            Render(
                Request(
                    RenderOperation,
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let effects: Vec<Event> = commands.events().collect();

        // the bellow we only care about "PostWriteReviewScreenEvents" confirming we got the .then_send correct
        insta::assert_debug_snapshot!(effects, @"[]");
    }
}
