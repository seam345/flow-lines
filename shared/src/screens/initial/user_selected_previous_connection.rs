use crate::internal_error_strings::{N025_USED, N051_USED, N060_USED};
use crate::model::{ExampleData, Screens, UserMessages};
use crate::model_ui::screens_common::remote_multi_choice::multi_choice::UiStringIndex;
use crate::screens::initial::influx_buckets_connection_test::influx_bucket_connection_test;
use crate::screens::input_fields_and_tags::RemoteResource::Loaded;
use crate::{Effect, Event};
use crux_core::Command;
use log::{error, trace, warn, Level};

pub fn user_selected_previous_connection(
    index: UiStringIndex,
    screen: &mut Screens,
    user_messages: &mut UserMessages,
    example_data: &mut ExampleData,
) -> Command<Effect, Event> {
    trace!("Initial screen event: Validating model");

    let Screens::InitialScreen(ref mut screen_data) = screen else {
        error!("Screen was in an unexpected state {:?}", screen);
        user_messages.add_message(N025_USED, Level::Error);
        return crux_core::render::render();
    };
    let Loaded(previous_connections) = &screen_data.previous_connections else {
        user_messages.error_report_to_developer(N051_USED);
        return crux_core::render::render();
    };

    let Ok(index) = index.to_bound_usize(previous_connections.len()) else {
        warn!("Index for previous connected could not be parsed as a usize");
        user_messages.error_report_to_developer(N060_USED);
        return crux_core::render::render();
    };

    match previous_connections.get(index) {
        Some(selected_connection) => {
            screen_data.base_url = Some(selected_connection.base_url.clone());
            screen_data.api_token = Some(selected_connection.api_token.clone());
            influx_bucket_connection_test(screen, user_messages, example_data)
        }
        None => {
            warn!("Index provided for previous connection was out of range!");
            crux_core::render::render()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::Model;
    use crate::screens::initial::InitialScreenData;
    use crate::screens_common::model::multi_choice::MultiChoice;
    use crate::screens_common::model::previous_connection::PreviousConnection;
    use crate::screens_common::test_common::test_shell;

    #[test]
    fn test_quick_actions_user_selected() {
        let mut model = Model {
            screen: Screens::InitialScreen(InitialScreenData {
                previous_connections: Loaded(MultiChoice {
                    options: vec![PreviousConnection {
                        name: None,
                        api_token: "test_token".to_string(),
                        base_url: "https://example.com".to_string(),
                    }],
                    selected: None,
                }),
                ..Default::default()
            }),
            user_messages: Default::default(),
            example_data: Default::default(),
        };

        let mut commands = user_selected_previous_connection(
            UiStringIndex("0".to_string()),
            &mut model.screen,
            &mut model.user_messages,
            &mut model.example_data,
        );

        // =========================================================================================
        // check that the view has been updated correctly
        // =========================================================================================
        insta::assert_debug_snapshot!(model, @r#"
        Model {
            screen: InitialScreen(
                InitialScreenData {
                    api_token: Some(
                        "test_token",
                    ),
                    base_url: Some(
                        "https://example.com",
                    ),
                    quick_actions: NotLoaded(
                        LoadingResource {
                            retry_count: 0,
                            failure_reasons: [],
                            current_state: Initialised,
                        },
                    ),
                    previous_connections: Loaded(
                        MultiChoice {
                            options: [
                                PreviousConnection {
                                    name: None,
                                    api_token: "test_token",
                                    base_url: "https://example.com",
                                },
                            ],
                            selected: None,
                        },
                    ),
                    failed_to_contact_influx: false,
                },
            ),
            user_messages: UserMessages {
                messages: [],
                current_id: 0,
            },
            example_data: Real,
        }
        "#);

        // =========================================================================================
        // confirm the correct events are called
        // =========================================================================================
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            Http(
                Request(
                    HttpRequest {
                        method: "GET",
                        url: "https://example.com/api/v2/buckets",
                        headers: [
                            HttpHeader {
                                name: "authorization",
                                value: "Token test_token",
                            },
                        ],
                        body: "",
                    },
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let events: Vec<Event> = commands.events().collect();

        insta::assert_debug_snapshot!(
            events, @r#"
        [
            EvInitialScreen(
                InfluxBucketsConnectionTestResponse(
                    Err(
                        Url(
                            "Just a fake test",
                        ),
                    ),
                ),
            ),
        ]
        "#);
    }
}
