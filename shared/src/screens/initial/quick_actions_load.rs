use crate::capabilities::persistent_storage;
use crate::capabilities::persistent_storage::PersistentStorageOutput;
use crate::model::QUICK_ACTIONS_FILENAME;
use crate::screens::initial::ScEvInitial;
use crate::Event::EvInitialScreen;
use crate::{Effect, Event};
use crux_core::Command;
use log::trace;

pub fn load_quick_actions() -> Command<Effect, Event> {
    trace!("load_quick_actions called");

    persistent_storage::load(QUICK_ACTIONS_FILENAME.to_owned()).then_send(construct_response_event)
}

//  F: FnOnce(crate::Result<Response<ExpectBody>>) -> Event + Send + 'static,
fn construct_response_event(body: PersistentStorageOutput) -> Event {
    EvInitialScreen(ScEvInitial::QuickActionsLoadResult(body))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::screens_common::test_common::test_shell;

    #[test]
    fn test_load_quick_actions() {
        let mut commands = load_quick_actions();

        // confirm the correct events are called
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            PersistentStorage(
                Request(
                    Load(
                        "quick_actions",
                    ),
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let events: Vec<Event> = commands.events().collect();

        insta::assert_debug_snapshot!(
            events, @r#"
        [
            EvInitialScreen(
                QuickActionsLoadResult(
                    FileData(
                        "Just a fake test",
                    ),
                ),
            ),
        ]
        "#);
    }
}
