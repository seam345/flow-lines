use crate::capabilities::persistent_storage::PersistentStorageOutput;
use crate::model::UserMessages;
use crate::screens::initial::InitialScreenData;
use crate::screens::input_fields_and_tags::RemoteResource;
use crate::screens_common::model::multi_choice::MultiChoice;
use crate::screens_common::model::previous_connection::PreviousConnection;
use crate::{Effect, Event};
use crux_core::Command;
use log::{debug, error, trace, warn, Level};

pub fn load_previous_connections_result(
    model: &mut InitialScreenData,
    user_messages: &mut UserMessages,
    response: PersistentStorageOutput,
) -> Command<Effect, Event> {
    trace!("load_previous_connections_results: called");
    match response {
        PersistentStorageOutput::FileData(data) => {
            debug!("loaded data: {data}");
            match serde_json::from_str::<Vec<PreviousConnection>>(&data) {
                Ok(value) => {
                    model.previous_connections = RemoteResource::Loaded(MultiChoice {
                        options: value,
                        selected: None,
                    });
                }
                Err(error) => {
                    error!("Failed to get previous connections, error: {:?}", error);
                    error!("data was {}", data);
                    user_messages.add_message(
                        format!("Failed to get previous connections, error: {:?}", error),
                        Level::Error,
                    );
                }
            }
        }
        PersistentStorageOutput::SaveOk
        | PersistentStorageOutput::AppendOk
        | PersistentStorageOutput::Error => todo!(),
        PersistentStorageOutput::LoadErrorFileDoesntExist => {
            warn!("Previous connections file doesnt exist!");
            model.previous_connections = RemoteResource::NonExistent;
        }
    }
    trace!("screen data before render call, {:?}", model);
    trace!("load_previous_connections_results: returning render");
    crux_core::render::render()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::screens::initial::InitialScreenData;
    use crate::screens_common::test_common::test_shell;

    #[test]
    #[ignore] // atm this doesn't give me the result I want todo probably should do the same as file doesn't exist
    fn test_load_previous_connections_result_empty() {
        let mut screen_data = InitialScreenData {
            ..Default::default()
        };
        let mut user_messages = UserMessages::default();
        let response = PersistentStorageOutput::FileData("".to_owned());

        let mut commands =
            load_previous_connections_result(&mut screen_data, &mut user_messages, response);

        // =========================================================================================
        // check that the view has been updated correctly
        // =========================================================================================
        insta::assert_debug_snapshot!(screen_data, @r#"
        InitialScreenData {
            api_token: None,
            base_url: None,
            quick_actions: NotLoaded(
                LoadingResource {
                    retry_count: 0,
                    failure_reasons: [],
                    current_state: Initialised,
                },
            ),
            previous_connections: ,
            failed_to_contact_influx: false,
        }
        "#);

        insta::assert_debug_snapshot!(user_messages, @r#"
        UserMessages {
            messages: [
                UserMessage {
                    message: "Failed to get previous connections, error: Error(\"EOF while parsing a value\", line: 1, column: 0)",
                    level: Error,
                    id: 1,
                },
            ],
            current_id: 1,
        }
        "#);

        // =========================================================================================
        // confirm the correct events are called
        // =========================================================================================
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            Render(
                Request(
                    RenderOperation,
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let events: Vec<Event> = commands.events().collect();

        insta::assert_debug_snapshot!(
            events, @"[]");
    }

    #[test]
    fn test_load_previous_connections_result_file_not_exist() {
        let mut screen_data = InitialScreenData {
            ..Default::default()
        };
        let mut user_messages = UserMessages::default();
        let response = PersistentStorageOutput::LoadErrorFileDoesntExist;

        let mut commands =
            load_previous_connections_result(&mut screen_data, &mut user_messages, response);

        // =========================================================================================
        // check that the view has been updated correctly
        // =========================================================================================
        insta::assert_debug_snapshot!(screen_data, @r#"
        InitialScreenData {
            api_token: None,
            base_url: None,
            quick_actions: NotLoaded(
                LoadingResource {
                    retry_count: 0,
                    failure_reasons: [],
                    current_state: Initialised,
                },
            ),
            previous_connections: NonExistent,
            failed_to_contact_influx: false,
        }
        "#);

        insta::assert_debug_snapshot!(user_messages, @r#"
        UserMessages {
            messages: [],
            current_id: 0,
        }
        "#);

        // =========================================================================================
        // confirm the correct events are called
        // =========================================================================================
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            Render(
                Request(
                    RenderOperation,
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let events: Vec<Event> = commands.events().collect();

        insta::assert_debug_snapshot!(
            events, @"[]");
    }

    #[test]
    fn test_load_previous_connections_result() {
        let mut screen_data = InitialScreenData {
            ..Default::default()
        };
        let mut user_messages = UserMessages::default();
        let response = PersistentStorageOutput::FileData(
            r#"[{"name": null, "api_token": "sdhabjwxbbaw", "base_url": "https://example.com" }]"#
                .to_owned(),
        );

        let mut commands =
            load_previous_connections_result(&mut screen_data, &mut user_messages, response);

        // =========================================================================================
        // check that the view has been updated correctly
        // =========================================================================================
        insta::assert_debug_snapshot!(screen_data, @r#"
        InitialScreenData {
            api_token: None,
            base_url: None,
            quick_actions: NotLoaded(
                LoadingResource {
                    retry_count: 0,
                    failure_reasons: [],
                    current_state: Initialised,
                },
            ),
            previous_connections: Loaded(
                MultiChoice {
                    options: [
                        PreviousConnection {
                            name: None,
                            api_token: "sdhabjwxbbaw",
                            base_url: "https://example.com",
                        },
                    ],
                    selected: None,
                },
            ),
            failed_to_contact_influx: false,
        }
        "#);

        insta::assert_debug_snapshot!(user_messages, @r#"
        UserMessages {
            messages: [],
            current_id: 0,
        }
        "#);

        // =========================================================================================
        // confirm the correct events are called
        // =========================================================================================
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            Render(
                Request(
                    RenderOperation,
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let events: Vec<Event> = commands.events().collect();

        insta::assert_debug_snapshot!(
            events, @"[]");
    }
}
