use crate::model::Screens;
use crate::{Effect, Event};
use crux_core::Command;

pub fn load_settings_screen(screen: &mut Screens) -> Command<Effect, Event> {
    *screen = Screens::SettingsScreen;
    crux_core::render::render()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::Model;
    use crate::screens::initial::InitialScreenData;
    use crate::screens_common::test_common::test_shell;

    #[test]
    fn test_load_settings_screen() {
        let mut model = Model {
            screen: Screens::InitialScreen(InitialScreenData {
                ..Default::default()
            }),
            ..Default::default()
        };
        let mut commands = load_settings_screen(&mut model.screen);

        // =========================================================================================
        // check that the view has been updated correctly
        // =========================================================================================
        insta::assert_debug_snapshot!(model, @r#"
        Model {
            screen: SettingsScreen,
            user_messages: UserMessages {
                messages: [],
                current_id: 0,
            },
            example_data: Real,
        }
        "#);

        // =========================================================================================
        // confirm the correct events are called
        // =========================================================================================
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            Render(
                Request(
                    RenderOperation,
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let events: Vec<Event> = commands.events().collect();

        insta::assert_debug_snapshot!(
            events, @"[]");
    }
}
