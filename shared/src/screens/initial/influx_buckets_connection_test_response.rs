use crate::internal_error_strings::{N023_USED, N024_USED, N038_USED};
use crate::model::{Screens, UserMessages};
use crate::screens::initial::append_previous_connection::append_previous_connection;
use crate::screens::input_fields_and_tags::RemoteResource;
use crate::screens::select_org_bucket_measurements::SelectOrgBucketMeasurementScreenData;
use crate::screens_common::model::multi_choice::MultiChoice;
use crate::screens_common::requests::BucketsResult;
use crate::{Effect, Event};
use crux_core::Command;
use crux_http::Response;
use log::{error, trace, Level};

/// Deserialise bucket request from Influxdb2 https://docs.influxdata.com/influxdb/v2/api/#operation/GetBuckets
/// There are a lot more fields to the reply but we only car about the name to be added to the Flux query so that's all we are deserializing
// todo do fake data
pub fn influx_buckets_connection_test_response(
    screen: &mut Screens,
    user_messages: &mut UserMessages,
    response: crux_http::Result<Response<BucketsResult>>,
) -> Command<Effect, Event> {
    match response {
        Ok(response) => {
            // ignoring status codes we will only get an Ok if an earlier block managed to parse the body to buckets result
            match response.body() {
                None => {
                    error!(
                        "How did I end up with no body in influx_buckets_connection_test_response"
                    );
                    user_messages.error_report_to_developer(N024_USED);
                    crux_core::render::render()
                }
                Some(response_body) => {
                    // match screen.try_into() { };
                    trace!("Extract screen_data");
                    let Screens::InitialScreen(current_data) = screen else {
                        error!("screen is wrong type");
                        user_messages.error_report_to_developer(N023_USED);
                        return crux_core::render::render();
                    };

                    trace!("Append to previous connections");
                    append_previous_connection(current_data, user_messages);

                    trace!("Move to next screen");
                    let Ok(mut select_org_bucket_measurement): Result<
                        SelectOrgBucketMeasurementScreenData,
                        _,
                    > = current_data.try_into() else {
                        error!("cannot convert to next screen");
                        user_messages.error_report_to_developer(N038_USED);
                        return crux_core::render::render();
                    };
                    let mut buckets = MultiChoice::default();
                    for bucket in &response_body.buckets {
                        buckets.push(bucket.name.clone());
                    }

                    select_org_bucket_measurement.buckets = RemoteResource::Loaded(buckets);

                    *screen = Screens::SelectOrgBucketMeasurementScreen(Box::from(
                        select_org_bucket_measurement,
                    ));
                    crux_core::render::render()
                }
            }
        }
        Err(err) => {
            error!("Got error response: {:?}", err);
            match screen {
                Screens::InitialScreen(initial) => {
                    initial.failed_to_contact_influx = true;
                }
                Screens::SelectOrgBucketMeasurementScreen(_)
                | Screens::InputFieldsTagsScreen(_)
                | Screens::SettingPreviousConnectionsScreen(_)
                | Screens::SettingsScreen => {
                    error!("Screen is incorrect type... this shouldn't happen");
                }
                Screens::PostWriteReviewScreen(_) => {}
            }

            user_messages.add_message(
                "Unable to contact Influx server! Please check Url and Token, Url should not end in a /",
                Level::Error,
            );
            crux_core::render::render()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::Model;
    use crate::screens::initial::{InitialScreenData, ScEvInitial};
    use crate::screens_common::requests::Bucket;
    use crate::screens_common::test_common::test_shell;
    use crate::Counter;
    use crate::Effect;
    use crate::Event;
    use crux_core::assert_effect;
    use crux_core::testing::AppTester;
    use crux_http::testing::ResponseBuilder;

    #[test]
    fn happy_path() {
        let mut model = Model {
            screen: Screens::InitialScreen(InitialScreenData {
                base_url: Some("http://notaurl.com".to_owned()),
                api_token: Some("notatoken".to_owned()),
                ..Default::default()
            }),
            ..Default::default()
        };

        let mut commands = influx_buckets_connection_test_response(
            &mut model.screen,
            &mut model.user_messages,
            Ok(ResponseBuilder::ok()
                .body(BucketsResult {
                    buckets: vec![
                        Bucket {
                            name: "First bucket".to_string(),
                        },
                        Bucket {
                            name: "second bucket".to_string(),
                        },
                        Bucket {
                            name: "Third bucket".to_string(),
                        },
                    ],
                })
                .build()),
        );

        // check that the view has been updated correctly
        insta::assert_debug_snapshot!(model, @r#"
        Model {
            screen: SelectOrgBucketMeasurementScreen(
                SelectOrgBucketMeasurementScreenData {
                    api_token: "notatoken",
                    base_url: Url {
                        scheme: "http",
                        cannot_be_a_base: false,
                        username: "",
                        password: None,
                        host: Some(
                            Domain(
                                "notaurl.com",
                            ),
                        ),
                        port: None,
                        path: "/",
                        query: None,
                        fragment: None,
                    },
                    orgs: NotLoaded(
                        LoadingResource {
                            retry_count: 0,
                            failure_reasons: [],
                            current_state: Initialised,
                        },
                    ),
                    buckets: Loaded(
                        MultiChoice {
                            options: [
                                "First bucket",
                                "second bucket",
                                "Third bucket",
                            ],
                            selected: None,
                        },
                    ),
                    measurements: UserInputOrMultiChoice {
                        options: NotLoaded(
                            LoadingResource {
                                retry_count: 0,
                                failure_reasons: [],
                                current_state: Initialised,
                            },
                        ),
                        selected: None,
                        notify_user_of_mode_swtich: None,
                    },
                },
            ),
            user_messages: UserMessages {
                messages: [],
                current_id: 0,
            },
            example_data: Real,
        }
        "#);

        // -----------------------------------------------------------------------------------------
        // confirm the correct events are called
        // -----------------------------------------------------------------------------------------
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            Render(
                Request(
                    RenderOperation,
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let effects: Vec<Event> = commands.events().collect();

        // the bellow we only care about "PostWriteReviewScreenEvents" confirming we got the .then_send correct
        insta::assert_debug_snapshot!(
            effects, @"[]");
    }

    #[test]
    fn unhappy_path() {
        let mut model = Model {
            ..Default::default()
        };

        let mut commands = influx_buckets_connection_test_response(
            &mut model.screen,
            &mut model.user_messages,
            Err(crux_http::http::Error::from_str(
                400,
                "EOF while parsing a value at line 1 column 0",
            )
            .into()),
        );

        // check that the view has been updated correctly
        insta::assert_debug_snapshot!(model, @r#"
        Model {
            screen: InitialScreen(
                InitialScreenData {
                    api_token: None,
                    base_url: None,
                    quick_actions: NotLoaded(
                        LoadingResource {
                            retry_count: 0,
                            failure_reasons: [],
                            current_state: Initialised,
                        },
                    ),
                    previous_connections: NotLoaded(
                        LoadingResource {
                            retry_count: 0,
                            failure_reasons: [],
                            current_state: Initialised,
                        },
                    ),
                    failed_to_contact_influx: true,
                },
            ),
            user_messages: UserMessages {
                messages: [
                    UserMessage {
                        message: "Unable to contact Influx server! Please check Url and Token, Url should not end in a /",
                        level: Error,
                        id: 1,
                    },
                ],
                current_id: 1,
            },
            example_data: Real,
        }
        "#);

        // -----------------------------------------------------------------------------------------
        // confirm the correct events are called
        // -----------------------------------------------------------------------------------------
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            Render(
                Request(
                    RenderOperation,
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let effects: Vec<Event> = commands.events().collect();

        // the bellow we only care about "PostWriteReviewScreenEvents" confirming we got the .then_send correct
        insta::assert_debug_snapshot!(
            effects, @"[]");
    }

    #[test]
    fn unhappy_path_to_happy() {
        let app = AppTester::<Counter>::default();

        let mut model = Model {
            screen: Screens::InitialScreen(InitialScreenData {
                base_url: Some("http://notaurl.com".to_owned()),
                api_token: Some("notatoken".to_owned()),
                ..Default::default()
            }),
            ..Default::default()
        };

        let update = app.update(
            Event::EvInitialScreen(ScEvInitial::InfluxBucketsConnectionTestResponse(Err(
                crux_http::http::Error::from_str(
                    400,
                    "EOF while parsing a value at line 1 column 0",
                )
                .into(),
            ))),
            &mut model,
        );

        // check that the app asked the shell to render
        assert_effect!(update, Effect::Render(_));

        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(&mut model), @r#"
        ---
        screen:
          InitialScreen:
            api_token: notatoken
            base_url: "http://notaurl.com"
            quick_actions:
              Loading:
                retry_count: 0
                current_state: Initialised
            previous_connections:
              Loading:
                retry_count: 0
                current_state: Initialised
            failed_to_contact_influx: true
        user_messages:
          messages:
            - message: "Unable to contact Influx server! Please check Url and Token, Url should not end in a /"
              level: Error
              id: 1
        tutorial: ~
        "#);

        let update = app.update(
            Event::EvInitialScreen(ScEvInitial::InfluxBucketsConnectionTestResponse(Ok(
                ResponseBuilder::ok()
                    .body(BucketsResult {
                        buckets: vec![
                            Bucket {
                                name: "First bucket".to_string(),
                            },
                            Bucket {
                                name: "second bucket".to_string(),
                            },
                            Bucket {
                                name: "Third bucket".to_string(),
                            },
                        ],
                    })
                    .build(),
            ))),
            &mut model,
        );

        // check that the app asked the shell to render
        assert_effect!(update, Effect::Render(_));

        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(&mut model), @r#"
        ---
        screen:
          SelectMeasurementScreen:
            base_url: "http://notaurl.com/"
            orgs:
              Loading:
                retry_count: 0
                current_state: Initialised
            buckets:
              Loaded:
                options:
                  - hidden_uid: "0"
                    user_facing_value: First bucket
                  - hidden_uid: "1"
                    user_facing_value: second bucket
                  - hidden_uid: "2"
                    user_facing_value: Third bucket
                selected_index: ~
            measurements:
              options: Loading
              selected: ~
              notify_user_of_mode_switch: ~
        user_messages:
          messages: []
        tutorial: ~
        "#);
    }
}
