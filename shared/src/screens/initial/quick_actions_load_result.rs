use crate::capabilities::persistent_storage::PersistentStorageOutput;
use crate::model::UserMessages;
use crate::screens::initial::{InitialScreenData, QuickAction};
use crate::screens::input_fields_and_tags::RemoteResource;
use crate::screens_common::model::multi_choice::MultiChoice;
use crate::{Effect, Event};
use crux_core::Command;
use log::{debug, error, trace, warn, Level};
use serde_jsonlines::JsonLinesReader;
use std::io::BufReader;

pub fn load_quick_actions_result(
    model: &mut InitialScreenData,
    user_messages: &mut UserMessages,
    response: PersistentStorageOutput,
) -> Command<Effect, Event> {
    trace!("load_quick_actions_results called");
    match response {
        PersistentStorageOutput::FileData(data) => {
            debug!("loaded data: {data}");

            let fp = BufReader::new(data.as_bytes());
            let reader = JsonLinesReader::new(fp);
            let items = reader
                .read_all::<QuickAction>()
                .collect::<std::io::Result<Vec<_>>>();

            match items {
                Ok(value) => {
                    model.quick_actions = RemoteResource::Loaded(MultiChoice {
                        options: value,
                        selected: None,
                    });
                    crux_core::render::render()
                }
                Err(error) => {
                    error!("Failed to get previous connections, error: {:?}", error);
                    error!("data was {}", data);
                    user_messages.add_message(
                        format!("Failed to get previous connections, error: {:?}", error),
                        Level::Error,
                    );
                    crux_core::render::render()
                }
            }
        }
        PersistentStorageOutput::SaveOk
        | PersistentStorageOutput::AppendOk
        | PersistentStorageOutput::Error => todo!(),
        PersistentStorageOutput::LoadErrorFileDoesntExist => {
            warn!("Quick actions file doesnt exist!");

            model.quick_actions = RemoteResource::NonExistent;
            crux_core::render::render()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::screens::initial::InitialScreenData;
    use crate::screens_common::test_common::test_shell;

    #[test]
    fn test_load_previous_connections_result_empty() {
        let mut screen_data = InitialScreenData {
            ..Default::default()
        };
        let mut user_messages = UserMessages::default();
        let response = PersistentStorageOutput::FileData("".to_owned());

        let mut commands =
            load_quick_actions_result(&mut screen_data, &mut user_messages, response);

        // =========================================================================================
        // check that the view has been updated correctly
        // =========================================================================================
        // todo confirm if I want an empty list or a non exist
        insta::assert_debug_snapshot!(screen_data, @r#"
        InitialScreenData {
            api_token: None,
            base_url: None,
            quick_actions: Loaded(
                MultiChoice {
                    options: [],
                    selected: None,
                },
            ),
            previous_connections: NotLoaded(
                LoadingResource {
                    retry_count: 0,
                    failure_reasons: [],
                    current_state: Initialised,
                },
            ),
            failed_to_contact_influx: false,
        }
        "#);

        insta::assert_debug_snapshot!(user_messages, @r#"
        UserMessages {
            messages: [],
            current_id: 0,
        }
        "#);

        // =========================================================================================
        // confirm the correct events are called
        // =========================================================================================
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            Render(
                Request(
                    RenderOperation,
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let events: Vec<Event> = commands.events().collect();

        insta::assert_debug_snapshot!(
            events, @"[]");
    }

    #[test]
    fn test_load_previous_connections_result_file_not_exist() {
        let mut screen_data = InitialScreenData {
            ..Default::default()
        };
        let mut user_messages = UserMessages::default();
        let response = PersistentStorageOutput::LoadErrorFileDoesntExist;

        let mut commands =
            load_quick_actions_result(&mut screen_data, &mut user_messages, response);

        // =========================================================================================
        // check that the view has been updated correctly
        // =========================================================================================
        insta::assert_debug_snapshot!(screen_data, @r#"
        InitialScreenData {
            api_token: None,
            base_url: None,
            quick_actions: NonExistent,
            previous_connections: NotLoaded(
                LoadingResource {
                    retry_count: 0,
                    failure_reasons: [],
                    current_state: Initialised,
                },
            ),
            failed_to_contact_influx: false,
        }
        "#);

        insta::assert_debug_snapshot!(user_messages, @r#"
        UserMessages {
            messages: [],
            current_id: 0,
        }
        "#);

        // =========================================================================================
        // confirm the correct events are called
        // =========================================================================================
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            Render(
                Request(
                    RenderOperation,
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let events: Vec<Event> = commands.events().collect();

        insta::assert_debug_snapshot!(
            events, @"[]");
    }

    #[test]
    fn test_load_previous_connections_result() {
        let mut screen_data = InitialScreenData {
            ..Default::default()
        };
        let mut user_messages = UserMessages::default();
        let response = PersistentStorageOutput::FileData(
            r#"{"name":"test","api_token":"vzO0_9-u3Y0Q-FyO88bIVK8g3beRkvk00hDppgMGiiNVmk1wm0Cmek_QT2hTxcqdUL607FSMtgephwprZPIl7A==","base_url":"http://100.108.38.15:8086/","org":{"name":"test","id":"3aeb5581111a8d5a"},"bucket":"personal","measurement":"test_crux","tags":{"new_tag2":"hey2"},"fields":{"akey":{"Float":{"inner":""}}}}"#
                .to_owned(),
        );

        let mut commands =
            load_quick_actions_result(&mut screen_data, &mut user_messages, response);

        // =========================================================================================
        // check that the view has been updated correctly
        // =========================================================================================
        insta::assert_debug_snapshot!(screen_data, @r#"
        InitialScreenData {
            api_token: None,
            base_url: None,
            quick_actions: Loaded(
                MultiChoice {
                    options: [
                        QuickAction {
                            name: "test",
                            api_token: "vzO0_9-u3Y0Q-FyO88bIVK8g3beRkvk00hDppgMGiiNVmk1wm0Cmek_QT2hTxcqdUL607FSMtgephwprZPIl7A==",
                            base_url: Url {
                                scheme: "http",
                                cannot_be_a_base: false,
                                username: "",
                                password: None,
                                host: Some(
                                    Ipv4(
                                        100.108.38.15,
                                    ),
                                ),
                                port: Some(
                                    8086,
                                ),
                                path: "/",
                                query: None,
                                fragment: None,
                            },
                            org: Org {
                                name: "test",
                                id: "3aeb5581111a8d5a",
                            },
                            bucket: "personal",
                            measurement: "test_crux",
                            tags: {
                                "new_tag2": Some(
                                    "hey2",
                                ),
                            },
                            fields: {
                                "akey": Float(
                                    ValidatedFieldF64 {
                                        inner: "",
                                    },
                                ),
                            },
                        },
                    ],
                    selected: None,
                },
            ),
            previous_connections: NotLoaded(
                LoadingResource {
                    retry_count: 0,
                    failure_reasons: [],
                    current_state: Initialised,
                },
            ),
            failed_to_contact_influx: false,
        }
        "#);

        insta::assert_debug_snapshot!(user_messages, @r#"
        UserMessages {
            messages: [],
            current_id: 0,
        }
        "#);

        // =========================================================================================
        // confirm the correct events are called
        // =========================================================================================
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            Render(
                Request(
                    RenderOperation,
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let events: Vec<Event> = commands.events().collect();

        insta::assert_debug_snapshot!(
            events, @"[]");
    }
}
