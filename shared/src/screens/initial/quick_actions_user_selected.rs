use crate::internal_error_strings::{N019_USED, N042_USED, N058_USED, N064_USED};
use crate::model::{Model, Screens};
use crate::model_ui::screens_common::remote_multi_choice::multi_choice::UiStringIndex;
use crate::screens::input_fields_and_tags::RemoteResource::Loaded;
use crate::screens::input_fields_and_tags::{
    validate_tag_value, InputFieldsAndTagsScreenData, LoadingResource, RemoteResource,
};
use crate::screens_common::model::user_input_or_multi_choice::{
    SelectedUserInputOrMultiChoice, UserInputOrMultiChoice, ValidatingStringPreTrimmed,
};
use crate::{Effect, Event};
use crux_core::Command;
use log::{error, trace, warn, Level};
use std::collections::BTreeMap;

pub fn quick_actions_user_selected(
    index: UiStringIndex,
    model: &mut Model,
) -> Command<Effect, Event> {
    trace!("Initial screen event: Validating model");

    let Screens::InitialScreen(ref mut screen_data) = model.screen else {
        error!("Screen was in an unexpected state {:?}", model.screen);
        model.user_messages.add_message(N019_USED, Level::Error);
        return crux_core::render::render();
    };
    let Loaded(quick_actions) = &screen_data.quick_actions else {
        model.user_messages.error_report_to_developer(N042_USED);
        return crux_core::render::render();
    };

    let Ok(index) = index.to_bound_usize(quick_actions.len()) else {
        warn!("Index for quick actions could not be parsed as a usize");
        model.user_messages.error_report_to_developer(N064_USED);
        return crux_core::render::render();
    };

    trace!("Running select org,bucket,measurement event");
    let Some(quick_action) = quick_actions.get(index) else {
        model.user_messages.add_message(N058_USED, Level::Error);
        return crux_core::render::render();
    };

    //tags and fields we are just adding without internet checking...
    // this means I either zip them together after a fetch or I don't know
    model.screen = Screens::InputFieldsTagsScreen(InputFieldsAndTagsScreenData {
        api_token: quick_action.api_token.clone(),
        base_url: quick_action.base_url.clone(),
        org: quick_action.org.clone(),
        bucket: quick_action.bucket.clone(),
        measurement: quick_action.measurement.clone(),
        available_tags: Default::default(), // todo load all tags
        tags: quick_action
            .tags
            .iter()
            .map(|(key, value)| {
                (
                    key,
                    match value {
                        None => UserInputOrMultiChoice {
                            options: RemoteResource::NotLoaded(LoadingResource {
                                retry_count: 0,
                                failure_reasons: vec![],
                                current_state: Default::default(),
                            }),
                            selected: None,
                            notify_user_of_mode_switch: None,
                            user_validating_function: validate_tag_value,
                        },
                        Some(inner) => UserInputOrMultiChoice {
                            options: RemoteResource::NotLoaded(LoadingResource {
                                retry_count: 0,
                                failure_reasons: vec![],
                                current_state: Default::default(),
                            }),
                            selected: Some(SelectedUserInputOrMultiChoice::User(
                                ValidatingStringPreTrimmed {
                                    inner: inner.clone(),
                                    validating_function: validate_tag_value,
                                },
                            )),
                            notify_user_of_mode_switch: None,
                            user_validating_function: validate_tag_value,
                        },
                    },
                )
            })
            .fold(BTreeMap::new(), |mut map, (key, value)| {
                map.insert(key.clone(), value);
                map
            }),
        new_tags: vec![],
        available_fields: Default::default(),
        fields: quick_action.fields.clone(),
        new_fields: vec![],
        line: "".to_string(),
        time: None,
        precision: Default::default(),
    });
    crux_core::render::render()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::screens::initial::{InitialScreenData, QuickAction};
    use crate::screens_common::model::multi_choice::MultiChoice;
    use crate::screens_common::test_common::test_shell;
    use http_types::Url;

    #[test]
    fn test_quick_actions_user_selected() {
        let mut model = Model {
            screen: Screens::InitialScreen(InitialScreenData {
                quick_actions: Loaded(MultiChoice {
                    options: vec![QuickAction {
                        name: "test".to_string(),
                        api_token: "test_token".to_string(),
                        base_url: Url::parse("http://notaurl.com").unwrap(),
                        org: Default::default(),
                        bucket: "".to_string(),
                        measurement: "".to_string(),
                        tags: Default::default(),
                        fields: Default::default(),
                    }],
                    selected: None,
                }),
                ..Default::default()
            }),
            user_messages: Default::default(),
            example_data: Default::default(),
        };

        let mut commands = quick_actions_user_selected(UiStringIndex("0".to_string()), &mut model);

        // =========================================================================================
        // check that the view has been updated correctly
        // =========================================================================================
        insta::assert_debug_snapshot!(model, @r#"
        Model {
            screen: InputFieldsTagsScreen(
                InputFieldsAndTagsScreenData {
                    api_token: "test_token",
                    base_url: Url {
                        scheme: "http",
                        cannot_be_a_base: false,
                        username: "",
                        password: None,
                        host: Some(
                            Domain(
                                "notaurl.com",
                            ),
                        ),
                        port: None,
                        path: "/",
                        query: None,
                        fragment: None,
                    },
                    org: Org {
                        name: "",
                        id: "",
                    },
                    bucket: "",
                    measurement: "",
                    available_tags: NotLoaded(
                        LoadingResource {
                            retry_count: 0,
                            failure_reasons: [],
                            current_state: Initialised,
                        },
                    ),
                    tags: {},
                    new_tags: [],
                    available_fields: NotLoaded(
                        LoadingResource {
                            retry_count: 0,
                            failure_reasons: [],
                            current_state: Initialised,
                        },
                    ),
                    fields: {},
                    new_fields: [],
                    line: "",
                    time: None,
                    precision: Nanoseconds,
                },
            ),
            user_messages: UserMessages {
                messages: [],
                current_id: 0,
            },
            example_data: Real,
        }
        "#);

        // =========================================================================================
        // confirm the correct events are called
        // =========================================================================================
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            Render(
                Request(
                    RenderOperation,
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let events: Vec<Event> = commands.events().collect();

        insta::assert_debug_snapshot!(
            events, @"[]");
    }
}
