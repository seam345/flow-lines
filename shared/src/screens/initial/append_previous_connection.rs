use crate::capabilities::persistent_storage;
use crate::capabilities::persistent_storage::{Data, PersistentStorageOutput};
use crate::model::{UserMessages, PREVIOUS_CONNECTIONS_FILENAME};
use crate::screens::initial::{InitialScreenData, ScEvInitial};
use crate::screens::input_fields_and_tags::RemoteResource;
use crate::screens_common::model::previous_connection::PreviousConnection;
use crate::Event::EvInitialScreen;
use crate::{Effect, Event};
use crux_core::Command;
use log::{debug, trace, Level};

/// Uses connection details just entered into the model to append to all previous connections
/// if connection is the same as a previous it will skip saving
/// For now i shall write the whole file out from here todo later send only the append data
pub fn append_previous_connection(
    screen_data: &InitialScreenData,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    if let Some(cap_vars) = append_previous_connection_testable(screen_data, user_messages) {
        return persistent_storage::save(cap_vars.filename, cap_vars.data)
            .then_send(cap_vars.event);
    }
    Command::done()
}

struct CapabilityVars {
    filename: String,
    data: Data,
    event: fn(PersistentStorageOutput) -> Event,
}

/// Append previous connection to the stored file if it doesn't already exist
fn append_previous_connection_testable(
    screen_data: &InitialScreenData,
    user_messages: &mut UserMessages,
) -> Option<CapabilityVars> {
    // ========================================================================
    // Validate data, early returns
    // ========================================================================
    trace!("confirm base url exists");
    let Some(base_url) = &screen_data.base_url else {
        user_messages.add_message("No base url, not saving", Level::Error);
        return None;
    };

    trace!("confirm token exists");
    let Some(api_token) = &screen_data.api_token else {
        user_messages.add_message("No token, not saving", Level::Error);
        return None;
    };
    // ========================================================================
    // data validated
    // ========================================================================

    let previous_connection = PreviousConnection {
        name: None,
        base_url: base_url.to_owned(),
        api_token: api_token.to_owned(),
    };
    match &screen_data.previous_connections {
        RemoteResource::Loaded(options) => {
            if !options.contains(&previous_connection) {
                let mut values = options.to_vec();
                values.push(previous_connection);
                let data = serde_json::to_string(&values).unwrap();
                Some(CapabilityVars {
                    filename: PREVIOUS_CONNECTIONS_FILENAME.to_owned(),
                    event: construct_response_event,
                    data,
                })
            } else {
                None
            }
        }
        RemoteResource::NotLoaded(_) => {
            debug!("user triggered append_previous_connection before previous connections loaded");
            None
        }
        RemoteResource::NonExistent => {
            let values = vec![previous_connection];
            let data = serde_json::to_string(&values).unwrap();
            Some(CapabilityVars {
                filename: PREVIOUS_CONNECTIONS_FILENAME.to_owned(),
                event: construct_response_event,
                data,
            })
        }
    }
}

//  F: FnOnce(crate::Result<Response<ExpectBody>>) -> Event + Send + 'static,
fn construct_response_event(body: PersistentStorageOutput) -> Event {
    EvInitialScreen(ScEvInitial::AppendPreviousConnectionResult(body))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model_ui::user_messages::UiUserMessages;
    use crate::screens::input_fields_and_tags::LoadingResource;
    use crate::screens::input_fields_and_tags::RemoteResource::{Loaded, NonExistent, NotLoaded};
    use crate::screens_common::model::multi_choice::MultiChoice;
    use crate::screens_common::test_common::test_shell;

    // previous connections hasn't been loaded an attempt to save may make a duplication and atm
    // override the file we shouldn't do anything
    #[test]
    fn previous_connections_not_loaded() {
        let screen = InitialScreenData {
            base_url: Some("a url".to_owned()),
            api_token: Some("some random token".to_owned()),
            previous_connections: NotLoaded(LoadingResource::default()),
            ..Default::default()
        };
        let mut user_messages: UserMessages = Default::default();

        let mut commands = append_previous_connection(&screen, &mut user_messages);

        // no need to confirm that the view has been not been updated, as it's not mutable

        // confirm user_messages changes
        let ui_user_message: UiUserMessages = user_messages.into();
        insta::assert_yaml_snapshot!(ui_user_message, @"messages: []");

        // -----------------------------------------------------------------------------------------
        // confirm the correct events are called
        // -----------------------------------------------------------------------------------------
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @"[]");

        test_shell(effects);

        let effects: Vec<Event> = commands.events().collect();

        // the bellow we only care about "PostWriteReviewScreenEvents" confirming we got the .then_send correct
        insta::assert_debug_snapshot!(
            effects, @"[]");
    }

    #[test]
    fn happy_path_no_previous_connections() {
        let screen = InitialScreenData {
            base_url: Some("a url".to_owned()),
            api_token: Some("some random token".to_owned()),
            previous_connections: Loaded(MultiChoice {
                options: vec![],
                selected: None,
            }),
            ..Default::default()
        };
        let mut user_messages: UserMessages = Default::default();

        let mut commands = append_previous_connection(&screen, &mut user_messages);

        // no need to confirm that the view has been not been updated, as it's not mutable

        // confirm user_messages changes
        let ui_user_message: UiUserMessages = user_messages.into();
        insta::assert_yaml_snapshot!(ui_user_message, @"messages: []");

        // -----------------------------------------------------------------------------------------
        // confirm the correct events are called
        // -----------------------------------------------------------------------------------------
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            PersistentStorage(
                Request(
                    Save(
                        "previous_connections",
                        "[{\"name\":null,\"api_token\":\"some random token\",\"base_url\":\"a url\"}]",
                    ),
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let effects: Vec<Event> = commands.events().collect();

        // the bellow we only care about "PostWriteReviewScreenEvents" confirming we got the .then_send correct
        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            EvInitialScreen(
                AppendPreviousConnectionResult(
                    FileData(
                        "Just a fake test",
                    ),
                ),
            ),
        ]
        "#);
    }

    #[test]
    fn previous_connections_file_not_exist() {
        let screen = InitialScreenData {
            base_url: Some("a url".to_owned()),
            api_token: Some("some random token".to_owned()),
            previous_connections: NonExistent,
            ..Default::default()
        };
        let mut user_messages: UserMessages = Default::default();

        let mut commands = append_previous_connection(&screen, &mut user_messages);

        // no need to confirm that the view has been not been updated, as it's not mutable

        // confirm user_messages changes
        let ui_user_message: UiUserMessages = user_messages.into();
        insta::assert_yaml_snapshot!(ui_user_message, @"messages: []");

        // -----------------------------------------------------------------------------------------
        // confirm the correct events are called
        // -----------------------------------------------------------------------------------------
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            PersistentStorage(
                Request(
                    Save(
                        "previous_connections",
                        "[{\"name\":null,\"api_token\":\"some random token\",\"base_url\":\"a url\"}]",
                    ),
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let effects: Vec<Event> = commands.events().collect();

        // the bellow we only care about "PostWriteReviewScreenEvents" confirming we got the .then_send correct
        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            EvInitialScreen(
                AppendPreviousConnectionResult(
                    FileData(
                        "Just a fake test",
                    ),
                ),
            ),
        ]
        "#);
    }
}
