use crate::capabilities::persistent_storage;
use crate::capabilities::persistent_storage::PersistentStorageOutput;
use crate::model::PREVIOUS_CONNECTIONS_FILENAME;
use crate::screens::initial::ScEvInitial;
use crate::Event::EvInitialScreen;
use crate::{Effect, Event};
use crux_core::Command;
use log::debug;

pub fn load_previous_connections() -> Command<Effect, Event> {
    debug!("load_previous_connections called");

    persistent_storage::load(PREVIOUS_CONNECTIONS_FILENAME.to_owned())
        .then_send(construct_response_event)
}

fn construct_response_event(body: PersistentStorageOutput) -> Event {
    EvInitialScreen(ScEvInitial::LoadPreviousConnectionsResult(body))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::screens_common::test_common::test_shell;

    #[test]
    fn test_load_previous_connections() {
        let mut commands = load_previous_connections();

        // confirm the correct events are called
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            PersistentStorage(
                Request(
                    Load(
                        "previous_connections",
                    ),
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let events: Vec<Event> = commands.events().collect();

        // the bellow we only care about "PostWriteReviewScreenEvents" confirming we got the .then_send correct
        insta::assert_debug_snapshot!(
            events, @r#"
        [
            EvInitialScreen(
                LoadPreviousConnectionsResult(
                    FileData(
                        "Just a fake test",
                    ),
                ),
            ),
        ]
        "#);

        // no need to confirm that the view has been not been updated, as it's not mutable
    }
}
