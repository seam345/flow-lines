use crate::screens::initial::InitialScreenData;
use crate::{Effect, Event};
use crux_core::Command;

pub fn set_base_url(model: &mut InitialScreenData, base_url: String) -> Command<Effect, Event> {
    model.base_url = Some(base_url);
    crux_core::render::render()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::screens::initial::InitialScreenData;
    use crate::screens_common::test_common::test_shell;

    #[test]
    fn test_set_base_url() {
        let mut screen_data = InitialScreenData {
            ..Default::default()
        };
        let mut commands = set_base_url(&mut screen_data, "new base url".to_owned());

        // =========================================================================================
        // check that the view has been updated correctly
        // =========================================================================================
        insta::assert_debug_snapshot!(screen_data, @r#"
        InitialScreenData {
            api_token: None,
            base_url: Some(
                "new base url",
            ),
            quick_actions: NotLoaded(
                LoadingResource {
                    retry_count: 0,
                    failure_reasons: [],
                    current_state: Initialised,
                },
            ),
            previous_connections: NotLoaded(
                LoadingResource {
                    retry_count: 0,
                    failure_reasons: [],
                    current_state: Initialised,
                },
            ),
            failed_to_contact_influx: false,
        }
        "#);

        // =========================================================================================
        // confirm the correct events are called
        // =========================================================================================
        let effects = commands.effects().collect();

        insta::assert_debug_snapshot!(
            effects, @r#"
        [
            Render(
                Request(
                    RenderOperation,
                ),
            ),
        ]
        "#);

        test_shell(effects);

        let events: Vec<Event> = commands.events().collect();

        insta::assert_debug_snapshot!(
            events, @"[]");
    }
}
