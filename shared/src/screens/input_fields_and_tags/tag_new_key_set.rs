use crate::model::UserMessages;
use crate::model_ui::screens_common::remote_multi_choice::multi_choice::UiStringIndex;
use crate::screens::input_fields_and_tags::InputFieldsAndTagsScreenData;
use crate::{Effect, Event};
use crux_core::Command;
use log::{error, Level};

pub fn tag_new_key_set(
    string_index: UiStringIndex,
    key: String,
    model: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    // todo validate key

    let Ok(index) = string_index.to_bound_usize(model.new_tags.len()) else {
        error!("Index check for set_new_tag_key failed");
        user_messages.add_message(crate::internal_error_strings::N003_USED, Level::Error);
        return crux_core::render::render();
    };

    let Some(new_tag) = model.new_tags.get_mut(index) else {
        error!(
            "Get mut failed after index check! index: {index}, len: {}",
            model.new_tags.len()
        );
        user_messages.add_message(crate::internal_error_strings::N004_USED, Level::Error);
        return crux_core::render::render();
    };

    new_tag.key = Some(key);
    crux_core::render::render()
}
