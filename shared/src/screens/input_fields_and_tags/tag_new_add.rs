use crate::model::UserMessages;
use crate::screens::input_fields_and_tags::{InputFieldsAndTagsScreenData, UserTag};
use crate::{Effect, Event};
use crux_core::Command;

pub fn tag_new_add(
    model: &mut InputFieldsAndTagsScreenData,
    _user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    model.new_tags.push(UserTag {
        key: None,
        value: None,
    });
    crux_core::render::render()
}
