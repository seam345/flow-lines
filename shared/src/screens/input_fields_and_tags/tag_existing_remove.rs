use crate::screens::input_fields_and_tags::InputFieldsAndTagsScreenData;
use crate::{Effect, Event};
use crux_core::Command;

pub fn tag_existing_remove(
    model: &mut InputFieldsAndTagsScreenData,
    key: String,
) -> Command<Effect, Event> {
    model.tags.remove(&key);
    crux_core::render::render()
}
