use crate::model::{ExampleData, UserMessages};
use crate::screens::input_fields_and_tags::flux_get_measurements_existing_fields::flux_get_measurements_existing_fields;
use crate::screens::input_fields_and_tags::{
    AvailableTagValues, AvailableTags, InputFieldsAndTagsScreenData, RemoteResource,
    ScEvInputFieldsAndTags,
};
use crate::Event::EvInputFieldsAndTagsEventsScreen;
use crate::{Effect, Event};
use crux_core::Command;
use log::Level::Trace;
use std::collections::BTreeMap;

/// get all tags in a given measurements, todo at some point I might want to paginate this
/// the intent of this is to grab existing tags to make it faster and easier to correctly input tags for a given point
/// https://docs.influxdata.com/flux/v0/stdlib/influxdata/influxdb/schema/measurementtagkeys/
///
///import "influxdata/influxdb/schema"
///
/// schema.measurementTagKeys(bucket: "example-bucket", measurement: "example-measurement")
pub fn flux_get_measurements_existing_tags(
    model: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
    example_data: &ExampleData,
) -> Command<Effect, Event> {
    user_messages.add_message("Calling existing tags", Trace);

    match example_data {
        ExampleData::Tutorial(_) | ExampleData::FakeData => {
            fake(model, user_messages, example_data)
        }
        ExampleData::Real => real(model, user_messages),
    }
}

fn fake(
    model: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
    example_data: &ExampleData,
) -> Command<Effect, Event> {
    let mut map = BTreeMap::new();

    // I ponder about already setting the tag values... I settled on setting them but this does mean on tutorial I am skipping another method...
    map.insert(
        "exampleTag1".to_owned(),
        RemoteResource::Loaded(AvailableTagValues(vec![
            "value1".to_owned(),
            "value2".to_owned(),
        ])),
    );
    map.insert(
        "exampleTag2".to_owned(),
        RemoteResource::Loaded(AvailableTagValues(vec![
            "a value".to_owned(),
            "another value".to_owned(),
        ])),
    );

    model.available_tags = RemoteResource::Loaded(AvailableTags(map));
    crux_core::render::render().and(flux_get_measurements_existing_fields(
        model,
        user_messages,
        example_data,
    ))
}

fn real(
    model: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    model.flux_call(
        format!(
            r#"
            import "influxdata/influxdb/schema"
            schema.measurementTagKeys(bucket: "{}", measurement: "{}",)
        "#,
            // todo crap this defaults to 30 days need to add (start: -xd )
            model.bucket,
            model.measurement,
        ),
        user_messages,
        construct_response_event,
    )
}

//  F: FnOnce(crate::Result<Response<ExpectBody>>) -> Event + Send + 'static,
fn construct_response_event(body: crux_http::Result<crux_http::Response<String>>) -> Event {
    EvInputFieldsAndTagsEventsScreen(
        ScEvInputFieldsAndTags::FluxGetMeasurementsExistingTagsResponse(body),
    )
}
