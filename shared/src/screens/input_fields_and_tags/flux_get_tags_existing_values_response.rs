use crate::internal_error_strings::N052_USED;
use crate::model::UserMessages;
use crate::screens::input_fields_and_tags::RemoteResource::Loaded;
use crate::screens::input_fields_and_tags::{AvailableTagValues, InputFieldsAndTagsScreenData};
use crate::{Effect, Event};
use crux_core::Command;
use crux_http::Response;
use csv::ReaderBuilder;
use log::Level;
use uniffi::deps::log::info;

#[derive(Debug, serde::Deserialize)]
struct Record {
    _value: String,
}

pub fn flux_get_tags_existing_values_response(
    model: &mut InputFieldsAndTagsScreenData,
    response: crux_http::Result<Response<String>>,
    user_messages: &mut UserMessages,
    saved_tag: String,
) -> Command<Effect, Event> {
    // todo remove unwraps
    let binding = match response {
        Ok(binding) => binding,
        Err(err) => {
            user_messages.add_message(
                format!("We got an error requesting tag {:?}", err),
                Level::Error,
            );
            return crux_core::render::render();
        }
    };

    info!(
        "flux_get_measurements_existing_tags_response: {}",
        binding.body().unwrap()
    );

    let mut rdr = ReaderBuilder::new().from_reader(binding.body().unwrap().as_bytes());
    let deserialize_iter = rdr.deserialize();

    // get tags result in a csv list of tag values, the bellow is pulling out all the values from the
    // csv to display to user as an option
    // it's possible for default tags for this list to be empty
    let mut values = vec![];
    for result in deserialize_iter {
        let record: Record = result.unwrap();
        values.push(record._value);
    }

    // This one first as it is less likely to have an entry and therefor reduces the need to clone the array
    model
        .tags
        .entry(saved_tag.clone())
        .and_modify(|multi_select| multi_select.options = Loaded(values.clone()));

    let Loaded(available_tags) = &mut model.available_tags else {
        user_messages.error_report_to_developer(N052_USED);
        return crux_core::render::render();
    };

    available_tags
        .entry(saved_tag)
        .and_modify(|array| *array = Loaded(AvailableTagValues(values)));
    crux_core::render::render()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::{Model, Screens};
    use crate::screens::input_fields_and_tags::{AvailableTags, LoadingResource, RemoteResource};
    use crate::Counter;
    use crux_core::testing::AppTester;
    use crux_http::testing::ResponseBuilder;
    use std::collections::BTreeMap;
    use RemoteResource::NotLoaded;

    fn base_test_screen_data() -> InputFieldsAndTagsScreenData {
        InputFieldsAndTagsScreenData {
            available_tags: Loaded(AvailableTags(BTreeMap::from([
                (
                    "tag1".to_owned(),
                    NotLoaded(LoadingResource {
                        retry_count: 0,
                        failure_reasons: vec![],
                        current_state: Default::default(),
                    }),
                ),
                ("tag2".to_owned(), Loaded(AvailableTagValues(vec![]))),
                (
                    "tag2".to_owned(),
                    Loaded(AvailableTagValues(vec![
                        "value1".to_owned(),
                        "value2".to_owned(),
                    ])),
                ),
            ]))),
            ..Default::default()
        }
    }
    #[test]
    fn happy_path() {
        let app = AppTester::<Counter>::default();

        let mut screen_model = base_test_screen_data();
        let mut user_messages: UserMessages = Default::default();

        flux_get_tags_existing_values_response(
            &mut screen_model,
            Ok(ResponseBuilder::ok()
                .body(",result,table,_value,tag_key\n,_result,0,Push-up,tag1\n".to_owned())
                .build()),
            &mut user_messages,
            "tag1".to_owned(),
        );

        let model = Model {
            screen: Screens::InputFieldsTagsScreen(screen_model),
            user_messages,
            ..Default::default()
        };

        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(& model), @r#"
        screen:
          InputFieldsTagsScreen:
            base_url: "http://notaurl.com/"
            org: fake_name
            bucket: ""
            measurement: ""
            available_tags:
              Loaded:
                tag1:
                  Loaded:
                    - Push-up
                tag2:
                  Loaded:
                    - value1
                    - value2
            tags: {}
            new_tags: []
            available_fields:
              Loading:
                retry_count: 0
                current_state: Initialised
            fields: {}
            new_fields: []
            line: ""
            ui_time: ~
            precision: Nanoseconds
            expected_line: "  "
        user_messages:
          messages: []
        tutorial: ~
        "#);
    }

    /// seen in the wild where I don't get a response but im not really sure why I shall investigate
    /// at a later stage
    /// I think this happens because influx has default tags so although the tags exist there are no
    /// values not really sure why, but it happens
    #[test]
    fn returns_nothing() {
        let app = AppTester::<Counter>::default();

        let mut screen_model = base_test_screen_data();
        let mut user_messages: UserMessages = Default::default();

        flux_get_tags_existing_values_response(
            &mut screen_model,
            Ok(ResponseBuilder::ok()
                .body(",result,table,_value,tag_key\n\n".to_owned())
                .build()),
            &mut user_messages,
            "tag1".to_owned(),
        );

        let model = Model {
            screen: Screens::InputFieldsTagsScreen(screen_model),
            user_messages,
            ..Default::default()
        };

        // check that the app asked the shell to render
        // assert_effect!(update, Effect::Render(_));

        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(&model), @r#"
        ---
        screen:
          InputFieldsTagsScreen:
            base_url: "http://notaurl.com/"
            org: fake_name
            bucket: ""
            measurement: ""
            available_tags:
              Loaded:
                tag1:
                  Loaded: []
                tag2:
                  Loaded:
                    - value1
                    - value2
            tags: {}
            new_tags: []
            available_fields:
              Loading:
                retry_count: 0
                current_state: Initialised
            fields: {}
            new_fields: []
            line: ""
            ui_time: ~
            precision: Nanoseconds
            expected_line: "  "
        user_messages:
          messages: []
        tutorial: ~
        "#);
    }
}
