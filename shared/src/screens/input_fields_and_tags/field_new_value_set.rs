use crate::internal_error_strings::{N002_USED, N017_USED};
use crate::model::UserMessages;
use crate::model_ui::screens_common::remote_multi_choice::multi_choice::UiStringIndex;
use crate::screens::input_fields_and_tags::InputFieldsAndTagsScreenData;
use crate::{Effect, Event};
use crux_core::Command;

pub fn field_new_value_set(
    new_value: String,
    index: UiStringIndex,
    screen_data: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    let Ok(index) = index.to_bound_usize(screen_data.new_fields.len()) else {
        user_messages.error_report_to_developer(N002_USED);
        return crux_core::render::render();
    };

    let Some(new_field) = screen_data.new_fields.get_mut(index) else {
        user_messages.error_report_to_developer(N017_USED);
        return crux_core::render::render();
    };

    new_field.1.update_user_input(new_value);
    crux_core::render::render()
}
