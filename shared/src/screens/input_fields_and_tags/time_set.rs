use crate::screens::input_fields_and_tags::InputFieldsAndTagsScreenData;
use crate::{Effect, Event};
use chrono::DateTime;
use crux_core::Command;
use log::trace;

// oki so time input in web only returns if it has a valid time or no time whatsoever when it's
// cleared, think ill continue the model in android
// time value is "2024-08-05T10:11:00Z"
// time value is ""
pub fn time_set(time: String, data: &mut InputFieldsAndTagsScreenData) -> Command<Effect, Event> {
    trace!("time value is {time}");
    if time.is_empty() {
        data.time = None;
    } else {
        data.time = Some(
            DateTime::parse_from_rfc3339(&time)
                .unwrap()
                .timestamp_nanos_opt()
                .unwrap(),
        );
    }
    crux_core::render::render()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::{Model, Screens, UserMessages};
    use crate::Counter;
    use crux_core::testing::AppTester;

    #[test]
    fn happy_path() {
        let app = AppTester::<Counter>::default();

        let mut screen_model = InputFieldsAndTagsScreenData {
            ..Default::default()
        };
        let user_messages: UserMessages = Default::default();

        time_set("2024-08-05T10:01:00Z".to_owned(), &mut screen_model);

        let model = Model {
            screen: Screens::InputFieldsTagsScreen(screen_model),
            user_messages,
            ..Default::default()
        };

        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(& model), @r#"
        ---
        screen:
          InputFieldsTagsScreen:
            base_url: "http://notaurl.com/"
            org: fake_name
            bucket: ""
            measurement: ""
            available_tags:
              Loading:
                retry_count: 0
                current_state: Initialised
            tags: {}
            new_tags: []
            available_fields:
              Loading:
                retry_count: 0
                current_state: Initialised
            fields: {}
            new_fields: []
            line: ""
            ui_time: "2024-08-05T10:01:00+00:00"
            precision: Nanoseconds
            expected_line: "  1722852060000000000"
        user_messages:
          messages: []
        tutorial: ~
        "#);
    }

    #[test]
    fn happy_path_empty_time() {
        let app = AppTester::<Counter>::default();

        let mut screen_model = InputFieldsAndTagsScreenData {
            ..Default::default()
        };
        let user_messages: UserMessages = Default::default();

        time_set("".to_owned(), &mut screen_model);

        let model = Model {
            screen: Screens::InputFieldsTagsScreen(screen_model),
            user_messages,
            ..Default::default()
        };

        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(& model), @r#"
        ---
        screen:
          InputFieldsTagsScreen:
            base_url: "http://notaurl.com/"
            org: fake_name
            bucket: ""
            measurement: ""
            available_tags:
              Loading:
                retry_count: 0
                current_state: Initialised
            tags: {}
            new_tags: []
            available_fields:
              Loading:
                retry_count: 0
                current_state: Initialised
            fields: {}
            new_fields: []
            line: ""
            ui_time: ~
            precision: Nanoseconds
            expected_line: "  "
        user_messages:
          messages: []
        tutorial: ~
        "#);
    }
}
