use crate::internal_error_strings::N041_USED;
use crate::model::{Screens, UserMessages};
use crate::screens::input_fields_and_tags::RemoteResource;
use crate::screens::select_org_bucket_measurements::{
    validate_measurement_string, SelectOrgBucketMeasurementScreenData,
};
use crate::screens_common::model::user_input_or_multi_choice::UserInputOrMultiChoice;
use crate::{Effect, Event};
use crux_core::Command;
use log::{error, trace};

pub fn back_button_pressed(
    screen: &mut Screens,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    trace!("select org,bucket,measurement screen event: Validating model");
    //
    let Screens::InputFieldsTagsScreen(ref mut screen_data) = screen else {
        error!("Screen was in an unexpected state {:?}", screen);
        user_messages.error_report_to_developer(N041_USED);
        return crux_core::render::render();
    };
    trace!("Running select org,bucket,measurement event");

    // for now im going to go back and deselect all the user selections... maybe I'll chang my mind on that later
    let select_org_bucket_measurement = SelectOrgBucketMeasurementScreenData {
        api_token: screen_data.api_token.clone(),
        base_url: screen_data.base_url.clone(),
        orgs: Default::default(),
        buckets: Default::default(),
        measurements: UserInputOrMultiChoice {
            options: RemoteResource::default(),
            selected: None,
            notify_user_of_mode_switch: None,
            user_validating_function: validate_measurement_string,
        },
    };

    *screen = Screens::SelectOrgBucketMeasurementScreen(Box::from(select_org_bucket_measurement));

    crux_core::render::render()
}
