use crate::internal_error_strings::N036_USED;
use crate::model::{ExampleData, UserMessages};
use crate::screens::input_fields_and_tags::{
    AvailableFields, InputFieldsAndTagsScreenData, LoadingState, RemoteResource,
    ScEvInputFieldsAndTags,
};
use crate::screens_common::model::field_type::FieldType;
use crate::Event::EvInputFieldsAndTagsEventsScreen;
use crate::{Effect, Event};
use crux_core::render::render;
use crux_core::Command;
use log::trace;
use std::collections::BTreeMap;

/// gets all the existing fields in a measurement
///
///https://docs.influxdata.com/flux/v0/stdlib/influxdata/influxdb/schema/measurementfieldkeys/
///
///import "influxdata/influxdb/schema"
///
/// schema.measurementFieldKeys(bucket: "example-bucket", measurement: "example-measurement")
pub fn flux_get_measurements_existing_fields(
    model: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
    example_data: &ExampleData,
) -> Command<Effect, Event> {
    trace!("flux_get_measurements_existing_fields");

    match example_data {
        ExampleData::Tutorial(_) | ExampleData::FakeData => fake(model),
        ExampleData::Real => real(model, user_messages),
    }
}

fn fake(model: &mut InputFieldsAndTagsScreenData) -> Command<Effect, Event> {
    trace!("Adding fake fields");
    let mut map = BTreeMap::new();

    map.insert("a_boolean".to_owned(), FieldType::Bool);
    map.insert("a_float".to_owned(), FieldType::Float);
    map.insert("an_unsigned_int".to_owned(), FieldType::UInt);
    map.insert("an_int".to_owned(), FieldType::Int);
    map.insert("a_string".to_owned(), FieldType::String);

    model.available_fields = RemoteResource::Loaded(AvailableFields(map));
    render()
}

fn real(
    screen_data: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    trace!("Fetching real fields");
    let command = screen_data.flux_call(
        format!(
            r#"
                import "array"
                import "influxdata/influxdb/schema"
                import "types"

                getType = (v) => {{
                    _types = [
                        "string",
                        "int",
                        "uint",
                        "float",
                        "bool",
                    ]
                    _eval = array.map(arr: _types, fn: (x) => ({{type: x, is: types.isType(v: v, type: x)}}))
                    _type = array.filter(arr: _eval, fn: (x) => x.is == true)

                    return _type[0].type
                }}

                from(bucket: "{}")
                    |> range(start: 0)
                    |> filter(fn: (r) => r["_measurement"] == "{}")
                    |> limit(n: 1)
                    |> map(fn: (r) => ({{_field: r._field, _value: getType(v: r._value)}}))
                    |> unique(column: "_field")
        "#,
            screen_data.bucket,
            screen_data.measurement
        ),
        user_messages,
        construct_response_event,
    );

    let RemoteResource::NotLoaded(loading_fields) = &mut screen_data.available_fields else {
        user_messages.error_report_to_developer(N036_USED);
        return command.and(render());
    };

    loading_fields.current_state = LoadingState::RequestSent;

    command.and(render())
}

//  F: FnOnce(crate::Result<Response<ExpectBody>>) -> Event + Send + 'static,
fn construct_response_event(body: crux_http::Result<crux_http::Response<String>>) -> Event {
    EvInputFieldsAndTagsEventsScreen(
        ScEvInputFieldsAndTags::FluxGetMeasurementsExistingFieldsResponse(body),
    )
}
