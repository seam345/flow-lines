use crate::model::UserMessages;
use crate::screens::input_fields_and_tags::{InputFieldsAndTagsScreenData, TagKey};
use crate::{Effect, Event};
use crux_core::Command;
use log::{error, Level};

pub fn tag_existing_value_dismiss_mode_change(
    key: TagKey,
    model: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    let Some(entry) = model.tags.get_mut(&key) else {
        error!("Key didnt exist, in theory this shouldn't happen");
        user_messages.add_message(crate::internal_error_strings::N029_USED, Level::Error);
        return crux_core::render::render();
    };

    entry.dismiss_mode_switch();
    crux_core::render::render()
}
