mod field_existing_add;
mod tag_existing_add;
// todo ideally these shouldn't be public
mod back_button_pressed;
mod field_existing_remove;
mod field_existing_value_set;
mod field_new_add;
mod field_new_change_type;
mod field_new_key_set;
mod field_new_remove;
mod field_new_value_set;
pub mod flux_get_measurements_existing_fields;
pub mod flux_get_measurements_existing_fields_response;
pub mod flux_get_measurements_existing_tags;
pub mod flux_get_measurements_existing_tags_response;
mod flux_get_tags_existing_values;
mod flux_get_tags_existing_values_response;
mod influx_write;
mod influx_write_response;
mod line_protocol_submit_line;
mod tag_existing_remove;
mod tag_existing_value_accept_mode_change;
mod tag_existing_value_dismiss_mode_change;
mod tag_existing_value_set_index;
mod tag_existing_value_set_user;
mod tag_new_add;
mod tag_new_key_set;
mod tag_new_remove;
mod tag_new_value_set;
mod time_set;

use crate::internal_error_strings::N065_USED;
use crate::model::{Model, Screens, UserMessages};
use crate::model_ui::screens::input_fields_and_tags::UiFieldType;
use crate::model_ui::screens_common::remote_multi_choice::multi_choice::UiStringIndex;
use crate::screens::input_fields_and_tags::back_button_pressed::back_button_pressed;
use crate::screens::input_fields_and_tags::field_existing_add::field_existing_add;
use crate::screens::input_fields_and_tags::field_existing_remove::field_existing_remove;
use crate::screens::input_fields_and_tags::field_existing_value_set::{
    field_existing_value_set, FieldKey, FieldValue,
};
use crate::screens::input_fields_and_tags::field_new_add::field_new_add;
use crate::screens::input_fields_and_tags::field_new_change_type::field_new_change_type;
use crate::screens::input_fields_and_tags::field_new_key_set::field_new_key_set;
use crate::screens::input_fields_and_tags::field_new_remove::field_new_remove;
use crate::screens::input_fields_and_tags::field_new_value_set::field_new_value_set;
use crate::screens::input_fields_and_tags::flux_get_measurements_existing_fields::flux_get_measurements_existing_fields;
use crate::screens::input_fields_and_tags::flux_get_measurements_existing_fields_response::flux_get_measurements_existing_fields_response;
use crate::screens::input_fields_and_tags::flux_get_measurements_existing_tags::flux_get_measurements_existing_tags;
use crate::screens::input_fields_and_tags::flux_get_measurements_existing_tags_response::flux_get_measurements_existing_tags_response;
use crate::screens::input_fields_and_tags::flux_get_tags_existing_values_response::flux_get_tags_existing_values_response;
use crate::screens::input_fields_and_tags::influx_write::influx_write;
use crate::screens::input_fields_and_tags::influx_write_response::influx_write_response;
use crate::screens::input_fields_and_tags::tag_existing_add::tag_existing_add;
use crate::screens::input_fields_and_tags::tag_existing_remove::tag_existing_remove;
use crate::screens::input_fields_and_tags::tag_existing_value_accept_mode_change::tag_existing_value_accept_mode_change;
use crate::screens::input_fields_and_tags::tag_existing_value_dismiss_mode_change::tag_existing_value_dismiss_mode_change;
use crate::screens::input_fields_and_tags::tag_existing_value_set_index::tag_existing_value_set_index;
use crate::screens::input_fields_and_tags::tag_existing_value_set_user::tag_existing_value_set_user;
use crate::screens::input_fields_and_tags::tag_new_add::tag_new_add;
use crate::screens::input_fields_and_tags::tag_new_key_set::tag_new_key_set;
use crate::screens::input_fields_and_tags::tag_new_remove::tag_new_remove;
use crate::screens::input_fields_and_tags::tag_new_value_set::tag_new_value_set;
use crate::screens::input_fields_and_tags::time_set::time_set;
use crate::screens::input_fields_and_tags::RemoteResource::Loaded;
use crate::screens::select_org_bucket_measurements::influx_get_orgs_response::Org;
use crate::screens::select_org_bucket_measurements::SelectOrgBucketMeasurementScreenData;
use crate::screens_common::influx::flux_call;
use crate::screens_common::model::field_type::{FieldType, UserFieldValue};
use crate::screens_common::model::user_input_or_multi_choice::{
    UserInputOrMultiChoice, ValidatingStringPreTrimmed,
};
use crate::{Effect, Event};
use crux_core::Command;
use crux_http::http::Url;
use crux_http::Response;
use log::{error, trace};
use serde::{Deserialize, Serialize};
use std::collections::btree_map::{Entry, Iter, Keys};
use std::collections::BTreeMap;
use std::fmt::{Display, Formatter};

// ============================================================================
// Screen data.
// ============================================================================
pub type TagKey = String;
pub type TagValue = String;

#[derive(Clone, Debug)]
pub struct InputFieldsAndTagsScreenData {
    pub api_token: String,
    pub base_url: Url,
    pub org: Org,
    pub bucket: String,
    pub measurement: String,
    pub available_tags: RemoteResource<AvailableTags>,
    pub tags: BTreeMap<TagKey, UserInputOrMultiChoice>,
    pub new_tags: Vec<UserTag>,
    // I tried to put this into the above tags, but it gets so messy it's not worth it
    pub available_fields: RemoteResource<AvailableFields>,
    // btree because when it gets converted to a vec in ui_model im too lazy to sort it, and it's messing with snapshots
    pub fields: BTreeMap<String, UserFieldValue>,
    pub new_fields: Vec<(ValidatingStringPreTrimmed, UserFieldValue)>,
    pub line: String,
    // https://docs.influxdata.com/influxdb/cloud/reference/syntax/line-protocol/#unix-timestamp
    // Minimum timestamp=-9223372036854775806, Maximum timestamp=9223372036854775806. so i64 +2 and -1 (oddly)
    pub time: Option<i64>,
    pub precision: Precision,
}

impl TryFrom<&mut Box<SelectOrgBucketMeasurementScreenData>> for InputFieldsAndTagsScreenData {
    type Error = &'static str;

    fn try_from(
        value: &mut Box<SelectOrgBucketMeasurementScreenData>,
    ) -> Result<Self, Self::Error> {
        trace!("validate org");
        let Loaded(multi_org) = &value.orgs else {
            error!("Org value was not loaded, unable to convert SelectOrgBucketMeasurement to InputFieldsAndTags");
            return Err("org value not loaded");
        };
        let Some(org) = multi_org.selected_as_option() else {
            error!("Org value was none, unable to convert SelectOrgBucketMeasurement to InputFieldsAndTags");
            return Err("Missing org value");
        };

        trace!("validate bucket");
        let Loaded(multi_buckets) = &value.buckets else {
            error!("Bucket value was not loaded, unable to convert SelectOrgBucketMeasurement to InputFieldsAndTags");
            return Err("bucket value not loaded");
        };
        let Some(bucket) = multi_buckets.selected_as_option() else {
            error!("Selected bucket value was none, unable to convert SelectOrgBucketMeasurement to InputFieldsAndTags");
            return Err("Missing bucket value");
        };

        trace!("validate measurement");
        let Some(measurement) = value.measurements.valid() else {
            error!("Selected measurement value was none, unable to convert SelectOrgBucketMeasurement to InputFieldsAndTags");
            return Err("Measurement value is invalid");
        };

        Ok(InputFieldsAndTagsScreenData {
            api_token: value.api_token.clone(),
            base_url: value.base_url.clone(),
            org: org.clone(),
            bucket: bucket.to_string(),
            measurement: measurement.to_string(),
            available_tags: Default::default(),
            tags: Default::default(),
            new_tags: vec![],
            available_fields: Default::default(),
            fields: Default::default(),
            new_fields: vec![],
            line: "".to_string(),
            time: None,
            precision: Default::default(),
        })
    }
}

impl InputFieldsAndTagsScreenData {
    pub fn flux_call<F>(
        &self,
        flux_query: String,
        user_messages: &mut UserMessages,
        event_to_send: F,
    ) -> Command<Effect, Event>
    where
        F: FnOnce(crux_http::Result<Response<String>>) -> Event + Send + 'static,
    {
        flux_call(
            user_messages,
            &self.org,
            &self.base_url,
            &self.api_token,
            flux_query,
            event_to_send,
        )
    }
}

//<editor-fold desc="...">
#[derive(Clone, Debug, Default)]
pub struct AvailableTags(BTreeMap<TagKey, RemoteResource<AvailableTagValues>>);

impl AvailableTags {
    pub fn iter(&self) -> Iter<TagKey, RemoteResource<AvailableTagValues>> {
        self.0.iter()
    }

    pub fn get(&self, key: &TagKey) -> Option<&RemoteResource<AvailableTagValues>> {
        self.0.get(key)
    }

    pub fn get_mut(&mut self, key: &TagKey) -> Option<&mut RemoteResource<AvailableTagValues>> {
        self.0.get_mut(key)
    }

    pub fn entry(&mut self, key: TagKey) -> Entry<'_, TagKey, RemoteResource<AvailableTagValues>> {
        self.0.entry(key)
    }
}

// impl Default for  {
//
// }

#[derive(Clone, Debug, Default)]
pub struct AvailableTagValues(pub(crate) Vec<TagValue>);

#[derive(Clone, Debug)]
pub struct AvailableFields(BTreeMap<String, FieldType>);

impl AvailableFields {
    pub fn keys(&self) -> Keys<'_, String, FieldType> {
        self.0.keys()
    }

    pub fn get(&self, key: &str) -> Option<&FieldType> {
        self.0.get(key)
    }
}

// #[derive(Clone, Debug)]
// pub struct Fields(BTreeMap<String, UserFieldValue>);

#[derive(Clone, Debug, PartialEq)]
pub enum RemoteResource<T> {
    NotLoaded(LoadingResource),
    // used for stuff like file not existing, not necessarily no data. Only used when the end result
    // will be a disabled input
    NonExistent,
    Loaded(T),
}

impl<T> Default for RemoteResource<T> {
    fn default() -> Self {
        RemoteResource::NotLoaded(LoadingResource::default())
    }
}

#[derive(Clone, Debug, PartialEq, Default)]
pub struct LoadingResource {
    pub retry_count: u8,
    pub failure_reasons: Vec<String>,
    // for debugging
    pub current_state: LoadingState,
}

#[derive(Clone, Debug, PartialEq, Default)]
pub enum LoadingState {
    AwaitingUserInteraction,
    RequestSent,
    #[default]
    Initialised,
}

#[derive(Clone, Debug, PartialEq)]
pub struct UserTag {
    pub key: Option<TagKey>,
    pub value: Option<TagValue>,
}

/// Tags can be arbitrarily added at any point, but most the time they will already exist, this allows us to validate ui message
#[derive(Clone, Debug, PartialEq)]
pub enum SelectedTagType {
    None,
    PreExisting(TagValue),
    UserDefined(Option<TagValue>),
}

impl SelectedTagType {
    pub fn is_some(&self) -> bool {
        match self {
            SelectedTagType::None => false,
            SelectedTagType::PreExisting(_) => true,
            SelectedTagType::UserDefined(value) => value.is_some(),
        }
    }

    pub fn unwrap_or(self, other: String) -> String {
        match self {
            SelectedTagType::None => other,
            SelectedTagType::PreExisting(value) => value,
            SelectedTagType::UserDefined(value) => value.unwrap_or(other),
        }
    }
}

pub enum WriteFieldValue {
    Bool(bool),
    Float(f64),
    Int(i64),
    UInt(u64),
    String(String),
}

impl TryFrom<UserFieldValue> for WriteFieldValue {
    type Error = ();

    fn try_from(_value: UserFieldValue) -> Result<Self, Self::Error> {
        todo!()
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Default)]
pub enum Precision {
    Seconds,
    //s
    Milliseconds,
    // ms
    Microseconds,
    // us
    #[default]
    Nanoseconds, //ns
}

impl Display for Precision {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Precision::Seconds => write!(f, "s"),
            Precision::Milliseconds => write!(f, "ms"),
            Precision::Microseconds => write!(f, "us"),
            Precision::Nanoseconds => write!(f, "ns"),
        }
    }
}

// ============================================================================
// Screen event data.
// ============================================================================

// todo learn and implement
pub(crate) fn validate_tag_value(_value: &str) -> Result<(), String> {
    // if value.trim().contains(" ") {
    //     return Err("Cannot contain spaces".to_owned());
    // }
    if _value.starts_with('_') {
        return Err("Cannot start with an _ this is reserved for Influx".to_owned());
    }
    Ok(())
}

// todo learn and implement
pub(crate) fn validate_field_key(_value: &str) -> Result<(), String> {
    // if value.trim().contains(" ") {
    //     return Err("Cannot contain spaces".to_owned());
    // }
    // if _value.starts_with('_') {
    //     return Err("Cannot start with an _ this is reserved for Influx".to_owned());
    // }
    Ok(())
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum ScEvInputFieldsAndTags {
    BackButtonPressed,
    FieldExistingAdd(String),
    FieldExistingRemove(String),
    FieldExistingValueSet(FieldKey, FieldValue),
    FieldNewAdd,
    FieldNewRemove(UiStringIndex),
    FieldNewChangeType(UiStringIndex, UiFieldType),
    FieldNewValueSet(UiStringIndex, String),
    FieldNewKeySet(UiStringIndex, String),
    FluxGetMeasurementsExistingFields,
    #[serde(skip)]
    FluxGetMeasurementsExistingFieldsResponse(crux_http::Result<Response<String>>),
    FluxGetMeasurementsExistingTags,
    #[serde(skip)]
    FluxGetMeasurementsExistingTagsResponse(crux_http::Result<Response<String>>),
    FluxGetTagsExistingValues,
    #[serde(skip)]
    FluxGetTagsExistingValuesResponse(crux_http::Result<Response<String>>, String),
    InfluxWrite,
    #[serde(skip)]
    InfluxWriteResponse(crux_http::Result<Response<Vec<u8>>>),
    LineProtocolSubmitLine,
    TagExistingAdd(String),
    TagExistingRemove(String),
    TagExistingValueAcceptModeChange(TagKey),
    TagExistingValueDismissModeChange(TagKey),
    TagExistingValueSetIndex(TagKey, UiStringIndex),
    TagExistingValueSetUser(TagKey, TagValue),
    TagNewAdd(),
    TagNewKaySet(UiStringIndex, String),
    TagNewRemove(UiStringIndex),
    TagNewValueSet(UiStringIndex, String),
    TimeSet(String),
}

// ============================================================================
// Screen event processing.
// ============================================================================
//</editor-fold>
pub fn input_fields_and_tags_events(
    initial_screen_event: ScEvInputFieldsAndTags,
    model: &mut Model,
) -> Command<Effect, Event> {
    trace!("input_fields_and_tags screen event: Validating model");

    let Screens::InputFieldsTagsScreen(ref mut screen_data) = model.screen else {
        error!("Screen was in an unexpected state {:?}", model.screen);
        model.user_messages.error_report_to_developer(N065_USED);
        return crux_core::render::render();
    };

    trace!("Running input fields and tags event");

    match initial_screen_event {
        ScEvInputFieldsAndTags::BackButtonPressed => {
            back_button_pressed(&mut model.screen, &mut model.user_messages)
        }

        ScEvInputFieldsAndTags::FieldExistingAdd(field) => {
            field_existing_add(screen_data, field, &mut model.user_messages)
        }
        ScEvInputFieldsAndTags::FieldExistingRemove(field) => {
            field_existing_remove(screen_data, field)
        }
        ScEvInputFieldsAndTags::FieldExistingValueSet(key, value) => {
            field_existing_value_set(key, value, screen_data, &mut model.user_messages)
        }
        ScEvInputFieldsAndTags::FluxGetMeasurementsExistingFields => {
            flux_get_measurements_existing_fields(
                screen_data,
                &mut model.user_messages,
                &model.example_data,
            )
        }
        ScEvInputFieldsAndTags::FluxGetMeasurementsExistingFieldsResponse(response) => {
            flux_get_measurements_existing_fields_response(
                screen_data,
                response,
                &mut model.user_messages,
                &model.example_data,
            )
        }
        ScEvInputFieldsAndTags::FluxGetMeasurementsExistingTags => {
            flux_get_measurements_existing_tags(
                screen_data,
                &mut model.user_messages,
                &model.example_data,
            )
        }
        ScEvInputFieldsAndTags::FluxGetMeasurementsExistingTagsResponse(response) => {
            flux_get_measurements_existing_tags_response(
                screen_data,
                response,
                &mut model.user_messages,
                &model.example_data,
            )
        }
        ScEvInputFieldsAndTags::FluxGetTagsExistingValues => Command::done(),
        ScEvInputFieldsAndTags::FluxGetTagsExistingValuesResponse(response, tag) => {
            flux_get_tags_existing_values_response(
                screen_data,
                response,
                &mut model.user_messages,
                tag,
            )
        }
        ScEvInputFieldsAndTags::InfluxWrite => influx_write(
            &mut model.screen,
            &mut model.user_messages,
            &model.example_data,
        ),
        ScEvInputFieldsAndTags::InfluxWriteResponse(response) => {
            influx_write_response(response, model)
        }
        ScEvInputFieldsAndTags::LineProtocolSubmitLine => Command::done(),
        ScEvInputFieldsAndTags::TagExistingAdd(tag) => tag_existing_add(
            screen_data,
            &mut model.user_messages,
            tag,
            &model.example_data,
        ),
        ScEvInputFieldsAndTags::TagExistingRemove(tag) => tag_existing_remove(screen_data, tag),
        ScEvInputFieldsAndTags::TagExistingValueAcceptModeChange(key) => {
            tag_existing_value_accept_mode_change(key, screen_data, &mut model.user_messages)
        }
        ScEvInputFieldsAndTags::TagExistingValueDismissModeChange(key) => {
            tag_existing_value_dismiss_mode_change(key, screen_data, &mut model.user_messages)
        }
        ScEvInputFieldsAndTags::TagExistingValueSetIndex(key, value) => {
            tag_existing_value_set_index(key, value, screen_data, &mut model.user_messages)
        }
        ScEvInputFieldsAndTags::TagExistingValueSetUser(key, value) => {
            tag_existing_value_set_user(key, value, screen_data, &mut model.user_messages)
        }
        ScEvInputFieldsAndTags::TagNewAdd() => tag_new_add(screen_data, &mut model.user_messages),
        ScEvInputFieldsAndTags::TagNewKaySet(string_index, key) => {
            tag_new_key_set(string_index, key, screen_data, &mut model.user_messages)
        }
        ScEvInputFieldsAndTags::TagNewRemove(string_index) => {
            tag_new_remove(string_index, screen_data, &mut model.user_messages)
        }
        ScEvInputFieldsAndTags::TagNewValueSet(string_index, value) => {
            tag_new_value_set(string_index, value, screen_data, &mut model.user_messages)
        }
        ScEvInputFieldsAndTags::TimeSet(time) => time_set(time, screen_data),
        ScEvInputFieldsAndTags::FieldNewAdd => field_new_add(screen_data, &mut model.user_messages),
        ScEvInputFieldsAndTags::FieldNewRemove(string_index) => {
            field_new_remove(string_index, screen_data, &mut model.user_messages)
        }
        ScEvInputFieldsAndTags::FieldNewValueSet(string_index, new_value) => field_new_value_set(
            new_value,
            string_index,
            screen_data,
            &mut model.user_messages,
        ),
        ScEvInputFieldsAndTags::FieldNewKeySet(string_index, new_key) => {
            field_new_key_set(new_key, string_index, screen_data, &mut model.user_messages)
        }
        ScEvInputFieldsAndTags::FieldNewChangeType(string_index, new_type) => {
            field_new_change_type(
                new_type,
                string_index,
                screen_data,
                &mut model.user_messages,
            )
        }
    }
}

// ============================================================================
// Screen test helper.
// ============================================================================

#[cfg(test)]
impl Default for InputFieldsAndTagsScreenData {
    fn default() -> Self {
        InputFieldsAndTagsScreenData {
            api_token: "".to_string(),
            base_url: Url::parse("http://notaurl.com").unwrap(),
            org: Org {
                name: "fake_name".to_string(),
                id: "fake_id".to_string(),
            },
            bucket: "".to_string(),
            measurement: "".to_string(),
            available_tags: Default::default(),
            tags: Default::default(),
            new_tags: vec![],
            available_fields: Default::default(),
            fields: Default::default(),
            new_fields: vec![],
            line: "".to_string(),
            precision: Default::default(),
            time: None,
        }
    }
}
