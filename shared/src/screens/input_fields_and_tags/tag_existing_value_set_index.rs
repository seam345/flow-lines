use crate::internal_error_strings::{N044_USED, N049_USED};
use crate::model::UserMessages;
use crate::model_ui::screens_common::remote_multi_choice::multi_choice::UiStringIndex;
use crate::screens::input_fields_and_tags::{InputFieldsAndTagsScreenData, TagKey};
use crate::screens_common::model::user_input_or_multi_choice::SelectStringIndexError;
use crate::{Effect, Event};
use crux_core::Command;
use log::{error, Level};

pub fn tag_existing_value_set_index(
    key: TagKey,
    index: UiStringIndex,
    model: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    let Some(entry) = model.tags.get_mut(&key) else {
        error!("Key didnt exist, in theory this shouldn't happen");
        user_messages.add_message(crate::internal_error_strings::N028_USED, Level::Error);
        return crux_core::render::render();
    };

    match entry.select_string_index(index, true) {
        Ok(_) => Command::done(),
        Err(SelectStringIndexError::UiStringIndexErrors(_)) => {
            user_messages.add_message(crate::internal_error_strings::N026_USED, Level::Error);
            crux_core::render::render()
        }
        Err(SelectStringIndexError::Other(_)) => {
            user_messages.add_message(crate::internal_error_strings::N027_USED, Level::Error);
            crux_core::render::render()
        }
        Err(SelectStringIndexError::NotLoadedError(_)) => {
            user_messages.error_report_to_developer(N044_USED);
            crux_core::render::render()
        }
        Err(SelectStringIndexError::SelectIndexError(_)) => {
            user_messages.error_report_to_developer(N049_USED);
            crux_core::render::render()
        }
    }
}
