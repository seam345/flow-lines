use crate::internal_error_strings::N020_USED;
use crate::model::{ExampleData, Screens, UserMessages};
use crate::screens::input_fields_and_tags::{ScEvInputFieldsAndTags, TagKey};
use crate::screens_common::influx::generate_flux_line;
use crate::screens_common::model::field_type::UserFieldValue;
use crate::screens_common::model::user_input_or_multi_choice::UserInputOrMultiChoice;
use crate::Event::EvInputFieldsAndTagsEventsScreen;
use crate::{Effect, Event};
use crux_core::Command;
use log::{error, trace, Level};
use std::collections::btree_map::Iter;

/// Writes the line to influx, will also do data validation, warning user of anything missing before
/// writing to influx
pub fn influx_write(
    screen: &mut Screens,
    user_messages: &mut UserMessages,
    example_data: &ExampleData,
) -> Command<Effect, Event> {
    trace!("Input fields and tag event: Validating model");

    let Screens::InputFieldsTagsScreen(ref mut screen_data) = screen else {
        error!("Screen was in an unexpected state {:?}", screen);
        user_messages.add_message(N020_USED, Level::Error);
        return crux_core::render::render();
    };

    if !tags_valid(screen_data.tags.iter()) {
        user_messages.add_message("There is an invalid value for a tag", Level::Warn);
        return crux_core::render::render();
    }

    // todo validate new tags
    // if !new_tags_valid(screen_data.tags.iter()) {
    //     user_messages.add_message("There is an invalid value for a tag", Level::Warn);
    //     return;
    // }

    if !field_valid(screen_data.fields.iter()) {
        user_messages.add_message("There is an invalid value for a field", Level::Warn);
        return crux_core::render::render();
    }

    if screen_data.fields.is_empty() && screen_data.new_fields.is_empty() {
        user_messages.add_message("There are no fields, please add a field", Level::Warn);
        return crux_core::render::render();
    }

    let line = match generate_flux_line(
        &screen_data.tags,
        &screen_data.new_tags,
        &screen_data.fields,
        &screen_data.new_fields,
        &screen_data.time,
        &screen_data.measurement,
    ) {
        Ok(value) => value,
        Err(error) => {
            user_messages.add_message(error.reason, Level::Warn);
            return crux_core::render::render();
        }
    };

    match example_data {
        ExampleData::Tutorial(_) | ExampleData::FakeData => {
            *screen = Screens::default();
            crux_core::render::render()
        }
        ExampleData::Real => {
            // .post("http://100.108.38.15:8086/api/v2/write?org=test&bucket=personal&precision=s")
            crux_http::command::Http::post(
                screen_data
                    .base_url
                    .join("/api/v2/write")
                    .expect("/api/v2/write should be a valid extension to base url")
                    .query_pairs_mut()
                    .append_pair("bucket", &screen_data.bucket)
                    .append_pair("precision", &screen_data.precision.to_string())
                    .append_pair("orgID", &screen_data.org.id)
                    .finish(),
            )
            .header("Authorization", format!("Token {}", screen_data.api_token))
            .body_string(line)
            .build()
            .then_send(construct_response_event)
        }
    }
}

/// Return true if all tags are valid, else false
fn tags_valid(tag_iter: Iter<TagKey, UserInputOrMultiChoice>) -> bool {
    tag_iter.fold(true, |valid, (_, tag_value)| {
        if !valid {
            false
        } else {
            tag_value.valid().is_some()
        }
    })
}

/// Return true if all fields are valid, else false
fn field_valid(tag_iter: Iter<'_, String, UserFieldValue>) -> bool {
    tag_iter.fold(
        true,
        |valid, (_, tag_value)| {
            if !valid {
                false
            } else {
                tag_value.is_valid()
            }
        },
    )
}

//  F: FnOnce(crate::Result<Response<ExpectBody>>) -> Event + Send + 'static,
fn construct_response_event(body: crux_http::Result<crux_http::Response<Vec<u8>>>) -> Event {
    EvInputFieldsAndTagsEventsScreen(ScEvInputFieldsAndTags::InfluxWriteResponse(body))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::{Model, Screens};
    use crate::screens::input_fields_and_tags::RemoteResource::{Loaded, NotLoaded};
    use crate::screens::input_fields_and_tags::{
        validate_tag_value, InputFieldsAndTagsScreenData, LoadingResource,
    };
    use crate::screens_common::model::field_type::{ValidatedFieldBool, ValidatedType};
    use crate::screens_common::model::user_input_or_multi_choice::SelectedUserInputOrMultiChoice;
    use crate::Counter;
    use crate::Effect;
    use crux_core::assert_effect;
    use crux_core::testing::AppTester;
    use std::collections::BTreeMap;

    #[test]
    fn missing_data_1_no_data() {
        let app = AppTester::<Counter>::default();

        let screen_model = InputFieldsAndTagsScreenData {
            ..Default::default()
        };
        // let user_messages: UserMessages = Default::default();

        // I cant accept capabilities directly so I have to call the method through app...
        // set up our initial model
        let mut model = Model {
            screen: Screens::InputFieldsTagsScreen(screen_model),
            ..Default::default()
        };

        // send a `Get` event to the app
        let update = app.update(
            EvInputFieldsAndTagsEventsScreen(ScEvInputFieldsAndTags::InfluxWrite),
            &mut model,
        );

        assert_effect!(update, Effect::Render(_));

        // assert_effect!(update, Effect::Http(_));
        for effect in update.effects {
            if matches!(effect, Effect::Http(_)) {
                panic!("update contains http call, when it shouldn't do");
            }
        }
        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(& model), @r#"
        ---
        screen:
          InputFieldsTagsScreen:
            base_url: "http://notaurl.com/"
            org: fake_name
            bucket: ""
            measurement: ""
            available_tags:
              Loading:
                retry_count: 0
                current_state: Initialised
            tags: {}
            new_tags: []
            available_fields:
              Loading:
                retry_count: 0
                current_state: Initialised
            fields: {}
            new_fields: []
            line: ""
            ui_time: ~
            precision: Nanoseconds
            expected_line: "  "
        user_messages:
          messages:
            - message: "There are no fields, please add a field"
              level: Warn
              id: 1
        tutorial: ~
        "#);
    }

    #[test]
    fn missing_data_2_missing_tag_value() {
        let app = AppTester::<Counter>::default();

        // tag1 is not filled in!
        let screen_model = InputFieldsAndTagsScreenData {
            tags: BTreeMap::from([
                (
                    "tag1".to_owned(),
                    UserInputOrMultiChoice {
                        options: NotLoaded(LoadingResource::default()),
                        selected: None,
                        notify_user_of_mode_switch: None,
                        user_validating_function: validate_tag_value,
                    },
                ),
                (
                    "tag2".to_owned(),
                    UserInputOrMultiChoice {
                        options: Loaded(vec!["value".to_owned()]),
                        selected: Some(SelectedUserInputOrMultiChoice::Index(0)),
                        notify_user_of_mode_switch: None,
                        user_validating_function: validate_tag_value,
                    },
                ),
            ]),
            fields: BTreeMap::from([(
                "tag1".to_owned(),
                UserFieldValue::Bool(ValidatedFieldBool::build(true)),
            )]),
            ..Default::default()
        };
        // let user_messages: UserMessages = Default::default();

        // I cant accept capabilities directly so I have to call the method through app...
        // set up our initial model
        let mut model = Model {
            screen: Screens::InputFieldsTagsScreen(screen_model),
            ..Default::default()
        };

        // send a `Get` event to the app
        let update = app.update(
            EvInputFieldsAndTagsEventsScreen(ScEvInputFieldsAndTags::InfluxWrite),
            &mut model,
        );

        assert_effect!(update, Effect::Render(_));

        // assert_effect!(update, Effect::Http(_));
        for effect in update.effects {
            if matches!(effect, Effect::Http(_)) {
                panic!("update contains http call, when it shouldn't do");
            }
        }
        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(& model), @r#"
        ---
        screen:
          InputFieldsTagsScreen:
            base_url: "http://notaurl.com/"
            org: fake_name
            bucket: ""
            measurement: ""
            available_tags:
              Loading:
                retry_count: 0
                current_state: Initialised
            tags:
              tag1:
                options: Loading
                selected: ~
                notify_user_of_mode_switch: ~
              tag2:
                options:
                  Loaded:
                    - hidden_uid: "0"
                      user_facing_value: value
                selected:
                  IndexMode: "0"
                notify_user_of_mode_switch: ~
            new_tags: []
            available_fields:
              Loading:
                retry_count: 0
                current_state: Initialised
            fields:
              tag1:
                field_type: Bool
                input:
                  value: "true"
                  valid: true
            new_fields: []
            line: ""
            ui_time: ~
            precision: Nanoseconds
            expected_line: ",tag1=,tag2=value tag1=true "
        user_messages:
          messages:
            - message: There is an invalid value for a tag
              level: Warn
              id: 1
        tutorial: ~
        "#);
    }

    #[test]
    fn missing_data_3_missing_field_value() {
        let app = AppTester::<Counter>::default();

        // tag1 is not filled in!
        let screen_model = InputFieldsAndTagsScreenData {
            fields: BTreeMap::from([(
                "tag1".to_owned(),
                UserFieldValue::Bool(ValidatedFieldBool::default()),
            )]),
            ..Default::default()
        };
        // let user_messages: UserMessages = Default::default();

        // I cant accept capabilities directly so I have to call the method through app...
        // set up our initial model
        let mut model = Model {
            screen: Screens::InputFieldsTagsScreen(screen_model),
            ..Default::default()
        };

        // send a `Get` event to the app
        let update = app.update(
            EvInputFieldsAndTagsEventsScreen(ScEvInputFieldsAndTags::InfluxWrite),
            &mut model,
        );

        assert_effect!(update, Effect::Render(_));

        // assert_effect!(update, Effect::Http(_));
        for effect in update.effects {
            if matches!(effect, Effect::Http(_)) {
                panic!("update contains http call, when it shouldn't do");
            }
        }
        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(& model), @r#"
        ---
        screen:
          InputFieldsTagsScreen:
            base_url: "http://notaurl.com/"
            org: fake_name
            bucket: ""
            measurement: ""
            available_tags:
              Loading:
                retry_count: 0
                current_state: Initialised
            tags: {}
            new_tags: []
            available_fields:
              Loading:
                retry_count: 0
                current_state: Initialised
            fields:
              tag1:
                field_type: Bool
                input:
                  value: ""
                  valid: false
            new_fields: []
            line: ""
            ui_time: ~
            precision: Nanoseconds
            expected_line: " tag1= "
        user_messages:
          messages:
            - message: There is an invalid value for a field
              level: Warn
              id: 1
        tutorial: ~
        "#);
    }

    #[test]
    fn missing_data_4_no_field_value_with_tags() {
        let app = AppTester::<Counter>::default();

        // tag1 is not filled in!
        let screen_model = InputFieldsAndTagsScreenData {
            tags: BTreeMap::from([(
                "tag2".to_owned(),
                UserInputOrMultiChoice {
                    options: Loaded(vec!["value".to_owned()]),
                    selected: Some(SelectedUserInputOrMultiChoice::Index(0)),
                    notify_user_of_mode_switch: None,
                    user_validating_function: validate_tag_value,
                },
            )]),
            ..Default::default()
        };
        // let user_messages: UserMessages = Default::default();

        // I cant accept capabilities directly so I have to call the method through app...
        // set up our initial model
        let mut model = Model {
            screen: Screens::InputFieldsTagsScreen(screen_model),
            ..Default::default()
        };

        // send a `Get` event to the app
        let update = app.update(
            EvInputFieldsAndTagsEventsScreen(ScEvInputFieldsAndTags::InfluxWrite),
            &mut model,
        );

        assert_effect!(update, Effect::Render(_));

        // assert_effect!(update, Effect::Http(_));
        for effect in update.effects {
            if matches!(effect, Effect::Http(_)) {
                panic!("update contains http call, when it shouldn't do");
            }
        }
        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(& model), @r#"
        ---
        screen:
          InputFieldsTagsScreen:
            base_url: "http://notaurl.com/"
            org: fake_name
            bucket: ""
            measurement: ""
            available_tags:
              Loading:
                retry_count: 0
                current_state: Initialised
            tags:
              tag2:
                options:
                  Loaded:
                    - hidden_uid: "0"
                      user_facing_value: value
                selected:
                  IndexMode: "0"
                notify_user_of_mode_switch: ~
            new_tags: []
            available_fields:
              Loading:
                retry_count: 0
                current_state: Initialised
            fields: {}
            new_fields: []
            line: ""
            ui_time: ~
            precision: Nanoseconds
            expected_line: ",tag2=value  "
        user_messages:
          messages:
            - message: "There are no fields, please add a field"
              level: Warn
              id: 1
        tutorial: ~
        "#);
    }

    #[test]
    fn missing_data_5_no_field_value_without_tags() {
        let app = AppTester::<Counter>::default();

        // tag1 is not filled in!
        let screen_model = InputFieldsAndTagsScreenData {
            ..Default::default()
        };
        // let user_messages: UserMessages = Default::default();

        // I cant accept capabilities directly so I have to call the method through app...
        // set up our initial model
        let mut model = Model {
            screen: Screens::InputFieldsTagsScreen(screen_model),
            ..Default::default()
        };

        // send a `Get` event to the app
        let update = app.update(
            EvInputFieldsAndTagsEventsScreen(ScEvInputFieldsAndTags::InfluxWrite),
            &mut model,
        );

        assert_effect!(update, Effect::Render(_));

        // assert_effect!(update, Effect::Http(_));
        for effect in update.effects {
            if matches!(effect, Effect::Http(_)) {
                panic!("update contains http call, when it shouldn't do");
            }
        }
        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(& model), @r#"
        ---
        screen:
          InputFieldsTagsScreen:
            base_url: "http://notaurl.com/"
            org: fake_name
            bucket: ""
            measurement: ""
            available_tags:
              Loading:
                retry_count: 0
                current_state: Initialised
            tags: {}
            new_tags: []
            available_fields:
              Loading:
                retry_count: 0
                current_state: Initialised
            fields: {}
            new_fields: []
            line: ""
            ui_time: ~
            precision: Nanoseconds
            expected_line: "  "
        user_messages:
          messages:
            - message: "There are no fields, please add a field"
              level: Warn
              id: 1
        tutorial: ~
        "#);
    }

    #[test]
    fn happy_path() {
        let app = AppTester::<Counter>::default();

        // tag1 is not filled in!
        let screen_model = InputFieldsAndTagsScreenData {
            tags: BTreeMap::from([
                (
                    "tag1".to_owned(),
                    UserInputOrMultiChoice {
                        options: Loaded(vec!["value".to_owned()]),
                        selected: Some(SelectedUserInputOrMultiChoice::Index(0)),
                        notify_user_of_mode_switch: None,
                        user_validating_function: validate_tag_value,
                    },
                ),
                (
                    "tag2".to_owned(),
                    UserInputOrMultiChoice {
                        options: Loaded(vec!["value".to_owned()]),
                        selected: Some(SelectedUserInputOrMultiChoice::Index(0)),
                        notify_user_of_mode_switch: None,
                        user_validating_function: validate_tag_value,
                    },
                ),
            ]),
            fields: BTreeMap::from([(
                "tag1".to_owned(),
                UserFieldValue::Bool(ValidatedFieldBool::build(true)),
            )]),
            ..Default::default()
        };
        // let user_messages: UserMessages = Default::default();

        // I cant accept capabilities directly so I have to call the method through app...
        // set up our initial model
        let mut model = Model {
            screen: Screens::InputFieldsTagsScreen(screen_model),
            ..Default::default()
        };

        // send a `Get` event to the app
        let update = app.update(
            EvInputFieldsAndTagsEventsScreen(ScEvInputFieldsAndTags::InfluxWrite),
            &mut model,
        );

        assert_effect!(update, Effect::Http(_));
        // for effect in update.effects {
        //     if matches!(effect, Effect::Http(_)) {
        //         panic!("update contains http call, when it shouldn't do");
        //     }
        // }
        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(& model), @r#"
        ---
        screen:
          InputFieldsTagsScreen:
            base_url: "http://notaurl.com/"
            org: fake_name
            bucket: ""
            measurement: ""
            available_tags:
              Loading:
                retry_count: 0
                current_state: Initialised
            tags:
              tag1:
                options:
                  Loaded:
                    - hidden_uid: "0"
                      user_facing_value: value
                selected:
                  IndexMode: "0"
                notify_user_of_mode_switch: ~
              tag2:
                options:
                  Loaded:
                    - hidden_uid: "0"
                      user_facing_value: value
                selected:
                  IndexMode: "0"
                notify_user_of_mode_switch: ~
            new_tags: []
            available_fields:
              Loading:
                retry_count: 0
                current_state: Initialised
            fields:
              tag1:
                field_type: Bool
                input:
                  value: "true"
                  valid: true
            new_fields: []
            line: ""
            ui_time: ~
            precision: Nanoseconds
            expected_line: ",tag1=value,tag2=value tag1=true "
        user_messages:
          messages: []
        tutorial: ~
        "#);
    }
}
