use crate::internal_error_strings::{N053_USED, N054_USED};
use crate::model::{ExampleData, UserMessages};
use crate::screens::input_fields_and_tags::flux_get_measurements_existing_fields::flux_get_measurements_existing_fields;
use crate::screens::input_fields_and_tags::{
    AvailableFields, InputFieldsAndTagsScreenData, RemoteResource,
};
use crate::{Effect, Event};
use crux_core::Command;
use crux_http::http::StatusCode;
use crux_http::Response;
use csv::ReaderBuilder;
use log::{error, trace, warn};
use std::collections::BTreeMap;
use uniffi::deps::log::info;

#[derive(Debug, serde::Deserialize)]
struct Record {
    _value: String,
    _field: String,
}

/// Result of the call to getting all Measurements in a bucket
/// used to make it easier for a user to correctly fill in the Measurement field
///
/// https://docs.influxdata.com/flux/v0/stdlib/influxdata/influxdb/schema/measurements/
///
/// import "influxdata/influxdb/schema"
///
/// schema.measurementFieldKeys(
///     bucket: "example-bucket",
///     measurement: "example-measurement",
/// )
pub fn flux_get_measurements_existing_fields_response(
    screen_data: &mut InputFieldsAndTagsScreenData,
    response: crux_http::Result<Response<String>>,
    user_messages: &mut UserMessages,
    example_data: &ExampleData,
) -> Command<Effect, Event> {
    trace!("Called fields response");

    match response {
        Ok(response) => ok_response(screen_data, response, user_messages, example_data),
        Err(_err) => err_response(screen_data, /*err,*/ user_messages, example_data),
    }
}

fn err_response(
    screen_data: &mut InputFieldsAndTagsScreenData,
    // err: crux_http::HttpError,
    user_messages: &mut UserMessages,
    example_data: &ExampleData,
) -> Command<Effect, Event> {
    let RemoteResource::NotLoaded(loading_fields) = &mut screen_data.available_fields else {
        user_messages.error_report_to_developer(N053_USED);
        return crux_core::render::render();
    };

    // For now, I just want to capture the errors for debugging if I learn I can do something with them, I'll add some better logic
    // let error_message = serde_json::to_string(&err).unwrap_or_else(|err| {
    //     error!("{err}");
    //     user_messages.error_report_to_developer(N038_USED);
    //     "".to_string()
    // });
    // loading_fields.failure_reasons.push(error_message);
    loading_fields.retry_count += 1;

    warn!(
        "fetching fields errors: {:?}",
        loading_fields.failure_reasons
    );

    flux_get_measurements_existing_fields(screen_data, user_messages, example_data)
}

fn ok_response(
    screen_data: &mut InputFieldsAndTagsScreenData,
    response: Response<String>,
    user_messages: &mut UserMessages,
    example_data: &ExampleData,
) -> Command<Effect, Event> {
    match response.status() {
        StatusCode::Ok => {
            let Some(body_string) = response.body() else {
                user_messages.error_report_to_developer(N054_USED);
                error!("Failed to unwrap body");
                return crux_core::render::render();
            };

            info!(
                "flux_get_measurements_existing_fields_response: {}",
                body_string
            );
            // todo find out if i get a pass then a failure in the text
            let mut rdr = ReaderBuilder::new().from_reader(body_string.as_bytes());
            let mut map = BTreeMap::new();
            for result in rdr.deserialize() {
                let record: Record = result.unwrap();
                println!("{:?}", record);
                map.insert(record._field, record._value.as_str().try_into().unwrap());
            }
            screen_data.available_fields = RemoteResource::Loaded(AvailableFields(map));

            info!(
                "flux_get_measurements_existing_fields_response, vec: {:?}",
                screen_data.available_fields
            );
            crux_core::render::render()
        }
        StatusCode::Continue
        | StatusCode::SwitchingProtocols
        | StatusCode::EarlyHints
        | StatusCode::Created
        | StatusCode::Accepted
        | StatusCode::NonAuthoritativeInformation
        | StatusCode::NoContent
        | StatusCode::ResetContent
        | StatusCode::PartialContent
        | StatusCode::MultiStatus
        | StatusCode::ImUsed
        | StatusCode::MultipleChoice
        | StatusCode::MovedPermanently
        | StatusCode::Found
        | StatusCode::SeeOther
        | StatusCode::NotModified
        | StatusCode::TemporaryRedirect
        | StatusCode::PermanentRedirect
        | StatusCode::BadRequest
        | StatusCode::Unauthorized
        | StatusCode::PaymentRequired
        | StatusCode::Forbidden
        | StatusCode::NotFound
        | StatusCode::MethodNotAllowed
        | StatusCode::NotAcceptable
        | StatusCode::ProxyAuthenticationRequired
        | StatusCode::RequestTimeout
        | StatusCode::Conflict
        | StatusCode::Gone
        | StatusCode::LengthRequired
        | StatusCode::PreconditionFailed
        | StatusCode::PayloadTooLarge
        | StatusCode::UriTooLong
        | StatusCode::UnsupportedMediaType
        | StatusCode::RequestedRangeNotSatisfiable
        | StatusCode::ExpectationFailed
        | StatusCode::ImATeapot
        | StatusCode::MisdirectedRequest
        | StatusCode::UnprocessableEntity
        | StatusCode::Locked
        | StatusCode::FailedDependency
        | StatusCode::TooEarly
        | StatusCode::UpgradeRequired
        | StatusCode::PreconditionRequired
        | StatusCode::TooManyRequests
        | StatusCode::RequestHeaderFieldsTooLarge
        | StatusCode::UnavailableForLegalReasons
        | StatusCode::InternalServerError
        | StatusCode::NotImplemented
        | StatusCode::BadGateway
        | StatusCode::ServiceUnavailable
        | StatusCode::GatewayTimeout
        | StatusCode::HttpVersionNotSupported
        | StatusCode::VariantAlsoNegotiates
        | StatusCode::InsufficientStorage
        | StatusCode::LoopDetected
        | StatusCode::NotExtended
        | StatusCode::NetworkAuthenticationRequired => {
            warn!(
                "Some error fetching fields, status_code: {}, body: {:?}",
                response.status(),
                response.body()
            );
            // todo update retry numbers
            flux_get_measurements_existing_fields(screen_data, user_messages, example_data)
        }
    }
}

// todo find failing cases and add them
#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::{Model, Screens};
    use crate::Counter;
    use crux_core::testing::AppTester;
    use crux_http::testing::ResponseBuilder;

    #[test]
    fn happy_path() {
        let app = AppTester::<Counter>::default();

        let mut user_messages: UserMessages = Default::default();

        let mut screen = InputFieldsAndTagsScreenData::default();

        flux_get_measurements_existing_fields_response(&mut screen, Ok(ResponseBuilder::ok()
            .body(",result,table,_field,_value\n,,0,bool,bool\n,,1,float,float\n,,2,int,float\n,,3,int2,int\n,,4,string,string\n,,5,uint,uint\n".to_owned())
            .build()), &mut user_messages, &ExampleData::Real );

        // check that the view has been updated correctly
        /* assert_eq!(screen.available_fields.get("bool"), Some(&FieldType::Bool));
        assert_eq!(
            screen.available_fields.get("float"),
            Some(&FieldType::Float)
        );
        assert_eq!(screen.available_fields.get("int"), Some(&FieldType::Float));
        assert_eq!(screen.available_fields.get("int2"), Some(&FieldType::Int));
        assert_eq!(
            screen.available_fields.get("string"),
            Some(&FieldType::String)
        );
        assert_eq!(screen.available_fields.get("uint"), Some(&FieldType::UInt));*/

        let model = Model {
            screen: Screens::InputFieldsTagsScreen(screen),
            user_messages,
            example_data: Default::default(),
        };

        insta::assert_yaml_snapshot!(app.view(&model), @r#"
        ---
        screen:
          InputFieldsTagsScreen:
            base_url: "http://notaurl.com/"
            org: fake_name
            bucket: ""
            measurement: ""
            available_tags:
              Loading:
                retry_count: 0
                current_state: Initialised
            tags: {}
            new_tags: []
            available_fields:
              Loaded:
                - bool
                - float
                - int
                - int2
                - string
                - uint
            fields: {}
            new_fields: []
            line: ""
            ui_time: ~
            precision: Nanoseconds
            expected_line: "  "
        user_messages:
          messages: []
        tutorial: ~
        "#);
    }
}
