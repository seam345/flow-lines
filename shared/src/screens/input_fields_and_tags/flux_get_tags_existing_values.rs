use crate::internal_error_strings::N055_USED;
use crate::model::{ExampleData, UserMessages};
use crate::screens::input_fields_and_tags::RemoteResource::Loaded;
use crate::screens::input_fields_and_tags::{
    AvailableTagValues, InputFieldsAndTagsScreenData, ScEvInputFieldsAndTags,
};
use crate::Event::EvInputFieldsAndTagsEventsScreen;
use crate::{Effect, Event};
use crux_core::Command;

/// get all values for a tag, todo i may have to paginate at some point
/// this will be called many times for all the tags a user selects
/// https://docs.influxdata.com/flux/v0/stdlib/influxdata/influxdb/schema/measurementtagvalues/
///
/// import "influxdata/influxdb/schema"
///
/// schema.measurementTagValues(
///     bucket: "example-bucket",
///     measurement: "example-measurement",
///     tag: "example-tag",
/// )
/// todo confirm if i ever call this event directly from shell
pub fn flux_get_tags_existing_values(
    model: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
    key: &str,
    example_data: &ExampleData,
) -> Command<Effect, Event> {
    match example_data {
        ExampleData::Tutorial(_) | ExampleData::FakeData => {
            // in theory this will never get called
            let values = vec![
                "predefined value1".to_owned(),
                "predefined value2".to_owned(),
                "predefined value3".to_owned(),
                "predefined value4".to_owned(),
            ];

            let Loaded(available_tags) = &mut model.available_tags else {
                user_messages.error_report_to_developer(N055_USED);
                return crux_core::render::render();
            };

            available_tags
                .entry(key.to_string())
                .and_modify(|array| *array = Loaded(AvailableTagValues(values)));
            crux_core::render::render()
        }
        ExampleData::Real => {
            let key: String = key.to_string(); // to be able to move it later
            model.flux_call(
                format!(
                    r#"
                    import "influxdata/influxdb/schema"
                    schema.measurementTagValues(
                        bucket: "{}",
                        measurement: "{}",
                        tag: "{}",
                    )
                "#,
                    model.bucket, model.measurement, key
                ),
                user_messages,
                move |body| construct_response_event(body, key.clone()),
            )
        }
    }
}

fn construct_response_event(
    body: crux_http::Result<crux_http::Response<String>>,
    tag: String,
) -> Event {
    EvInputFieldsAndTagsEventsScreen(ScEvInputFieldsAndTags::FluxGetTagsExistingValuesResponse(
        body, tag,
    ))
}
