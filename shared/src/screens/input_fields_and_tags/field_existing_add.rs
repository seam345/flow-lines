use crate::internal_error_strings::{N037_USED, N062_USED};
use crate::model::UserMessages;
use crate::screens::input_fields_and_tags::InputFieldsAndTagsScreenData;
use crate::screens::input_fields_and_tags::RemoteResource::Loaded;
use crate::{Effect, Event};
use crux_core::Command;
use uniffi::deps::log::{error, trace};

pub fn field_existing_add(
    screen_data: &mut InputFieldsAndTagsScreenData,
    key: String,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    let Loaded(available_fields) = &mut screen_data.available_fields else {
        user_messages.error_report_to_developer(N037_USED);
        return crux_core::render::render();
    };

    match available_fields.get(&key) {
        None => {
            error!("how did we get here... should I panic... in theory it shouldn't happen");
            user_messages.error_report_to_developer(N062_USED);
        }
        Some(value) => {
            trace!("Adding key to sending tags");
            screen_data.fields.insert(key, value.into());
        }
    }
    crux_core::render::render()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::{Model, Screens};
    use crate::screens::input_fields_and_tags::{AvailableFields, FieldType, RemoteResource};
    use crate::Counter;
    use crux_core::testing::AppTester;
    use std::collections::BTreeMap;

    #[test]
    fn happy_path() {
        let key = "random_key";
        let mut map = BTreeMap::new();
        map.insert(key.to_string(), FieldType::Float);
        let available_fields: RemoteResource<AvailableFields> = Loaded(AvailableFields(map));

        let mut data = InputFieldsAndTagsScreenData {
            available_fields,
            ..Default::default()
        };

        let mut user_messages: UserMessages = Default::default();
        field_existing_add(&mut data, key.to_string(), &mut user_messages);

        let model = Model {
            screen: Screens::InputFieldsTagsScreen(data),
            user_messages,
            ..Default::default()
        };

        let app = AppTester::<Counter>::default();
        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(& model), @r#"
        ---
        screen:
          InputFieldsTagsScreen:
            base_url: "http://notaurl.com/"
            org: fake_name
            bucket: ""
            measurement: ""
            available_tags:
              Loading:
                retry_count: 0
                current_state: Initialised
            tags: {}
            new_tags: []
            available_fields:
              Loaded:
                - random_key
            fields:
              random_key:
                field_type: Float
                input:
                  value: ""
                  valid: false
            new_fields: []
            line: ""
            ui_time: ~
            precision: Nanoseconds
            expected_line: " random_key= "
        user_messages:
          messages: []
        tutorial: ~
        "#);
    }

    // todo add when willing to make green
    /*#[test]
    fn key_not_in_available_fields() {
        let mut model = Model {
            ..Default::default()
        };

        let key = "random_key";

        let old_model = model.clone();

        add_field(&mut model, key.to_string());

        // confirm field was not added tbh I probably want to throw some error in some way
        assert_eq!(model.error, "Internal error unable to add field ");
        // assert_eq!(model.fields.get(key), Some(&UserFieldValue::Float(None)));

        // check for any unexpected changes
        assert_eq!(model.api_token, old_model.api_token);
        assert_eq!(model.base_url, old_model.base_url);
        assert_eq!(model.org, old_model.org);
        assert_eq!(model.available_measurements, old_model.available_measurements);
        assert_eq!(model.measurement, old_model.measurement);
        assert_eq!(model.available_tags, old_model.available_tags);
        assert_eq!(model.tags, old_model.tags);
        assert_eq!(model.available_fields, old_model.available_fields);
        assert_eq!(model.fields, old_model.fields);
        assert_eq!(model.line, old_model.line);
        assert_eq!(model.precision, old_model.precision);
        assert_eq!(model.available_buckets, old_model.available_buckets);
        assert_eq!(model.bucket, old_model.bucket);
    }*/
}
