use crate::model::UserMessages;
use crate::screens::input_fields_and_tags::{validate_field_key, InputFieldsAndTagsScreenData};
use crate::screens_common::model::field_type::UserFieldValue;
use crate::screens_common::model::user_input_or_multi_choice::ValidatingStringPreTrimmed;
use crate::{Effect, Event};
use crux_core::Command;

pub fn field_new_add(
    screen_data: &mut InputFieldsAndTagsScreenData,
    _user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    screen_data.new_fields.push((
        ValidatingStringPreTrimmed {
            inner: "".to_string(),
            validating_function: validate_field_key,
        },
        UserFieldValue::default(),
    ));
    crux_core::render::render()
}
