use crate::internal_error_strings::{N033_USED, N034_USED};
use crate::model::{Model, Screens};
use crate::screens::initial::QuickAction;
use crate::screens::input_fields_and_tags::RemoteResource;
use crate::screens::input_fields_and_tags::RemoteResource::{Loaded, NotLoaded};
use crate::screens::post_write_review::PostWriteReviewScreenData;
use crate::screens_common::model::user_input_or_multi_choice::SelectedUserInputOrMultiChoice;
use crate::{Effect, Event};
use crux_core::Command;
use crux_http::http::StatusCode;
use crux_http::Response;
use log::Level::Error;
use log::{error, trace};
use std::collections::BTreeMap;

pub fn influx_write_response(
    response: crux_http::Result<Response<Vec<u8>>>,
    model: &mut Model,
) -> Command<Effect, Event> {
    // an ok just means we got something but not necessarily a good response
    // todo add test for the bellow responses;
    // `
    // Ok(Response { version: None, status: BadRequest, headers: {"content-type": "application/octet-stream"}, .. })
    // body {"code":"invalid","message":"unable to parse 'waist,event= cm= ': missing tag value"}
    // 2
    // ok response: Response { version: None, status: NoContent, headers: {"content-type": "application/octet-stream"}, .. }
    trace!("select org,bucket,measurement screen event: Validating model");

    let Screens::InputFieldsTagsScreen(ref mut screen_model) = model.screen else {
        error!("Screen was in an unexpected state {:?}", model.screen);
        model
            .user_messages
            .add_message("Unrecoverable internal error state", Error);
        return crux_core::render::render();
    };
    trace!("Running select org,bucket,measurement event");
    match response {
        Ok(response) => {
            trace!("ok response: {:?}", response);
            match response.status() {
                StatusCode::NoContent | StatusCode::Ok => {
                    // model.screen = Screens::default();
                    model.screen = Screens::PostWriteReviewScreen(PostWriteReviewScreenData {
                        previous_write: QuickAction {
                            name: "".to_string(),
                            api_token: screen_model.api_token.clone(),
                            base_url: screen_model.base_url.clone(),
                            org: screen_model.org.clone(),
                            bucket: screen_model.bucket.clone(),
                            measurement: screen_model.measurement.clone(),
                            tags: screen_model
                                .tags
                                .iter()
                                .map(|(key, value)| {
                                    (
                                        key,
                                        match &value.selected {
                                            None => {
                                                todo!()
                                            }
                                            Some(SelectedUserInputOrMultiChoice::User(value)) => {
                                                value.as_result().expect("todo asdihbqw")
                                            }
                                            Some(SelectedUserInputOrMultiChoice::Index(index)) => {
                                                match &value.options {
                                                    NotLoaded(_) | RemoteResource::NonExistent => {
                                                        model
                                                            .user_messages
                                                            .add_message(N033_USED, Error);
                                                        "".to_owned()
                                                    }
                                                    Loaded(inner_value) => {
                                                        match inner_value.get(*index) {
                                                            None => {
                                                                model
                                                                    .user_messages
                                                                    .add_message(N034_USED, Error);
                                                                "".to_owned()
                                                            }
                                                            Some(inner_inner_value) => {
                                                                inner_inner_value.to_string()
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        },
                                    )
                                })
                                .fold(BTreeMap::new(), |mut acc, (key, value)| {
                                    acc.insert(key.to_string(), Some(value));
                                    acc
                                }),
                            fields: screen_model.fields.clone().iter().fold(
                                BTreeMap::new(),
                                |mut acc, (key, value)| {
                                    let mut value = value.clone();
                                    value.clear();
                                    {
                                        acc.insert(key.clone(), value);
                                        acc
                                    }
                                },
                            ),
                        },
                        eleven_data_points: vec![],
                    })
                }
                StatusCode::Accepted => {}
                StatusCode::Continue => {}
                StatusCode::SwitchingProtocols => {}
                StatusCode::EarlyHints => {}
                StatusCode::Created => {}
                StatusCode::NonAuthoritativeInformation => {}
                StatusCode::ResetContent => {}
                StatusCode::PartialContent => {}
                StatusCode::MultiStatus => {}
                StatusCode::ImUsed => {}
                StatusCode::MultipleChoice => {}
                StatusCode::MovedPermanently => {}
                StatusCode::Found => {}
                StatusCode::SeeOther => {}
                StatusCode::NotModified => {}
                StatusCode::TemporaryRedirect => {}
                StatusCode::PermanentRedirect => {}
                StatusCode::BadRequest => {}
                StatusCode::Unauthorized => {}
                StatusCode::PaymentRequired => {}
                StatusCode::Forbidden => {}
                StatusCode::NotFound => {}
                StatusCode::MethodNotAllowed => {}
                StatusCode::NotAcceptable => {}
                StatusCode::ProxyAuthenticationRequired => {}
                StatusCode::RequestTimeout => {}
                StatusCode::Conflict => {}
                StatusCode::Gone => {}
                StatusCode::LengthRequired => {}
                StatusCode::PreconditionFailed => {}
                StatusCode::PayloadTooLarge => {}
                StatusCode::UriTooLong => {}
                StatusCode::UnsupportedMediaType => {}
                StatusCode::RequestedRangeNotSatisfiable => {}
                StatusCode::ExpectationFailed => {}
                StatusCode::ImATeapot => {}
                StatusCode::MisdirectedRequest => {}
                StatusCode::UnprocessableEntity => {}
                StatusCode::Locked => {}
                StatusCode::FailedDependency => {}
                StatusCode::TooEarly => {}
                StatusCode::UpgradeRequired => {}
                StatusCode::PreconditionRequired => {}
                StatusCode::TooManyRequests => {}
                StatusCode::RequestHeaderFieldsTooLarge => {}
                StatusCode::UnavailableForLegalReasons => {}
                StatusCode::InternalServerError => {}
                StatusCode::NotImplemented => {}
                StatusCode::BadGateway => {}
                StatusCode::ServiceUnavailable => {}
                StatusCode::GatewayTimeout => {}
                StatusCode::HttpVersionNotSupported => {}
                StatusCode::VariantAlsoNegotiates => {}
                StatusCode::InsufficientStorage => {}
                StatusCode::LoopDetected => {}
                StatusCode::NotExtended => {}
                StatusCode::NetworkAuthenticationRequired => {}
            }
        }
        Err(_) => {
            model
                .user_messages
                .add_message("Bad response from influx server, try again", Error);
        }
    }
    crux_core::render::render()
}
