use crate::internal_error_strings::N002_USED;
use crate::model::UserMessages;
use crate::model_ui::screens_common::remote_multi_choice::multi_choice::UiStringIndex;
use crate::screens::input_fields_and_tags::InputFieldsAndTagsScreenData;
use crate::{Effect, Event};
use crux_core::Command;

pub fn field_new_remove(
    index: UiStringIndex,
    screen_data: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    let Ok(index) = index.to_bound_usize(screen_data.new_fields.len()) else {
        user_messages.error_report_to_developer(N002_USED);
        return crux_core::render::render();
    };

    screen_data.new_fields.remove(index);
    crux_core::render::render()
}
