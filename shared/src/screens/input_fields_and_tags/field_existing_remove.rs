use crate::screens::input_fields_and_tags::InputFieldsAndTagsScreenData;
use crate::{Effect, Event};
use crux_core::Command;

pub fn field_existing_remove(
    model: &mut InputFieldsAndTagsScreenData,
    key: String,
) -> Command<Effect, Event> {
    model.fields.remove(&key);
    crux_core::render::render()
}
