use crate::model::{ExampleData, UserMessages};
use crate::screens::input_fields_and_tags::flux_get_tags_existing_values::flux_get_tags_existing_values;
use crate::screens::input_fields_and_tags::{
    AvailableTags, InputFieldsAndTagsScreenData, LoadingResource, RemoteResource,
};
use crate::{Effect, Event};
use crux_core::Command;
use crux_http::Response;
use csv::ReaderBuilder;
use log::Level::Trace;
use std::collections::BTreeMap;
use uniffi::deps::log::info;

#[derive(Debug, serde::Deserialize)]
struct Record {
    _value: String,
}

/// Result of the call to getting all tags for a measurement
///
/// naming restrictions mean I can ignore all tags beginning with _
/// https://docs.influxdata.com/influxdb/v2/reference/syntax/line-protocol/#naming-restrictions
pub fn flux_get_measurements_existing_tags_response(
    model: &mut InputFieldsAndTagsScreenData,
    response: crux_http::Result<Response<String>>,
    user_messages: &mut UserMessages,
    example_data: &ExampleData,
) -> Command<Effect, Event> {
    user_messages.add_message("Called tag response", Trace);
    // todo remove unwraps
    let binding = response.unwrap();
    info!(
        "flux_get_measurements_existing_tags_response: {}",
        binding.body().unwrap()
    );
    let mut rdr = ReaderBuilder::new().from_reader(binding.body().unwrap().as_bytes());
    let mut map = BTreeMap::new();
    for result in rdr.deserialize() {
        let record: Record = result.unwrap();
        println!("{:?}", record);
        // tags starting with _ are reserved internally for InfluxDB, the user should not be using them
        if !record._value.starts_with('_') {
            map.insert(
                record._value,
                RemoteResource::NotLoaded(LoadingResource::default()),
            );
        }
    }
    model.available_tags = RemoteResource::Loaded(AvailableTags(map));
    info!(
        "flux_get_measurements_existing_tags_response, hashmap: {:?}",
        model.available_tags
    );

    let mut command = Command::done();
    // if we loaded from quick actions we have a bunch of tags to load already...
    for tag in model.tags.clone().iter() {
        command = command.and(flux_get_tags_existing_values(
            model,
            user_messages,
            tag.0,
            example_data,
        ));
    }
    command
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::{Model, Screens};
    use crate::Counter;
    use crux_core::testing::AppTester;
    use crux_http::testing::ResponseBuilder;

    #[test]
    fn happy_path() {
        let app = AppTester::<Counter>::default();

        let mut screen = InputFieldsAndTagsScreenData::default();
        let mut user_messages: UserMessages = Default::default();

        flux_get_measurements_existing_tags_response(&mut screen, Ok(ResponseBuilder::ok()
            .body(",result,table,_value\n,_result,0,_start\n,_result,0,_stop\n,_result,0,_field\n,_result,0,_measurement\n".to_owned())
            .build()), &mut user_messages, &ExampleData::Real);

        let model = Model {
            screen: Screens::InputFieldsTagsScreen(screen),
            ..Default::default()
        };
        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(& model), @r#"
        ---
        screen:
          InputFieldsTagsScreen:
            base_url: "http://notaurl.com/"
            org: fake_name
            bucket: ""
            measurement: ""
            available_tags:
              Loaded: {}
            tags: {}
            new_tags: []
            available_fields:
              Loading:
                retry_count: 0
                current_state: Initialised
            fields: {}
            new_fields: []
            line: ""
            ui_time: ~
            precision: Nanoseconds
            expected_line: "  "
        user_messages:
          messages: []
        tutorial: ~
        "#);
    }
}
