use crate::model::UserMessages;
use crate::screens::input_fields_and_tags::{InputFieldsAndTagsScreenData, TagKey, TagValue};
use crate::{Effect, Event};
use crux_core::Command;
use log::{error, Level};

pub fn tag_existing_value_set_user(
    key: TagKey,
    value: TagValue,
    model: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    let Some(entry) = model.tags.get_mut(&key) else {
        error!("Key didnt exist, in theory this shouldn't happen");
        user_messages.add_message(
            "Internal error 002, Please report to developer",
            Level::Error,
        );

        return crux_core::render::render();
    };

    entry.set_user_input(value, true);
    crux_core::render::render()
    // *entry = SelectedTagType::UserDefined(validate_user_tag_value(value));
}
