use crate::internal_error_strings::{N039_USED, N040_USED, N047_USED};
use crate::model::{ExampleData, UserMessages};
use crate::screens::input_fields_and_tags::flux_get_tags_existing_values::flux_get_tags_existing_values;
use crate::screens::input_fields_and_tags::RemoteResource::Loaded;
use crate::screens::input_fields_and_tags::{
    validate_tag_value, InputFieldsAndTagsScreenData, RemoteResource,
};
use crate::screens_common::model::user_input_or_multi_choice::UserInputOrMultiChoice;
use crate::{Effect, Event};
use crux_core::Command;
use log::warn;
use uniffi::deps::log::{debug, trace};

pub fn tag_existing_add(
    model: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
    key: String,
    example_data: &ExampleData,
) -> Command<Effect, Event> {
    let Loaded(ref mut available_tags) = &mut model.available_tags else {
        user_messages.error_report_to_developer(N039_USED);
        return crux_core::render::render();
    };
    match available_tags.get_mut(&key) {
        None => {
            user_messages.error_report_to_developer(N040_USED);
            crux_core::render::render()
        }
        Some(value) => match value {
            RemoteResource::NotLoaded(inner) => {
                trace!("Adding key to sending tags");
                model.tags.insert(
                    key.clone(),
                    UserInputOrMultiChoice {
                        options: RemoteResource::default(),
                        selected: None,
                        notify_user_of_mode_switch: None,
                        user_validating_function: validate_tag_value,
                    },
                );
                inner.retry_count += 1;
                debug!("values not fetch for tag key, queuing http request");
                flux_get_tags_existing_values(model, user_messages, &key, example_data)
            }
            Loaded(_) => {
                warn!("why have we loaded twice?");
                trace!("Adding key to sending tags");
                model.tags.insert(
                    key,
                    UserInputOrMultiChoice {
                        options: RemoteResource::default(),
                        selected: None,
                        notify_user_of_mode_switch: None,
                        user_validating_function: validate_tag_value,
                    },
                );
                crux_core::render::render()
            }
            RemoteResource::NonExistent => {
                user_messages.error_report_to_developer(N047_USED);

                trace!("No idea how we got here maybe bellow code will work...");
                model.tags.insert(
                    key.clone(),
                    UserInputOrMultiChoice {
                        options: RemoteResource::default(),
                        selected: None,
                        notify_user_of_mode_switch: None,
                        user_validating_function: validate_tag_value,
                    },
                );
                debug!("values not fetch for tag key, queuing http request");
                flux_get_tags_existing_values(model, user_messages, &key, example_data)
            }
        },
    }
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use super::*;
    use crate::model::{Model, Screens};
    use crate::screens::input_fields_and_tags::{
        AvailableTags, LoadingResource, ScEvInputFieldsAndTags,
    };
    use crate::Counter;
    use crate::Event;
    use crux_core::testing::AppTester;
    use std::collections::BTreeMap;

    #[test]
    fn happy_path_select_non_loaded_tag() {
        let app = AppTester::<Counter>::default();

        let mut model = Model {
            screen: Screens::InputFieldsTagsScreen(InputFieldsAndTagsScreenData {
                available_tags: Loaded(AvailableTags(BTreeMap::from([(
                    "value".to_owned(),
                    RemoteResource::NotLoaded(LoadingResource {
                        retry_count: 0,
                        failure_reasons: vec![],
                        current_state: Default::default(),
                    }),
                )]))),
                bucket: "bucket".to_owned(),
                ..Default::default()
            }),
            ..Default::default()
        };

        let _update = app.update(
            Event::EvInputFieldsAndTagsEventsScreen(ScEvInputFieldsAndTags::TagExistingAdd(
                "value".to_owned(),
            )),
            &mut model,
        );

        // check that the view has been updated correctly
        insta::assert_yaml_snapshot!(app.view(&mut model), @r#"
        ---
        screen:
          InputFieldsTagsScreen:
            base_url: "http://notaurl.com/"
            org: fake_name
            bucket: bucket
            measurement: ""
            available_tags:
              Loaded:
                value:
                  Loading:
                    retry_count: 1
                    current_state: Initialised
            tags:
              value:
                options: Loading
                selected: ~
                notify_user_of_mode_switch: ~
            new_tags: []
            available_fields:
              Loading:
                retry_count: 0
                current_state: Initialised
            fields: {}
            new_fields: []
            line: ""
            ui_time: ~
            precision: Nanoseconds
            expected_line: ",value=  "
        user_messages:
          messages: []
        tutorial: ~
        "#);
    }
}
