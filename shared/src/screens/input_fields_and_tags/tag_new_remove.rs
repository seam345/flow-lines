use crate::model::UserMessages;
use crate::model_ui::screens_common::remote_multi_choice::multi_choice::UiStringIndex;
use crate::screens::input_fields_and_tags::InputFieldsAndTagsScreenData;
use crate::{Effect, Event};
use crux_core::Command;
use log::{error, Level};

// todo consider some kind of confirmation
pub fn tag_new_remove(
    string_index: UiStringIndex,
    model: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    let Ok(index) = string_index.to_bound_usize(model.new_tags.len()) else {
        error!("Index check for remove_new_tag failed");
        user_messages.add_message(
            "Internal error 001, Please report to developer",
            Level::Error,
        );
        return crux_core::render::render();
    };
    model.new_tags.remove(index);
    crux_core::render::render()
}
