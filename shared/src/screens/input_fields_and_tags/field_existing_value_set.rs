use crate::internal_error_strings::N022_USED;
use crate::model::UserMessages;
use crate::screens::input_fields_and_tags::InputFieldsAndTagsScreenData;
use crate::{Effect, Event};
use crux_core::Command;
use log::{error, trace};

pub type FieldKey = String;
pub type FieldValue = String;

pub fn field_existing_value_set(
    key: FieldKey,
    value: FieldValue,
    model: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    trace!("Get field type");

    let entry = model.fields.get_mut(&key);
    match entry {
        None => {
            error!("Failed to get key in model.fields");
            user_messages.error_report_to_developer(N022_USED);
        }
        Some(entry) => {
            entry.update_user_input(value);
        }
    }
    crux_core::render::render()
}
