use crate::model::UserMessages;
use crate::model_ui::screens_common::remote_multi_choice::multi_choice::UiStringIndex;
use crate::screens::input_fields_and_tags::InputFieldsAndTagsScreenData;
use crate::{Effect, Event};
use crux_core::Command;
use log::{error, Level};

pub fn tag_new_value_set(
    string_index: UiStringIndex,
    value: String,
    model: &mut InputFieldsAndTagsScreenData,
    user_messages: &mut UserMessages,
) -> Command<Effect, Event> {
    // todo validate key

    let Ok(index) = string_index.to_bound_usize(model.new_tags.len()) else {
        error!("Index check for remove_new_tag failed");
        user_messages.add_message(crate::internal_error_strings::N005_USED, Level::Error);
        return crux_core::render::render();
    };

    let Some(new_tag) = model.new_tags.get_mut(index) else {
        error!(
            "Get mut failed after index check! index: {index}, len: {}",
            model.new_tags.len()
        );
        user_messages.add_message(crate::internal_error_strings::N006_USED, Level::Error);
        return crux_core::render::render();
    };

    new_tag.value = Some(value);
    crux_core::render::render()
}
