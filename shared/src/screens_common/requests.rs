use crate::{Effect, Event};
use crux_core::Command;
use crux_http::http::{Method, Url};
use crux_http::Response;
use serde::Deserialize;

#[derive(Deserialize, Clone, Debug, PartialEq)]
pub struct BucketsResult {
    pub(crate) buckets: Vec<Bucket>,
}

#[derive(Deserialize, Clone, Debug, PartialEq)]
pub struct Bucket {
    pub(crate) name: String,
}

pub fn influx_bucket_connection_request<F>(
    url: Url,
    api_token: &str,
    response_event: F,
) -> Command<Effect, Event>
where
    F: FnOnce(crux_http::Result<Response<BucketsResult>>) -> Event + Send + 'static,
{
    crux_http::command::Http::request(Method::Get, url)
        .header("Authorization", format!("Token {}", api_token))
        .expect_json()
        .build()
        .then_send(response_event)
}
