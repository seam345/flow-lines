pub(crate) mod influx;

pub(crate) mod model;
pub(crate) mod requests;

/// Most my commands are actions.then_send, I didn't have a neat way at getting at the then_send
/// result without just fake resolving the effect, this is a bit messy
#[cfg(test)]
pub mod test_common {
    use crate::capabilities::persistent_storage::PersistentStorageOutput;
    use crate::Effect;
    use crux_http::protocol::HttpResult;
    use crux_http::HttpError;
    pub fn test_shell(effects: Vec<Effect>) {
        for effect in effects {
            match effect {
                Effect::Http(mut inner) => inner
                    .resolve(HttpResult::Err(HttpError::Url(
                        "Just a fake test".to_owned(),
                    )))
                    .unwrap(),
                Effect::PersistentStorage(mut inner) => inner
                    .resolve(PersistentStorageOutput::FileData(
                        "Just a fake test".to_owned(),
                    ))
                    .unwrap(),
                Effect::Render(_) => (),
            }
        }
    }
}
