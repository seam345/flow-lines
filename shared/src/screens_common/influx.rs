/// a helper to make flux calls
///
// curl --request POST 'INFLUX_URL/api/v2/query?org=INFLUX_ORG' \
// --header 'Content-Type: application/vnd.flux' \
// --header 'Accept: application/csv \
// --header 'Authorization: Token INFLUX_API_TOKEN' \
// --data 'from(bucket: "example-bucket")
//           |> range(start: -5m)
//           |> filter(fn: (r) => r._measurement == "example-measurement")'
use crate::model::UserMessages;
use crate::screens::input_fields_and_tags::UserTag;
use crate::screens::select_org_bucket_measurements::influx_get_orgs_response::Org;
use crate::screens_common::model::field_type::UserFieldValue;
use crate::screens_common::model::user_input_or_multi_choice::{
    UserInputOrMultiChoice, ValidatingStringPreTrimmed,
};
use crate::{Effect, Event};
use crux_core::Command;
use crux_http::http::{Method, Url};
use crux_http::Response;
use log::{error, Level};
use std::collections::BTreeMap;
use std::fmt::Write;

pub fn flux_call<F>(
    user_messages: &mut UserMessages,
    org: &Org,
    base_url: &Url,
    api_token: &String,
    flux_query: String,
    event_to_send: F,
) -> Command<Effect, Event>
where
    F: FnOnce(crux_http::Result<Response<String>>) -> Event + Send + 'static,
{
    // todo this error message probably needs to change based on how i get to the error now
    let Ok(url) = base_url.join(&format!("/api/v2/query?orgID={}", org.id)) else {
        error!("Unable to parse base url");
        user_messages.add_message(
            "Unable to generate a valid url from base url. Please check and amend base url",
            Level::Error,
        );
        return crux_core::render::render();
    };

    crux_http::command::Http::request(Method::Post, url)
        .body_string(flux_query)
        .header("Authorization", format!("Token {}", api_token))
        .header("Accept", "application/csv")
        .header("Content-Type", "application/vnd.flux")
        .expect_string()
        .build()
        .then_send(event_to_send)
}

pub struct GenerateLineError {
    pub partial_line: String,
    pub reason: String,
}

pub fn generate_flux_line(
    tags: &BTreeMap<String, UserInputOrMultiChoice>,
    new_tags: &[UserTag],
    fields: &BTreeMap<String, UserFieldValue>,
    new_fields: &[(ValidatingStringPreTrimmed, UserFieldValue)],
    time: &Option<i64>, // precision not needed as it is attached as http header
    measurement: &str,
) -> Result<String, GenerateLineError> {
    let mut capture_error: Option<GenerateLineError> = None;
    let tags: String = tags.iter().fold(String::new(), |mut output, (key, value)| {
        let _ = write!(
            output,
            ",{}={}",
            key,
            match value.valid() {
                None => {
                    "".to_owned()
                }
                Some(value) => {
                    value
                }
            }
        );
        output
    });

    let new_tags = new_tags
        .iter()
        .fold(String::new(), |mut output: String, new_tag| {
            let _ = write!(
                output,
                ",{}={}",
                new_tag.key.clone().unwrap_or("".to_owned()),
                new_tag.value.clone().unwrap_or("".to_owned())
            );
            output
        });
    let tags = format!("{}{}", tags, new_tags);

    let fields: String =
        fields
            .iter()
            .enumerate()
            .fold(String::new(), |mut output, (index, (key, value))| {
                let value: String = match value.string_for_line_protocol() {
                    Ok(value) => value,
                    Err(error) => {
                        match &mut capture_error {
                            None => {
                                capture_error = Some(GenerateLineError {
                                    partial_line: "".to_string(),
                                    reason: error,
                                });
                            }
                            Some(val) => {
                                val.reason = format!("{}\n{}", val.reason, error);
                            }
                        }
                        "".to_owned()
                    }
                };
                match index {
                    0 => {
                        let _ = write!(output, "{}={}", key, value);
                    }
                    _ => {
                        let _ = write!(output, ",{}={}", key, value);
                    }
                }
                output
            });

    let new_fields: String =
        new_fields
            .iter()
            .enumerate()
            .fold(String::new(), |mut output, (index, (key, value))| {
                let key: String = match key.as_result() {
                    Ok(value) => value,
                    Err(error) => {
                        match &mut capture_error {
                            None => {
                                capture_error = Some(GenerateLineError {
                                    partial_line: "".to_string(),
                                    reason: error,
                                });
                            }
                            Some(val) => {
                                val.reason = format!("{}\n{}", val.reason, error);
                            }
                        }
                        "".to_owned()
                    }
                };
                let value: String = match value.string_for_line_protocol() {
                    Ok(value) => value,
                    Err(error) => {
                        match &mut capture_error {
                            None => {
                                capture_error = Some(GenerateLineError {
                                    partial_line: "".to_string(),
                                    reason: error,
                                });
                            }
                            Some(val) => {
                                val.reason = format!("{}\n{}", val.reason, error);
                            }
                        }
                        "".to_owned()
                    }
                };
                match index {
                    0 => {
                        let _ = write!(output, "{}={}", key, value);
                    }
                    _ => {
                        let _ = write!(output, ",{}={}", key, value);
                    }
                }
                output
            });
    let fields = if fields.is_empty() {
        new_fields
    } else if new_fields.is_empty() {
        fields
    } else {
        format!("{},{}", fields, new_fields)
    };

    let time: String = match time {
        Some(time) => time.to_string(),
        None => "".to_owned(),
    };

    match capture_error {
        None => Ok(format!("{}{} {} {}", measurement, tags, fields, time)),
        Some(mut error) => {
            error.partial_line = format!("{}{} {} {}", measurement, tags, fields, time);
            Err(error)
        }
    }
}
