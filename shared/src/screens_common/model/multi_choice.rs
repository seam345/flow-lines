use log::error;
use std::slice::Iter;

/*#[derive(Clone, Debug)]
pub struct RemoteMultiChoice<T>(RemoteResource<MultiChoice<T>>);

impl<T> RemoteMultiChoice<T>
where
    T: ToString + Clone,
{
    pub fn selected_as_option(&self) -> Option<T> {
        match &self.0 {
            RemoteResource::Loaded(multi_select) => {
                match &multi_select.selected {
                    None => None,
                    Some(index) => {
                        match multi_select.get(*index) {
                            None => {
                                error!("Hmm how do I ever capture this error");
                                None
                            } // in theory this should never happen...
                            Some(value) => Some(value.clone()),
                        }
                    }
                }
            }
            RemoteResource::NonExistent | RemoteResource::NotLoaded(_) => None
        }
    }
}*/

#[derive(Clone, Debug)]
pub struct MultiChoice<T> {
    pub options: Vec<T>,
    pub selected: Option<usize>,
}

impl<T> MultiChoice<T>
where
    T: ToString + Clone,
{
    pub fn iter(&self) -> Iter<'_, T> {
        self.options.iter()
    }

    pub fn len(&self) -> usize {
        self.options.len()
    }

    pub fn is_empty(&self) -> bool {
        self.options.is_empty()
    }

    pub fn clear(&mut self) {
        self.options.clear()
    }

    pub fn to_vec(&self) -> Vec<T> {
        self.options.to_vec()
    }

    pub fn push(&mut self, value: T) {
        self.options.push(value)
    }

    pub fn contains(&self, value: &T) -> bool
    where
        T: PartialEq,
    {
        self.options.contains(value)
    }

    pub fn select_index(&mut self, index: usize) -> Result<(), String> {
        if index >= self.options.len() {
            return Err("out of range".to_owned());
        }

        self.selected = Some(index);

        Ok(())
    }

    pub fn get(&self, index: usize) -> Option<&T> {
        self.options.get(index)
    }

    pub fn selected_as_option(&self) -> Option<T> {
        match &self.selected {
            None => None,
            Some(index) => {
                match self.get(*index) {
                    None => {
                        error!("Hmm how do i ever capture this error");
                        None
                    } // in theory this should never happen...
                    Some(value) => Some(value.clone()),
                }
            }
        }
    }
}

impl<T> Default for MultiChoice<T>
where
    T: ToString + Clone,
{
    fn default() -> Self {
        MultiChoice {
            options: vec![],
            selected: None,
        }
    }
}
