use crate::model_ui::screens_common::remote_multi_choice::multi_choice::{
    UiStringIndex, UiStringIndexErrors,
};
use crate::model_ui::screens_common::validated_string::UiValidatedString;
use crate::screens::input_fields_and_tags::RemoteResource;
use crate::screens::input_fields_and_tags::RemoteResource::{Loaded, NonExistent, NotLoaded};
use log::warn;
use std::fmt::{Debug, Formatter};

/// when we have predefined values for a field but also a user can make a new one
///
#[derive(Clone)]
pub struct UserInputOrMultiChoice {
    // went back and forth for a while where to put the Remote resource, in the end im going with it
    // being the options and not the whole type because user input is not remote
    pub options: RemoteResource<Vec<String>>,
    pub selected: Option<SelectedUserInputOrMultiChoice>,
    /// used to notify users when they have changed input methods...
    /// the string is sent to UI to display to the user, but the second part iy s temp store of the
    /// new value encase the user selects undo (the latter is not sent to ui)
    pub notify_user_of_mode_switch: Option<(String, SelectedUserInputOrMultiChoice)>,
    pub user_validating_function: fn(&str) -> Result<(), String>,
}

impl Debug for UserInputOrMultiChoice {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("UserInputOrMultiChoice")
            .field("options", &self.options)
            .field("selected", &self.selected)
            .field(
                "notify_user_of_mode_swtich",
                &self.notify_user_of_mode_switch,
            )
            // omitted from debug until I can find a good way to insta this :D
            // .field("user_validating_function", std::any::type_name::<>)
            .finish()
    }
}

//todo the bellow seems bad.... I haven't done much error handling :D
pub enum SelectStringIndexError {
    UiStringIndexErrors(UiStringIndexErrors),
    NotLoadedError(NotLoadedError),
    Other(String),
    SelectIndexError(SelectIndexError),
}

impl From<NotLoadedError> for SelectStringIndexError {
    fn from(value: NotLoadedError) -> Self {
        SelectStringIndexError::NotLoadedError(value)
    }
}

impl From<SelectIndexError> for SelectStringIndexError {
    fn from(value: SelectIndexError) -> Self {
        SelectStringIndexError::SelectIndexError(value)
    }
}

pub enum NotLoadedError {
    NotLoaded,
    NonExistent,
}

pub enum SelectIndexError {
    OutOfRange,
    NotLoadedError(NotLoadedError),
}

impl From<NotLoadedError> for SelectIndexError {
    fn from(value: NotLoadedError) -> Self {
        SelectIndexError::NotLoadedError(value)
    }
}

pub enum AcceptModeSwitchError {
    // when notify_user_of_mode_switch is none
    BadInitialState,
}

impl UserInputOrMultiChoice {
    pub fn accept_mode_switch(&mut self) -> Result<(), AcceptModeSwitchError> {
        match &self.notify_user_of_mode_switch {
            None => Err(AcceptModeSwitchError::BadInitialState),
            Some((_, new_value)) => {
                self.selected = Some(new_value.clone());
                self.notify_user_of_mode_switch = None;
                Ok(())
            }
        }
    }

    pub fn dismiss_mode_switch(&mut self) {
        self.notify_user_of_mode_switch = None;
    }

    pub fn push(&mut self, value: String) {
        match self.options {
            NotLoaded(_) => {
                self.options = Loaded(vec![value.clone()]);
            }
            Loaded(ref mut inner) => {
                inner.push(value.clone());
            }
            NonExistent => {
                warn!("hmm this should maybe be an error: 8n29hb");
                self.options = Loaded(vec![value.clone()]);
            }
        }
        let options_reference = match &self.options {
            NotLoaded(_) | NonExistent => {
                panic!("we just set it to loaded!")
            }
            Loaded(inner) => inner,
        };
        if let Some(SelectedUserInputOrMultiChoice::User(user_value)) = &self.selected {
            if let Ok(valid_user_value) = user_value.as_result() {
                if value == valid_user_value {
                    self.selected = Some(SelectedUserInputOrMultiChoice::Index(
                        options_reference.len() - 1,
                    ))
                }
            }
        }
    }

    pub fn valid(&self) -> Option<String> {
        match &self.selected {
            None => None,
            Some(SelectedUserInputOrMultiChoice::Index(index)) => match &self.options {
                NonExistent | NotLoaded(_) => None,
                Loaded(inner) => inner.get(*index).cloned(),
            },
            Some(SelectedUserInputOrMultiChoice::User(value)) => value.as_result().ok(),
        }
    }

    pub fn len(&self) -> Result<usize, NotLoadedError> {
        match &self.options {
            NotLoaded(_) => Err(NotLoadedError::NotLoaded),
            NonExistent => Err(NotLoadedError::NonExistent),
            Loaded(vec) => Ok(vec.len()),
        }
    }
    pub fn select_string_index(
        &mut self,
        index: UiStringIndex,
        warn_user_preference: bool,
    ) -> Result<(), SelectStringIndexError> {
        let index = index
            .to_bound_usize(self.len()?)
            .map_err(SelectStringIndexError::UiStringIndexErrors)?;

        self.select_index(index, warn_user_preference)?;
        Ok(())
    }

    pub fn select_index(
        &mut self,
        index: usize,
        warn_user_preference: bool,
    ) -> Result<(), SelectIndexError> {
        if index >= self.len()? {
            return Err(SelectIndexError::OutOfRange);
        }

        match (warn_user_preference, &self.selected) {
            (true, Some(SelectedUserInputOrMultiChoice::User(_))) => {
                self.notify_user_of_mode_switch = Some(
                    (
                        "Measurements was previously user defined you have now selected a previous measurement".to_owned(),
                        SelectedUserInputOrMultiChoice::Index(index)
                    )
                )
            }
            // allow the bellow is really everything else, but I don't trust myself to not change the type so while I don't have tests this is what we have
            // (_,_) => 
            (false, _) | (true, None) | (true, Some(SelectedUserInputOrMultiChoice::Index(_))) => {
                self.selected = Some(SelectedUserInputOrMultiChoice::Index(index));
            }
        }

        Ok(())
    }

    pub fn set_user_input(&mut self, input: String, warn_user: bool) {
        match (warn_user, &self.selected) {
            (true, Some(SelectedUserInputOrMultiChoice::Index(_))) => {
                self.notify_user_of_mode_switch = Some(
                    (
                        "Measurements was previously selected from pre-existing measurements you have now selected a user defined value".to_owned(),
                        SelectedUserInputOrMultiChoice::User(ValidatingStringPreTrimmed { inner: input, validating_function: self.user_validating_function })
                    )
                )
            }
            // atow the bellow is really everything else, but I don't trust myself to not change the type so while I don't have tests this is what we have
            // (_,_) => 
            (false, _) | (true, None) | (true, Some(SelectedUserInputOrMultiChoice::User(_))) => {
                self.selected = Some(SelectedUserInputOrMultiChoice::User(ValidatingStringPreTrimmed { inner: input, validating_function: self.user_validating_function }));
            }
        }
    }
}

/// internal type for UserInputOrMultiChoice, holding which value has been selected
#[derive(Clone, Debug)]
pub enum SelectedUserInputOrMultiChoice {
    Index(usize),
    User(ValidatingStringPreTrimmed),
}

/// whenever inner is returned it is first trimmed to reduce annoyances
#[derive(Clone, Debug)]
pub struct ValidatingStringPreTrimmed {
    pub inner: String,
    /// Validating function returns Ok() if string is valid and Err(String) if its os invalid
    /// the string is a user facing strut ti explain why it failed
    pub validating_function: fn(&str) -> Result<(), String>,
}

impl ValidatingStringPreTrimmed {
    pub fn is_valid(&self) -> bool {
        (self.validating_function)(self.inner.trim()).is_ok()
    }

    pub fn as_result(&self) -> Result<String, String> {
        match (self.validating_function)(&self.inner) {
            Ok(_) => Ok(self.inner.clone().trim().to_string()),
            Err(reason) => Err(reason),
        }
    }

    pub fn as_ui_validated_string(&self) -> UiValidatedString {
        UiValidatedString {
            value: self.inner.clone(),
            valid: self.is_valid(),
        }
    }
}

impl From<ValidatingStringPreTrimmed> for Option<String> {
    fn from(val: ValidatingStringPreTrimmed) -> Self {
        match val.is_valid() {
            true => Some(val.inner.trim().to_string()),
            false => None,
        }
    }
}
