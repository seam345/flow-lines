use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};

/// Saved previous connections to quickly select
#[derive(Serialize, Deserialize, Clone, Debug, Eq)]
pub struct PreviousConnection {
    pub name: Option<String>,
    pub api_token: String,
    pub base_url: String,
}

impl PartialEq for PreviousConnection {
    fn eq(&self, other: &Self) -> bool {
        self.api_token == other.api_token && self.base_url == other.base_url
    }
}

impl Display for PreviousConnection {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let Some(name) = &self.name else {
            return write!(f, "{}", self.base_url);
        };
        write!(f, "{name}")
    }
}
