use serde::{Deserialize, Serialize};

/// # String index
///
/// Yes this is horrid but since `let temp: u128 = value.into();` fails due to the possibility of
/// usize in future being 256 (I'm maybe remembering wrong) I convert to string since usize -> String
/// always works and Sting -> usize should work if the string was made on the same platform it's now
/// portable code :D open to other suggestions
///
/// I did consider a UID for each item but that seemed to heavyweight when the arrays I was using
/// are short-lived
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct StringIndex(pub String);

impl StringIndex {
    pub fn to_bound_usize(&self, limit: usize) -> Result<usize, StringIndexErrors> {
        let maybe_index = self
            .0
            .parse::<usize>()
            .map_err(|_| StringIndexErrors::Parse)?;
        if maybe_index < limit {
            Ok(maybe_index)
        } else {
            Err(StringIndexErrors::OutOfBounds)
        }
    }
}

impl From<usize> for StringIndex {
    fn from(value: usize) -> Self {
        StringIndex(value.to_string())
    }
}
pub enum StringIndexErrors {
    Parse,
    OutOfBounds,
}
