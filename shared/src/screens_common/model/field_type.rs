use crate::internal_error_strings::N021_USED;
use crate::model_ui::screens::input_fields_and_tags::UiFieldType;
use crate::model_ui::screens_common::validated_string::UiValidatedString;
use serde::{Deserialize, Serialize};
use std::num::IntErrorKind;

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Default)]
pub enum FieldType {
    Bool,
    #[default]
    Float,
    Int,
    UInt,
    String,
}

impl From<UiFieldType> for FieldType {
    fn from(value: UiFieldType) -> Self {
        match value {
            UiFieldType::String => FieldType::String,
            UiFieldType::Int => FieldType::Int,
            UiFieldType::UInt => FieldType::Int,
            UiFieldType::Float => FieldType::Float,
            UiFieldType::Bool => FieldType::Bool,
        }
    }
}

impl TryFrom<&str> for FieldType {
    type Error = String;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value.trim().to_lowercase().as_str() {
            "float" => Ok(FieldType::Float),
            "bool" => Ok(FieldType::Bool),
            "int" => Ok(FieldType::Int),
            "string" => Ok(FieldType::String),
            "uint" => Ok(FieldType::UInt),
            _ => { Err(format!("Unable to match value as a Field type, expected: float|bool|int|string|uint got: {}", value.trim().to_lowercase())) }
        }
    }
}

// None represents user not filling in data or data entered not of that type/valid
// We need to both hold onto whatever the user types in because the UI is very ephemeral and make sure we have a simple way to validate it

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum UserFieldValue {
    Bool(ValidatedFieldBool),
    // why am I currently passing bool as a string!, should really have a checkbox
    Float(ValidatedFieldF64),
    Int(ValidatedFieldInt),
    UInt(ValidatedFieldU64),
    String(ValidatedFieldString),
}

impl Default for UserFieldValue {
    fn default() -> Self {
        Self::Float(ValidatedFieldF64::default())
    }
}

impl UserFieldValue {
    pub fn clear(&mut self) {
        match self {
            UserFieldValue::Bool(inner) => inner.clear(),
            UserFieldValue::Float(inner) => inner.clear(),
            UserFieldValue::Int(inner) => inner.clear(),
            UserFieldValue::UInt(inner) => inner.clear(),
            UserFieldValue::String(inner) => inner.clear(),
        }
    }
    pub fn is_valid(&self) -> bool {
        match self {
            UserFieldValue::Bool(inner) => inner.is_valid(),
            UserFieldValue::Float(inner) => inner.is_valid(),
            UserFieldValue::Int(inner) => inner.is_valid(),
            UserFieldValue::UInt(inner) => inner.is_valid(),
            UserFieldValue::String(inner) => inner.is_valid(),
        }
    }

    pub fn as_ui_validated_string(&self) -> UiValidatedString {
        match self {
            UserFieldValue::Bool(value) => value.as_ui_validated_string(),
            UserFieldValue::Float(value) => value.as_ui_validated_string(),
            UserFieldValue::Int(value) => value.as_ui_validated_string(),
            UserFieldValue::UInt(value) => value.as_ui_validated_string(),
            UserFieldValue::String(value) => value.as_ui_validated_string(),
        }
    }

    pub fn invalid_reason(&self) -> Option<String> {
        match self {
            UserFieldValue::Bool(val) => val.as_result().err(),
            UserFieldValue::Float(val) => val.as_result().err(),
            UserFieldValue::Int(val) => val.as_result().err(),
            UserFieldValue::UInt(val) => val.as_result().err(),
            UserFieldValue::String(val) => val.as_result().err(),
        }
    }

    pub fn string_for_line_protocol(&self) -> Result<String, String> {
        match self {
            UserFieldValue::Bool(value) => Ok(value.as_result()?.to_string()),
            UserFieldValue::Float(value) => Ok(value.as_result()?.to_string()),
            UserFieldValue::Int(value) => Ok(value.as_result()?.to_string()),
            UserFieldValue::UInt(value) => Ok(value.as_result()?.to_string()),
            UserFieldValue::String(value) => {
                let to_send = value.as_result()?;
                Ok(format!("\"{}\"", to_send))
            }
        }
    }

    pub fn update_user_input(&mut self, new_value: String) {
        match self {
            UserFieldValue::Bool(val) => val.update_user_input(new_value),
            UserFieldValue::Float(val) => val.update_user_input(new_value),
            UserFieldValue::Int(val) => val.update_user_input(new_value),
            UserFieldValue::UInt(val) => val.update_user_input(new_value),
            UserFieldValue::String(val) => val.update_user_input(new_value),
        }
    }

    pub fn convert_type(&mut self, new_type: FieldType) -> UserFieldValue {
        match self {
            UserFieldValue::Bool(validated_field) => {
                make_validated_field(new_type, validated_field.inner.clone())
            }
            UserFieldValue::Float(validated_field) => {
                make_validated_field(new_type, validated_field.inner.clone())
            }
            UserFieldValue::Int(validated_field) => {
                make_validated_field(new_type, validated_field.inner.clone())
            }
            UserFieldValue::UInt(validated_field) => {
                make_validated_field(new_type, validated_field.inner.clone())
            }
            UserFieldValue::String(validated_field) => {
                make_validated_field(new_type, validated_field.inner.clone())
            }
        }
    }
}

fn make_validated_field(new_type: FieldType, inner_string: String) -> UserFieldValue {
    match new_type {
        FieldType::Bool => UserFieldValue::Bool(ValidatedFieldBool {
            inner: inner_string,
        }),
        FieldType::Float => UserFieldValue::Float(ValidatedFieldF64 {
            inner: inner_string,
        }),
        FieldType::Int => UserFieldValue::Int(ValidatedFieldInt {
            inner: inner_string,
        }),
        FieldType::UInt => UserFieldValue::UInt(ValidatedFieldU64 {
            inner: inner_string,
        }),
        FieldType::String => UserFieldValue::String(ValidatedFieldString {
            inner: inner_string,
        }),
    }
}

impl From<FieldType> for UserFieldValue {
    fn from(value: FieldType) -> Self {
        (&value).into()
    }
}

impl From<&FieldType> for UserFieldValue {
    fn from(value: &FieldType) -> Self {
        match value {
            FieldType::Bool => UserFieldValue::Bool(ValidatedFieldBool::default()),
            FieldType::Float => UserFieldValue::Float(ValidatedFieldF64::default()),
            FieldType::Int => UserFieldValue::Int(ValidatedFieldInt::default()),
            FieldType::UInt => UserFieldValue::UInt(ValidatedFieldU64::default()),
            FieldType::String => UserFieldValue::String(ValidatedFieldString::default()),
        }
    }
}

pub trait ValidatedType<T> {
    #[allow(dead_code)]
    fn build(value: T) -> Self;
    /// quite simply just implementing the is_ok on the as_result function
    fn is_valid(&self) -> bool;
    fn as_result(&self) -> Result<T, String>;
    fn as_ui_validated_string(&self) -> UiValidatedString;

    fn update_user_input(&mut self, new_value: String);
}

// fn is_valid

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Default)]
pub struct ValidatedFieldBool {
    inner: String,
}

impl ValidatedFieldBool {
    pub(crate) fn clear(&mut self) {
        self.inner.clear()
    }
}

impl ValidatedType<bool> for ValidatedFieldBool {
    fn build(value: bool) -> Self {
        ValidatedFieldBool {
            inner: value.to_string(),
        }
    }
    fn is_valid(&self) -> bool {
        self.as_result().is_ok()
    }
    fn as_result(&self) -> Result<bool, String> {
        self.inner.parse::<bool>().map_err(|err| err.to_string())
    }

    fn as_ui_validated_string(&self) -> UiValidatedString {
        UiValidatedString {
            value: self.inner.clone(),
            valid: self.is_valid(),
        }
    }

    fn update_user_input(&mut self, new_value: String) {
        self.inner = new_value;
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Default)]
pub struct ValidatedFieldString {
    inner: String,
}

impl ValidatedFieldString {
    pub(crate) fn clear(&mut self) {
        self.inner.clear()
    }
}

impl ValidatedFieldString {}

impl ValidatedType<String> for ValidatedFieldString {
    fn build(value: String) -> Self {
        ValidatedFieldString { inner: value }
    }
    fn is_valid(&self) -> bool {
        self.as_result().is_ok()
    }

    fn as_result(&self) -> Result<String, String> {
        // todo at some point understand what can be validated
        Ok(self.inner.to_string())
    }

    fn as_ui_validated_string(&self) -> UiValidatedString {
        UiValidatedString {
            value: self.inner.clone(),
            valid: self.is_valid(),
        }
    }

    fn update_user_input(&mut self, new_value: String) {
        self.inner = new_value;
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Default)]
pub struct ValidatedFieldU64 {
    inner: String, // raw characters user typed in used to reconstruct UI in any case
}

impl ValidatedFieldU64 {
    pub(crate) fn clear(&mut self) {
        self.inner.clear()
    }
}

impl ValidatedFieldU64 {}

impl ValidatedType<u64> for ValidatedFieldU64 {
    fn build(value: u64) -> Self {
        ValidatedFieldU64 {
            inner: value.to_string(),
        }
    }
    fn is_valid(&self) -> bool {
        self.as_result().is_ok()
    }

    fn as_result(&self) -> Result<u64, String> {
        self.inner.parse::<u64>().map_err(|err| err.to_string())
    }

    fn as_ui_validated_string(&self) -> UiValidatedString {
        UiValidatedString {
            value: self.inner.clone(),
            valid: self.is_valid(),
        }
    }

    fn update_user_input(&mut self, new_value: String) {
        self.inner = new_value;
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Default)]
pub struct ValidatedFieldF64 {
    inner: String, // raw characters user typed in used to reconstruct UI in any case
}

impl ValidatedFieldF64 {
    pub(crate) fn clear(&mut self) {
        self.inner.clear()
    }
}

impl ValidatedType<f64> for ValidatedFieldF64 {
    fn build(value: f64) -> Self {
        ValidatedFieldF64 {
            inner: value.to_string(),
        }
    }
    fn is_valid(&self) -> bool {
        self.as_result().is_ok()
    }
    fn as_result(&self) -> Result<f64, String> {
        self.inner.parse::<f64>().map_err(|err| err.to_string())
    }

    fn as_ui_validated_string(&self) -> UiValidatedString {
        UiValidatedString {
            value: self.inner.clone(),
            valid: self.is_valid(),
        }
    }

    fn update_user_input(&mut self, new_value: String) {
        self.inner = new_value;
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Default)]
pub struct ValidatedFieldInt {
    inner: String, // raw characters user typed in used to reconstruct UI in any case
}

impl ValidatedFieldInt {
    pub(crate) fn clear(&mut self) {
        self.inner.clear()
    }
}

impl ValidatedType<i64> for ValidatedFieldInt {
    fn build(value: i64) -> Self {
        ValidatedFieldInt {
            inner: value.to_string(),
        }
    }
    fn is_valid(&self) -> bool {
        self.as_result().is_ok()
    }

    fn as_result(&self) -> Result<i64, String> {
        self.inner.parse::<i64>().map_err(|err| match err.kind() {
            IntErrorKind::Empty => "Empty value, please enter a number".to_owned(),
            IntErrorKind::InvalidDigit => "Invalid digit or misplaced +-".to_owned(),
            IntErrorKind::PosOverflow => "Number too large".to_owned(),
            IntErrorKind::NegOverflow => "Number too small".to_owned(),
            IntErrorKind::Zero => N021_USED.to_owned(), // todo understand this error before writing a proper message
            _ => "Unknown parse error".to_owned(),
        })
    }

    fn as_ui_validated_string(&self) -> UiValidatedString {
        UiValidatedString {
            value: self.inner.clone(),
            valid: self.is_valid(),
        }
    }

    fn update_user_input(&mut self, new_value: String) {
        self.inner = new_value;
    }
}
