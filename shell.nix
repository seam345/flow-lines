{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  # buildInputs is for dependencies you'd need "at run time",
  # were you to to use nix-build not nix-shell and build whatever you were working on
  buildInputs = [
    pkgs.androidStudioPackages.stable.all
    pkgs.rustup
    pkgs.cargo

    # think these were needed for android
    pkgs.nodePackages.pnpm
    pkgs.python39

    # for web
    pkgs.trunk

    # for dev help
    pkgs.cargo-watch
    pkgs.cargo-insta
  ];
 # Set Environment Variables
  RUST_BACKTRACE = 1;
}

