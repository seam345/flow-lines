use crux_core::typegen::TypeGen;
use crux_http::HttpError;
use shared;
use shared::model_ui::screens::input_fields_and_tags::{
    UiFieldType, UiRemoteResourceAvailableFields, UiRemoteResourceAvailableTagValues,
    UiRemoteResourceAvailableTags,
};
use shared::model_ui::screens::settings_preview_connections_actions::UiPrecision;
use shared::model_ui::screens_common::loading::UiLoadingState;
use shared::model_ui::screens_common::remote_multi_choice::remote_resource_multi_choice::UiRemoteResourceMultiChoice;
use shared::model_ui::screens_common::remote_multi_choice::user_input_or_multi_select::{
    UiRemoteResourceUserInputOrMultiChoiceOptions, UiSelectedUserInputOrMultiChoice,
};
use shared::model_ui::user_messages::UiLevel;
use shared::Counter;
use shared::{model_ui, screens};
use std::path::PathBuf;

fn main() -> anyhow::Result<()> {
    println!("cargo:rerun-if-changed=../shared");

    let mut gen = TypeGen::new();

    gen.register_app::<Counter>()?;
    gen.register_type::<UiPrecision>()?;
    gen.register_type::<UiFieldType>()?;
    gen.register_type::<UiLoadingState>()?;
    gen.register_type::<UiRemoteResourceMultiChoice>()?;
    gen.register_type::<UiLevel>()?;
    gen.register_type::<HttpError>()?;
    gen.register_type::<model_ui::UiScreens>()?;
    gen.register_type::<UiRemoteResourceAvailableFields>()?;
    gen.register_type::<UiRemoteResourceAvailableTags>()?;
    gen.register_type::<UiRemoteResourceAvailableTagValues>()?;
    gen.register_type::<UiSelectedUserInputOrMultiChoice>()?;
    gen.register_type::<UiRemoteResourceUserInputOrMultiChoiceOptions>()?;
    gen.register_type::<screens::select_org_bucket_measurements::DontShowDialogAgain>()?;
    gen.register_type::<screens::select_org_bucket_measurements::ConfirmUndo>()?;

    gen.register_type::<screens::initial::ScEvInitial>()?;
    gen.register_type::<screens::select_org_bucket_measurements::ScEvSelectOrgBucketMeasurement>()?;
    gen.register_type::<screens::input_fields_and_tags::ScEvInputFieldsAndTags>()?;
    gen.register_type::<screens::settings::ScEvSettings>()?;
    gen.register_type::<screens::settings_previous_connections_actions::ScEvSettingPreviousConnections>()?;
    gen.register_type::<screens::post_write_review::ScEvPostWriteReview>()?;
    gen.register_type::<shared::global::GlEvGlobal>()?;

    let output_root = PathBuf::from("./generated");

    gen.swift("SharedTypes", output_root.join("swift"))?;

    gen.java(
        "tech.seanborg.flowlines.shared_types",
        output_root.join("java"),
    )?;

    gen.typescript("shared_types", output_root.join("typescript"))?;

    Ok(())
}
