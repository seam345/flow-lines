package tech.seanborg.flowlines.ui;


import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text

import androidx.compose.runtime.Composable;
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import tech.seanborg.flowlines.shared_types.Event
import tech.seanborg.flowlines.shared_types.UiPostWriteReviewScreenData
import tech.seanborg.flowlines.shared_types.UiQuickActionSummery
import kotlinx.coroutines.launch
import tech.seanborg.flowlines.Core;
import tech.seanborg.flowlines.shared_types.ScEvPostWriteReview
import tech.seanborg.flowlines.ui.theme.FlowLinesTheme

@Composable
fun PostWriteReviewScreen(
    data: UiPostWriteReviewScreenData,
    core: Core = viewModel(),
) {

    val coroutineScope = rememberCoroutineScope()

    PostWriteReviewScreenReal(
        data,
        onRepeatClicked = {
            coroutineScope.launch {
                core.update(
                    Event.EvPostWriteReviewScreen(ScEvPostWriteReview.RepeatAction())
                )
            }
        },
        onHomeClicked = {
            coroutineScope.launch {
                core.update(
                    Event.EvPostWriteReviewScreen(ScEvPostWriteReview.MoveToHomeScreen())
                )
            }
        },
        clickedSaveQuickAction = { name ->
            coroutineScope.launch {
                core.update(
                    Event.EvPostWriteReviewScreen(ScEvPostWriteReview.SaveQuickAction(name))
                )
            }

        }
    )
}

@Composable
fun PostWriteReviewScreenReal(
    data: UiPostWriteReviewScreenData,
    onRepeatClicked: () -> Unit,
    onHomeClicked: () -> Unit,
    clickedSaveQuickAction: (String)-> Unit
) {
    Column (
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .background(color = MaterialTheme.colorScheme.background)
    ){
        Text(text = data.previous_write.value)
        Button(onRepeatClicked) {
            Text("repeat entry")
        }
        Button(onHomeClicked) {
            Text("home screen")
        }
        Row(verticalAlignment = Alignment.CenterVertically, modifier = Modifier.padding(8.dp)) {
            var name by remember { mutableStateOf("") }
            OutlinedTextField(
                value = name,
                onValueChange = {
                    name = it
                },
                label = { Text(text = "Quick action name")}
            )
            Button(onClick = {clickedSaveQuickAction(name)}) {
                Text(text = "Save")
            }
        }
    }

}


@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewPostReview() {
    FlowLinesTheme {
        PostWriteReviewScreenReal(
            data = UiPostWriteReviewScreenData(
                UiQuickActionSummery("previos"),
                mutableListOf()
            ),
            {},
            {},
            {},
        )
    }
}

