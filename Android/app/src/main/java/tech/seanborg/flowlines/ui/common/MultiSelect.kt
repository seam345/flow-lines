package tech.seanborg.flowlines.ui.common

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material.icons.outlined.Downloading
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.wear.compose.material.ContentAlpha
import tech.seanborg.flowlines.shared_types.UiLoadingResource
import tech.seanborg.flowlines.shared_types.UiLoadingState
import tech.seanborg.flowlines.shared_types.UiMultiChoice
import tech.seanborg.flowlines.shared_types.UiMultiChoiceItem
import tech.seanborg.flowlines.shared_types.UiRemoteResourceMultiChoice
import tech.seanborg.flowlines.shared_types.UiStringIndex
import tech.seanborg.flowlines.ui.theme.FlowLinesTheme
import java.util.Optional

@Composable
fun MultiSelect(
    data: UiRemoteResourceMultiChoice,
    selectAction: (UiStringIndex) -> Unit,
    buttonName: String,
    modifier: Modifier,
) {
    val displayedOption =
        if (data is UiRemoteResourceMultiChoice.Loaded && data.value.selected_index.isPresent) data.value.options[data.value.selected_index.get().value.toInt()].user_facing_value else ""
    val disable = data is UiRemoteResourceMultiChoice.NonExistent
    val showRetry =
        data is UiRemoteResourceMultiChoice.Loading && data.value.current_state is UiLoadingState.AwaitingUserInteraction

    val hasOptions = data is UiRemoteResourceMultiChoice.Loaded && data.value.options.size > 0
    val textAlpha = if (hasOptions) ContentAlpha.high else ContentAlpha.disabled
    val options =
        if (data is UiRemoteResourceMultiChoice.Loaded) data.value.options else mutableListOf()
    val loading =
        data is UiRemoteResourceMultiChoice.Loading && data.value.current_state is UiLoadingState.RequestSent
    var mExpanded by remember { mutableStateOf(false) }
    val iconOrg =
        if (data is UiRemoteResourceMultiChoice.Loading) Icons.Outlined.Downloading
        else if (mExpanded) Icons.Filled.KeyboardArrowUp
        else Icons.Filled.KeyboardArrowDown

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier.padding(all = 16.dp)
    ) {

        if (displayedOption != "") {
            Text(
                text = displayedOption,
                modifier = Modifier
                    .background(MaterialTheme.colorScheme.primary)
                    .padding(horizontal = 8.dp,),
                MaterialTheme.colorScheme.onPrimary.copy(alpha = textAlpha)
            )
        }
        Row(verticalAlignment = Alignment.CenterVertically) {


            Button(
                onClick = { mExpanded = !mExpanded },
                colors = ButtonDefaults.buttonColors(
                    containerColor = MaterialTheme.colorScheme.primary
                ),
                contentPadding = PaddingValues(0.dp),
                enabled = hasOptions,
            ) {
                Text(
                    text = buttonName,
                    color = MaterialTheme.colorScheme.onPrimary.copy(alpha = textAlpha),
                    modifier = Modifier.padding(ButtonDefaults.ContentPadding)
                )
                if (loading) {
                    CircularProgressIndicator(
                        modifier = Modifier
                            .width((24 + 8).dp)
                            .height(24.dp)
                            .padding(start = 0.dp, end = 8.dp),
                        color = MaterialTheme.colorScheme.secondary,
                        trackColor = MaterialTheme.colorScheme.surfaceVariant,
                    )
                } else {
                    Icon(
                        iconOrg,
                        "contentDescription",
                        Modifier.padding(start = 0.dp, end = 8.dp)
                    )
                }

            }

            if (hasOptions) {
                DropdownMenu(
                    expanded = mExpanded, onDismissRequest = { mExpanded = false },
                ) {
                    options.forEach { label ->
                        DropdownMenuItem(onClick = {
                            mExpanded = false
                            selectAction(label.hidden_uid)
                        }, text = { Text(text = label.user_facing_value) })
                    }
                }
            }
        }
    }
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewLoading() {
    FlowLinesTheme {
        UiRemoteResourceMultiChoice.Loading(
            UiLoadingResource(
                0,
                UiLoadingState.RequestSent()
            )
        )
    }
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewLoaded() {
    FlowLinesTheme {
        UiRemoteResourceMultiChoice.Loaded(
            UiMultiChoice(
                mutableListOf(
                    UiMultiChoiceItem(
                        UiStringIndex("0"), "index measurement",
                    ), UiMultiChoiceItem(
                        UiStringIndex("1"), "index measurement2"
                    )
                ),
                Optional.empty(),
            ),
        )
    }
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewLoadedEmpty() {
    FlowLinesTheme {
        UiRemoteResourceMultiChoice.NonExistent(
        )
    }
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewLoadedSelected() {
    FlowLinesTheme {
        UiRemoteResourceMultiChoice.Loaded(
            UiMultiChoice(
                mutableListOf(
                    UiMultiChoiceItem(
                        UiStringIndex("0"), "index measurement",
                    ), UiMultiChoiceItem(
                        UiStringIndex("1"), "index measurement2"
                    )
                ),
                Optional.of(UiStringIndex("1")),
            )
        )
    }
}

