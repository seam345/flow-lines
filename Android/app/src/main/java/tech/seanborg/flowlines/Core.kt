package tech.seanborg.flowlines

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import tech.seanborg.flowlines.shared_types.Effect
import tech.seanborg.flowlines.shared_types.Event
import tech.seanborg.flowlines.shared_types.PersistentStorageOperation
import tech.seanborg.flowlines.shared_types.PersistentStorageOutput
import tech.seanborg.flowlines.shared_types.Request
import tech.seanborg.flowlines.shared_types.Requests
import tech.seanborg.flowlines.shared_types.UiModel
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.plugins.HttpTimeout
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import tech.seanborg.flow.lines.shared.handleResponse
import tech.seanborg.flow.lines.shared.processEvent
import tech.seanborg.flow.lines.shared.view
import tech.seanborg.flowlines.shared_types.HttpResult

data class PersistentStorage(
    val loadData: LoadData? = null,
    val saveData: SaveData? = null,
    val appendData: AppendData? = null,
)

data class AppendData(
    val filename: String,
    val appendContents: String,
    val uuid: UInt,
)

data class SaveData(
    val filename: String,
    val fileContents: String,
    val uuid: UInt,
)

data class LoadData(
    val filename: String,
    val uuid: UInt,
)


open class Core : androidx.lifecycle.ViewModel() {
    var view: UiModel by mutableStateOf(UiModel.bincodeDeserialize(view()))
        private set

    private val _persistentStorage = MutableStateFlow(PersistentStorage())
    val persistentStorage: StateFlow<PersistentStorage> = _persistentStorage.asStateFlow()

    private val httpClient = HttpClient(CIO) {
        install(HttpTimeout)
    }

    suspend fun persistentStorageSaved(uuid: UInt) {
        Log.v("FL:PS Save data", "saved called")
        _persistentStorage.update { currentUiState ->
            currentUiState.copy(saveData = null)
        }
        Log.v("FL:PS Save data", "save state nulled")
        val response: PersistentStorageOutput = PersistentStorageOutput.SaveOk()
        val effects = handleResponse(uuid, response.bincodeSerialize())
        val requests = Requests.bincodeDeserialize(effects)
        for (request in requests) {
            processEffect(request)
        }
    }

    suspend fun persistentStorageAppended(uuid: UInt) {
        Log.v("FL:PS Append data", "appended called")
        _persistentStorage.update { currentUiState ->
            currentUiState.copy(appendData = null)
        }
        Log.v("FL:PS Append data", "appended state nulled")
        val response: PersistentStorageOutput = PersistentStorageOutput.AppendOk()
        val effects = handleResponse(uuid, response.bincodeSerialize())
        val requests = Requests.bincodeDeserialize(effects)
        for (request in requests) {
            processEffect(request)
        }
    }

    suspend fun persistentStorageLoaded(uuid: UInt, fileData: String) {
        _persistentStorage.update { currentUiState ->
            currentUiState.copy(loadData = null)
        }
        val response: PersistentStorageOutput = PersistentStorageOutput.FileData(fileData)
        val effects = handleResponse(uuid, response.bincodeSerialize())
        val requests = Requests.bincodeDeserialize(effects)
        for (request in requests) {
            processEffect(request)
        }
    }

    suspend fun persistentStorageFileNotExistError(uuid: UInt,) {
        _persistentStorage.update { currentUiState ->
            currentUiState.copy(loadData = null)
        }
        val response: PersistentStorageOutput = PersistentStorageOutput.LoadErrorFileDoesntExist()
        val effects = handleResponse(uuid, response.bincodeSerialize())
        val requests = Requests.bincodeDeserialize(effects)
        for (request in requests) {
            processEffect(request)
        }

    }

    suspend fun update(event: Event) {
        Log.v("FL:AC", "update was called");
        val effects = processEvent(event.bincodeSerialize())

        val requests = Requests.bincodeDeserialize(effects)
        for (request in requests) {
            processEffect(request)
        }
    }

    private suspend fun processEffect(request: Request) {
//        Log.v("FL:AC", "request${request::class.qualifiedName}")
        Log.v("FL:AC", "processEffect=${request.effect::class.qualifiedName}")
        when (val effect = request.effect) {
            is Effect.Render -> {
                Log.v("FL:AC", "render was called");
                this.view = UiModel.bincodeDeserialize(view())
            }

            is Effect.Http -> {
                val response = requestHttp(httpClient, effect.value)
                Log.v("FL: http request", "Response: status: ${response.status}, body: ${response.body}")
                val effects =
                    handleResponse(
                        request.id.toUInt(),
                        HttpResult.Ok(response).bincodeSerialize()
                    )
                val requests = Requests.bincodeDeserialize(effects)
                for (request in requests) {
                    processEffect(request)
                }
            }

            is Effect.PersistentStorage -> {
                when (val operation = effect.value) {
                    is PersistentStorageOperation.Save -> {
                        val filename = operation.field0
                        val fileContents = operation.field1
                        Log.v("FL:PS Save data", "$filename : $fileContents")

                        _persistentStorage.update { currentUiState ->
                            currentUiState.copy(
                                saveData = SaveData(
                                    filename = filename,
                                    fileContents = fileContents,
                                    uuid = request.id.toUInt()
                                )
                            )
                        }
                    }

                    is PersistentStorageOperation.AppendOrCreate -> {
                        val filename = operation.field0
                        val fileContents = operation.field1

                        Log.v("FL:PS Append data", "$filename : $fileContents")

                        _persistentStorage.update { currentUiState ->
                            currentUiState.copy(
                                appendData = AppendData(
                                    filename = filename,
                                    appendContents = fileContents,
                                    uuid = request.id.toUInt()
                                )
                            )
                        }
                    }

                    is PersistentStorageOperation.Load -> {
                        val filename = operation.value
                        Log.v("FL:PS Load data", filename)

                        _persistentStorage.update { currentUiState ->
                            currentUiState.copy(
                                loadData = LoadData(
                                    filename = filename,
                                    uuid = request.id.toUInt()
                                )
                            )
                        }
                    }
                }
            }
        }
    }
}