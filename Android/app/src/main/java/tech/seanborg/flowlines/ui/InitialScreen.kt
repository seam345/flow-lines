package tech.seanborg.flowlines.ui

import android.content.res.Configuration
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Settings
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusEvent
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import tech.seanborg.flowlines.shared_types.Event
import tech.seanborg.flowlines.shared_types.ScEvInitial
import kotlinx.coroutines.launch
import tech.seanborg.flowlines.Core
import tech.seanborg.flowlines.partialFunc1to0
import tech.seanborg.flowlines.ui.common.MultiSelect
import tech.seanborg.flowlines.ui.theme.FlowLinesTheme
import java.util.Optional
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.LinkAnnotation
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextLinkStyles
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withLink
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.PreviewScreenSizes
import androidx.wear.compose.material.ContentAlpha
import tech.seanborg.flowlines.shared_types.GlEvGlobal
import tech.seanborg.flowlines.shared_types.UiConnectQuickActionScreenData
import tech.seanborg.flowlines.shared_types.UiLoadingResource
import tech.seanborg.flowlines.shared_types.UiLoadingState
import tech.seanborg.flowlines.shared_types.UiRemoteResourceMultiChoice
import tech.seanborg.flowlines.shared_types.UiStringIndex
import tech.seanborg.flowlines.ui.common.HighlightorArgs
import tech.seanborg.flowlines.ui.common.HighlightorOverlay


@Composable
fun InitialScreen(
    data: UiConnectQuickActionScreenData,
    core: Core = viewModel(),
    tutorial: Optional<@com.novi.serde.Unsigned Byte>,
) {
    val coroutineScope = rememberCoroutineScope()

    // ====================================================================
    // Load content from persistent storage
    // ====================================================================
    LaunchedEffect(Unit) {
        Log.v("FL:LE", "calling load previous connections")
        coroutineScope.launch {
            core.update(
                Event.EvInitialScreen(
                    ScEvInitial.LoadPreviousConnections()
                )
            )
        }
        Log.v("FL:LE", "calling load Quick actions")
        coroutineScope.launch {
            core.update(
                Event.EvInitialScreen(
                    ScEvInitial.QuickActionsLoad()
                )
            )
        }
    }

    // ====================================================================
    // Declare all interactions with the core and call ui render
    // ====================================================================
    InitialScreenReal(
        data = data,
        tutorial = tutorial,
        onBaseUrlChanged = {
            coroutineScope.launch {
                core.update(
                    Event.EvInitialScreen(ScEvInitial.SetBaseUrl(it))
                )
            }
        },
        onTokenChanged = {
            coroutineScope.launch {
                core.update(
                    Event.EvInitialScreen(ScEvInitial.SetApiToken(it))
                )
            }
        },
        onConnectButtonClicked = {
            coroutineScope.launch {
                core.update(
                    Event.EvInitialScreen(
                        ScEvInitial.InfluxBucketsConnectionTest()
                    )
                )
            }
        },
        onSettingsButtonClicked = {
            coroutineScope.launch {
                core.update(
                    Event.EvInitialScreen(
                        ScEvInitial.LoadSettingsScreen()
                    )
                )
            }
        },
        onPreviousConnectionItemClicked = { stringIndex ->
            coroutineScope.launch {
                core.update(
                    Event.EvInitialScreen(
                        ScEvInitial.UserSelectedPreviousConnection(stringIndex)
                    )
                )
            }
        },
        nextTutorial = { tutorialId ->
            coroutineScope.launch {
                core.update(
                    Event.EvGlobal(
                        GlEvGlobal.UpdateTutorial(tutorialId)
                    )
                )
            }
        },
        moveToNextTutorialScreen = {
            coroutineScope.launch {
                // reset counter
                core.update(
                    Event.EvGlobal(
                        GlEvGlobal.UpdateTutorial(0)
                    )
                )
                // move screen, its in tutoral mode no network activity will happen
                core.update(
                    Event.EvInitialScreen(
                        ScEvInitial.InfluxBucketsConnectionTest()
                    )
                )
            }
        },
        onQuickActionItemClicked = { stringIndex ->
            coroutineScope.launch {
                core.update(
                    Event.EvInitialScreen(
                        ScEvInitial.QuickActionsUserSelected(stringIndex)
                    )
                )
            }
        },
    )
}

@Composable
fun InitialScreenReal(
    data: UiConnectQuickActionScreenData,
    tutorial: Optional<@com.novi.serde.Unsigned Byte>,
    onBaseUrlChanged: (String) -> Unit,
    onTokenChanged: (String) -> Unit,
    onConnectButtonClicked: () -> Unit,
    onSettingsButtonClicked: () -> Unit,
    onPreviousConnectionItemClicked: (UiStringIndex) -> Unit,
    onQuickActionItemClicked: (UiStringIndex) -> Unit,
    nextTutorial: (@com.novi.serde.Unsigned Byte) -> Unit,
    moveToNextTutorialScreen: () -> Unit,
) {
    val coordsMap = remember { mutableStateMapOf<@com.novi.serde.Unsigned Byte, HighlightorArgs>() }

    // ====================================================================
    // Render UI
    // ====================================================================
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .background(color = MaterialTheme.colorScheme.surface)

        ) {

            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End) {
                FloatingActionButton(
                    onClick = onSettingsButtonClicked,
                    modifier = Modifier.onGloballyPositioned { coordinates ->
                        coordsMap[0] = HighlightorArgs(
                            text = AnnotatedString("This tutorial/hint cards can be accessed again in the settings menu"),
                            coordinates = coordinates,
                            nextFunc = partialFunc1to0(nextTutorial, 1),
                            endFunc = {},
                            layoutDeep = 3
                        )

                    }) {
                    Icon(Icons.Rounded.Settings, contentDescription = "Settings")
                }
            }
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight()

            ) {

                Card(
                    modifier = Modifier.onGloballyPositioned { coords ->
                        coordsMap[1] = HighlightorArgs(
                            text = AnnotatedString("New Connection sets the credentials needed to authenticate with an Influx DB database"),
                            coordinates = coords,
                            nextFunc = partialFunc1to0(nextTutorial, 2),
                            endFunc = {},
                            layoutDeep = 2
                        )
                    }
                )
                {
                    Text(
                        text = "New Connection",
                        modifier = Modifier.padding(start = 16.dp, top = 4.dp)
                    )
                    var baseUrl by remember {
                        mutableStateOf(
                            data.base_url.orElse(
                                ""
                            ).toString()
                        )
                    }
                    var baseUrlInteracted by remember {
                        mutableStateOf(false)
                    }
                    OutlinedTextField(
                        value = baseUrl,
                        onValueChange = { value: String ->
                            baseUrl = value
                            onBaseUrlChanged(value)
                        },
                        label = { Text(text = "Base Url") },
                        placeholder = { Text(text = "https://url.com:8086") },
                        singleLine = true,
                        modifier = Modifier
                            .onFocusEvent {
                                if (it.isFocused) {
                                    baseUrlInteracted = true
                                }
                            }
                            .padding(top = 16.dp, end = 16.dp, start = 16.dp, bottom = 4.dp)
                            .onGloballyPositioned { coords ->
                                coordsMap[2] = HighlightorArgs(
                                    text = AnnotatedString("Base Url is the url needed to reach the Influx DB instance, if it is on antoher port than 80/443 please append ':[port num]' e.g http://example.com:8834"),
                                    coordinates = coords,
                                    nextFunc = partialFunc1to0(nextTutorial, 3),
                                    endFunc = {},
                                    layoutDeep = 4
                                )
                            },
                        isError = data.base_url.isEmpty && baseUrlInteracted,
                    )
                    var token by remember {
                        mutableStateOf(
                            data.api_token.orElse(
                                ""
                            ).toString()
                        )
                    }
                    var tokenInteracted by remember {
                        mutableStateOf(false)
                    }
                    val uriHandler = LocalUriHandler.current
                    OutlinedTextField(
                        value = token,
                        onValueChange = { value: String ->
                            token = value
                            onTokenChanged(value)
                        },
                        label = { Text(text = "Token") },
                        placeholder = { Text(text = "") },
                        singleLine = true,
                        modifier = Modifier
                            .onFocusEvent {
                                if (it.isFocused) {
                                    tokenInteracted = true
                                }
                            }
                            .padding(horizontal = 16.dp, vertical = 4.dp)
                            .onGloballyPositioned { coords ->
                                coordsMap[3] = HighlightorArgs(
                                    text = buildAnnotatedString {
                                        append("Login token that can be acquired following the ")

                                        val link =
                                            LinkAnnotation.Url(
                                                "https://docs.influxdata.com/influxdb/v2/admin/tokens/create-token/",
                                                TextLinkStyles(SpanStyle(textDecoration = TextDecoration.Underline))

                                            ) {
                                                val url = (it as LinkAnnotation.Url).url
                                                uriHandler.openUri(url)
                                            }
                                        withLink(link) { append("influx documentation") }
                                        append(" Sorry i don't have an easy way to fetch this jumble of letters (future improvement to come)")
                                    },
                                    coordinates = coords,
                                    nextFunc = partialFunc1to0(nextTutorial, 5),
                                    endFunc = {},
                                    layoutDeep = 5
                                )
                            },
                        isError = tokenInteracted && data.api_token.isEmpty,
                    )
                    val textAlpha = if ( data.base_url.isPresent && data.api_token.isPresent) ContentAlpha.high else ContentAlpha.disabled

                    Button(
                        onClick = onConnectButtonClicked,
                        colors = ButtonDefaults.buttonColors(
                            containerColor = MaterialTheme.colorScheme.primary
                        ),
                        enabled = data.base_url.isPresent && data.api_token.isPresent,
                        modifier = Modifier
                            .padding(
                                top = 4.dp,
                                end = 16.dp,
                                start = 16.dp,
                                bottom = 16.dp
                            )
                            .align(Alignment.CenterHorizontally).onGloballyPositioned { coords ->
                            coordsMap[7] = HighlightorArgs(
                                text = AnnotatedString("Lets move to next screen to define where our data will be placed on this server"),
                                coordinates = coords,
                                nextFunc = moveToNextTutorialScreen,
                                endFunc = {},
                                layoutDeep = 5
                            )
                        }
                        ) { Text(text = "Connect", color = MaterialTheme.colorScheme.onPrimary.copy(alpha = textAlpha)) }
                }
                MultiSelect(
                    data.previous_connections,
                    onPreviousConnectionItemClicked,
                    "Use previous connection",
                    modifier = Modifier.onGloballyPositioned { coords ->
                        coordsMap[5] = HighlightorArgs(
                            text = AnnotatedString("Previous connections are automatically saved in this drop down to reduce pasting in the long token, The list can be managed in the settings"),
                            coordinates = coords,
                            nextFunc = partialFunc1to0(nextTutorial, 6),
                            endFunc = {},
                            layoutDeep = 2
                        )
                    }
                )
                MultiSelect(
                    data.quick_actions,
                    onQuickActionItemClicked,
                    "Quick actions",
                    modifier = Modifier.onGloballyPositioned { coords ->
                        coordsMap[6] = HighlightorArgs(
                            text = AnnotatedString("Previous connections are automatically saved in this drop down to reduce pasting in the long token, The list can be managed in the settings"),
                            coordinates = coords,
                            nextFunc = partialFunc1to0(nextTutorial, 7),
                            endFunc = {},
                            layoutDeep = 2
                        )
                    }
                )
            }
        }

        if (tutorial.isPresent) {
            coordsMap[tutorial.get()]?.let {
                HighlightorOverlay(it)
            }
        }
    }
}


@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewInitialScreen() {
    FlowLinesTheme {
        Column {
            InitialScreenReal(
                data = UiConnectQuickActionScreenData(
                    Optional.of("hey"),
                    Optional.of("http://baseurl"),
                    UiRemoteResourceMultiChoice.Loading(
                        UiLoadingResource(
                            0,
                            UiLoadingState.RequestSent()
                        )
                    ),
                    UiRemoteResourceMultiChoice.Loading(
                        UiLoadingResource(
                            0,
                            UiLoadingState.RequestSent()
                        )
                    ),
                    false
                ),
                Optional.empty(),
                onBaseUrlChanged = { _ -> Unit },
                onTokenChanged = { _ -> Unit },
                onConnectButtonClicked = { Unit },
                onSettingsButtonClicked = { Unit },
                onPreviousConnectionItemClicked = { _ -> Unit },
                moveToNextTutorialScreen = { Unit },
                nextTutorial = { _ -> },
                onQuickActionItemClicked = { _ -> Unit },
            )
        }
    }
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewInitialScreenEmpty() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            InitialScreenReal(
                data = UiConnectQuickActionScreenData(
                    Optional.empty(),
                    Optional.empty(),
                    UiRemoteResourceMultiChoice.NonExistent(),
                    UiRemoteResourceMultiChoice.NonExistent(),
                    false
                ),
                Optional.empty(),
                onBaseUrlChanged = { _ -> Unit },
                onTokenChanged = { _ -> Unit },
                onConnectButtonClicked = { Unit },
                onSettingsButtonClicked = { Unit },
                onPreviousConnectionItemClicked = { _ -> Unit },
                onQuickActionItemClicked = { _ -> Unit },
                moveToNextTutorialScreen = { Unit },
                nextTutorial = { _ -> },
            )
        }
    }
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewInitialScreenTutorial() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            InitialScreenReal(
                data = UiConnectQuickActionScreenData(
                    Optional.empty(),
                    Optional.empty(),
                    UiRemoteResourceMultiChoice.NonExistent(),
                    UiRemoteResourceMultiChoice.NonExistent(),
                    false
                ),
                Optional.of(0),
                moveToNextTutorialScreen = { Unit },
                onBaseUrlChanged = { _ -> Unit },
                onTokenChanged = { _ -> Unit },
                onConnectButtonClicked = { Unit },
                onSettingsButtonClicked = { Unit },
                onPreviousConnectionItemClicked = { _ -> Unit },
                onQuickActionItemClicked = { _ -> Unit },
                nextTutorial = { _ -> },
            )
        }
    }
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewInitialScreenTutorial1() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            InitialScreenReal(
                data = UiConnectQuickActionScreenData(
                    Optional.empty(),
                    Optional.empty(),
                    UiRemoteResourceMultiChoice.NonExistent(),
                    UiRemoteResourceMultiChoice.NonExistent(),
                    false
                ),
                Optional.of(1),
                onBaseUrlChanged = { _ -> Unit },
                onTokenChanged = { _ -> Unit },
                onConnectButtonClicked = { Unit },
                onSettingsButtonClicked = { Unit },
                onPreviousConnectionItemClicked = { _ -> Unit },
                onQuickActionItemClicked = { _ -> Unit },
                moveToNextTutorialScreen = { Unit },
                nextTutorial = { _ -> },
            )
        }
    }
}

@Preview
@Composable
fun PreviewInitialScreenTutorial2() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            InitialScreenReal(
                data = UiConnectQuickActionScreenData(
                    Optional.empty(),
                    Optional.empty(),
                    UiRemoteResourceMultiChoice.NonExistent(),
                    UiRemoteResourceMultiChoice.NonExistent(),
                    false
                ),
                Optional.of(2),
                onBaseUrlChanged = { _ -> Unit },
                onTokenChanged = { _ -> Unit },
                onConnectButtonClicked = { Unit },
                onSettingsButtonClicked = { Unit },
                onPreviousConnectionItemClicked = { _ -> Unit },
                onQuickActionItemClicked = { _ -> Unit },
                moveToNextTutorialScreen = { Unit },
                nextTutorial = { _ -> },
            )
        }
    }
}

@Preview
@Composable
fun PreviewInitialScreenTutorial3() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            InitialScreenReal(
                data = UiConnectQuickActionScreenData(
                    Optional.empty(),
                    Optional.empty(),
                    UiRemoteResourceMultiChoice.NonExistent(),
                    UiRemoteResourceMultiChoice.NonExistent(),

                    false
                ),
                Optional.of(3),
                onBaseUrlChanged = { _ -> Unit },
                onTokenChanged = { _ -> Unit },
                onConnectButtonClicked = { Unit },
                onSettingsButtonClicked = { Unit },
                onPreviousConnectionItemClicked = { _ -> Unit },
                onQuickActionItemClicked = { _ -> Unit },
                moveToNextTutorialScreen = { Unit },
                nextTutorial = { _ -> },
            )
        }
    }
}


@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Preview(name = "PIXEL", device = Devices.PIXEL)
@Composable
fun PreviewInitialScreenTutorial4() {
    FlowLinesTheme {
        InitialScreenReal(
            data = UiConnectQuickActionScreenData(
                Optional.empty(),
                Optional.empty(),
                UiRemoteResourceMultiChoice.NonExistent(),
                UiRemoteResourceMultiChoice.NonExistent(),

                false
            ),
            Optional.of(4),
            onBaseUrlChanged = { _ -> Unit },
            onTokenChanged = { _ -> Unit },
            onConnectButtonClicked = { Unit },
            onSettingsButtonClicked = { Unit },
            onPreviousConnectionItemClicked = { _ -> Unit },
            onQuickActionItemClicked = { _ -> Unit },
            moveToNextTutorialScreen = { Unit },
            nextTutorial = { _ -> },
        )
    }
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Preview(name = "PIXEL", device = Devices.PIXEL)
@PreviewScreenSizes
@Composable
fun PreviewInitialScreenTutorial5() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            InitialScreenReal(
                data = UiConnectQuickActionScreenData(
                    Optional.empty(),
                    Optional.empty(),
                    UiRemoteResourceMultiChoice.NonExistent(),
                    UiRemoteResourceMultiChoice.NonExistent(),

                    false
                ),
                Optional.of(5),
                onBaseUrlChanged = { _ -> Unit },
                onTokenChanged = { _ -> Unit },
                onConnectButtonClicked = { Unit },
                onSettingsButtonClicked = { Unit },
                moveToNextTutorialScreen = { Unit },
                onPreviousConnectionItemClicked = { _ -> Unit },
                onQuickActionItemClicked = { _ -> Unit },
                nextTutorial = { _ -> },
            )
        }
    }
}


@Preview
@Composable
fun PreviewInitialScreenTutorial7() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            InitialScreenReal(
                data = UiConnectQuickActionScreenData(
                    Optional.empty(),
                    Optional.empty(),
                    UiRemoteResourceMultiChoice.NonExistent(),
                    UiRemoteResourceMultiChoice.NonExistent(),

                    false
                ),
                Optional.of(7),
                onBaseUrlChanged = { _ -> Unit },
                onTokenChanged = { _ -> Unit },
                onConnectButtonClicked = { Unit },
                onSettingsButtonClicked = { Unit },
                onPreviousConnectionItemClicked = { _ -> Unit },
                onQuickActionItemClicked = { _ -> Unit },
                moveToNextTutorialScreen = { Unit },
                nextTutorial = { _ -> },
            )
        }
    }
}
