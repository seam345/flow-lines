package tech.seanborg.flowlines


import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.addCallback
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import tech.seanborg.flowlines.shared_types.Event
import tech.seanborg.flowlines.shared_types.UiLevel
import tech.seanborg.flowlines.shared_types.UiLevel.Warn
import tech.seanborg.flowlines.shared_types.UiScreens
import kotlinx.coroutines.launch
import tech.seanborg.flowlines.shared_types.GlEvGlobal
import tech.seanborg.flowlines.ui.InitialScreen
import tech.seanborg.flowlines.ui.InputFieldsTagsScreen
import tech.seanborg.flowlines.ui.PostWriteReviewScreen
import tech.seanborg.flowlines.ui.SelectOrgBucketMeasurementsScreen
import tech.seanborg.flowlines.ui.SettingsPreviousConnectionsScreen
import tech.seanborg.flowlines.ui.SettingsScreen
import tech.seanborg.flowlines.ui.theme.FlowLinesTheme
import java.io.FileNotFoundException


class MainActivity : ComponentActivity() {
    private val viewModel: Core by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // ====================================================================
        // Handle back button
        // ====================================================================
        onBackPressedDispatcher.addCallback(this, true) {
            Log.d("FL: MainActivity", "back button clicked")
            lifecycleScope.launch {
                viewModel.update(Event.EvGlobal(GlEvGlobal.BackButtonClicked()))
            }
        }


        // ====================================================================
        // Handle custom capabilities
        // ====================================================================
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.persistentStorage.collect { uiState ->
                    uiState.saveData?.let { saveData ->
                        applicationContext.openFileOutput(saveData.filename, Context.MODE_PRIVATE)
                            .use {
                                it.write(saveData.fileContents.toByteArray())
                                viewModel.persistentStorageSaved(saveData.uuid)
                            }
                        Log.v("FL:PS Load data", "data saved")
                    }
                    uiState.loadData?.let { loadData ->
                        try {

                        val data =
                            applicationContext.openFileInput(loadData.filename).readAllBytes()
                                .toString(Charsets.UTF_8)
                        Log.v("FL:PS Load data", "data loaded: $data")

                        viewModel.persistentStorageLoaded(loadData.uuid, data)
                        }
                        catch (e: FileNotFoundException) {
                            Log.v("FL:PS Load data", "file not found error")
                            viewModel.persistentStorageFileNotExistError(loadData.uuid)
                        }
                    }
                    uiState.appendData?.let { appendData ->
                        applicationContext.openFileOutput(appendData.filename, Context.MODE_APPEND)
                            .use {
                                it.write(appendData.appendContents.toByteArray())
                                viewModel.persistentStorageAppended(appendData.uuid)
                            }
                        Log.v("FL:PS Append data", "appended: $appendData")
                    }
                }
            }
        }

        // ====================================================================
        // Compose ui
        // ====================================================================
        setContent {
            FlowLinesTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background
                ) {
                    View()
                }
            }
        }
    }
}

@Composable
fun View(core: Core = viewModel()) {
    val coroutineScope = rememberCoroutineScope()
    Log.v("model view", "${core.view}")

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
            .fillMaxHeight()
            .verticalScroll(rememberScrollState())
    ) {
        for (message in core.view.user_messages?.messages.orEmpty()) {
            Log.d("user_message", message.message.toString())
            when (message.level){
                is UiLevel.Warn -> {
                    Text(text = (message.message).toString(), modifier = Modifier.padding(10.dp))
                }
                is UiLevel.Error -> {
                    Text(text = (message.message).toString(), modifier = Modifier.padding(10.dp))
                }
            }
        }

        val tutorial = core.view.tutorial

        when (core.view.screen) {
            is UiScreens.InitialScreen -> {
                InitialScreen(
                    data = (core.view.screen as UiScreens.InitialScreen).value,
                    tutorial = tutorial,
                )
            }

            is UiScreens.InputFieldsTagsScreen -> {
                InputFieldsTagsScreen(
                    (core.view.screen as UiScreens.InputFieldsTagsScreen).value,
                    tutorial = tutorial,

                )
            }

            is UiScreens.SelectMeasurementScreen -> {
                SelectOrgBucketMeasurementsScreen(
                    (core.view.screen as UiScreens.SelectMeasurementScreen).value,
                    tutorial = tutorial,
                )
            }

            is UiScreens.Settings -> {
                SettingsScreen()
            }

            is UiScreens.SettingPreviousConnections -> {
                SettingsPreviousConnectionsScreen((core.view.screen as UiScreens.SettingPreviousConnections).value)
            }

            is UiScreens.PostWriteReview -> {
                PostWriteReviewScreen((core.view.screen as UiScreens.PostWriteReview).value)
            }
        }
    }


}


@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    FlowLinesTheme {
        View()
    }
}