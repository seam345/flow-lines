package tech.seanborg.flowlines.ui.common

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.RoundRect
import androidx.compose.ui.graphics.ClipOp
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.graphics.drawscope.clipPath
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.LayoutCoordinates
import androidx.compose.ui.layout.findRootCoordinates
import androidx.compose.ui.layout.positionInParent
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.unit.dp
import kotlin.math.roundToInt

data class HighlightorArgs(
    val text: AnnotatedString,
    val coordinates: LayoutCoordinates,
    val nextFunc: () -> Unit, // moves to next tutorial screen
    val endFunc: () -> Unit, // ends the tutorial
    val layoutDeep: Int,
)

@Composable
fun Int.pxToDp() = with(LocalDensity.current) { this@pxToDp.toDp() }

@Composable
fun Float.pxToDp() = with(LocalDensity.current) { this@pxToDp.toDp() }

@Composable
fun HighlightorOverlay(content: HighlightorArgs) {
    val cornerRadius = 12f
    val focusPadding = 8
//    var root = content.coordinates.parentCoordinates!!.parentLayoutCoordinates!!.parentCoordinates!!.parentCoordinates!!.parentLayoutCoordinates!!.parentCoordinates!!.parentCoordinates!!.parentLayoutCoordinates!!
    var root = content.coordinates
    var xs = 0.0
    var ys = 0.0
    for (x in 0..<content.layoutDeep) {
//    for (x in 0..9) {
        xs += root.positionInParent().x
        ys += root.positionInParent().y
        root = root.parentLayoutCoordinates!!

    }
//    content.coordinates.localPositionOf(root)

    val offSetInRoot = root.localPositionOf(content.coordinates)
    val contentSize = content.coordinates.size
    val rootSize = content.coordinates.findRootCoordinates().size
//    content.coordinates.positionInParent()
//    content.coordinates.parentCoordinates.positionInRoot()
    val color = MaterialTheme.colorScheme.primaryContainer.copy(alpha = 0.7f)


    val pathToClip = Path().apply {
        addRoundRect(
            RoundRect(
                left = (xs - focusPadding).toFloat(),
                top = (ys - (focusPadding)).toFloat(),
                right = (xs + contentSize.width.toFloat() + focusPadding).toFloat(),
                bottom = (ys + contentSize.height.toFloat() + focusPadding).toFloat(),
                radiusX = cornerRadius,
                radiusY = cornerRadius
            )
        )
    }

    Layout(

        content =
        {
            Canvas(
                modifier = Modifier
                    .width(rootSize.width.pxToDp())
                    .height((rootSize.height + 400).pxToDp()), onDraw = {
                    clipPath(pathToClip, clipOp = ClipOp.Difference) {
                        drawRect(
                            SolidColor(
                                value = color
                            ),
                            topLeft = Offset(0f, 0f)
                        )
                    }
                })
            Card {
                Text(
                    content.text,
                    color = MaterialTheme.colorScheme.onPrimaryContainer,
                    modifier = Modifier
                        .padding(16.dp)
                        .width(
                            (rootSize.width / 2).pxToDp()

                        )
                )
                Row {
                    Button(onClick = content.endFunc, modifier = Modifier.padding(all = 8.dp)) {
                        Text("Skip tutorial")
                    }
                    Button(onClick = content.nextFunc, modifier = Modifier.padding(all = 8.dp)) {
                        Text("Next")
                    }
                }
            }
        },
        // disable tap on everything else
        modifier = Modifier.pointerInput(Unit) {
            detectTapGestures(
                onTap = { _ ->

                }
            )
        }
    )
    { measurables, constraints ->
        check(measurables.size == 2)
        val placeables = measurables.map {
            it.measure(constraints.copy(minWidth = 0, minHeight = 0))
        }

        val highlight = placeables[0]
        val card = placeables[1]

        layout(constraints.maxWidth, rootSize.height + card.height + 10) {
            highlight.placeRelative(
                x = 0,
                y = 0
            )
            card.placeRelative(
                x = (rootSize.width / 2 - card.width / 2),
                y = (ys + contentSize.height.toFloat()).roundToInt() + 24,
            )
        }

    }

}