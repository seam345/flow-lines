package tech.seanborg.flowlines.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import tech.seanborg.flowlines.shared_types.Event
import kotlinx.coroutines.launch
import tech.seanborg.flowlines.Core
import tech.seanborg.flowlines.shared_types.ScEvSettings

@Composable
fun SettingsScreen(
    core: Core = viewModel(),
) {
    val coroutineScope = rememberCoroutineScope()

    SettingsScreenReal(
        onPreviousConnectionsButtonClicked = {
            coroutineScope.launch {
                core.update(
                    Event.EvSettingsScreen(ScEvSettings.OpenPreviousConnectionsScreen())
                )
            }
        },
        onTutorialButtonClicked = {
            coroutineScope.launch {
                core.update(
                    Event.EvSettingsScreen(ScEvSettings.EnterTutorialMode())
                )
            }
        },
        onBackButtonClicked = {
            coroutineScope.launch {
                core.update(
                    Event.EvSettingsScreen(ScEvSettings.Back())
                )
            }
        },
    )

}

@Composable
fun SettingsScreenReal(
    onPreviousConnectionsButtonClicked: () -> Unit,
    onTutorialButtonClicked: () -> Unit,
    onBackButtonClicked: () -> Unit,
) {
    // ====================================================================
    // Render UI
    // ====================================================================
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        Button(
            onClick = onPreviousConnectionsButtonClicked,
            colors = ButtonDefaults.buttonColors(
                containerColor = MaterialTheme.colorScheme.primary
            )
        ) { Text(text = "Previous connections", color = Color.White) }
        Button(
            onClick = onTutorialButtonClicked,
            colors = ButtonDefaults.buttonColors(
                containerColor = MaterialTheme.colorScheme.primary
            )
        ) { Text(text = "Enter Tutorial mode", color = Color.White) }
        Button(
            onClick = onBackButtonClicked,
            colors = ButtonDefaults.buttonColors(
                containerColor = MaterialTheme.colorScheme.secondary
            )
        ) { Text(text = "Back", color = Color.White) }
    }
}

@Preview
@Composable
fun PreviewSettingsScreen() {
    SettingsScreenReal(
        onBackButtonClicked = { Unit },
        onPreviousConnectionsButtonClicked = { Unit },
        onTutorialButtonClicked = { Unit },
    )
}