package tech.seanborg.flowlines

import android.util.Log
import tech.seanborg.flowlines.shared_types.HttpRequest
import com.novi.serde.Bytes
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.timeout
import io.ktor.client.request.headers
import io.ktor.client.request.request
import io.ktor.client.request.setBody
import io.ktor.http.HttpMethod
import io.ktor.util.flattenEntries
import tech.seanborg.flowlines.shared_types.HttpHeader
import tech.seanborg.flowlines.shared_types.HttpResponse
import java.util.concurrent.CancellationException

suspend fun requestHttp(
    client: HttpClient,
    request: HttpRequest,
): HttpResponse {
    Log.v("FL:AC","http called" )
    Log.d("FL: request http", "http called, url ${request.url}")
    var requestSuccess = false
    var retryCount = 0
    do {
        try {
            val response = client.request(request.url) {

                timeout {
                    requestTimeoutMillis = 60000
                }
                this.method = HttpMethod(request.method)
                this.headers {
                    for (header in request.headers) {
                        append(header.name, header.value)
                    }
                }
                if (request.body.content().size > 0) {
                    Log.d("FL: http request", "body: ${String(request.body.content(), Charsets.UTF_8)}")
                    this.setBody(String(request.body.content(), Charsets.UTF_8))
                }else {
                    Log.d("FL: http request", "body is empty")
                }
            }
            val bytes: Bytes = Bytes.valueOf(response.body())
            val headers = response.headers.flattenEntries().map {
                Log.d("http request", "first: ${it.first}, second: ${it.second}")
                HttpHeader(it.first, it.second)
            }
            requestSuccess = true
            return HttpResponse(response.status.value.toShort(), headers, bytes)
        } catch (ce: CancellationException) {
            Log.d("FL: request http", "Got cancellation exception: $ce")
            val headers =
                listOf(HttpHeader("Content-type", "text/plain; charset=utf-8"))
            return HttpResponse(500.toShort(), headers, Bytes.valueOf("Coroutine exception!".toByteArray()))
        } catch (e: Exception) {
            retryCount++
            Log.d("FL: request http", "Got exception: $e")
        }
    }while (!requestSuccess && retryCount < 3)
    val headers =
        listOf(HttpHeader("Content-type", "text/plain; charset=utf-8"))
    return HttpResponse(500.toShort(), headers, Bytes.valueOf("Coroutine exception!".toByteArray()))
}
