package tech.seanborg.flowlines.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.viewmodel.compose.viewModel
import tech.seanborg.flowlines.shared_types.Event
import tech.seanborg.flowlines.shared_types.UiPreviousConnection
import tech.seanborg.flowlines.shared_types.UiSettingsPreviousConnectionsScreenData
import kotlinx.coroutines.launch
import tech.seanborg.flowlines.Core
import tech.seanborg.flowlines.shared_types.ScEvSettingPreviousConnections
import java.util.Optional

@Composable
fun SettingsPreviousConnectionsScreen(
    data: UiSettingsPreviousConnectionsScreenData,
    core: Core = viewModel(),
) {
    val coroutineScope = rememberCoroutineScope()


    SettingsPreviousConnectionsScreenReal(
        data= data,
        onSaveButtonClicked = {andExit ->
            {
                coroutineScope.launch {
                    core.update(
                        Event.EvSettingsPreviousConnectionsScreen(
                            ScEvSettingPreviousConnections.SavePreviousConnections(andExit))
                    )
                }
            }
        },
        onBackButtonClicked = {
            coroutineScope.launch {
                core.update(
                    Event.EvSettingsPreviousConnectionsScreen(
                        ScEvSettingPreviousConnections.ExitToSettingsScreen())
                )
            }
        },
        onConfirmExitScreen = {
            coroutineScope.launch {
                core.update(
                    Event.EvSettingsPreviousConnectionsScreen(
                        ScEvSettingPreviousConnections.ConfirmExitToSettingsScreen())
                )
            }
        },
        renamePreviousConnection = { index, newName ->
            coroutineScope.launch {
                core.update(
                    Event.EvSettingsPreviousConnectionsScreen(
                        ScEvSettingPreviousConnections.RenamePreviousConnection(index, newName))
                )
            }
        },
        deletePreviousConnection = {index ->
            coroutineScope.launch {
                core.update(
                    Event.EvSettingsPreviousConnectionsScreen(
                        ScEvSettingPreviousConnections.DeletePreviousConnection(index))
                )
            }
        },
    )

}

@Composable
fun SettingsPreviousConnectionsScreenReal(
    data: UiSettingsPreviousConnectionsScreenData,
    // boolean, true when clicked on exit confirmation screen, false when clicked normally
    // todo do i need a boolean on the save button... i can just infer from state?
    onSaveButtonClicked: (Boolean) -> () -> Unit,
    onBackButtonClicked: () -> Unit,
    onConfirmExitScreen: () -> Unit,
    renamePreviousConnection: (String, String) -> Unit,
    /// User deletes a connection, edits are not saved to disk
    deletePreviousConnection: (String)-> Unit,

) {
    // ====================================================================
    // Render UI
    // ====================================================================
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        Button(
            onClick = onBackButtonClicked,
            colors = ButtonDefaults.buttonColors(
                containerColor = MaterialTheme.colorScheme.primary
            )
        ) { Text(text = "Back", color = Color.White) }
        for (uiPreviousConnection in data.ui_previous_connections) {
            EditablePreviousConnectionRow(data =uiPreviousConnection , onEdit =  renamePreviousConnection, onDelete = deletePreviousConnection)
        }
        if (data.ui_unsaved_data) {
            Button(
                onClick = onSaveButtonClicked(false),
                colors = ButtonDefaults.buttonColors(
                    containerColor = MaterialTheme.colorScheme.primary
                )
            ) { Text(text = "Save", color = Color.White) }
        }
        if (data.ui_display_save_dialog) {
            ExitSaveDialog(onSaveButtonClicked = onSaveButtonClicked, onConfirmExitScreen = onConfirmExitScreen)
        }
    }
}
fun  onEditPartial(f: (String, String) -> Unit, a: String): (String) -> Unit {
    return { b: String -> f(a, b)}
}

fun  onDeletePartial(f: (String) -> Unit, a: String): () -> Unit {
    return {f(a)}
}
@Composable
fun EditablePreviousConnectionRow(
    data: UiPreviousConnection,
    onEdit: (String, String) -> Unit,
    onDelete: (String)-> Unit,
){
    Row {
        TextField(
            value = (data.name.orElse(
                ""
            )).toString(),
            onValueChange = onEditPartial(onEdit, data.internal_index)
        )
        Text(text = data.base_url)
        Button(
            onClick = onDeletePartial(onDelete, data.internal_index),
            colors = ButtonDefaults.buttonColors(
                containerColor = MaterialTheme.colorScheme.error
            )
        ) { Text(text = "Back", color = Color.White) }
    }
}

@Composable
fun ExitSaveDialog(
    onSaveButtonClicked: (Boolean) -> () -> Unit,
    onConfirmExitScreen: () -> Unit,
) {
    Dialog(onDismissRequest = { /*TODO*/ }) {
        Column {

        Button(
            onClick = onConfirmExitScreen,
            colors = ButtonDefaults.buttonColors(
                containerColor = MaterialTheme.colorScheme.error
            )
        ) { Text(text = "Exit without save", color = Color.White) }
        Button(
            onClick = onSaveButtonClicked(true),
            colors = ButtonDefaults.buttonColors(
                containerColor = MaterialTheme.colorScheme.primary
            )
        ) { Text(text = "Save and exit", color = Color.White) }
        }
    }
}

@Preview
@Composable
fun PreviewSettingsPreviousConnectionsScreen() {
    SettingsPreviousConnectionsScreenReal(
        data = UiSettingsPreviousConnectionsScreenData(
            mutableListOf(UiPreviousConnection("1", Optional.of("name"), "http//:a_url.com")),
    false,
     false,
    ),
        onSaveButtonClicked = { {} },
        onBackButtonClicked = {  },
        renamePreviousConnection = {  _, _ ->  },
        deletePreviousConnection = {  },
        onConfirmExitScreen = {  },
    )
}

@Preview
@Composable
fun PreviewSettingsPreviousConnectionsScreen_UnsavedData() {
    SettingsPreviousConnectionsScreenReal(
        data = UiSettingsPreviousConnectionsScreenData(
            mutableListOf(UiPreviousConnection("1", Optional.of("name"), "http//:a_url.com")),
            true,
            false,
        ),
        onSaveButtonClicked = { {} },
        onBackButtonClicked = {  },
        renamePreviousConnection = {  _, _ ->  },
        deletePreviousConnection = {  },
        onConfirmExitScreen = {  },
    )
}

@Preview
@Composable
fun PreviewSettingsPreviousConnectionsScreen_AttemptedExit() {
    SettingsPreviousConnectionsScreenReal(
        data = UiSettingsPreviousConnectionsScreenData(
            mutableListOf(UiPreviousConnection("1", Optional.of("name"), "http//:a_url.com")),
            true,
            true,
        ),
        onSaveButtonClicked = { {} },
        onBackButtonClicked = {  },
        renamePreviousConnection = {  _, _ ->  },
        deletePreviousConnection = {  },
        onConfirmExitScreen = {  },
    )
}