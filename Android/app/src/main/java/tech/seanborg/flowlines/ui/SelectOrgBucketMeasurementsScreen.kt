package tech.seanborg.flowlines.ui

import android.content.res.Configuration
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TimePicker
import androidx.compose.material3.TimePickerState
import androidx.compose.material3.rememberTimePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.LinkAnnotation
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextLinkStyles
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withLink
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.viewmodel.compose.viewModel
import tech.seanborg.flowlines.shared_types.ConfirmUndo
import tech.seanborg.flowlines.shared_types.DontShowDialogAgain
import tech.seanborg.flowlines.shared_types.Event
import tech.seanborg.flowlines.shared_types.UiRemoteResourceMultiChoice
import tech.seanborg.flowlines.shared_types.UiSelectedUserInputOrMultiChoice
import tech.seanborg.flowlines.shared_types.UiStringIndex
import tech.seanborg.flowlines.shared_types.UiUserInputOrMultiChoice
import tech.seanborg.flowlines.shared_types.UiValidatedString
import kotlinx.coroutines.launch
import tech.seanborg.flowlines.Core
import tech.seanborg.flowlines.partialFunc1to0
import tech.seanborg.flowlines.shared_types.GlEvGlobal
import tech.seanborg.flowlines.shared_types.ScEvSelectOrgBucketMeasurement
import tech.seanborg.flowlines.shared_types.UiLoadingResource
import tech.seanborg.flowlines.shared_types.UiLoadingState
import tech.seanborg.flowlines.shared_types.UiMultiChoice
import tech.seanborg.flowlines.shared_types.UiMultiChoiceItem
import tech.seanborg.flowlines.shared_types.UiRemoteResourceUserInputOrMultiChoiceOptions
import tech.seanborg.flowlines.shared_types.UiSelectOrgBucketMeasurementScreenData
import tech.seanborg.flowlines.ui.common.HighlightorArgs
import tech.seanborg.flowlines.ui.common.HighlightorOverlay
import tech.seanborg.flowlines.ui.common.MultiSelect
import tech.seanborg.flowlines.ui.common.UserInputOrMultiSelect
import tech.seanborg.flowlines.ui.theme.FlowLinesTheme
import java.util.Calendar
import java.util.Optional


@Composable
fun SelectOrgBucketMeasurementsScreen(
    data: UiSelectOrgBucketMeasurementScreenData,
    core: Core = viewModel(),
    tutorial: Optional<@com.novi.serde.Unsigned Byte>,
) {
    val coroutineScope = rememberCoroutineScope()

    // ====================================================================
    // Load remote contetnt for screen
    // ====================================================================
    LaunchedEffect(Unit) {
        Log.v("FL:IFTS", "calling load orgs")
        coroutineScope.launch {
            core.update(
                Event.EvSelectOrgBucketMeasurementScreen(
                    ScEvSelectOrgBucketMeasurement.InfluxGetOrgs()
                )
            )
        }
        Log.v("FL:IFTS", "calling load buckets")
        coroutineScope.launch {
            core.update(
                Event.EvSelectOrgBucketMeasurementScreen(
                    ScEvSelectOrgBucketMeasurement.InfluxGetBuckets()
                )
            )
        }
    }

    SelectOrgBucketMeasurementsScreenReal(
        data = data,
        onClickContinue = {
            coroutineScope.launch {
                core.update(
                    Event.EvSelectOrgBucketMeasurementScreen(
                        ScEvSelectOrgBucketMeasurement.MoveToNextScreen()
                    )
                )
            }
        },
        onClickGetExistingMeasurements = {
            coroutineScope.launch {
                core.update(
                    Event.EvSelectOrgBucketMeasurementScreen(
                        ScEvSelectOrgBucketMeasurement.FluxGetBucketsExistingMeasurements()
                    )
                )
            }
        },
        orgSelected = { stringIndex ->
            coroutineScope.launch {
                core.update(
                    Event.EvSelectOrgBucketMeasurementScreen(
                        ScEvSelectOrgBucketMeasurement.SetOrg(UiStringIndex(stringIndex.value))
                    )
                )
            }
        },
        bucketSelected = { stringIndex ->
            coroutineScope.launch {
                core.update(
                    Event.EvSelectOrgBucketMeasurementScreen(
                        ScEvSelectOrgBucketMeasurement.SetBucket(
                            UiStringIndex(
                                stringIndex.value
                            )
                        )
                    )
                )
            }
        },
        measurementSelected = { stringIndex ->
            coroutineScope.launch {
                core.update(
                    Event.EvSelectOrgBucketMeasurementScreen(
                        ScEvSelectOrgBucketMeasurement.SelectMeasurement(
                            UiStringIndex(
                                stringIndex.value
                            )
                        )
                    )
                )
            }
        },
        measurementUserDefined = { userValue ->
            coroutineScope.launch {
                core.update(
                    Event.EvSelectOrgBucketMeasurementScreen(
                        ScEvSelectOrgBucketMeasurement.UserSetMeasurement(
                            userValue
                        )
                    )
                )
            }
        },

        confirmUserInputOnUserDefinableDropDownList = { confirmUndo ->
            coroutineScope.launch {
                core.update(
                    Event.EvSelectOrgBucketMeasurementScreen(
                        ScEvSelectOrgBucketMeasurement.ConfirmInputChangeOnMeasurements(
                            confirmUndo, DontShowDialogAgain.KeepShowing()
                        )
                    )
                )
            }
        },
        tutorial = tutorial,
        nextTutorial = { tutorialId ->
            coroutineScope.launch {
                core.update(
                    Event.EvGlobal(
                        GlEvGlobal.UpdateTutorial(tutorialId)
                    )
                )
            }
        },
        moveToNextTutorialScreen = {
            coroutineScope.launch {
                // reset counter
                core.update(
                    Event.EvGlobal(
                        GlEvGlobal.UpdateTutorial(0)
                    )
                )
                // move screen, its in tutoral mode no network activity will happen
                core.update(
                    Event.EvSelectOrgBucketMeasurementScreen(
                        ScEvSelectOrgBucketMeasurement.MoveToNextScreen()
                    )
                )
            }
        }
    )
}

// todo make a variant to the drop down list that can be userdeined for measurements
// todo im confused what selected is
@Composable
fun SelectOrgBucketMeasurementsScreenReal(
    data: UiSelectOrgBucketMeasurementScreenData,
    onClickContinue: () -> Unit,
    onClickGetExistingMeasurements: () -> Unit,
    orgSelected: (UiStringIndex) -> Unit,
    bucketSelected: (UiStringIndex) -> Unit,
    measurementSelected: (UiStringIndex) -> Unit,
    measurementUserDefined: (String) -> Unit,
    confirmUserInputOnUserDefinableDropDownList: (ConfirmUndo) -> Unit, // true for never show  again
    tutorial: Optional<@com.novi.serde.Unsigned Byte>,
    nextTutorial: (@com.novi.serde.Unsigned Byte) -> Unit,
    moveToNextTutorialScreen: () -> Unit
) {
    val coordsMap = remember { mutableStateMapOf<@com.novi.serde.Unsigned Byte, HighlightorArgs>() }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .background(color = MaterialTheme.colorScheme.surface/* MaterialTheme.colorScheme.primaryContainer*/)
        ) {


            // organisation
            // todo add button text string
            MultiSelect(
                data = data.orgs, selectAction = orgSelected, buttonName = "Select Org",
                modifier = Modifier.onGloballyPositioned { coords ->
                    coordsMap[0] = HighlightorArgs(
                        text = AnnotatedString("Orgs can be selected here, if there is only 1 org available it will be auto selected"),
                        coordinates = coords,
                        nextFunc = partialFunc1to0(nextTutorial, 1),
                        endFunc = {},
                        layoutDeep = 2
                    )
                }
            )

            MultiSelect(
                data = data.buckets,
                selectAction = bucketSelected,
                buttonName = "Select Bucket",
                modifier = Modifier.onGloballyPositioned { coords ->
                    coordsMap[1] = HighlightorArgs(
                        text = AnnotatedString("Buckets can be selected here, once a bucket is selected we will load the mesasurement available"),
                        coordinates = coords,
                        nextFunc = partialFunc1to0(nextTutorial, 2),
                        endFunc = {},
                        layoutDeep = 2
                    )
                }
            )
            val uriHandler = LocalUriHandler.current
            UserInputOrMultiSelect(
                data.measurements, measurementSelected, measurementUserDefined,
                acceptUserDialog = partialFunc1to0(
                    confirmUserInputOnUserDefinableDropDownList,
                    ConfirmUndo.Confirm()
                ),
                dismissUserDialog = partialFunc1to0(
                    confirmUserInputOnUserDefinableDropDownList,
                    ConfirmUndo.Undo()
                ),
                enabled = data.buckets is UiRemoteResourceMultiChoice.Loaded && (data.buckets as UiRemoteResourceMultiChoice.Loaded).value.selected_index.isPresent,
                label = "Measurement",

                modifier = Modifier.onGloballyPositioned { coords ->
                    coordsMap[2] = HighlightorArgs(
                        text = buildAnnotatedString {
                            append("We can select an existing ")
                            val link =
                                LinkAnnotation.Url(
                                    "https://docs.influxdata.com/influxdb/v1/concepts/glossary/#measurement",
                                    TextLinkStyles(SpanStyle(textDecoration = TextDecoration.Underline))

                                ) {
                                    val url = (it as LinkAnnotation.Url).url
                                    uriHandler.openUri(url)
                                }
                            withLink(link) { append("measurement") }
                            append(" clicking the drop down arrow, or type in a new measurement we want to start logging")
                        },
                        coordinates = coords,
                        nextFunc = partialFunc1to0(nextTutorial, 3),
                        endFunc = {},
                        layoutDeep = 2
                    )
                }
            )
            Row {
                Button(
                    onClick = onClickContinue, colors = ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.primary
                    ),
                    modifier = Modifier.onGloballyPositioned { coords ->
                        coordsMap[3] = HighlightorArgs(
                            text = buildAnnotatedString {
                                append("Now we have defined where our data is going to live in Influx we continue to add data in the next page")
                            },
                            coordinates = coords,
                            nextFunc = moveToNextTutorialScreen,
                            endFunc = {},
                            layoutDeep = 2
                        )
                    }
                ) {
                    Text(
                        text = "Continue to add data",
                        color = MaterialTheme.colorScheme.onPrimary
                    )
                }


            }
        }

        if (tutorial.isPresent) {
            coordsMap[tutorial.get()]?.let {
                HighlightorOverlay(it)
            }
        }
    }
}

@Composable
fun NotifyUserMessage(
    message: String,
    onConfirm: (Boolean) -> Unit,
) {

    Dialog(
        onDismissRequest = { /*TODO*/ },
    ) {
        Card {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.padding(all = 16.dp)
            ) {
                Text(text = message, modifier = Modifier.padding(all = 8.dp))
                Row {

                    Button(onClick = { onConfirm(false) }) {
                        Text("Confirm selection")
                    }
                }
            }
        }
    }
}


@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewSelectOrgBucketMeasurementsScreenRealSelectedItems() {
    FlowLinesTheme {
        Column {
            SelectOrgBucketMeasurementsScreenReal(
                data = UiSelectOrgBucketMeasurementScreenData(
                    /* base_url = */ "base url",
                    /* orgs = */
                    UiRemoteResourceMultiChoice.Loaded(
                        UiMultiChoice(
                            listOf(UiMultiChoiceItem(UiStringIndex("0"), "org1")),
                            Optional.of(UiStringIndex("0"))
                        )

                    ),
                    /* buckets = */
                    UiRemoteResourceMultiChoice.Loaded(
                        UiMultiChoice(
                            listOf(UiMultiChoiceItem(UiStringIndex("0"), "bucket1")),
                            Optional.of(UiStringIndex("0"))
                        )

                    ),
                    /* measurements = */
                    UiUserInputOrMultiChoice(

                        /* options = */ UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), /* selected = */ Optional.of(
                            UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                        ), /* notify_user_of_mode_switch = */ Optional.empty()
                    ),
                ),
                onClickContinue = { Unit },
                onClickGetExistingMeasurements = { Unit },
                orgSelected = { _ -> Unit },
                bucketSelected = { _ -> Unit },
                measurementSelected = { _ -> Unit },
                measurementUserDefined = { _ -> Unit },
                confirmUserInputOnUserDefinableDropDownList = { _ -> Unit },
                nextTutorial = { _ -> Unit },
                tutorial = Optional.empty(),
                moveToNextTutorialScreen = {},
            )
        }
    }
}

@Preview
@Composable
fun PreviewSelectOrgBucketMeasurementsScreenRealNotSelectedItems() {
    FlowLinesTheme {
        Column {
            SelectOrgBucketMeasurementsScreenReal(
                data = UiSelectOrgBucketMeasurementScreenData(
                    "base urk",
                    UiRemoteResourceMultiChoice.Loading(
                        UiLoadingResource(
                            0,
                            UiLoadingState.RequestSent()
                        )
                    ),
                    UiRemoteResourceMultiChoice.Loading(
                        UiLoadingResource(
                            0,
                            UiLoadingState.RequestSent()
                        )
                    ),
                    UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loading(),
                        Optional.empty(),
                        Optional.empty()
                    ),
                ),
                onClickContinue = { Unit },
                onClickGetExistingMeasurements = { Unit },
                orgSelected = { _ -> Unit },
                bucketSelected = { _ -> Unit },
                measurementSelected = { _ -> Unit },
                measurementUserDefined = { _ -> Unit },
                confirmUserInputOnUserDefinableDropDownList = { _ -> Unit },
                moveToNextTutorialScreen = {},
                nextTutorial = { _ -> Unit },
                tutorial = Optional.empty(),
            )
        }
    }
}

@Preview
@Composable
fun PreviewSelectOrgBucketMeasurementsScreenRealMeasurementUserFilled() {
    FlowLinesTheme {
        Column {
            SelectOrgBucketMeasurementsScreenReal(
                data = UiSelectOrgBucketMeasurementScreenData(
                    "base urk",
                    UiRemoteResourceMultiChoice.Loading(
                        UiLoadingResource(
                            0,
                            UiLoadingState.AwaitingUserInteraction()
                        )
                    ),
                    UiRemoteResourceMultiChoice.Loading(
                        UiLoadingResource(
                            0,
                            UiLoadingState.AwaitingUserInteraction()
                        )
                    ),
                    UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), Optional.of(
                            UiSelectedUserInputOrMultiChoice.UserMode(
                                UiValidatedString(
                                    "user measurement",
                                    false
                                )
                            )
                        ), Optional.empty()
                    ),
                ),
                onClickContinue = { Unit },
                onClickGetExistingMeasurements = { Unit },
                orgSelected = { _ -> Unit },
                bucketSelected = { _ -> Unit },
                moveToNextTutorialScreen = {},
                measurementSelected = { _ -> Unit },
                measurementUserDefined = { _ -> Unit },
                confirmUserInputOnUserDefinableDropDownList = { _ -> Unit },
                nextTutorial = { _ -> Unit },
                tutorial = Optional.empty(),
            )
        }
    }
}

@Preview
@Composable
fun Tutorial0() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            SelectOrgBucketMeasurementsScreenReal(
                data = UiSelectOrgBucketMeasurementScreenData(
                    /* base_url = */ "base url",
                    /* orgs = */
                    UiRemoteResourceMultiChoice.Loaded(
                        UiMultiChoice(
                            listOf(UiMultiChoiceItem(UiStringIndex("0"), "org1")),
                            Optional.of(UiStringIndex("0"))
                        )

                    ),
                    /* buckets = */
                    UiRemoteResourceMultiChoice.Loaded(
                        UiMultiChoice(
                            listOf(UiMultiChoiceItem(UiStringIndex("0"), "bucket1")),
                            Optional.of(UiStringIndex("0"))
                        )

                    ),
                    /* measurements = */
                    UiUserInputOrMultiChoice(

                        /* options = */ UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), /* selected = */ Optional.of(
                            UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                        ), /* notify_user_of_mode_switch = */ Optional.empty()
                    ),
                ),
                onClickContinue = { Unit },
                onClickGetExistingMeasurements = { Unit },
                orgSelected = { _ -> Unit },
                bucketSelected = { _ -> Unit },
                measurementSelected = { _ -> Unit },
                measurementUserDefined = { _ -> Unit },
                moveToNextTutorialScreen = {},
                confirmUserInputOnUserDefinableDropDownList = { _ -> Unit },
                nextTutorial = { _ -> Unit },
                tutorial = Optional.of(0),
            )
        }
    }
}


@Preview
@Composable
fun Tutorial1() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            SelectOrgBucketMeasurementsScreenReal(
                data = UiSelectOrgBucketMeasurementScreenData(
                    /* base_url = */ "base url",
                    /* orgs = */
                    UiRemoteResourceMultiChoice.Loaded(
                        UiMultiChoice(
                            listOf(UiMultiChoiceItem(UiStringIndex("0"), "org1")),
                            Optional.of(UiStringIndex("0"))
                        )

                    ),
                    /* buckets = */
                    UiRemoteResourceMultiChoice.Loaded(
                        UiMultiChoice(
                            listOf(UiMultiChoiceItem(UiStringIndex("0"), "bucket1")),
                            Optional.of(UiStringIndex("0"))
                        )

                    ),
                    /* measurements = */
                    UiUserInputOrMultiChoice(

                        /* options = */ UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), /* selected = */ Optional.of(
                            UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                        ), /* notify_user_of_mode_switch = */ Optional.empty()
                    ),
                ),
                onClickContinue = { Unit },
                onClickGetExistingMeasurements = { Unit },
                orgSelected = { _ -> Unit },
                moveToNextTutorialScreen = {},
                bucketSelected = { _ -> Unit },
                measurementSelected = { _ -> Unit },
                measurementUserDefined = { _ -> Unit },
                confirmUserInputOnUserDefinableDropDownList = { _ -> Unit },
                nextTutorial = { _ -> Unit },
                tutorial = Optional.of(1),
            )
        }
    }
}

@Preview
@Composable
fun Tutorial2() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            SelectOrgBucketMeasurementsScreenReal(
                data = UiSelectOrgBucketMeasurementScreenData(
                    /* base_url = */ "base url",
                    /* orgs = */
                    UiRemoteResourceMultiChoice.Loaded(
                        UiMultiChoice(
                            listOf(UiMultiChoiceItem(UiStringIndex("0"), "org1")),
                            Optional.of(UiStringIndex("0"))
                        )

                    ),
                    /* buckets = */
                    UiRemoteResourceMultiChoice.Loaded(
                        UiMultiChoice(
                            listOf(UiMultiChoiceItem(UiStringIndex("0"), "bucket1")),
                            Optional.of(UiStringIndex("0"))
                        )

                    ),
                    /* measurements = */
                    UiUserInputOrMultiChoice(

                        /* options = */ UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), /* selected = */ Optional.of(
                            UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                        ), /* notify_user_of_mode_switch = */ Optional.empty()
                    ),
                ),
                onClickContinue = { Unit },
                onClickGetExistingMeasurements = { Unit },
                orgSelected = { _ -> Unit },
                bucketSelected = { _ -> Unit },
                measurementSelected = { _ -> Unit },
                measurementUserDefined = { _ -> Unit },
                moveToNextTutorialScreen = {},
                confirmUserInputOnUserDefinableDropDownList = { _ -> Unit },
                nextTutorial = { _ -> Unit },
                tutorial = Optional.of(2),
            )
        }
    }
}

@Preview
@Composable
fun Tutorial3() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            SelectOrgBucketMeasurementsScreenReal(
                data = UiSelectOrgBucketMeasurementScreenData(
                    /* base_url = */ "base url",
                    /* orgs = */
                    UiRemoteResourceMultiChoice.Loaded(
                        UiMultiChoice(
                            listOf(UiMultiChoiceItem(UiStringIndex("0"), "org1")),
                            Optional.of(UiStringIndex("0"))
                        )

                    ),
                    /* buckets = */
                    UiRemoteResourceMultiChoice.Loaded(
                        UiMultiChoice(
                            listOf(UiMultiChoiceItem(UiStringIndex("0"), "bucket1")),
                            Optional.of(UiStringIndex("0"))
                        )

                    ),
                    /* measurements = */
                    UiUserInputOrMultiChoice(

                        /* options = */ UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), /* selected = */ Optional.of(
                            UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                        ), /* notify_user_of_mode_switch = */ Optional.empty()
                    ),
                ),
                onClickContinue = { Unit },
                onClickGetExistingMeasurements = { Unit },
                orgSelected = { _ -> Unit },
                bucketSelected = { _ -> Unit },
                measurementSelected = { _ -> Unit },
                measurementUserDefined = { _ -> Unit },
                confirmUserInputOnUserDefinableDropDownList = { _ -> Unit },
                nextTutorial = { _ -> Unit },
                moveToNextTutorialScreen = {},
                tutorial = Optional.of(3),
            )
        }
    }
}

@Preview
@Composable
fun Tutorial4() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            SelectOrgBucketMeasurementsScreenReal(
                data = UiSelectOrgBucketMeasurementScreenData(
                    /* base_url = */ "base url",
                    /* orgs = */
                    UiRemoteResourceMultiChoice.Loaded(
                        UiMultiChoice(
                            listOf(UiMultiChoiceItem(UiStringIndex("0"), "org1")),
                            Optional.of(UiStringIndex("0"))
                        )

                    ),
                    /* buckets = */
                    UiRemoteResourceMultiChoice.Loaded(
                        UiMultiChoice(
                            listOf(UiMultiChoiceItem(UiStringIndex("0"), "bucket1")),
                            Optional.of(UiStringIndex("0"))
                        )

                    ),
                    /* measurements = */
                    UiUserInputOrMultiChoice(

                        /* options = */ UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), /* selected = */ Optional.of(
                            UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                        ), /* notify_user_of_mode_switch = */ Optional.empty()
                    ),
                ),
                onClickContinue = { Unit },
                onClickGetExistingMeasurements = { Unit },
                orgSelected = { _ -> Unit },
                bucketSelected = { _ -> Unit },
                measurementSelected = { _ -> Unit },
                measurementUserDefined = { _ -> Unit },
                confirmUserInputOnUserDefinableDropDownList = { _ -> Unit },
                moveToNextTutorialScreen = {},
                nextTutorial = { _ -> Unit },
                tutorial = Optional.of(4),
            )
        }
    }
}

@Preview
@Composable
fun Tutorial5() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            SelectOrgBucketMeasurementsScreenReal(
                data = UiSelectOrgBucketMeasurementScreenData(
                    /* base_url = */ "base url",
                    /* orgs = */
                    UiRemoteResourceMultiChoice.Loaded(
                        UiMultiChoice(
                            listOf(UiMultiChoiceItem(UiStringIndex("0"), "org1")),
                            Optional.of(UiStringIndex("0"))
                        )

                    ),
                    /* buckets = */
                    UiRemoteResourceMultiChoice.Loaded(
                        UiMultiChoice(
                            listOf(UiMultiChoiceItem(UiStringIndex("0"), "bucket1")),
                            Optional.of(UiStringIndex("0"))
                        )

                    ),
                    /* measurements = */
                    UiUserInputOrMultiChoice(

                        /* options = */ UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), /* selected = */ Optional.of(
                            UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                        ), /* notify_user_of_mode_switch = */ Optional.empty()
                    ),
                ),
                onClickContinue = { Unit },
                onClickGetExistingMeasurements = { Unit },
                orgSelected = { _ -> Unit },
                bucketSelected = { _ -> Unit },
                measurementSelected = { _ -> Unit },
                measurementUserDefined = { _ -> Unit },
                confirmUserInputOnUserDefinableDropDownList = { _ -> Unit },
                moveToNextTutorialScreen = {},
                nextTutorial = { _ -> Unit },
                tutorial = Optional.of(5),
            )
        }
    }
}