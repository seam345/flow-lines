package tech.seanborg.flowlines.ui.common

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import tech.seanborg.flowlines.shared_types.UiMultiChoiceItem
import tech.seanborg.flowlines.shared_types.UiRemoteResourceUserInputOrMultiChoiceOptions
import tech.seanborg.flowlines.shared_types.UiSelectedUserInputOrMultiChoice
import tech.seanborg.flowlines.shared_types.UiStringIndex
import tech.seanborg.flowlines.shared_types.UiUserInputOrMultiChoice
import tech.seanborg.flowlines.ui.theme.FlowLinesTheme
import java.util.Optional

@Composable
fun UserInputOrMultiSelect(
    multi: UiUserInputOrMultiChoice,
    indexSelected: (UiStringIndex) -> Unit,
    userInput: (String) -> Unit,
    acceptUserDialog: () -> Unit,
    dismissUserDialog: () -> Unit,
    enabled: Boolean,
    label: String,
    modifier: Modifier,
) {
    Row (modifier = modifier){
        // Declaring a boolean value to store the expanded state of the Text Field
        var expandedMeasurement by remember { mutableStateOf(false) }
        var displayText = ""
        var valid = true
        if (multi.selected.isPresent) {
            when (multi.selected.get()) {
                is UiSelectedUserInputOrMultiChoice.IndexMode -> {

                    when (multi.options) {
                        is UiRemoteResourceUserInputOrMultiChoiceOptions.Loading -> {
                            valid = false
                        }

                        is UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded -> {
                            val options = (multi.options as UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded).value
                            displayText =
                                options.get(index = ((multi.selected.get() as UiSelectedUserInputOrMultiChoice.IndexMode).value.value).toInt()).user_facing_value
                            valid = true
                        }
                    }
                }

                is UiSelectedUserInputOrMultiChoice.UserMode -> {
                    displayText =
                        (multi.selected.get() as UiSelectedUserInputOrMultiChoice.UserMode).value.value
                    valid =
                        (multi.selected.get() as UiSelectedUserInputOrMultiChoice.UserMode).value.valid
                }
            }
        }

        val selectedValid by remember {
            mutableStateOf(valid)
        }


        val iconOrg =  if (expandedMeasurement) Icons.Filled.KeyboardArrowUp
        else Icons.Filled.KeyboardArrowDown

        OutlinedTextField(
            value = displayText,
            onValueChange = {
                userInput(it)
            },
            enabled = enabled,
            isError = !selectedValid,
            label = { Text(text = label) },
            trailingIcon = {
                if (multi.options is UiRemoteResourceUserInputOrMultiChoiceOptions.Loading && enabled) {
                    CircularProgressIndicator(
                        modifier = Modifier
                            .width((24 + 8).dp)
                            .height(24.dp)
                            .padding(start = 0.dp, end = 8.dp),
                        color = MaterialTheme.colorScheme.secondary,
                        trackColor = MaterialTheme.colorScheme.surfaceVariant,
                    )
                } else {
                  Icon(iconOrg,
                    "contentDescription",
                    Modifier.clickable { expandedMeasurement = !expandedMeasurement })
                }
            },

            )
        if (multi.options is UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded) {
            val options = (multi.options as UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded).value

            DropdownMenu(
                expanded = expandedMeasurement, onDismissRequest = { expandedMeasurement = false },
            ) {
                options.forEach { label ->
                    DropdownMenuItem(onClick = {
                        expandedMeasurement = false
                        indexSelected((label.hidden_uid))
                    }, text = { Text(text = label.user_facing_value) })
                }
            }
        }

        if (multi.notify_user_of_mode_switch.isPresent) {
            Dialog(onDismissRequest = { /*TODO*/ }) {
                Column(
                    modifier = Modifier
                        .background(color = MaterialTheme.colorScheme.surfaceVariant),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally,
                ) {
                    Text(
                        multi.notify_user_of_mode_switch.get(),
                        color = MaterialTheme.colorScheme.onSurfaceVariant,
                        modifier = Modifier.padding(all = 8.dp)
                    )
                    Row(modifier = Modifier.padding(all = 8.dp)) {
                        Button(onClick = dismissUserDialog) {
                            Text(
                                text = "Dismiss\n(undo)",
                                textAlign = TextAlign.Center
                            )
                        }
                        Button(onClick = acceptUserDialog) {
                            Text("Confirm")
                        }
                    }
                }
            }
        }
    }
}


@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewScreenWithTimePicker() {
    FlowLinesTheme {
        UserInputOrMultiSelect(UiUserInputOrMultiChoice(
            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                mutableListOf(
                    UiMultiChoiceItem(
                        UiStringIndex("0"), "index measurement",
                    ), UiMultiChoiceItem(
                        UiStringIndex("1"), "index measurement2"
                    )
                )), Optional.of(
                UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
            ), Optional.empty()
        ), {_ -> }, { _ -> }, {}, {}, true,
            "test1", Modifier)
    }
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewNotLoaded() {
    FlowLinesTheme {
        UserInputOrMultiSelect(UiUserInputOrMultiChoice(
            UiRemoteResourceUserInputOrMultiChoiceOptions.Loading(), Optional.empty(), Optional.empty()
        ), {_ -> }, { _ -> }, {}, {}, true, "preview 2", Modifier)
    }
}


@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewNotLoadedDisabled() {
    FlowLinesTheme {
        UserInputOrMultiSelect(UiUserInputOrMultiChoice(
            UiRemoteResourceUserInputOrMultiChoiceOptions.Loading(), Optional.empty(), Optional.empty()
        ), {_ -> }, { _ -> }, {}, {}, false, "preview 3 not loaded disabled", Modifier)
    }
}