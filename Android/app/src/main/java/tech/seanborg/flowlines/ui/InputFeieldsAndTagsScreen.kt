package tech.seanborg.flowlines.ui

import android.content.res.Configuration
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material.icons.outlined.Delete
import androidx.compose.material.icons.outlined.Downloading
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerDialog
import androidx.compose.material3.DatePickerState
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TimePicker
import androidx.compose.material3.TimePickerState
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.material3.rememberTimePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.LinkAnnotation
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextLinkStyles
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withLink
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.viewmodel.compose.viewModel
import tech.seanborg.flowlines.shared_types.Event
import tech.seanborg.flowlines.shared_types.ScEvInputFieldsAndTags.FieldNewValueSet
import tech.seanborg.flowlines.shared_types.UiAvailableFields
import tech.seanborg.flowlines.shared_types.UiAvailableTagValues
import tech.seanborg.flowlines.shared_types.UiAvailableTags
import tech.seanborg.flowlines.shared_types.UiFieldType
import tech.seanborg.flowlines.shared_types.UiFieldUserInput
import tech.seanborg.flowlines.shared_types.UiLoadingResource
import tech.seanborg.flowlines.shared_types.UiLoadingState
import tech.seanborg.flowlines.shared_types.UiMultiChoiceItem
import tech.seanborg.flowlines.shared_types.UiPrecision
import tech.seanborg.flowlines.shared_types.UiRemoteResourceAvailableFields
import tech.seanborg.flowlines.shared_types.UiRemoteResourceAvailableTagValues
import tech.seanborg.flowlines.shared_types.UiRemoteResourceAvailableTags
import tech.seanborg.flowlines.shared_types.UiRemoteResourceMultiChoice
import tech.seanborg.flowlines.shared_types.UiRemoteResourceUserInputOrMultiChoiceOptions
import tech.seanborg.flowlines.shared_types.UiSelectedUserInputOrMultiChoice
import tech.seanborg.flowlines.shared_types.UiStringIndex
import tech.seanborg.flowlines.shared_types.UiUserFields
import tech.seanborg.flowlines.shared_types.UiUserInputOrMultiChoice
import tech.seanborg.flowlines.shared_types.UiUserTags
import tech.seanborg.flowlines.shared_types.UiValidatedString
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import tech.seanborg.flowlines.Core
import tech.seanborg.flowlines.partialFunc1to0
import tech.seanborg.flowlines.partialFunc2to1
import tech.seanborg.flowlines.shared_types.GlEvGlobal
import tech.seanborg.flowlines.shared_types.ScEvInputFieldsAndTags
import tech.seanborg.flowlines.shared_types.UiInputFieldsAndTagsScreenData
import tech.seanborg.flowlines.ui.common.HighlightorArgs
import tech.seanborg.flowlines.ui.common.HighlightorOverlay
import tech.seanborg.flowlines.ui.common.UserInputOrMultiSelect
import tech.seanborg.flowlines.ui.theme.FlowLinesTheme
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Optional


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun InputFieldsTagsScreen(
    data: UiInputFieldsAndTagsScreenData,
    core: Core = viewModel(),
    tutorial: Optional<@com.novi.serde.Unsigned Byte>,
) {
    val coroutineScope = rememberCoroutineScope()

    var setTime by remember { mutableStateOf(false) }
    var time by remember { mutableStateOf(Optional.empty<String>()) }
    var setDate by remember { mutableStateOf(false) }
    var date by remember { mutableStateOf(Optional.empty<String>()) }

    // ====================================================================
    // Load remote content for screen
    // ====================================================================
    LaunchedEffect(Unit) {
        Log.v("FL:IFTS", "calling load fields")
        coroutineScope.launch {
            core.update(
                Event.EvInputFieldsAndTagsEventsScreen(
                    ScEvInputFieldsAndTags.FluxGetMeasurementsExistingFields()
                )
            )
        }
        Log.v("FL:IFTS", "calling load tags")
        coroutineScope.launch {
            core.update(
                Event.EvInputFieldsAndTagsEventsScreen(
                    ScEvInputFieldsAndTags.FluxGetMeasurementsExistingTags()
                )
            )
        }
    }


    InputFieldsTagsScreenReal(
        data = data,
        availableTagsOnClick = { tagName: String ->
            availableTagOnClick(
                tagName = tagName,
                selected = data.tags.keys,
                coroutineScope = coroutineScope,
                core = core
            )
        },
        availableFieldOnClick = { fieldName: String ->
            availableFieldOnClick(
                fieldName = fieldName,
                selected = data.fields.keys,
                coroutineScope = coroutineScope,
                core = core
            )
        },
        fieldExistingValueSet = { key: String, value: String ->
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.FieldExistingValueSet(key, value)
                    )
                )
            }
        },
        onWriteClick = {
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.InfluxWrite()
                    )
                )
            }
        },
        setTime,
        onSetTimeClicked = { setTime = true },
        onTimeConfirm = { timePickerState: TimePickerState ->
            setTime = false
            val string =
                "T${"%02d".format(timePickerState.hour)}:${"%02d".format(timePickerState.minute)}:00Z"
            Log.v("FL:TimeString", string)
            time = Optional.of(string)
            if (!time.isEmpty and !date.isEmpty) {
                Log.v("FL:TimeString", "string sent to rust: ${date.get()}${time.get()}")
                coroutineScope.launch {
                    core.update(
                        Event.EvInputFieldsAndTagsEventsScreen(
                            ScEvInputFieldsAndTags.TimeSet("${date.get()}${time.get()}")
                        )
                    )
                }
            }
        },
        setDate,
        onSetDateClicked = { setDate = true },
        onDateConfirmed = { datePickerState: DatePickerState ->
            setDate = false
            datePickerState.selectedDateMillis?.let {
                val formatter = SimpleDateFormat("yyyy-MM-dd")
                val calendar = Calendar.getInstance()
                calendar.timeInMillis = it
                val string = formatter.format(calendar.time)

                date = Optional.of(string)
                if (!time.isEmpty and !date.isEmpty) {
                    Log.v("FL:TimeString", "time sent to rust ${date.get()}${time.get()}")
                    coroutineScope.launch {
                        core.update(
                            Event.EvInputFieldsAndTagsEventsScreen(
                                ScEvInputFieldsAndTags.TimeSet("${date.get()}${time.get()}")
                            )
                        )
                    }
                }
            }
        },
        tagExistingValueSetIndex = { tagKey, tagValueIndex
            ->
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.TagExistingValueSetIndex(
                            tagKey,
                            tagValueIndex
                        )
                    )
                )
            }
        },
        tagExistingValueSetUser = { tagKey, tagValue ->
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.TagExistingValueSetUser(tagKey, tagValue)
                    )
                )
            }
        },
        tagNewKaySet = { stringIndex, key ->
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.TagNewKaySet(stringIndex, key)
                    )
                )
            }
        },
        tagNewValueSet = { stringIndex, tagValue ->
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.TagNewValueSet(stringIndex, tagValue)
                    )
                )
            }

        },
        tagNewAdd = {
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.TagNewAdd()
                    )
                )
            }
        },
        tagExistingValueAcceptModeChange = { tagKey ->
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.TagExistingValueAcceptModeChange(tagKey)
                    )
                )
            }
        },
        tagExistingValueDismissModeChange = { tagKey ->
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.TagExistingValueDismissModeChange(tagKey)
                    )
                )
            }
        },
        tagNewRemove = { stringIndex ->
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.TagNewRemove(stringIndex)
                    )
                )
            }

        },
        requestTagsFetch = {
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.FluxGetMeasurementsExistingFields()
                    )
                )
            }
        },
        requestFieldsFetch = {
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.FluxGetMeasurementsExistingFields()
                    )
                )
            }
        },
        fieldNewValueSet = { index: UiStringIndex, value: String ->
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.FieldNewValueSet(index, value)
                    )
                )
            }
        },
        fieldNewKeySet = { index: UiStringIndex, key: String ->
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.FieldNewKeySet(index, key)
                    )
                )
            }
        },
        fieldNewChangeType = { index: UiStringIndex, type: UiFieldType ->
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.FieldNewChangeType(index, type)
                    )
                )
            }
        },
        fieldNewAdd = {
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.FieldNewAdd()
                    )
                )
            }
        },
        fieldNewRemove = { index: UiStringIndex ->
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.FieldNewRemove(index)
                    )
                )
            }
        },
        tutorial = tutorial,
        nextTutorial = { tutorialId ->
            coroutineScope.launch {
                core.update(
                    Event.EvGlobal(
                        GlEvGlobal.UpdateTutorial(tutorialId)
                    )
                )
            }
        },
        moveToNextTutorialScreen = {
            coroutineScope.launch {
                // reset counter
                core.update(
                    Event.EvGlobal(
                        GlEvGlobal.UpdateTutorial(0)
                    )
                )
                // move screen, its in tutoral mode no network activity will happen
//                core.update(
//                    Event.SelectOrgBucketMeasurementScreenEvents(
//                        SelectOrgBucketMeasurementScreenEventsData.MoveToNextScreen()
//                    )
//                )
            }
        },
        endTutorial = {
            coroutineScope.launch {

            }
        }
    )
}

// todo where are the edit tags selected...
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun InputFieldsTagsScreenReal(
    data: UiInputFieldsAndTagsScreenData,
    availableTagsOnClick: (String) -> () -> Unit,
    availableFieldOnClick: (String) -> () -> Unit,
    fieldExistingValueSet: (String, String) -> Unit,
    onWriteClick: () -> Unit,
    selectTime: Boolean,
    onSetTimeClicked: () -> Unit,
    onTimeConfirm: (TimePickerState) -> Unit,
    selectDate: Boolean,
    onSetDateClicked: () -> Unit,
    onDateConfirmed: (DatePickerState) -> Unit,
    tagExistingValueSetIndex: (String, UiStringIndex) -> Unit,
    tagExistingValueSetUser: (String, String) -> Unit,
    tagNewValueSet: (UiStringIndex, String) -> Unit,
    tagNewKaySet: (UiStringIndex, String) -> Unit,
    tagNewRemove: (UiStringIndex) -> Unit,
    tagNewAdd: () -> Unit,
    tagExistingValueAcceptModeChange: (String) -> Unit,
    tagExistingValueDismissModeChange: (String) -> Unit,
    requestFieldsFetch: () -> Unit,
    requestTagsFetch: () -> Unit,
    fieldNewValueSet: (UiStringIndex, String) -> Unit,
    fieldNewKeySet: (UiStringIndex, String) -> Unit,
    fieldNewChangeType: (UiStringIndex, UiFieldType) -> Unit,
    fieldNewAdd: () -> Unit,
    fieldNewRemove: (UiStringIndex) -> Unit,
    tutorial: Optional<@com.novi.serde.Unsigned Byte>,
    nextTutorial: (@com.novi.serde.Unsigned Byte) -> Unit,
    moveToNextTutorialScreen: () -> Unit,
    endTutorial: () -> Unit
) {
    val coordsMap = remember { mutableStateMapOf<@com.novi.serde.Unsigned Byte, HighlightorArgs>() }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        Column(
            verticalArrangement = Arrangement.Center,
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .background(color = MaterialTheme.colorScheme.background/* MaterialTheme.colorScheme.primaryContainer*/)
                .onGloballyPositioned { coords ->
                    coordsMap[0] = HighlightorArgs(
                        text = AnnotatedString("This page got quite dense, but here is where you enter the data!"),
                        coordinates = coords,
                        nextFunc = partialFunc1to0(nextTutorial, 1),
                        endFunc = {},
                        layoutDeep = 0
                    )
                }
        ) {
            var bigFont = MaterialTheme.typography.headlineSmall.fontSize
            val uriHandler = LocalUriHandler.current

            Tags(
                rrAvailableTags = data.available_tags,
                newTags = data.new_tags,
                tagNewAdd = tagNewAdd,
                availableTagsOnClick = availableTagsOnClick,
                tags = data.tags,
                tagExistingValueSetIndex = tagExistingValueSetIndex,
                tagExistingValueSetUser = tagExistingValueSetUser,
                tagExistingValueAcceptModeChange = tagExistingValueAcceptModeChange,
                tagExistingValueDismissModeChange = tagExistingValueDismissModeChange,
                tagNewValueSet = tagNewValueSet,
                tagNewKaySet = tagNewKaySet,
                tagNewRemove = tagNewRemove,
                requestTagsFetch = requestTagsFetch,
                modifier = Modifier.onGloballyPositioned { coords ->
                    coordsMap[1] = HighlightorArgs(
                        text = buildAnnotatedString {
                            pushStyle(SpanStyle(fontSize = bigFont))
                            append("Tags\n")
                            pop()
                            append("I load any tags already entered for this measurement,")
                            append("you can select which tags you would like to include with the radio buttons,")
                            append("or add a completely new tag")
                        },
                        coordinates = coords,
                        nextFunc = partialFunc1to0(nextTutorial, 2),
                        endFunc = {},
                        layoutDeep = 2
                    )
                }
            )

            Text(text = "Fields", color = MaterialTheme.colorScheme.onBackground)

            Fields(
                rrAvailableFields = data.available_fields,
                newFields = data.new_fields,
                selectedFields = data.fields,
                availableFieldOnClick = availableFieldOnClick,
                fieldExistingValueSet = fieldExistingValueSet,
                requestFieldsFetch = requestFieldsFetch,
                fieldNewValueSet = fieldNewValueSet,
                fieldNewKeySet = fieldNewKeySet,
                fieldNewChangeType = fieldNewChangeType,
                fieldNewAdd = fieldNewAdd,
                fieldNewRemove = fieldNewRemove,
                modifier = Modifier.onGloballyPositioned { coords ->
                    coordsMap[2] = HighlightorArgs(
                        text = buildAnnotatedString {
                            pushStyle(SpanStyle(fontSize = bigFont))
                            append("Fields\n")
                            pop()
                            append("Similar to tags I auto load any entered in the past.")
                            append("you can select which fields you would like to enter data for with the radio buttons,")
                            append("or add new fields")
                        },
                        coordinates = coords,
                        nextFunc = partialFunc1to0(nextTutorial, 3),
                        endFunc = {},
                        layoutDeep = 2
                    )
                }
            )

            Button(
                onClick = onWriteClick, colors = ButtonDefaults.buttonColors(
                    containerColor = MaterialTheme.colorScheme.primary
                ),
                modifier = Modifier.onGloballyPositioned { coords ->
                coordsMap[4] = HighlightorArgs(
                    text = AnnotatedString("Finally hitting write influx will send the data to the InfluxDB server"),
                    coordinates = coords,
                    nextFunc = partialFunc1to0(nextTutorial, 4),
                    endFunc = endTutorial,
                    layoutDeep = 2
                )
            }
            ) { Text(text = "write influx", color = MaterialTheme.colorScheme.onPrimary) }


            Text(text = data.ui_time.orElse(""), color = MaterialTheme.colorScheme.onBackground)

            Row (modifier = Modifier.onGloballyPositioned { coords ->
                coordsMap[3] = HighlightorArgs(
                    text = AnnotatedString("Time and Date of the data can be set here, it is a little buggy atm as nothing gets set until both have been set"),
                    coordinates = coords,
                    nextFunc = partialFunc1to0(nextTutorial, 4),
                    endFunc = endTutorial,
                    layoutDeep = 2
                )
            }){
                Button(
                    onClick = onSetTimeClicked, colors = ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.secondary
                    )
                ) { Text(text = "setTime", color = MaterialTheme.colorScheme.onSecondary) }

                Button(
                    onClick = onSetDateClicked, colors = ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.secondary
                    )
                ) { Text(text = "set date", color = MaterialTheme.colorScheme.onSecondary) }
            }
            if (selectTime) {
                DialUseStateExample(onConfirm = onTimeConfirm) { }
            }
            if (selectDate) {
                DatePickerModal(onDateSelected = onDateConfirmed) { }
            }

            Text(text = data.expected_line)

        }
        if (tutorial.isPresent) {
            coordsMap[tutorial.get()]?.let {
                HighlightorOverlay(it)
            }
        }
    }
}

fun availableTagOnClick(
    tagName: String, selected: Set<String>, coroutineScope: CoroutineScope, core: Core
): () -> Unit {
    return {
        Log.v("FL AvailableTagOnClick", "Running AvailableTagOnClick with tag: $tagName")
        if (tagName in selected) {
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.TagExistingRemove(tagName)
                    )
                )
            }
        } else {
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.TagExistingAdd(tagName)
                    )
                )
            }
        }
    }
}


fun availableFieldOnClick(
    fieldName: String, selected: Set<String>, coroutineScope: CoroutineScope, core: Core
): () -> Unit {
    return {
        Log.v("FL AvailableFieldOnClick", "Running AvailableFieldOnClick with field: $fieldName")
        if (fieldName in selected) {
            coroutineScope.launch {
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.FieldExistingRemove(fieldName)
                    )
                )
            }
        } else {
            coroutineScope.launch {
                Log.v("FL AvailableFieldOnClick", "coroutine launched no selected")
                core.update(
                    Event.EvInputFieldsAndTagsEventsScreen(
                        ScEvInputFieldsAndTags.FieldExistingAdd(fieldName)
                    )
                )
            }
        }
    }
}


@Composable
fun FieldsRow(
    fieldName: String,
    userValue: UiFieldUserInput?,
    availableFieldOnClick: (String) -> () -> Unit,
    fieldExistingValueSet: (String, String) -> Unit,

    ) {
    val selected = userValue != null

    Row(
        Modifier
            .padding(horizontal = 8.dp)
            .selectable(
                selected = selected, onClick = availableFieldOnClick(fieldName)
            ), verticalAlignment = Alignment.CenterVertically
    ) {
        RadioButton(
            selected = selected, onClick = availableFieldOnClick(fieldName)
        )

        if (!selected) {

            Text(
                text = fieldName,
                modifier = Modifier.padding(start = 8.dp),
                color = MaterialTheme.colorScheme.onBackground
            )
        } else {
            FieldInput(
                userValue!!,
                fieldName,
                partialFunc2to1(fieldExistingValueSet, fieldName),
                Modifier
            )

        }
    }

}

@Composable
fun FieldInput(
    userValue: UiFieldUserInput,
    displayLabel: String,
    valueSet: (String) -> Unit,
    modifier: Modifier
) {
    when (userValue.field_type) {
        is UiFieldType.String -> {
            OutlinedTextField(
                value = userValue.input.value,
                onValueChange = { value: String ->
                    valueSet(value)
                },
                label = { Text(text = displayLabel) },
                isError = !userValue.input.valid,
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
                modifier = modifier,
            )
        }

        is UiFieldType.Bool -> {
            OutlinedTextField(
                value = userValue.input.value,
                onValueChange = { value: String ->
                    valueSet(value)
                },
                label = { Text(text = displayLabel) },
                isError = !userValue.input.valid,
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
                modifier = modifier,
            )
        }

        is UiFieldType.Int -> {
            OutlinedTextField(
                value = userValue.input.value,
                onValueChange = { value: String ->
                    valueSet(value)
                },
                label = { Text(text = displayLabel) },
                isError = !userValue.input.valid,
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                modifier = modifier,
            )
        }

        is UiFieldType.UInt -> {
            OutlinedTextField(
                value = userValue.input.value,
                onValueChange = { value: String ->
                    valueSet(value)
                },
                label = { Text(text = displayLabel) },
                isError = !userValue.input.valid,
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                modifier = modifier,
            )
        }

        is UiFieldType.Float -> {
            OutlinedTextField(
                value = userValue.input.value,
                onValueChange = { value: String ->
                    valueSet(value)
                },
                label = { Text(text = displayLabel) },
                isError = !userValue.input.valid,
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                modifier = modifier,
            )
        }
    }

}

@Composable
fun Fields(
    rrAvailableFields: UiRemoteResourceAvailableFields,
    newFields: List<UiUserFields>,
    selectedFields: Map<String, UiFieldUserInput>,
    availableFieldOnClick: (String) -> () -> Unit,
    requestFieldsFetch: () -> Unit,
    fieldExistingValueSet: (String, String) -> Unit,
    fieldNewValueSet: (UiStringIndex, String) -> Unit,
    fieldNewKeySet: (UiStringIndex, String) -> Unit,
    fieldNewChangeType: (UiStringIndex, UiFieldType) -> Unit,
    fieldNewAdd: () -> Unit,
    fieldNewRemove: (UiStringIndex) -> Unit,
    modifier: Modifier
) {
    Column (modifier = modifier){
        when (rrAvailableFields) {
            is UiRemoteResourceAvailableFields.Loaded -> {

                rrAvailableFields.value.value.map { fieldName: String ->
                    FieldsRow(
                        fieldName,
                        selectedFields[fieldName],
                        availableFieldOnClick,
                        fieldExistingValueSet
                    )
                }
            }

            is UiRemoteResourceAvailableFields.Loading -> {
                when (rrAvailableFields.value.current_state) {
                    is UiLoadingState.RequestSent -> {
                        CircularProgressIndicator(
                            modifier = Modifier
                                .width((32 + 8).dp)
                                .height(32.dp)
                                .padding(start = 8.dp, end = 0.dp),
                            color = MaterialTheme.colorScheme.secondary,
                            trackColor = MaterialTheme.colorScheme.surfaceVariant,
                        )
                    }

                    else -> {
                        Button(
                            onClick = requestFieldsFetch, colors = ButtonDefaults.buttonColors(
                                containerColor = MaterialTheme.colorScheme.secondary
                            )
                        ) {
                            Text(
                                text = "Retry tag fetch",
                                color = MaterialTheme.colorScheme.onSecondary
                            )
                        }
                    }
                }
            }
        }
        newFields.forEach { userField ->
            Row {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 8.dp)
                ) {
                    Icon(
                        Icons.Outlined.Delete,
                        "contentDescription",
                        tint =
                        MaterialTheme.colorScheme.onBackground,
                        modifier = Modifier
                            .padding(start = 10.dp, end = 1.dp)
                            .size(32.dp)
                            .clickable { fieldNewRemove(userField.hidden_uid) }
                    )

                    OutlinedTextField(
                        value = userField.key.value.toString(),
                        onValueChange = { value: String ->
                            fieldNewKeySet(userField.hidden_uid, value)
                        },
                        label = { Text(text = "Name") },
                        isError = !userField.key.valid,
                        modifier = Modifier.fillMaxWidth(0.35F)
                    )
                    val label = when (userField.value.field_type) {
                        is UiFieldType.String -> {
                            "Value (String)"
                        }

                        is UiFieldType.Bool -> {
                            "Value (Bool)"
                        }

                        is UiFieldType.Int -> {
                            "Value (Int)"
                        }

                        is UiFieldType.UInt -> {
                            "Value (Uint)"
                        }

                        is UiFieldType.Float -> {
                            "Value (Float)"
                        }

                        else -> {
                            "Value"
                        }
                    }
                    FieldInput(
                        userField.value,
                        label,
                        partialFunc2to1(fieldNewValueSet, userField.hidden_uid),
                        Modifier.fillMaxWidth(0.6F)
                    )
                    /*OutlinedTextField(
                        value = userField.value.input.value.toString(),
                        onValueChange = { value: String ->
                            fieldNewValueSet((userField.hidden_uid), value)
                        },
                        label = { Text(text = "New Field Value") },
                        isError = userField.value.input.valid,
                        modifier = Modifier.fillMaxWidth(0.6F)
                    )*/
                    var mExpanded by remember { mutableStateOf(false) }
                    val iconOrg =
                        if (mExpanded) Icons.Filled.KeyboardArrowUp
                        else Icons.Filled.KeyboardArrowDown
                    Button(
                        onClick = { mExpanded = !mExpanded },
                        colors = ButtonDefaults.buttonColors(
                            containerColor = MaterialTheme.colorScheme.primary
                        ),
                        contentPadding = PaddingValues(0.dp),
                        modifier = Modifier
                            .padding(start = 1.dp, end = 10.dp)
//                            .size(50.dp)
                            .width(95.dp)
                        //            modifier = Modifier.padding(all = .dp),

                    ) {
                        Text(
                            text = "change\r\ntype",
                            color = MaterialTheme.colorScheme.onPrimary,
//                            modifier = Modifier.padding(ButtonDefaults.ContentPadding),
                            textAlign = TextAlign.Center
                        )

                        Icon(
                            iconOrg,
                            "contentDescription",
                            Modifier.padding(start = 0.dp, end = 8.dp)
                        )

                    }

//                val options = (data.options as DropDownListOptions.Loaded).value

                    val options = mutableListOf(
                        Pair("String", UiFieldType.String()),
                        Pair("UInt", UiFieldType.UInt()),
                        Pair("Int", UiFieldType.Int()),
                        Pair("bool", UiFieldType.Bool()),
                        Pair("Float", UiFieldType.Float()),
                    )
                    DropdownMenu(
                        expanded = mExpanded, onDismissRequest = { mExpanded = false },
                    ) {
                        options.forEach { label ->
                            DropdownMenuItem(onClick = {
                                mExpanded = false
                                fieldNewChangeType(userField.hidden_uid, label.second)
                            }, text = { Text(text = label.first) })
                        }
                    }


                }
            }
        }
        Button(
            onClick = fieldNewAdd,
            colors = ButtonDefaults.buttonColors(
                containerColor = MaterialTheme.colorScheme.secondary
            )
        ) {
            Text(
                text = "Add Field",
                modifier = Modifier.padding(start = 8.dp),
                color = MaterialTheme.colorScheme.onSecondary
            )
        }
    }
}


@Composable
fun TagRow(
    selected: Boolean,
    tagKey: String,
    tagValue: UiUserInputOrMultiChoice?,
    onRadioClick: (() -> Unit),
    tagExistingValueSetIndex: (String, UiStringIndex) -> Unit,
    tagExistingValueSetUser: (String, String) -> Unit,
    tagExistingValueAcceptModeChange: (String) -> Unit,
    tagExistingValueDismissModeChange: (String) -> Unit,

    ) {
    Row(
        Modifier.padding(horizontal = 8.dp), verticalAlignment = Alignment.CenterVertically
    ) {
        RadioButton(
            selected = selected, onClick = onRadioClick
        )
        Text(
            text = tagKey,
            modifier = Modifier.padding(horizontal = 8.dp),
            color = MaterialTheme.colorScheme.onBackground
        )
        if (tagValue != null) {
            UserInputOrMultiSelect(
                multi = tagValue,
                indexSelected = partialFunc2to1(tagExistingValueSetIndex, tagKey),
                userInput = partialFunc2to1(tagExistingValueSetUser, tagKey),
                acceptUserDialog = partialFunc1to0(tagExistingValueAcceptModeChange, tagKey),
                dismissUserDialog = partialFunc1to0(tagExistingValueDismissModeChange, tagKey),
                enabled = true,
                label = "$tagKey value",
                modifier = Modifier,
            )
        }
    }
}


@Composable
fun Tags(
    rrAvailableTags: UiRemoteResourceAvailableTags,
    newTags: List<UiUserTags>,
    tagNewAdd: () -> Unit,
    availableTagsOnClick: (String) -> () -> Unit,
    tags: MutableMap<String, UiUserInputOrMultiChoice>,
    tagExistingValueSetIndex: (String, UiStringIndex) -> Unit,
    tagExistingValueSetUser: (String, String) -> Unit,
    tagExistingValueAcceptModeChange: (String) -> Unit,
    tagExistingValueDismissModeChange: (String) -> Unit,
    tagNewValueSet: (UiStringIndex, String) -> Unit,
    tagNewKaySet: (UiStringIndex, String) -> Unit,
    tagNewRemove: (UiStringIndex) -> Unit,
    requestTagsFetch: () -> Unit,
    modifier: Modifier,

    ) {
    Column(modifier = modifier) {
        Text(text = "Tags", color = MaterialTheme.colorScheme.onBackground)
        when (rrAvailableTags) {
            is UiRemoteResourceAvailableTags.Loaded -> {
                rrAvailableTags.value.value.keys.map { tagName: String ->
                    TagRow(
                        tagKey = tagName,
                        onRadioClick = availableTagsOnClick(tagName),
                        selected = tagName in tags.keys,
                        tagValue = tags[tagName],
                        tagExistingValueSetIndex = tagExistingValueSetIndex,
                        tagExistingValueSetUser = tagExistingValueSetUser,
                        tagExistingValueAcceptModeChange = tagExistingValueAcceptModeChange,
                        tagExistingValueDismissModeChange = tagExistingValueDismissModeChange,
                    )
                }
            }

            is UiRemoteResourceAvailableTags.Loading -> {
                when (rrAvailableTags.value.current_state) {
                    is UiLoadingState.RequestSent -> {
                        CircularProgressIndicator(
                            modifier = Modifier
                                .width((32 + 8).dp)
                                .height(32.dp)
                                .padding(start = 8.dp, end = 0.dp),
                            color = MaterialTheme.colorScheme.secondary,
                            trackColor = MaterialTheme.colorScheme.surfaceVariant,
                        )
                    }

                    else -> {
                        Button(
                            onClick = requestTagsFetch, colors = ButtonDefaults.buttonColors(
                                containerColor = MaterialTheme.colorScheme.secondary
                            )
                        ) {
                            Text(
                                text = "Retry tag fetch",
                                color = MaterialTheme.colorScheme.onSecondary
                            )
                        }
                    }
                }
            }
        }

        newTags.forEach { userTag ->
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 8.dp)
            ) {
                Icon(
                    Icons.Outlined.Delete,
                    "contentDescription",
                    modifier = Modifier
                        .padding(start = 10.dp, end = 1.dp)
                        .size(32.dp)
                        .clickable { tagNewRemove(UiStringIndex(userTag.hidden_uid)) },
                    tint = MaterialTheme.colorScheme.onBackground
                )
                OutlinedTextField(
                    value = userTag.key.orElse("").toString(),
                    onValueChange = { value: String ->
                        tagNewKaySet(UiStringIndex(userTag.hidden_uid), value)
                    },
                    label = { Text(text = "New Tag Name") },
                    isError = userTag.key.isEmpty,
                    modifier = Modifier.fillMaxWidth(0.5F)
                )

                OutlinedTextField(
                    value = userTag.value.orElse("").toString(),
                    onValueChange = { value: String ->
                        tagNewValueSet(UiStringIndex(userTag.hidden_uid), value)
                    },
                    label = { Text(text = "New Tag Value") },
                    isError = userTag.value.isEmpty,
                    modifier = Modifier.fillMaxWidth(1F)
                )
            }
        }

        Button(
            onClick = tagNewAdd, colors = ButtonDefaults.buttonColors(
                containerColor = MaterialTheme.colorScheme.secondary
            )
        ) { Text(text = "Add Tag", color = MaterialTheme.colorScheme.onSecondary) }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DialUseStateExample(
    onConfirm: (TimePickerState) -> Unit,
    onDismiss: () -> Unit,
) {
    val currentTime = Calendar.getInstance()

    val timePickerState = rememberTimePickerState(
        initialHour = currentTime.get(Calendar.HOUR_OF_DAY),
        initialMinute = currentTime.get(Calendar.MINUTE),
        is24Hour = true,
    )
    Dialog(
        onDismissRequest = { /*TODO*/ }) {
        Column(
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.background),
        ) {
            TimePicker(
                state = timePickerState,
            )
            Row {
                Button(onClick = onDismiss) {
                    Text("Dismiss picker")
                }
                Button(onClick = { onConfirm(timePickerState) }) {
                    Text("Confirm selection")
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DatePickerModal(
    onDateSelected: (DatePickerState) -> Unit, onDismiss: () -> Unit
) {
    val datePickerState = rememberDatePickerState()

    DatePickerDialog(onDismissRequest = onDismiss, confirmButton = {
        TextButton(onClick = {
            onDateSelected(datePickerState)
            onDismiss()
        }) {
            Text("OK")
        }
    }, dismissButton = {
        TextButton(onClick = onDismiss) {
            Text("Cancel")
        }
    }) {
        DatePicker(state = datePickerState)
    }
}


// =================================================================================================
// Previews
// =================================================================================================


@OptIn(ExperimentalMaterial3Api::class)
@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewScreen() {
    FlowLinesTheme {
        InputFieldsTagsScreenReal(
            data = UiInputFieldsAndTagsScreenData(
                "http://baseurl",
                "org",
                "bucket",
                "measurement",
                UiRemoteResourceAvailableTags.Loaded(
                    UiAvailableTags(
                        mapOf(
                            "tag1" to UiRemoteResourceAvailableTagValues.Loaded(
                                UiAvailableTagValues(
                                    mutableListOf("tag1 1", "tag1 2")
                                )
                            ),
                            "tag2" to UiRemoteResourceAvailableTagValues.Loaded(
                                UiAvailableTagValues(
                                    mutableListOf("tag2 1", "tag2 2")
                                )
                            ),
                            "tag3" to UiRemoteResourceAvailableTagValues.Loading(
                                UiLoadingResource(
                                    0,
                                    UiLoadingState.Initialised()
                                )
                            ),
                            "tag4" to UiRemoteResourceAvailableTagValues.Loaded(
                                UiAvailableTagValues(
                                    mutableListOf("tag4 1", "tag4 2")
                                )
                            ),
                            "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                UiLoadingResource(
                                    0,
                                    UiLoadingState.RequestSent()
                                )
                            ),
                            "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                UiLoadingResource(
                                    3,
                                    UiLoadingState.AwaitingUserInteraction()
                                )
                            ),
                        )
                    )
                ),
                mapOf(
                    "tag1" to UiUserInputOrMultiChoice(

                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), Optional.of(
                            UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                        ), Optional.empty()
                    ),
                    "tag2" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), Optional.of(
                            UiSelectedUserInputOrMultiChoice.UserMode(
                                UiValidatedString(
                                    "hey",
                                    true
                                )
                            )
                        ), Optional.empty()
                    ),
                    "tag4" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), Optional.of(
                            UiSelectedUserInputOrMultiChoice.UserMode(
                                UiValidatedString(
                                    "hey invalid",
                                    true
                                )
                            )
                        ), Optional.empty()
                    ),
                    "tag5" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loading(), Optional.of(
                            UiSelectedUserInputOrMultiChoice.UserMode(
                                UiValidatedString(
                                    "hey invalid",
                                    true
                                )
                            )
                        ), Optional.empty()
                    ),

                    ),
                mutableListOf(UiUserTags("0", Optional.of("user key"), Optional.of("User value"))),
                UiRemoteResourceAvailableFields.Loaded(
                    UiAvailableFields(
                        mutableListOf("field1", "field2")
                    )
                ),
                mapOf(
                    "field1" to UiFieldUserInput(
                        UiFieldType.String(),
                        UiValidatedString("value", true)
                    ), // valid
                    "field3" to UiFieldUserInput(
                        UiFieldType.String(),
                        UiValidatedString("bad value", false)
                    ), // invalid
                ),
                mutableListOf(
                    UiUserFields(
                        UiStringIndex("0"),
                        UiValidatedString("hey", true),
                        UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                    ),
                    UiUserFields(
                        UiStringIndex("1"),
                        UiValidatedString("hey", true),
                        UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                    )

                ),
                "line preview",
                Optional.of("time"),
                UiPrecision.Nanoseconds(),
                "expected line", // why is this different to line...


            ),
            availableTagsOnClick = { _ -> { } },
            availableFieldOnClick = { _ -> { } },
            fieldExistingValueSet = { _, _ -> },
            onWriteClick = { },
            false,
            onSetTimeClicked = {},
            onTimeConfirm = { _ -> },
            false,
            onSetDateClicked = {},
            onDateConfirmed = { _ -> },
            tagNewValueSet = { _, _ -> },
            tagNewKaySet = { _, _ -> },
            tagExistingValueSetIndex = { _, _ -> },
            tagExistingValueSetUser = { _, _ -> },
            tagNewAdd = {},
            tagExistingValueAcceptModeChange = {},
            tagExistingValueDismissModeChange = {},
            tagNewRemove = { _ -> },
            requestTagsFetch = {},
            requestFieldsFetch = {},
            fieldNewValueSet = { _, _ -> },
            fieldNewKeySet = { _, _ -> },
            fieldNewChangeType = { _, _ -> },
            fieldNewAdd = { },
            fieldNewRemove = { _ -> },
            tutorial = Optional.empty(),
            endTutorial = {},
            nextTutorial = { _ -> },
            moveToNextTutorialScreen = {},
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewScreenTagsNotLoaded() {
    FlowLinesTheme {
        InputFieldsTagsScreenReal(
            data = UiInputFieldsAndTagsScreenData(
                "http://baseurl",
                "org",
                "bucket",
                "measurement",
                UiRemoteResourceAvailableTags.Loading(
                    UiLoadingResource(
                        1,
                        UiLoadingState.RequestSent()
                    )
                ),

                mapOf(
                    "tag1" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), Optional.of(
                            UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                        ), Optional.empty()
                    ),
                    "tag2" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), Optional.of(
                            UiSelectedUserInputOrMultiChoice.UserMode(
                                UiValidatedString(
                                    "hey",
                                    true
                                )
                            )
                        ), Optional.empty()
                    ),
                    "tag4" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), Optional.of(
                            UiSelectedUserInputOrMultiChoice.UserMode(
                                UiValidatedString(
                                    "hey invalid",
                                    true
                                )
                            )
                        ), Optional.empty()
                    ),
                    "tag5" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loading(), Optional.of(
                            UiSelectedUserInputOrMultiChoice.UserMode(
                                UiValidatedString(
                                    "hey invalid",
                                    true
                                )
                            )
                        ), Optional.empty()
                    ),

                    ),
                mutableListOf(UiUserTags("0", Optional.of("user key"), Optional.of("User value"))),
                UiRemoteResourceAvailableFields.Loading(
                    UiLoadingResource(
                        1,
                        UiLoadingState.RequestSent()
                    )
                ),
                mapOf(
                    "field1" to UiFieldUserInput(
                        UiFieldType.String(),
                        UiValidatedString("value", true)
                    ), // valid
                    "field3" to UiFieldUserInput(
                        UiFieldType.String(),
                        UiValidatedString("bad value", false)
                    ), // invalid
                ),
                mutableListOf(
                    UiUserFields(
                        UiStringIndex("0"),
                        UiValidatedString("hey", true),
                        UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                    )
                ),
                "line preview",
                Optional.of("time"),
                UiPrecision.Nanoseconds(),
                "expected line", // why is this different to line...


            ),
            availableTagsOnClick = { _ -> { } },
            availableFieldOnClick = { _ -> { } },
            fieldExistingValueSet = { _, _ -> },
            onWriteClick = { },
            false,
            onSetTimeClicked = {},
            onTimeConfirm = { _ -> },
            false,
            onSetDateClicked = {},
            onDateConfirmed = { _ -> },
            tagNewValueSet = { _, _ -> },
            tagNewKaySet = { _, _ -> },
            tagExistingValueSetIndex = { _, _ -> },
            tagExistingValueSetUser = { _, _ -> },
            tagNewAdd = {},
            tagExistingValueAcceptModeChange = {},
            tagExistingValueDismissModeChange = {},
            tagNewRemove = { _ -> },
            requestTagsFetch = {},
            requestFieldsFetch = {},
            fieldNewValueSet = { _, _ -> },
            fieldNewKeySet = { _, _ -> },
            fieldNewChangeType = { _, _ -> },
            fieldNewAdd = { },
            fieldNewRemove = { _ -> },
            tutorial = Optional.empty(),
            endTutorial = {},
            nextTutorial = { _ -> },
            moveToNextTutorialScreen = {},
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewScreenTagsUserInteraction() {
    FlowLinesTheme {
        InputFieldsTagsScreenReal(
            data = UiInputFieldsAndTagsScreenData(
                "http://baseurl",
                "org",
                "bucket",
                "measurement",
                UiRemoteResourceAvailableTags.Loading(
                    UiLoadingResource(
                        1,
                        UiLoadingState.AwaitingUserInteraction()
                    )
                ),

                mapOf(
                    "tag1" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), Optional.of(
                            UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                        ), Optional.empty()
                    ),
                    "tag2" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), Optional.of(
                            UiSelectedUserInputOrMultiChoice.UserMode(
                                UiValidatedString(
                                    "hey",
                                    true
                                )
                            )
                        ), Optional.empty()
                    ),
                    "tag4" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), Optional.of(
                            UiSelectedUserInputOrMultiChoice.UserMode(
                                UiValidatedString(
                                    "hey invalid",
                                    true
                                )
                            )
                        ), Optional.empty()
                    ),
                    "tag5" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loading(), Optional.of(
                            UiSelectedUserInputOrMultiChoice.UserMode(
                                UiValidatedString(
                                    "hey invalid",
                                    true
                                )
                            )
                        ), Optional.empty()
                    ),

                    ),
                mutableListOf(UiUserTags("0", Optional.of("user key"), Optional.of("User value"))),
                UiRemoteResourceAvailableFields.Loading(
                    UiLoadingResource(
                        1,
                        UiLoadingState.AwaitingUserInteraction()
                    )
                ),
                mapOf(
                    "field1" to UiFieldUserInput(
                        UiFieldType.String(),
                        UiValidatedString("value", true)
                    ), // valid
                    "field3" to UiFieldUserInput(
                        UiFieldType.String(),
                        UiValidatedString("bad value", false)
                    ), // invalid
                ),
                mutableListOf(
                    UiUserFields(
                        UiStringIndex("0"),
                        UiValidatedString("hey", true),
                        UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                    )
                ),
                "line preview",
                Optional.of("time"),
                UiPrecision.Nanoseconds(),
                "expected line", // why is this different to line...


            ),
            availableTagsOnClick = { _ -> { } },
            availableFieldOnClick = { _ -> { } },
            fieldExistingValueSet = { _, _ -> },
            onWriteClick = { },
            false,
            onSetTimeClicked = {},
            onTimeConfirm = { _ -> },
            false,
            onSetDateClicked = {},
            onDateConfirmed = { _ -> },
            tagNewValueSet = { _, _ -> },
            tagNewKaySet = { _, _ -> },
            tagExistingValueSetIndex = { _, _ -> },
            tagExistingValueSetUser = { _, _ -> },
            tagNewAdd = {},
            tagExistingValueAcceptModeChange = {},
            tagExistingValueDismissModeChange = {},
            tagNewRemove = { _ -> },
            requestTagsFetch = {},
            requestFieldsFetch = {},
            fieldNewValueSet = { _, _ -> },
            fieldNewKeySet = { _, _ -> },
            fieldNewChangeType = { _, _ -> },
            fieldNewAdd = { },
            fieldNewRemove = { _ -> },
            tutorial = Optional.empty(),
            endTutorial = {},
            nextTutorial = { _ -> },
            moveToNextTutorialScreen = {},
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewScreenInputChange() {
    FlowLinesTheme {
        InputFieldsTagsScreenReal(
            data = UiInputFieldsAndTagsScreenData(
                "http://baseurl",
                "org",
                "bucket",
                "measurement",
                UiRemoteResourceAvailableTags.Loaded(
                    UiAvailableTags(
                        mapOf(
                            "tag1" to UiRemoteResourceAvailableTagValues.Loaded(
                                UiAvailableTagValues(
                                    mutableListOf("tag1 1", "tag1 2")
                                )
                            ),
                            "tag2" to UiRemoteResourceAvailableTagValues.Loaded(
                                UiAvailableTagValues(
                                    mutableListOf("tag2 1", "tag2 2")
                                )
                            ),
                            "tag3" to UiRemoteResourceAvailableTagValues.Loading(
                                UiLoadingResource(
                                    0,
                                    UiLoadingState.Initialised()
                                )
                            ),
                        )
                    )
                ),

                mapOf(
                    "tag1" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), Optional.of(
                            UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                        ), Optional.of("Value has changed is that ok?")
                    ),
                    "tag2" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), Optional.of(
                            UiSelectedUserInputOrMultiChoice.UserMode(
                                UiValidatedString(
                                    "hey",
                                    true
                                )
                            )
                        ), Optional.empty()
                    ),
                    "tag4" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), Optional.of(
                            UiSelectedUserInputOrMultiChoice.UserMode(
                                UiValidatedString(
                                    "hey invalid",
                                    true
                                )
                            )
                        ), Optional.empty()
                    ),

                    ),
                mutableListOf(UiUserTags("0", Optional.of("user key"), Optional.of("User value"))),
                UiRemoteResourceAvailableFields.Loaded(
                    UiAvailableFields(
                        mutableListOf("field1", "field2")
                    )
                ),
                mapOf(
                    "field1" to UiFieldUserInput(
                        UiFieldType.String(),
                        UiValidatedString("value", true)
                    ), // valid
                    "field3" to UiFieldUserInput(
                        UiFieldType.String(),
                        UiValidatedString("bad value", false)
                    ), // invalid
                ),
                mutableListOf(
                    UiUserFields(
                        UiStringIndex("0"),
                        UiValidatedString("hey", true),
                        UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                    )
                ),
                "line preview",
                Optional.of("time"),
                UiPrecision.Nanoseconds(),
                "expected line", // why is this different to line...


            ),
            availableTagsOnClick = { _ -> { } },
            availableFieldOnClick = { _ -> { } },
            fieldExistingValueSet = { _, _ -> },
            onWriteClick = { },
            false,
            onSetTimeClicked = {},
            onTimeConfirm = { _ -> },
            false,
            onSetDateClicked = {},
            onDateConfirmed = { _ -> },
            tagNewValueSet = { _, _ -> },
            tagNewKaySet = { _, _ -> },
            tagNewRemove = { _ -> },
            tagExistingValueSetIndex = { _, _ -> },
            tagExistingValueSetUser = { _, _ -> },
            tagNewAdd = {},
            tagExistingValueAcceptModeChange = {},
            tagExistingValueDismissModeChange = {},
            requestFieldsFetch = {},
            requestTagsFetch = {},
            fieldNewValueSet = { _, _ -> },
            fieldNewKeySet = { _, _ -> },
            fieldNewChangeType = { _, _ -> },
            fieldNewAdd = { },
            fieldNewRemove = { _ -> },
            tutorial = Optional.empty(),
            endTutorial = {},
            nextTutorial = { _ -> },
            moveToNextTutorialScreen = {},
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewScreenWithDatePicker() {
    FlowLinesTheme {
        InputFieldsTagsScreenReal(
            data = UiInputFieldsAndTagsScreenData(
                "http://baseurl",
                "org",
                "bucket",
                "measurement",
                UiRemoteResourceAvailableTags.Loaded(
                    UiAvailableTags(
                        mapOf(
                            "tag1" to UiRemoteResourceAvailableTagValues.Loaded(
                                UiAvailableTagValues(
                                    mutableListOf("tag1 1", "tag1 2")
                                )
                            ),
                            "tag2" to UiRemoteResourceAvailableTagValues.Loaded(
                                UiAvailableTagValues(
                                    mutableListOf("tag2 1", "tag2 2")
                                )
                            ),
                            "tag3" to UiRemoteResourceAvailableTagValues.Loading(
                                UiLoadingResource(
                                    0,
                                    UiLoadingState.Initialised()
                                )
                            ),
                            "tag4" to UiRemoteResourceAvailableTagValues.Loaded(
                                UiAvailableTagValues(
                                    mutableListOf("tag4 1", "tag4 2")
                                )
                            ),
                            "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                UiLoadingResource(
                                    0,
                                    UiLoadingState.RequestSent()
                                )
                            ),
                            "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                UiLoadingResource(
                                    3,
                                    UiLoadingState.AwaitingUserInteraction()
                                )
                            ),
                        )
                    )
                ),
                mapOf(
                    "tag1" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), Optional.of(
                            UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                        ), Optional.empty()
                    ),
                ),
                mutableListOf(UiUserTags("0", Optional.of("user key"), Optional.of("User value"))),
                UiRemoteResourceAvailableFields.Loaded(
                    UiAvailableFields(
                        mutableListOf("field1", "field2")
                    )
                ),
                mapOf(
                    "field1" to UiFieldUserInput(
                        UiFieldType.String(),
                        UiValidatedString("value", true)
                    ), // valid
                    "field3" to UiFieldUserInput(
                        UiFieldType.String(),
                        UiValidatedString("bad value", false)
                    ), // invalid
                ),
                mutableListOf(
                    UiUserFields(
                        UiStringIndex("0"),
                        UiValidatedString("hey", true),
                        UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                    )
                ),
                "line preview",
                Optional.of("time"),
                UiPrecision.Nanoseconds(),
                "expected line", // why is this different to line...


            ),
            availableTagsOnClick = { _ -> { } },
            availableFieldOnClick = { _ -> { } },
            fieldExistingValueSet = { _, _ -> },
            onWriteClick = { },
            false,
            onSetTimeClicked = {},
            onTimeConfirm = { _ -> },
            true,
            onSetDateClicked = {},
            onDateConfirmed = { _ -> },
            tagNewKaySet = { _, _ -> },
            tagNewValueSet = { _, _ -> },
            tagExistingValueSetIndex = { _, _ -> },
            tagExistingValueSetUser = { _, _ -> },
            tagNewAdd = {},
            tagExistingValueAcceptModeChange = {},
            tagExistingValueDismissModeChange = {},
            tagNewRemove = { _ -> },
            requestTagsFetch = {},
            requestFieldsFetch = {},
            fieldNewValueSet = { _, _ -> },
            fieldNewKeySet = { _, _ -> },
            fieldNewChangeType = { _, _ -> },
            fieldNewAdd = { },
            fieldNewRemove = { _ -> },
            tutorial = Optional.empty(),
            endTutorial = {},
            nextTutorial = { _ -> },
            moveToNextTutorialScreen = {},
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewScreenWithTimePicker() {
    FlowLinesTheme {
        InputFieldsTagsScreenReal(
            data = UiInputFieldsAndTagsScreenData(
                "http://baseurl",
                "org",
                "bucket",
                "measurement",
                UiRemoteResourceAvailableTags.Loaded(
                    UiAvailableTags(
                        mapOf(
                            "tag1" to UiRemoteResourceAvailableTagValues.Loaded(
                                UiAvailableTagValues(
                                    mutableListOf("tag1 1", "tag1 2")
                                )
                            ),
                            "tag2" to UiRemoteResourceAvailableTagValues.Loaded(
                                UiAvailableTagValues(
                                    mutableListOf("tag2 1", "tag2 2")
                                )
                            ),
                            "tag3" to UiRemoteResourceAvailableTagValues.Loading(
                                UiLoadingResource(
                                    0,
                                    UiLoadingState.Initialised()
                                )
                            ),
                            "tag4" to UiRemoteResourceAvailableTagValues.Loaded(
                                UiAvailableTagValues(
                                    mutableListOf("tag4 1", "tag4 2")
                                )
                            ),
                            "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                UiLoadingResource(
                                    0,
                                    UiLoadingState.RequestSent()
                                )
                            ),
                            "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                UiLoadingResource(
                                    3,
                                    UiLoadingState.AwaitingUserInteraction()
                                )
                            ),
                        )
                    )
                ),
                mapOf(
                    "tag1" to UiUserInputOrMultiChoice(
                        UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                            mutableListOf(
                                UiMultiChoiceItem(
                                    UiStringIndex("0"), "index measurement",
                                ), UiMultiChoiceItem(
                                    UiStringIndex("1"), "index measurement2"
                                )
                            ),
                        ), Optional.of(
                            UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                        ), Optional.empty()
                    ),
                ),
                mutableListOf(UiUserTags("0", Optional.of("user key"), Optional.of("User value"))),

                UiRemoteResourceAvailableFields.Loaded(
                    UiAvailableFields(
                        mutableListOf("field1", "field2")
                    )
                ),
                mapOf(
                    "field1" to UiFieldUserInput(
                        UiFieldType.String(),
                        UiValidatedString("value", true)
                    ), // valid
                    "field3" to UiFieldUserInput(
                        UiFieldType.String(),
                        UiValidatedString("bad value", true)
                    ), // invalid
                ),
                mutableListOf(
                    UiUserFields(
                        UiStringIndex("0"),
                        UiValidatedString("hey", true),
                        UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                    )
                ),
                "line preview",
                Optional.of("time"),
                UiPrecision.Nanoseconds(),
                "expected line", // why is this different to line...


            ),
            availableTagsOnClick = { _ -> { } },
            availableFieldOnClick = { _ -> { } },
            fieldExistingValueSet = { _, _ -> },
            onWriteClick = { },
            true,
            onSetTimeClicked = {},
            onTimeConfirm = { _ -> },
            false,
            onSetDateClicked = {},
            onDateConfirmed = { _ -> },
            tagNewValueSet = { _, _ -> },
            tagNewRemove = { _ -> },
            tagNewKaySet = { _, _ -> },
            tagExistingValueSetIndex = { _, _ -> },
            tagExistingValueSetUser = { _, _ -> },
            tagNewAdd = {},
            tagExistingValueAcceptModeChange = {},
            tagExistingValueDismissModeChange = {},
            requestFieldsFetch = {},
            requestTagsFetch = {},
            fieldNewValueSet = { _, _ -> },
            fieldNewKeySet = { _, _ -> },
            fieldNewChangeType = { _, _ -> },
            fieldNewAdd = { },
            fieldNewRemove = { _ -> }, tutorial = Optional.empty(),
            endTutorial = {},
            nextTutorial = { _ -> },
            moveToNextTutorialScreen = {},
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun IFATSTutorial0() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            InputFieldsTagsScreenReal(
                data = UiInputFieldsAndTagsScreenData(
                    "http://baseurl",
                    "org",
                    "bucket",
                    "measurement",
                    UiRemoteResourceAvailableTags.Loaded(
                        UiAvailableTags(
                            mapOf(
                                "tag1" to UiRemoteResourceAvailableTagValues.Loaded(
                                    UiAvailableTagValues(
                                        mutableListOf("tag1 1", "tag1 2")
                                    )
                                ),
                                "tag2" to UiRemoteResourceAvailableTagValues.Loaded(
                                    UiAvailableTagValues(
                                        mutableListOf("tag2 1", "tag2 2")
                                    )
                                ),
                                "tag3" to UiRemoteResourceAvailableTagValues.Loading(
                                    UiLoadingResource(
                                        0,
                                        UiLoadingState.Initialised()
                                    )
                                ),
                                "tag4" to UiRemoteResourceAvailableTagValues.Loaded(
                                    UiAvailableTagValues(
                                        mutableListOf("tag4 1", "tag4 2")
                                    )
                                ),
                                "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                    UiLoadingResource(
                                        0,
                                        UiLoadingState.RequestSent()
                                    )
                                ),
                                "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                    UiLoadingResource(
                                        3,
                                        UiLoadingState.AwaitingUserInteraction()
                                    )
                                ),
                            )
                        )
                    ),
                    mapOf(
                        "tag1" to UiUserInputOrMultiChoice(

                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                                mutableListOf(
                                    UiMultiChoiceItem(
                                        UiStringIndex("0"), "index measurement",
                                    ), UiMultiChoiceItem(
                                        UiStringIndex("1"), "index measurement2"
                                    )
                                ),
                            ), Optional.of(
                                UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                            ), Optional.empty()
                        ),
                        "tag2" to UiUserInputOrMultiChoice(
                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                                mutableListOf(
                                    UiMultiChoiceItem(
                                        UiStringIndex("0"), "index measurement",
                                    ), UiMultiChoiceItem(
                                        UiStringIndex("1"), "index measurement2"
                                    )
                                ),
                            ), Optional.of(
                                UiSelectedUserInputOrMultiChoice.UserMode(
                                    UiValidatedString(
                                        "hey",
                                        true
                                    )
                                )
                            ), Optional.empty()
                        ),
                        "tag4" to UiUserInputOrMultiChoice(
                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                                mutableListOf(
                                    UiMultiChoiceItem(
                                        UiStringIndex("0"), "index measurement",
                                    ), UiMultiChoiceItem(
                                        UiStringIndex("1"), "index measurement2"
                                    )
                                ),
                            ), Optional.of(
                                UiSelectedUserInputOrMultiChoice.UserMode(
                                    UiValidatedString(
                                        "hey invalid",
                                        true
                                    )
                                )
                            ), Optional.empty()
                        ),
                        "tag5" to UiUserInputOrMultiChoice(
                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loading(), Optional.of(
                                UiSelectedUserInputOrMultiChoice.UserMode(
                                    UiValidatedString(
                                        "hey invalid",
                                        true
                                    )
                                )
                            ), Optional.empty()
                        ),

                        ),
                    mutableListOf(
                        UiUserTags(
                            "0",
                            Optional.of("user key"),
                            Optional.of("User value")
                        )
                    ),
                    UiRemoteResourceAvailableFields.Loaded(
                        UiAvailableFields(
                            mutableListOf("field1", "field2")
                        )
                    ),
                    mapOf(
                        "field1" to UiFieldUserInput(
                            UiFieldType.String(),
                            UiValidatedString("value", true)
                        ), // valid
                        "field3" to UiFieldUserInput(
                            UiFieldType.String(),
                            UiValidatedString("bad value", false)
                        ), // invalid
                    ),
                    mutableListOf(
                        UiUserFields(
                            UiStringIndex("0"),
                            UiValidatedString("hey", true),
                            UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                        ),
                        UiUserFields(
                            UiStringIndex("1"),
                            UiValidatedString("hey", true),
                            UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                        )

                    ),
                    "line preview",
                    Optional.of("time"),
                    UiPrecision.Nanoseconds(),
                    "expected line", // why is this different to line...


                ),
                availableTagsOnClick = { _ -> { } },
                availableFieldOnClick = { _ -> { } },
                fieldExistingValueSet = { _, _ -> },
                onWriteClick = { },
                false,
                onSetTimeClicked = {},
                onTimeConfirm = { _ -> },
                false,
                onSetDateClicked = {},
                onDateConfirmed = { _ -> },
                tagNewValueSet = { _, _ -> },
                tagNewKaySet = { _, _ -> },
                tagExistingValueSetIndex = { _, _ -> },
                tagExistingValueSetUser = { _, _ -> },
                tagNewAdd = {},
                tagExistingValueAcceptModeChange = {},
                tagExistingValueDismissModeChange = {},
                tagNewRemove = { _ -> },
                requestTagsFetch = {},
                requestFieldsFetch = {},
                fieldNewValueSet = { _, _ -> },
                fieldNewKeySet = { _, _ -> },
                fieldNewChangeType = { _, _ -> },
                fieldNewAdd = { },
                fieldNewRemove = { _ -> },
                tutorial = Optional.of(0),
                endTutorial = {},
                nextTutorial = { _ -> },
                moveToNextTutorialScreen = {},
            )
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun IFATSTutorial1() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            InputFieldsTagsScreenReal(
                data = UiInputFieldsAndTagsScreenData(
                    "http://baseurl",
                    "org",
                    "bucket",
                    "measurement",
                    UiRemoteResourceAvailableTags.Loaded(
                        UiAvailableTags(
                            mapOf(
                                "tag1" to UiRemoteResourceAvailableTagValues.Loaded(
                                    UiAvailableTagValues(
                                        mutableListOf("tag1 1", "tag1 2")
                                    )
                                ),
                                "tag2" to UiRemoteResourceAvailableTagValues.Loaded(
                                    UiAvailableTagValues(
                                        mutableListOf("tag2 1", "tag2 2")
                                    )
                                ),
                                "tag3" to UiRemoteResourceAvailableTagValues.Loading(
                                    UiLoadingResource(
                                        0,
                                        UiLoadingState.Initialised()
                                    )
                                ),
                                "tag4" to UiRemoteResourceAvailableTagValues.Loaded(
                                    UiAvailableTagValues(
                                        mutableListOf("tag4 1", "tag4 2")
                                    )
                                ),
                                "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                    UiLoadingResource(
                                        0,
                                        UiLoadingState.RequestSent()
                                    )
                                ),
                                "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                    UiLoadingResource(
                                        3,
                                        UiLoadingState.AwaitingUserInteraction()
                                    )
                                ),
                            )
                        )
                    ),
                    mapOf(
                        "tag1" to UiUserInputOrMultiChoice(

                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                                mutableListOf(
                                    UiMultiChoiceItem(
                                        UiStringIndex("0"), "index measurement",
                                    ), UiMultiChoiceItem(
                                        UiStringIndex("1"), "index measurement2"
                                    )
                                ),
                            ), Optional.of(
                                UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                            ), Optional.empty()
                        ),
                        "tag2" to UiUserInputOrMultiChoice(
                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                                mutableListOf(
                                    UiMultiChoiceItem(
                                        UiStringIndex("0"), "index measurement",
                                    ), UiMultiChoiceItem(
                                        UiStringIndex("1"), "index measurement2"
                                    )
                                ),
                            ), Optional.of(
                                UiSelectedUserInputOrMultiChoice.UserMode(
                                    UiValidatedString(
                                        "hey",
                                        true
                                    )
                                )
                            ), Optional.empty()
                        ),
                        "tag4" to UiUserInputOrMultiChoice(
                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                                mutableListOf(
                                    UiMultiChoiceItem(
                                        UiStringIndex("0"), "index measurement",
                                    ), UiMultiChoiceItem(
                                        UiStringIndex("1"), "index measurement2"
                                    )
                                ),
                            ), Optional.of(
                                UiSelectedUserInputOrMultiChoice.UserMode(
                                    UiValidatedString(
                                        "hey invalid",
                                        true
                                    )
                                )
                            ), Optional.empty()
                        ),
                        "tag5" to UiUserInputOrMultiChoice(
                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loading(), Optional.of(
                                UiSelectedUserInputOrMultiChoice.UserMode(
                                    UiValidatedString(
                                        "hey invalid",
                                        true
                                    )
                                )
                            ), Optional.empty()
                        ),

                        ),
                    mutableListOf(
                        UiUserTags(
                            "0",
                            Optional.of("user key"),
                            Optional.of("User value")
                        )
                    ),
                    UiRemoteResourceAvailableFields.Loaded(
                        UiAvailableFields(
                            mutableListOf("field1", "field2")
                        )
                    ),
                    mapOf(
                        "field1" to UiFieldUserInput(
                            UiFieldType.String(),
                            UiValidatedString("value", true)
                        ), // valid
                        "field3" to UiFieldUserInput(
                            UiFieldType.String(),
                            UiValidatedString("bad value", false)
                        ), // invalid
                    ),
                    mutableListOf(
                        UiUserFields(
                            UiStringIndex("0"),
                            UiValidatedString("hey", true),
                            UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                        ),
                        UiUserFields(
                            UiStringIndex("1"),
                            UiValidatedString("hey", true),
                            UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                        )

                    ),
                    "line preview",
                    Optional.of("time"),
                    UiPrecision.Nanoseconds(),
                    "expected line", // why is this different to line...


                ),
                availableTagsOnClick = { _ -> { } },
                availableFieldOnClick = { _ -> { } },
                fieldExistingValueSet = { _, _ -> },
                onWriteClick = { },
                false,
                onSetTimeClicked = {},
                onTimeConfirm = { _ -> },
                false,
                onSetDateClicked = {},
                onDateConfirmed = { _ -> },
                tagNewValueSet = { _, _ -> },
                tagNewKaySet = { _, _ -> },
                tagExistingValueSetIndex = { _, _ -> },
                tagExistingValueSetUser = { _, _ -> },
                tagNewAdd = {},
                tagExistingValueAcceptModeChange = {},
                tagExistingValueDismissModeChange = {},
                tagNewRemove = { _ -> },
                requestTagsFetch = {},
                requestFieldsFetch = {},
                fieldNewValueSet = { _, _ -> },
                fieldNewKeySet = { _, _ -> },
                fieldNewChangeType = { _, _ -> },
                fieldNewAdd = { },
                fieldNewRemove = { _ -> },
                tutorial = Optional.of(1),
                endTutorial = {},
                nextTutorial = { _ -> },
                moveToNextTutorialScreen = {},
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Preview
@Composable
fun IFATSTutorial2() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            InputFieldsTagsScreenReal(
                data = UiInputFieldsAndTagsScreenData(
                    "http://baseurl",
                    "org",
                    "bucket",
                    "measurement",
                    UiRemoteResourceAvailableTags.Loaded(
                        UiAvailableTags(
                            mapOf(
                                "tag1" to UiRemoteResourceAvailableTagValues.Loaded(
                                    UiAvailableTagValues(
                                        mutableListOf("tag1 1", "tag1 2")
                                    )
                                ),
                                "tag2" to UiRemoteResourceAvailableTagValues.Loaded(
                                    UiAvailableTagValues(
                                        mutableListOf("tag2 1", "tag2 2")
                                    )
                                ),
                                "tag3" to UiRemoteResourceAvailableTagValues.Loading(
                                    UiLoadingResource(
                                        0,
                                        UiLoadingState.Initialised()
                                    )
                                ),
                                "tag4" to UiRemoteResourceAvailableTagValues.Loaded(
                                    UiAvailableTagValues(
                                        mutableListOf("tag4 1", "tag4 2")
                                    )
                                ),
                                "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                    UiLoadingResource(
                                        0,
                                        UiLoadingState.RequestSent()
                                    )
                                ),
                                "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                    UiLoadingResource(
                                        3,
                                        UiLoadingState.AwaitingUserInteraction()
                                    )
                                ),
                            )
                        )
                    ),
                    mapOf(
                        "tag1" to UiUserInputOrMultiChoice(

                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                                mutableListOf(
                                    UiMultiChoiceItem(
                                        UiStringIndex("0"), "index measurement",
                                    ), UiMultiChoiceItem(
                                        UiStringIndex("1"), "index measurement2"
                                    )
                                ),
                            ), Optional.of(
                                UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                            ), Optional.empty()
                        ),
                        "tag2" to UiUserInputOrMultiChoice(
                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                                mutableListOf(
                                    UiMultiChoiceItem(
                                        UiStringIndex("0"), "index measurement",
                                    ), UiMultiChoiceItem(
                                        UiStringIndex("1"), "index measurement2"
                                    )
                                ),
                            ), Optional.of(
                                UiSelectedUserInputOrMultiChoice.UserMode(
                                    UiValidatedString(
                                        "hey",
                                        true
                                    )
                                )
                            ), Optional.empty()
                        ),
                        "tag4" to UiUserInputOrMultiChoice(
                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                                mutableListOf(
                                    UiMultiChoiceItem(
                                        UiStringIndex("0"), "index measurement",
                                    ), UiMultiChoiceItem(
                                        UiStringIndex("1"), "index measurement2"
                                    )
                                ),
                            ), Optional.of(
                                UiSelectedUserInputOrMultiChoice.UserMode(
                                    UiValidatedString(
                                        "hey invalid",
                                        true
                                    )
                                )
                            ), Optional.empty()
                        ),
                        "tag5" to UiUserInputOrMultiChoice(
                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loading(), Optional.of(
                                UiSelectedUserInputOrMultiChoice.UserMode(
                                    UiValidatedString(
                                        "hey invalid",
                                        true
                                    )
                                )
                            ), Optional.empty()
                        ),

                        ),
                    mutableListOf(
                        UiUserTags(
                            "0",
                            Optional.of("user key"),
                            Optional.of("User value")
                        )
                    ),
                    UiRemoteResourceAvailableFields.Loaded(
                        UiAvailableFields(
                            mutableListOf("field1", "field2")
                        )
                    ),
                    mapOf(
                        "field1" to UiFieldUserInput(
                            UiFieldType.String(),
                            UiValidatedString("value", true)
                        ), // valid
                        "field3" to UiFieldUserInput(
                            UiFieldType.String(),
                            UiValidatedString("bad value", false)
                        ), // invalid
                    ),
                    mutableListOf(
                        UiUserFields(
                            UiStringIndex("0"),
                            UiValidatedString("hey", true),
                            UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                        ),
                        UiUserFields(
                            UiStringIndex("1"),
                            UiValidatedString("hey", true),
                            UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                        )

                    ),
                    "line preview",
                    Optional.of("time"),
                    UiPrecision.Nanoseconds(),
                    "expected line", // why is this different to line...


                ),
                availableTagsOnClick = { _ -> { } },
                availableFieldOnClick = { _ -> { } },
                fieldExistingValueSet = { _, _ -> },
                onWriteClick = { },
                false,
                onSetTimeClicked = {},
                onTimeConfirm = { _ -> },
                false,
                onSetDateClicked = {},
                onDateConfirmed = { _ -> },
                tagNewValueSet = { _, _ -> },
                tagNewKaySet = { _, _ -> },
                tagExistingValueSetIndex = { _, _ -> },
                tagExistingValueSetUser = { _, _ -> },
                tagNewAdd = {},
                tagExistingValueAcceptModeChange = {},
                tagExistingValueDismissModeChange = {},
                tagNewRemove = { _ -> },
                requestTagsFetch = {},
                requestFieldsFetch = {},
                fieldNewValueSet = { _, _ -> },
                fieldNewKeySet = { _, _ -> },
                fieldNewChangeType = { _, _ -> },
                fieldNewAdd = { },
                fieldNewRemove = { _ -> },
                tutorial = Optional.of(2),
                endTutorial = {},
                nextTutorial = { _ -> },
                moveToNextTutorialScreen = {},
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Preview
@Composable
fun IFATSTutorial3() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            InputFieldsTagsScreenReal(
                data = UiInputFieldsAndTagsScreenData(
                    "http://baseurl",
                    "org",
                    "bucket",
                    "measurement",
                    UiRemoteResourceAvailableTags.Loaded(
                        UiAvailableTags(
                            mapOf(
                                "tag1" to UiRemoteResourceAvailableTagValues.Loaded(
                                    UiAvailableTagValues(
                                        mutableListOf("tag1 1", "tag1 2")
                                    )
                                ),
                                "tag2" to UiRemoteResourceAvailableTagValues.Loaded(
                                    UiAvailableTagValues(
                                        mutableListOf("tag2 1", "tag2 2")
                                    )
                                ),
                                "tag3" to UiRemoteResourceAvailableTagValues.Loading(
                                    UiLoadingResource(
                                        0,
                                        UiLoadingState.Initialised()
                                    )
                                ),
                                "tag4" to UiRemoteResourceAvailableTagValues.Loaded(
                                    UiAvailableTagValues(
                                        mutableListOf("tag4 1", "tag4 2")
                                    )
                                ),
                                "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                    UiLoadingResource(
                                        0,
                                        UiLoadingState.RequestSent()
                                    )
                                ),
                                "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                    UiLoadingResource(
                                        3,
                                        UiLoadingState.AwaitingUserInteraction()
                                    )
                                ),
                            )
                        )
                    ),
                    mapOf(
                        "tag1" to UiUserInputOrMultiChoice(

                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                                mutableListOf(
                                    UiMultiChoiceItem(
                                        UiStringIndex("0"), "index measurement",
                                    ), UiMultiChoiceItem(
                                        UiStringIndex("1"), "index measurement2"
                                    )
                                ),
                            ), Optional.of(
                                UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                            ), Optional.empty()
                        ),
                        "tag2" to UiUserInputOrMultiChoice(
                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                                mutableListOf(
                                    UiMultiChoiceItem(
                                        UiStringIndex("0"), "index measurement",
                                    ), UiMultiChoiceItem(
                                        UiStringIndex("1"), "index measurement2"
                                    )
                                ),
                            ), Optional.of(
                                UiSelectedUserInputOrMultiChoice.UserMode(
                                    UiValidatedString(
                                        "hey",
                                        true
                                    )
                                )
                            ), Optional.empty()
                        ),
                        "tag4" to UiUserInputOrMultiChoice(
                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                                mutableListOf(
                                    UiMultiChoiceItem(
                                        UiStringIndex("0"), "index measurement",
                                    ), UiMultiChoiceItem(
                                        UiStringIndex("1"), "index measurement2"
                                    )
                                ),
                            ), Optional.of(
                                UiSelectedUserInputOrMultiChoice.UserMode(
                                    UiValidatedString(
                                        "hey invalid",
                                        true
                                    )
                                )
                            ), Optional.empty()
                        ),
                        "tag5" to UiUserInputOrMultiChoice(
                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loading(), Optional.of(
                                UiSelectedUserInputOrMultiChoice.UserMode(
                                    UiValidatedString(
                                        "hey invalid",
                                        true
                                    )
                                )
                            ), Optional.empty()
                        ),

                        ),
                    mutableListOf(UiUserTags("0", Optional.of("user key"), Optional.of("User value"))),
                    UiRemoteResourceAvailableFields.Loaded(
                        UiAvailableFields(
                            mutableListOf("field1", "field2")
                        )
                    ),
                    mapOf(
                        "field1" to UiFieldUserInput(
                            UiFieldType.String(),
                            UiValidatedString("value", true)
                        ), // valid
                        "field3" to UiFieldUserInput(
                            UiFieldType.String(),
                            UiValidatedString("bad value", false)
                        ), // invalid
                    ),
                    mutableListOf(
                        UiUserFields(
                            UiStringIndex("0"),
                            UiValidatedString("hey", true),
                            UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                        ),
                        UiUserFields(
                            UiStringIndex("1"),
                            UiValidatedString("hey", true),
                            UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                        )

                    ),
                    "line preview",
                    Optional.of("time"),
                    UiPrecision.Nanoseconds(),
                    "expected line", // why is this different to line...


                ),
                availableTagsOnClick = { _ -> { } },
                availableFieldOnClick = { _ -> { } },
                fieldExistingValueSet = { _, _ -> },
                onWriteClick = { },
                false,
                onSetTimeClicked = {},
                onTimeConfirm = { _ -> },
                false,
                onSetDateClicked = {},
                onDateConfirmed = { _ -> },
                tagNewValueSet = { _, _ -> },
                tagNewKaySet = { _, _ -> },
                tagExistingValueSetIndex = { _, _ -> },
                tagExistingValueSetUser = { _, _ -> },
                tagNewAdd = {},
                tagExistingValueAcceptModeChange = {},
                tagExistingValueDismissModeChange = {},
                tagNewRemove = { _ -> },
                requestTagsFetch = {},
                requestFieldsFetch = {},
                fieldNewValueSet = { _, _ -> },
                fieldNewKeySet = { _, _ -> },
                fieldNewChangeType = { _, _ -> },
                fieldNewAdd = { },
                fieldNewRemove = { _ -> },
                tutorial = Optional.of(3),
                endTutorial = {},
                nextTutorial = {_ -> },
                moveToNextTutorialScreen = {},
            )
        }}
}


@OptIn(ExperimentalMaterial3Api::class)
@Preview
@Composable
fun IFATSTutorial4() {
    FlowLinesTheme {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.surface)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
        ) {
            InputFieldsTagsScreenReal(
                data = UiInputFieldsAndTagsScreenData(
                    "http://baseurl",
                    "org",
                    "bucket",
                    "measurement",
                    UiRemoteResourceAvailableTags.Loaded(
                        UiAvailableTags(
                            mapOf(
                                "tag1" to UiRemoteResourceAvailableTagValues.Loaded(
                                    UiAvailableTagValues(
                                        mutableListOf("tag1 1", "tag1 2")
                                    )
                                ),
                                "tag2" to UiRemoteResourceAvailableTagValues.Loaded(
                                    UiAvailableTagValues(
                                        mutableListOf("tag2 1", "tag2 2")
                                    )
                                ),
                                "tag3" to UiRemoteResourceAvailableTagValues.Loading(
                                    UiLoadingResource(
                                        0,
                                        UiLoadingState.Initialised()
                                    )
                                ),
                                "tag4" to UiRemoteResourceAvailableTagValues.Loaded(
                                    UiAvailableTagValues(
                                        mutableListOf("tag4 1", "tag4 2")
                                    )
                                ),
                                "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                    UiLoadingResource(
                                        0,
                                        UiLoadingState.RequestSent()
                                    )
                                ),
                                "tag5" to UiRemoteResourceAvailableTagValues.Loading(
                                    UiLoadingResource(
                                        3,
                                        UiLoadingState.AwaitingUserInteraction()
                                    )
                                ),
                            )
                        )
                    ),
                    mapOf(
                        "tag1" to UiUserInputOrMultiChoice(

                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                                mutableListOf(
                                    UiMultiChoiceItem(
                                        UiStringIndex("0"), "index measurement",
                                    ), UiMultiChoiceItem(
                                        UiStringIndex("1"), "index measurement2"
                                    )
                                ),
                            ), Optional.of(
                                UiSelectedUserInputOrMultiChoice.IndexMode(UiStringIndex("0"))
                            ), Optional.empty()
                        ),
                        "tag2" to UiUserInputOrMultiChoice(
                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                                mutableListOf(
                                    UiMultiChoiceItem(
                                        UiStringIndex("0"), "index measurement",
                                    ), UiMultiChoiceItem(
                                        UiStringIndex("1"), "index measurement2"
                                    )
                                ),
                            ), Optional.of(
                                UiSelectedUserInputOrMultiChoice.UserMode(
                                    UiValidatedString(
                                        "hey",
                                        true
                                    )
                                )
                            ), Optional.empty()
                        ),
                        "tag4" to UiUserInputOrMultiChoice(
                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loaded(
                                mutableListOf(
                                    UiMultiChoiceItem(
                                        UiStringIndex("0"), "index measurement",
                                    ), UiMultiChoiceItem(
                                        UiStringIndex("1"), "index measurement2"
                                    )
                                ),
                            ), Optional.of(
                                UiSelectedUserInputOrMultiChoice.UserMode(
                                    UiValidatedString(
                                        "hey invalid",
                                        true
                                    )
                                )
                            ), Optional.empty()
                        ),
                        "tag5" to UiUserInputOrMultiChoice(
                            UiRemoteResourceUserInputOrMultiChoiceOptions.Loading(), Optional.of(
                                UiSelectedUserInputOrMultiChoice.UserMode(
                                    UiValidatedString(
                                        "hey invalid",
                                        true
                                    )
                                )
                            ), Optional.empty()
                        ),

                        ),
                    mutableListOf(UiUserTags("0", Optional.of("user key"), Optional.of("User value"))),
                    UiRemoteResourceAvailableFields.Loaded(
                        UiAvailableFields(
                            mutableListOf("field1", "field2")
                        )
                    ),
                    mapOf(
                        "field1" to UiFieldUserInput(
                            UiFieldType.String(),
                            UiValidatedString("value", true)
                        ), // valid
                        "field3" to UiFieldUserInput(
                            UiFieldType.String(),
                            UiValidatedString("bad value", false)
                        ), // invalid
                    ),
                    mutableListOf(
                        UiUserFields(
                            UiStringIndex("0"),
                            UiValidatedString("hey", true),
                            UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                        ),
                        UiUserFields(
                            UiStringIndex("1"),
                            UiValidatedString("hey", true),
                            UiFieldUserInput(UiFieldType.String(), UiValidatedString("hey", true))
                        )

                    ),
                    "line preview",
                    Optional.of("time"),
                    UiPrecision.Nanoseconds(),
                    "expected line", // why is this different to line...


                ),
                availableTagsOnClick = { _ -> { } },
                availableFieldOnClick = { _ -> { } },
                fieldExistingValueSet = { _, _ -> },
                onWriteClick = { },
                false,
                onSetTimeClicked = {},
                onTimeConfirm = { _ -> },
                false,
                onSetDateClicked = {},
                onDateConfirmed = { _ -> },
                tagNewValueSet = { _, _ -> },
                tagNewKaySet = { _, _ -> },
                tagExistingValueSetIndex = { _, _ -> },
                tagExistingValueSetUser = { _, _ -> },
                tagNewAdd = {},
                tagExistingValueAcceptModeChange = {},
                tagExistingValueDismissModeChange = {},
                tagNewRemove = { _ -> },
                requestTagsFetch = {},
                requestFieldsFetch = {},
                fieldNewValueSet = { _, _ -> },
                fieldNewKeySet = { _, _ -> },
                fieldNewChangeType = { _, _ -> },
                fieldNewAdd = { },
                fieldNewRemove = { _ -> },
                tutorial = Optional.of(4),
                endTutorial = {},
                nextTutorial = {_ -> },
                moveToNextTutorialScreen = {},
            )
        }}
}

