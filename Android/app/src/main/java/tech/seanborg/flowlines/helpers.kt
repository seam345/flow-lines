package tech.seanborg.flowlines

fun <A, B, C> partialFunc2to1(f: (A, B) -> C, a: A): (B) -> C {
    return { b: B -> f(a, b) }
}

fun <A, C> partialFunc1to0(f: (A) -> C, a: A): () -> C {
    return { f(a) }
}